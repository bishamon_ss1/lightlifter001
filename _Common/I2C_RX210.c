/******************************************************************************
* DISCLAIMER

* This software is supplied by Renesas Electronics Corp. and is only 
* intended for use with Renesas products. No other uses are authorized.

* This software is owned by Renesas Electronics Corp. and is protected under 
* all applicable laws, including copyright laws.

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED.

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORP. NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software.  
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010. Renesas Electronics Corporation. All rights reserved.
*******************************************************************************	
* File Name    : iic.c
* Version      : 1.00
* Description  : RIIC single master program
******************************************************************************
* History
* Aug 01 '11  H.Inoue	RX62Nボード用にポーティング
*         : 16.07.2010 1.00    First Release
* 2014.05.14  H.Inoue	RX210用にポーティング
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include	<machine.h>

#define		_I2C_DRIVER_
#include	"I2C_RX210.h"


//--------------------------------------------------------------------------
//	変数型定義
//--------------------------------------------------------------------------



//--------------------------------------------------------------------------
//	マクロ定義
//--------------------------------------------------------------------------

//#define	BUS_SPEED			_100K				// 100kbps
#define	BUS_SPEED			_400K				// 400kbps
#if		BUS_SPEED == _100K						// 100kbps
	#if		PCLK == 48
		#define	RIIC_SPEED_CKS		3					// IICphi=PCLK/8 clock
		#define	RIIC_SPEED_BRH		24					// 100kbps (PCLK=48MHz)
		#define	RIIC_SPEED_BRL		26					// 100kbps (PCLK=48MHz)
	#elif	PCLK == 25
		#define	RIIC_SPEED_CKS		2					// IICphi=PCLK/4 clock
		#define	RIIC_SPEED_BRH		24					// 100kbps (PCLK=25MHz)
		#define	RIIC_SPEED_BRL		29					// 100kbps (PCLK=25MHz)
	#endif
#elif	BUS_SPEED == _400K						// 400kbps
	#if		PCLK == 48
		#define	RIIC_SPEED_CKS		2					// IICphi=PCLK/4 clock
		#define	RIIC_SPEED_BRH		7					// 400kbps (PCLK=48MHz)
		#define	RIIC_SPEED_BRL		15					// 400kbps (PCLK=48MHz)
	#elif	PCLK == 25
		#define	RIIC_SPEED_CKS		1					// IICphi=PCLK/2 clock
		#define	RIIC_SPEED_BRH		7					// 400kbps (PCLK=25MHz)
		#define	RIIC_SPEED_BRL		16					// 400kbps (PCLK=25MHz)
	#endif
#else											// Error
	#error	I2C(RIIC) Bus Speed define error.
#endif


// LoopWaitCount
#define	DELAY_COUNT_25U		420					// 25uS待ち(ICLK=50MHz)


//--------------------------------------------------------------------------
//	グローバル変数,関数プロトタイプ宣言
//--------------------------------------------------------------------------
//------------------------------
// グローバル変数
//------------------------------
static I2C_TFR_INF				*gpI2cTfrInf ;	// 転送情報
static I2C_DRV_INF				gI2cDrvInf ;	// I2Cドライバ内部情報

//------------------------------
// 関数プロトタイプ
//------------------------------
static void IIC_EEI_Int_TimeOut( void ) ;
static void IIC_EEI_Int_AL( void ) ;
static void IIC_EEI_Int_STOP( void ) ;
static void IIC_EEI_Int_START( void ) ;
static void IIC_EEI_Int_Nack( void ) ;
static void IIC_RXI_Int_TfrRead ( void ) ;
static void IIC_TXI_Int_TfrWrite( void ) ;
static void IIC_TXI_Int_TfrRead( void ) ;
static void IIC_TEI_Int_TfrWrite( void ) ;
static void IIC_TEI_Int_TfrRead( void ) ;
static void IIC_BusInitialize( void ) ;
static void IIC_Error( I2C_ERR_CODE ) ;




//-----------------------------------------------------------
//	RIICモジュールとI/Oポート初期化
//	引数
//	戻り値
//-----------------------------------------------------------
void	I2C_InzModule( void )
{
	BYTE			bTry ;								// Bus回復リトライ数
	UWORD			uwWait ;							// 待ちCounter
	
	
	// ** I2C割り込み設定
//#ifdef RIIC_CH0
	// 安全の為に割込フラグをクリアする(macro)
	IR( RIIC0, EEI0 ) = 0 ;								// RIIC0 EEI0
	IR( RIIC0, RXI0 ) = 0 ;								// RIIC0 RXI0
	IR( RIIC0, TXI0 ) = 0 ;								// RIIC0 TXI0
	IR( RIIC0, TEI0 ) = 0 ;								// RIIC0 TEI0
	// 割込禁止(macro)
	IEN( RIIC0, EEI0 ) = 0 ;							// RIIC0 EEI0
	IEN( RIIC0, RXI0 ) = 0 ;							// RIIC0 RXI0
	IEN( RIIC0, TXI0 ) = 0 ;							// RIIC0 TXI0
	IEN( RIIC0, TEI0 ) = 0 ;							// RIIC0 TEI0
/*
#else		// #ifdef RIIC_CH0
	// 安全の為に割込フラグをクリアする(macro)
	IR( RIIC1, EEI1 ) = 0 ;								// RIIC1 EEI1
	IR( RIIC1, RXI1 ) = 0 ;								// RIIC1 RXI1
	IR( RIIC1, TXI1 ) = 0 ;								// RIIC1 TXI1
	IR( RIIC1, TEI1 ) = 0 ;								// RIIC1 TEI1
	// 割込許可(macro)
	IEN( RIIC1, EEI1 ) = 0 ;							// RIIC1 EEI1
	IEN( RIIC1, RXI1 ) = 0 ;							// RIIC1 RXI1
	IEN( RIIC1, TXI1 ) = 0 ;							// RIIC1 TXI1
	IEN( RIIC1, TEI1 ) = 0 ;							// RIIC1 TEI1
#endif		// #ifdef RIIC_CH0
*/
	
	
	// ** I2C モジュール起動
	SYSTEM.PRCR.WORD = 0xa502 ;							// MSTPmnを書込可へ
//#ifdef RIIC_CH0
	MSTP( RIIC0 ) = 0 ;									// Wakeup RIIC0 (macro)
//#else		// #ifdef RIIC_CH0
//	MSTP( RIIC1 ) = 0 ;									// Wakeup RIIC1 (macro)
//#endif		// #ifdef RIIC_CH0
	SYSTEM.PRCR.WORD = 0xa500 ;							// 書き込み不可へ
	
	
	// ** I2C バスチェック
	// バスロックしていたらSCLにダミークロックを出して開放させる
	MPC.PWPR.BIT.B0WI  = 0 ;							// PFSWE書込許可
	MPC.PWPR.BIT.PFSWE = 1 ;							// PFSreg書込許可
	RIIC_SDA_PMR = PORT_PMR_IOP ;						// SDAn
	RIIC_SCL_PMR = PORT_PMR_IOP ;						// SCLn
	RIIC_SDA_PDR = PORTDIR_IN ;							// SDAn
	RIIC_SCL_PDR = PORTDIR_IN ;							// SCLn
	if ( (RIIC_SCL_PIDR == PORT_IN_HIGH) && (RIIC_SDA_PIDR == PORT_IN_LOW) ) {
		RIIC_SDA_MPC = 0x0f ;							// SDAポート指定
		RIIC_SCL_MPC = 0x0f ;							// SCLポート指定
		I2C_Destroy( ) ;								// I2Cバス破棄
		M_IIC.ICCR1.BIT.ICE = 1 ;						// Enable RIIC bus I/F
		M_IIC.ICCR1.BIT.SOWP = 0 ;						// Enable SCLO,SDAO
		RIIC_SDA_PMR = PORT_PMR_PHE ;					// 周辺機能として使用
		RIIC_SCL_PMR = PORT_PMR_PHE ;					// 周辺機能として使用
		for ( bTry = 21 ; bTry-- ; ) {
			M_IIC.ICCR1.BIT.SCLO ^= EXTOUT_HIGH ;		// SCLn 反転
			uwWait = DELAY_COUNT_25U ;					// 待ちCounter 25uS
			while ( uwWait-- ) {
				nop( ) ;
			}
		}
		I2C_Destroy( ) ;								// I2Cバス破棄
		RIIC_SDA_MPC = 0x00 ;							// SDAポート指定
		RIIC_SCL_MPC = 0x00 ;							// SCLポート指定
		RIIC_SDA_PMR = PORT_PMR_IOP ;					// IOポートとして使用
		RIIC_SCL_PMR = PORT_PMR_IOP ;					// IOポートとして使用
		
	}
	MPC.PWPR.BIT.PFSWE = 0 ;							// PFSreg書込禁止
	MPC.PWPR.BIT.B0WI  = 1 ;							// PFSWE書込禁止
	
	
	// ** I2C I/Oポート設定
	RIIC_SDA_PDR = PORTDIR_IN ;							// SDAn
	RIIC_SCL_PDR = PORTDIR_IN ;							// SCLn
	RIIC_SDA_PMR = PORT_PMR_PHE ;						// 周辺機能として使用
	RIIC_SCL_PMR = PORT_PMR_PHE ;						// 周辺機能として使用
	MPC.PWPR.BIT.B0WI  = 0 ;							// PFSWE書込許可
	MPC.PWPR.BIT.PFSWE = 1 ;							// PFSreg書込許可
	RIIC_SDA_MPC = 0x0f ;								// SDAポート指定
	RIIC_SCL_MPC = 0x0f ;								// SCLポート指定
	MPC.PWPR.BIT.PFSWE = 0 ;							// PFSreg書込禁止
	MPC.PWPR.BIT.B0WI  = 1 ;							// PFSWE書込禁止
	
	// RIICリセット (レジスタもリセットされる)
	I2C_Destroy( ) ;									// I2Cバス破棄
	
	
	// ** I2C割り込み設定
#ifdef RIIC_CH0
	// 割込レベル(macro)
	IPR( RIIC0, EEI0 ) = I2C_INT_LEVEL ;				// RIIC0 EEI0
	IPR( RIIC0, RXI0 ) = I2C_INT_LEVEL ;				// RIIC0 RXI0
	IPR( RIIC0, TXI0 ) = I2C_INT_LEVEL ;				// RIIC0 TXI0
	IPR( RIIC0, TEI0 ) = I2C_INT_LEVEL ;				// RIIC0 TEI0
	// 安全の為に割込フラグをクリアする(macro)
	IR( RIIC0, EEI0 ) = 0 ;								// RIIC0 EEI0
	IR( RIIC0, RXI0 ) = 0 ;								// RIIC0 RXI0
	IR( RIIC0, TXI0 ) = 0 ;								// RIIC0 TXI0
	IR( RIIC0, TEI0 ) = 0 ;								// RIIC0 TEI0
	// 割込許可(macro)
	IEN( RIIC0, EEI0 ) = 1 ;							// RIIC0 EEI0
	IEN( RIIC0, RXI0 ) = 1 ;							// RIIC0 RXI0
	IEN( RIIC0, TXI0 ) = 1 ;							// RIIC0 TXI0
	IEN( RIIC0, TEI0 ) = 1 ;							// RIIC0 TEI0
#else		// #ifdef RIIC_CH0
	// 割込レベル(macro)
	IPR( RIIC1, EEI1 ) = I2C_INT_LEVEL ;				// RIIC1 EEI1
	IPR( RIIC1, RXI1 ) = I2C_INT_LEVEL ;				// RIIC1 RXI1
	IPR( RIIC1, TXI1 ) = I2C_INT_LEVEL ;				// RIIC1 TXI1
	IPR( RIIC1, TEI1 ) = I2C_INT_LEVEL ;				// RIIC1 TEI1
	// 安全の為に割込フラグをクリアする(macro)
	IR( RIIC1, EEI1 ) = 0 ;								// RIIC1 EEI1
	IR( RIIC1, RXI1 ) = 0 ;								// RIIC1 RXI1
	IR( RIIC1, TXI1 ) = 0 ;								// RIIC1 TXI1
	IR( RIIC1, TEI1 ) = 0 ;								// RIIC1 TEI1
	// 割込許可(macro)
	IEN( RIIC1, EEI1 ) = 1 ;							// RIIC1 EEI1
	IEN( RIIC1, RXI1 ) = 1 ;							// RIIC1 RXI1
	IEN( RIIC1, TXI1 ) = 1 ;							// RIIC1 TXI1
	IEN( RIIC1, TEI1 ) = 1 ;							// RIIC1 TEI1
#endif		// #ifdef RIIC_CH0
	
	
}


//=============================================================================
//	I2Cバス初期化
//	引数
//	戻り値
//=============================================================================
void	I2C_Create( void )
{
	
	
	// RIICリセット (レジスタもリセットされる)
	I2C_Destroy( ) ;									// I2Cバス破棄
	
	// Set baud rate
	M_IIC.ICMR1.BIT.CKS = RIIC_SPEED_CKS ;		// IIC phi = PCLK/8 clock
	M_IIC.ICBRH.BIT.BRH = RIIC_SPEED_BRH ;		// High Width
	M_IIC.ICBRL.BIT.BRL = RIIC_SPEED_BRL ;		// Low Width
	
	// SDA 出力遅延 波形チェックして調整
	M_IIC.ICMR2.BIT.DLCS = 1 ;					// Dlay count source is I2Cφ/2
	M_IIC.ICMR2.BIT.SDDL = 0x04 ;				// Dlay count is 7-8
	

	// タイムアウト検出設定
	M_IIC.ICFER.BIT.TMOE = 0 ;					// タイムアウト検出無効
	M_IIC.ICMR2.BIT.TMOL = 0 ;					// Disable SCL=Low time out
	M_IIC.ICMR2.BIT.TMOH = 1 ;					// Enable SCL=High time out
	M_IIC.ICMR2.BIT.TMOS = 0 ;					// Long mode (16bits counter).
												// PCLK=48MHz, IIC phi=48MHz,
												// Long mode is about 1.365 ms
	M_IIC.ICFER.BIT.TMOE = 1 ;					// タイムアウト検出有効
	
	
	// ノイズフィルタ設定
	M_IIC.ICMR3.BIT.NF = 0 ;					// 0:1段, 1:2段, 2:3段, 3:4段
	
	// Disable all address detection. 
	// This sample code is for only master mode
	M_IIC.ICSER.BYTE = 0x00 ;					// 全無効(シングルマスタ)

	// ACKWP is protect bit for ACKBT. NAK出力の為に必要
	M_IIC.ICMR3.BIT.ACKWP = 1 ;					// ACKBTへの書込を許可

	// Enable/Disable each interrupt
//	RIICn.ICIER.BYTE = 0xBB ;		
		// b0:TMOIE Enable  Time Out interrupt (TMOI)
		// b1:ALIE  Enable  Arbitration Lost interrupt (ALI)
		// b2:STIE  Disable Start Condition Detection Interrupt (STI)
		// b3:SPIE  Enable  Stop condition detection interrupt (SPI)
		// b4:NAKIE Enable  NACK reception interrupt (NKAI)
		// b5:RIE   Enable  Receive Data Full Interrupt (ICRXI)
		// b6:TEIE  Disable Transmit End Interrupt (ICTEI)
		// b7:TIE   Enable  Transmit Data Empty Interrupt (ICTXI)
	M_IIC.ICIER.BIT.TMOIE = 1 ;					// Level int. Timeout
	M_IIC.ICIER.BIT.ALIE = 1 ;					// Level int. ArbitolationLost
	M_IIC.ICIER.BIT.STIE = 0 ;					// Level int. StartCondition
	M_IIC.ICIER.BIT.SPIE = 1 ;					// Level int. StopCondition
	M_IIC.ICIER.BIT.NAKIE = 1 ;					// Level int. Rx NACK
	M_IIC.ICIER.BIT.RIE = 1 ;					// Edge int. RxFull
	M_IIC.ICIER.BIT.TEIE = 0 ;					// Level int. TxEnd
	M_IIC.ICIER.BIT.TIE = 1 ;					// Edge int. TxDataEmpty
	
	// Initialize ram for RIIC
	gI2cDrvInf.Mode = I2C_MODE_IDLE ;			// Internal IIC mode
	gI2cDrvInf.Stat = I2C_STAT_IDLE ;			// I2C_GetStatus() Stat buffer
	gI2cDrvInf.udwTxBytes = 0 ;					// Clear the Tx 転送カウンタ
	gI2cDrvInf.udwRxBytes = 0 ;					// Clear the Rx 転送カウンタ
	
}


//=============================================================================
//	I2Cバス破棄
//	引数
//	戻り値
//=============================================================================
void	I2C_Destroy( void )
{
	
	// RIICリセット (レジスタもリセットされる)
	M_IIC.ICCR1.BIT.ICE = 0 ;						// RIIC disable
	M_IIC.ICCR1.BIT.IICRST = 1 ;					// RIIC all reset
	
#ifdef RIIC_CH0
	// 安全の為に割込フラグをクリアする(macro)
	IR( RIIC0, EEI0 ) = 0 ;								// RIIC0 EEI0
	IR( RIIC0, RXI0 ) = 0 ;								// RIIC0 RXI0
	IR( RIIC0, TXI0 ) = 0 ;								// RIIC0 TXI0
	IR( RIIC0, TEI0 ) = 0 ;								// RIIC0 TEI0
#else		// #ifdef RIIC_CH0
	// 安全の為に割込フラグをクリアする(macro)
	IR( RIIC1, EEI1 ) = 0 ;								// RIIC1 EEI1
	IR( RIIC1, RXI1 ) = 0 ;								// RIIC1 RXI1
	IR( RIIC1, TXI1 ) = 0 ;								// RIIC1 TXI1
	IR( RIIC1, TEI1 ) = 0 ;								// RIIC1 TEI1
#endif		// #ifdef RIIC_CH0

	// Enable RIIC
	M_IIC.ICCR1.BIT.ICE = 1 ;					// Enable RIIC bus interface
	nop( ) ;
	M_IIC.ICCR1.BIT.IICRST = 0 ;					// RIIC return reset
	
}


//=============================================================================
//	Function Name:	I2C_TfrWrite
//	Description  :	Start device write process or Acknowledge polling.
//					I2cWrtData.ubRegBufSize == I2cWrtData.ubRegBufPtr &
//					I2cWrtData.ubDataBufSize == I2cWrtData.ubDataPtr
//					になったらACKポーリングする
//	Argument     :	*pI2cWrtData
//						ubSlvAdr				Slave Device Address.
//												bit0はR/Wビットなので設定禁止
//						ubRegBufSize			実Reg.バッファサイズ
//						ubRegBuf[2]				Reg.バッファ
//						ubRegBufPtr				Reg.バッファPTR
//						ubDataBufSize 			実データバッファサイズ
//						ubDataBuf[]				データバッファ
//						ubDataPtr				NextデータバッファPTR
//	Return Value :
//					I2C_API_OK				Success
//					I2C_API_MODE_ERROR		Running I2C on other mode
//					I2C_API_PERM_ERROR		Parameter error
//					I2C_API_BUS_BUSY		I2C Bus is busy
//=============================================================================
I2C_TFR_FNC		I2C_TfrWrite( I2C_TFR_INF *pI2cWrtData )
{
	
	
	// Check the IIC mode (controled by software)
	if ( gI2cDrvInf.Mode != I2C_MODE_IDLE ) {
		return I2C_API_MODE_ERROR ;				// Running I2C on other mode
	}
	
	// Store the parameter data to ram for IIC
	gpI2cTfrInf = pI2cWrtData ;
	
	// Check parameter
	if ( (gpI2cTfrInf->ubDataBufSize == 0) || 
		 (gpI2cTfrInf->ubRegBufSize == 0) ) {
		return I2C_API_PERM_ERROR ;				// Parameter error
	}
	
	// Check IIC bus busy
	if ( M_IIC.ICCR2.BIT.BBSY != 0 ) {
		return I2C_API_BUS_BUSY ;				// If I2C bus is not free
	}
	
	// Set each RAM
	gI2cDrvInf.Mode = I2C_MODE_TFR_WRITE ;		// Change the internal mode
	gI2cDrvInf.Stat = I2C_STAT_ON_COMM ;		// Change the status
	gI2cDrvInf.udwTxBytes = 0 ;					// Clear the Tx 転送カウンタ
	
	// Generate start condition
	M_IIC.ICCR2.BIT.ST = 1 ;					// START 発行
	
	
	return I2C_API_OK ;
}


//=============================================================================
//	Function Name:	I2C_TfrRead
//	Description  :	デバイスのレジスタ読取を開始する
//	Argument     :	*pI2cWrtData
//						ubSlvAdr				Slave Device Address.
//												bit0はR/Wビットなので設定禁止
//						ubRegBufSize			実Reg.バッファサイズ
//						ubRegBuf[]				Reg.バッファ
//						ubRegBufPtr				Reg.バッファPTR
//						ubDataBufSize 			実データバッファサイズ
//						ubDataBuf[]				データバッファ
//						ubDataPtr				NextデータバッファPTR
//	Return Value :
//					I2C_API_OK				Success
//					I2C_API_MODE_ERROR		Running IIC on other mode
//					I2C_API_PERM_ERROR		Parameter error
//					I2C_API_BUS_BUSY		I2C Bus is busy
//=============================================================================
I2C_TFR_FNC		I2C_TfrRead( I2C_TFR_INF *pI2cWrtData )
{
	
	
	// Check the IIC mode (controled by software)
	if ( gI2cDrvInf.Mode != I2C_MODE_IDLE ) {
		return I2C_API_MODE_ERROR ;				// Running I2C on other mode
	}
	
	// Store the parameter data to ram for IIC
	gpI2cTfrInf = pI2cWrtData ;
	
	// Check parameter
	if ( (gpI2cTfrInf->ubDataBufSize == 0) || 
		 (gpI2cTfrInf->ubRegBufSize == 0) ) {
		return I2C_API_PERM_ERROR ;				// Parameter error
	}
	
	// Check IIC bus busy
	if ( M_IIC.ICCR2.BIT.BBSY != 0 ) {
		return I2C_API_BUS_BUSY ;				// If I2C bus is not free
	}
	
	// Set each RAM
	gI2cDrvInf.Mode = I2C_MODE_TFR_READ ;		// Change the internal mode
	gI2cDrvInf.Stat = I2C_STAT_ON_COMM ;		// Change the status
	gI2cDrvInf.udwTxBytes = 0 ;					// Clear the Tx 転送カウンタ
	gI2cDrvInf.udwRxBytes = 0 ;					// Clear the Rx 転送カウンタ
	
	// Generate start condition
	M_IIC.ICCR2.BIT.ST = 1 ;					// START発行
	
	
	return I2C_API_OK ;
}


//=============================================================================
//	Function Name:	I2C_GetStatus
//	Description  :	Get I2C status
//	Argument     : 
//					*pI2cStat -- Pointer for I2C communication status buffer
//						I2C_STAT_IDLE			I2C is Idle mode.
//												通信開始可能
//						I2C_STAT_ON_COMM		I2C is on communication.
//												通信開始できない
//						I2C_STAT_NACK			I2C has received NACK.
//												次の通信開始可能
//					 	I2C_STAT_FAILED			I2IC has stopped.
//												次の通信開始可能だが
//												前回の通信でエラーがあった
//					*pI2cBusStat -- Pointer for I2C Bus status buffer
//						I2C_BUS_STAT_FREE		I2C bus is free
//						I2C_BUS_STAT_BUSY		I2C bus is busy
//	Return Value :
//=============================================================================
void	I2C_GetStatus( I2C_STATUS *pI2cStat, I2C_BUS_STAT *pI2cBusStat )
{
	
	// 現在の通信ステータスを返す
	*pI2cStat = gI2cDrvInf.Stat ;
	
	// 現在のバス状態を返す
	if ( M_IIC.ICCR2.BIT.BBSY == 0 ) {
		*pI2cBusStat = I2C_BUS_STAT_FREE ;
	} else {
		*pI2cBusStat = I2C_BUS_STAT_BUSY ;
	}
}


//-----------------------------------------------------------------------------
//	Function Name:	IIC_EEI_Int
//	Description  :	I2C 通信エラー(EEI)割り込みハンドラ
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
#ifdef	RIIC_CH0
	#pragma interrupt IIC_EEI_Int(vect=VECT_RIIC0_EEI0) 
#else
	#pragma interrupt IIC_EEI_Int(vect=VECT_RIIC1_EEI1) 
#endif
void	IIC_EEI_Int( void )
{
	
	
	// Check Timeout
	if ( (M_IIC.ICSR2.BIT.TMOF != 0) && (M_IIC.ICIER.BIT.TMOIE != 0) ) {
		IIC_EEI_Int_TimeOut( ) ;
	}
	
	// Check Arbitration Lost
	if ( (M_IIC.ICSR2.BIT.AL != 0) && (M_IIC.ICIER.BIT.ALIE != 0) ) {
		IIC_EEI_Int_AL( ) ;
	}
	
	// Check stop condition detection
	if ( (M_IIC.ICSR2.BIT.STOP != 0) && (M_IIC.ICIER.BIT.SPIE != 0) ) {
		IIC_EEI_Int_STOP( ) ;
	}
	
	// Check NACK reception
	if ( (M_IIC.ICSR2.BIT.NACKF != 0 ) && (M_IIC.ICIER.BIT.NAKIE != 0) ) {
		IIC_EEI_Int_Nack( ) ;
	}
	
	// Check start condition detection
	if ( (M_IIC.ICSR2.BIT.START != 0 ) && (M_IIC.ICIER.BIT.STIE != 0) ) {
		IIC_EEI_Int_START( ) ;
	}
	
	nop( ) ;
	
}


//-----------------------------------------------------------------------------
//	Function Name:	IIC_EEI_Int_TimeOut
//	Description  :	IIC_EEI_Int()割り込みハンドラのタイムアウト処理
//					SCL=Highのタイムアウト監視をしているので
//					他のデバイスがLowにしたままだとこのエラーが出る
//					このAPIではシングルマスターなので通常発生しない
//					考えられるのはプルアップが切れたとか...
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
static void	IIC_EEI_Int_TimeOut( void )
{
	gI2cDrvInf.Stat = I2C_STAT_IDLE ;			// バスクロックタイムアウト
	IIC_BusInitialize( ) ;
}


//-----------------------------------------------------------------------------
//	Function Name:	IIC_EEI_Int_AL
//	Description  :	IIC_EEI_Int()割り込みハンドラのバス剥奪時処理
//					送信時SDLが他から操作された
//					結果、バス占有権が剥奪された
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
static void	IIC_EEI_Int_AL( void )
{
	gI2cDrvInf.Stat = I2C_STAT_ERR_ARB_LOST ;	// バス占有権が奪われた
	IIC_BusInitialize( ) ;
}


//-----------------------------------------------------------------------------
//	Function Name:	IIC_EEI_Int_STOP
//	Description  :	IIC_EEI_Int()割り込みハンドラのSTOP検出時処理
//					通信中にSTOP検出
//					シングルマスターでは通常あり得ない
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
static void	IIC_EEI_Int_STOP( void )
{
	
	// Clear each status flag
	M_IIC.ICSR2.BIT.NACKF = 0 ;					// NACK未検出へ
	M_IIC.ICSR2.BIT.STOP = 0 ;					// STOPコンディション未検出へ

	// Enable/Disable each interrupt
//	RIIC.ICIER.BYTE = 0xBB ;		
		// b0:TMOIE Enable  Time Out interrupt (TMOI)
		// b1:ALIE  Enable  Arbitration Lost interrupt (ALI)
		// b2:STIE  Disable Start Condition Detection Interrupt (STI)
		// b3:SPIE  Enable  Stop condition detection interrupt (SPI)
		// b4:NAKIE Enable  NACK reception interrupt (NKAI)
		// b5:RIE   Enable  Receive Data Full Interrupt (ICRXI)
		// b6:TEIE  Disable Transmit End Interrupt (ICTEI)
		// b7:TIE   Enable  Transmit Data Empty Interrupt (ICTXI)
	M_IIC.ICIER.BIT.TMOIE = 1 ;					// Level int. Timeout
	M_IIC.ICIER.BIT.ALIE = 1 ;					// Level int. ArbitolationLost
	M_IIC.ICIER.BIT.STIE = 0 ;					// Level int. StartCondition
	M_IIC.ICIER.BIT.SPIE = 1 ;					// Level int. StopCondition
	M_IIC.ICIER.BIT.NAKIE = 1 ;					// Level int. Rx NACK
	M_IIC.ICIER.BIT.RIE = 1 ;					// Edge int. RxFull
	M_IIC.ICIER.BIT.TEIE = 0 ;					// Level int. TxEnd
	M_IIC.ICIER.BIT.TIE = 1 ;					// Edge int. TxDataEmpty
	
	
	// 内部ステータスによってI2Cステータスをセット
	switch( gI2cDrvInf.Mode ) {
		case I2C_MODE_TFR_WRITE :					// 書き込み
			if ( gpI2cTfrInf->ubDataPtr < 
				 gpI2cTfrInf->ubDataBufSize ) {		// 書き込み未完
				gI2cDrvInf.Stat = I2C_STAT_ERR_DETECT_STOP ;
			} else {								// 書き込み完了
				if ( gI2cDrvInf.Stat != I2C_STAT_NACK ) {	// NACK以外
					gI2cDrvInf.Stat = I2C_STAT_IDLE ;
				}
			}
			break ;
		case I2C_MODE_TFR_READ :					// 読み込み
			if ( gpI2cTfrInf->ubDataPtr < 
				 gpI2cTfrInf->ubDataBufSize ) {			// 読み込み未完
				gI2cDrvInf.Stat = I2C_STAT_ERR_DETECT_STOP ;
			} else {									// 読み込み完了
				gI2cDrvInf.Stat = I2C_STAT_IDLE ;
			}
			break ;
		case I2C_MODE_IDLE :						// アイドル中
			// 他のデバイスがSTOPを発行した
			// シングルマスターでは通常あり得ない
			gI2cDrvInf.Stat = I2C_STAT_ERR_DETECT_STOP ;
			IIC_Error( I2C_ERR_MODE_SP_INT ) ;	// Internal mode error
			break ;
		default :
			gI2cDrvInf.Stat = I2C_STAT_ERR_DETECT_STOP ;
			IIC_Error( I2C_ERR_MODE_SP_INT ) ;		// Internal mode error
			break ;
	}

	// Initialize ram for RIIC
	gI2cDrvInf.Mode = I2C_MODE_IDLE ;	// Internal IIC mode
	gI2cDrvInf.udwTxBytes = 0 ;			// Clear the Tx 転送カウンタ
	gI2cDrvInf.udwRxBytes = 0 ;			// Clear the Rx 転送カウンタ
	
}


//-----------------------------------------------------------------------------
//	Function Name:	IIC_EEI_IntSTART
//	Description  :	IIC_EEI_Int()割り込みハンドラのSTART検出時処理
//					通信中にSTART検出
//					受信処理でレジスタ値送信後リスタート掛けるので
//					その時に発生する。よってデータ受信の為にスレーブアドレス
//					を送信する。
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
static void	IIC_EEI_Int_START( void )
{
	
	// START Condition 割込を禁止させる
	M_IIC.ICSR2.BIT.START = 0 ;			// STARTコンディション未検出へ
	M_IIC.ICIER.BIT.STIE = 0 ;			// STARTコンディション割り込み禁止へ
	
	// Transfer slave device address (Read)
	M_IIC.ICDRT = gpI2cTfrInf->ubSlvAdr | 0x01 ;	// マスタ転送時 b0 は必ず'1'
	
}


//-----------------------------------------------------------
//	Function Name:	IIC_EEI_Int_Nack
//	Description  :	IIC_EEI_Int()割り込みハンドラのNAK受信時の処理
//					通常自分(Master)が発行するのでSTOP発行して
//					トランザクションを終了する
//	引数
//	戻り値
//-----------------------------------------------------------
static	void	IIC_EEI_Int_Nack( void )
{
	
	
	gI2cDrvInf.Stat = I2C_STAT_NACK ;				// 内部モード更新
	
	M_IIC.ICIER.BIT.NAKIE = 0 ;						// NAK割込禁止へ

	// STOP Condition 発行してバス開放
	M_IIC.ICSR2.BIT.STOP = 0 ;						// STOP condition 未検出状態
	M_IIC.ICCR2.BIT.SP = 1 ;						// STOP condition 発行要求
	
}

//-----------------------------------------------------------------------------
//	Function Name:	IIC_RXI_Int
//	Description  :	受信データフル時の割り込みハンドラ
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
//#ifdef	RIIC_CH0
	#pragma interrupt IIC_RXI_Int(vect=VECT_RIIC0_RXI0) 
//#else
//	#pragma interrupt IIC_RXI_Int(vect=VECT_RIIC1_RXI1) 
//#endif
void	IIC_RXI_Int( void )
{
	
	// 内部ステータスによって処理振り分け
	switch( gI2cDrvInf.Mode ) {
		case I2C_MODE_TFR_READ :
			IIC_RXI_Int_TfrRead( ) ;						// get Rx data
			break ;
		case I2C_MODE_TFR_WRITE :
			IIC_Error( I2C_ERR_MODE_RX_INT_TFR_WRITE ) ;	// Internal mode err
			break ;
		default :
			IIC_Error( I2C_ERR_MODE_RX_INT ) ;				// Internal mode err
			break ;
	}
	
	nop( ) ;
	
}


//--------------------------------------------------------------------------
//	Function Name:	IIC_RXI_Int_TfrRead
//	Description  :	受信処理用のRXI割込ハンドラ
//					IIC_RXI_Int() の割込ルーチンから呼ばれる
//	Argument     :
//	Return Value :
//--------------------------------------------------------------------------
static void		IIC_RXI_Int_TfrRead( void )
{
	volatile static BYTE	bDat ;
	
	
	// Increase internal reception counter on RAM.
	// It is a receive data counter.
	gI2cDrvInf.udwRxBytes++ ;						// Rx 転送カウンタ
	
	
	if( gI2cDrvInf.udwRxBytes == 1 ) {				// 1st受信
		// 最終
		if ( (gpI2cTfrInf->ubDataPtr + 1) == gpI2cTfrInf->ubDataBufSize ) {
			// WAIT ビットが“1” のとき、1 バイト受信ごとに9 クロック目の
			// 立ち下がり以降、ICDRR レジスタの値が読み出されるまでの間 SCLn
			// ラインを Low にホールドします。(STOP condition) これにより 1 
			// バイトごとの受信動作が可能です。
			M_IIC.ICMR3.BIT.WAIT = 1 ;
			
			// If RIIC sends ACK when it receives last data, EEPROM may prevent
			// RIIC generating stop condition. Because EEPROM desn't know how
			// many RIIC try to read data.
			// When RIIC sends ACK, EEPROM realizes the next rising SCL for
			// stop condition as next first bit SCL.
			M_IIC.ICMR3.BIT.ACKBT = 1 ;				// Set NACK for final data.
		}
//		M_IIC.ICSR2.BIT.STOP = 0 ;					// STOP condition 未検出
//		M_IIC.ICCR2.BIT.SP = 1 ;					// STOP condition 発行要求
		bDat = M_IIC.ICDRR ;						// dummy read
	// 最終2nd受信
	} else if ( (gpI2cTfrInf->ubDataPtr + 2) == gpI2cTfrInf->ubDataBufSize ) {
		// WAIT ビットが“1” のとき、1 バイト受信ごとに9 クロック目の
		// 立ち下がり以降、ICDRR レジスタの値が読み出されるまでの間 SCLn
		// ラインを Low にホールドします。(STOP condition) これにより 1 
		// バイトごとの受信動作が可能です。
		M_IIC.ICMR3.BIT.WAIT = 1 ;
		
		// If RIIC sends ACK when it receives last data, EEPROM may prevent
		// RIIC generating stop condition. Because EEPROM desn't know how
		// many RIIC try to read data.
		// When RIIC sends ACK, EEPROM realizes the next rising SCL for
		// stop condition as next first bit SCL.
		M_IIC.ICMR3.BIT.ACKBT = 1 ;					// Set NACK for final data.
		
		// Read data
		gpI2cTfrInf->ubDataBuf[gpI2cTfrInf->ubDataPtr++] = M_IIC.ICDRR ;
	// 最終data
	} else if ( (gpI2cTfrInf->ubDataPtr + 1) == gpI2cTfrInf->ubDataBufSize ) {
		
		// After read final data, RIIC generates stop condtion
		gpI2cTfrInf->ubDataBuf[gpI2cTfrInf->ubDataPtr++] = M_IIC.ICDRR ;
		M_IIC.ICMR3.BIT.WAIT = 1 ;
		// Generate Stop Condition
		M_IIC.ICSR2.BIT.STOP = 0 ;					// STOP condition 未検出
		M_IIC.ICCR2.BIT.SP = 1 ;					// STOP condition 発行要求
	} else {
		// Read data
//		bDat = RIIC.ICDRR ;						// dummy read
		gpI2cTfrInf->ubDataBuf[gpI2cTfrInf->ubDataPtr++] = M_IIC.ICDRR ;
		M_IIC.ICMR3.BIT.WAIT = 1 ;
		nop( ) ;
	}
	
}


//-----------------------------------------------------------------------------
//	Function Name:	IIC_TXI_Int
//	Description  :	送信データエンプティ割り込みハンドラ
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
//#ifdef	RIIC_CH0
	#pragma interrupt IIC_TXI_Int(vect=VECT_RIIC0_TXI0) 
//#else
//	#pragma interrupt IIC_TXI_Int(vect=VECT_RIIC1_TXI1) 
//#endif
void	IIC_TXI_Int( void )
{
	
	// 内部ステータスによって処理振り分け
	switch( gI2cDrvInf.Mode ) {
		case I2C_MODE_TFR_WRITE :
			IIC_TXI_Int_TfrWrite( ) ;					// 書込用データ送信
			break ;
		case I2C_MODE_TFR_READ :
			IIC_TXI_Int_TfrRead( ) ;					// 読込用データ送信
			break ;
		default:
			IIC_Error( I2C_ERR_MODE_TX_INT ) ;			// Internal mode err
			break ;
	}
	
	nop( ) ;
	
}


//--------------------------------------------------------------------------
//	Function Name:	IIC_TXI_Int_TfrWrite
//	Description  :	送信処理用のTXI割込ハンドラ
//					IIC_TXI_Int() の割込ルーチンから呼ばれる
//	Argument     :
//	Return Value :
//--------------------------------------------------------------------------
static void		IIC_TXI_Int_TfrWrite( void )
{
	
	// Increase internal transmission counter on RAM.
	gI2cDrvInf.udwTxBytes++ ;								// Tx 転送カウンタ
	
	// Transfer slave device address
	if( gI2cDrvInf.udwTxBytes == 1 ) {		// 1st転送
		// Transfer slave device address (Read)
		M_IIC.ICDRT = gpI2cTfrInf->ubSlvAdr & 0xfe ;// マスタ転送時b0は必ず'0'
		return ;
	}
	
	// Transfer reg.# for writting
	if( gpI2cTfrInf->ubRegBufSize > gpI2cTfrInf->ubRegBufPtr ) {	// 最終以外
		M_IIC.ICDRT = gpI2cTfrInf->ubRegBuf[gpI2cTfrInf->ubRegBufPtr++] ;
		return ;
	}
	
	// Transfer data for writting
	if( gpI2cTfrInf->ubDataPtr < gpI2cTfrInf->ubDataBufSize ) {	// データは最終
		M_IIC.ICDRT = gpI2cTfrInf->ubDataBuf[gpI2cTfrInf->ubDataPtr++] ;
	}
	// この後TEI割込でリスタートを掛ける
	if( gpI2cTfrInf->ubDataPtr == gpI2cTfrInf->ubDataBufSize ) {// データは最終
		M_IIC.ICIER.BIT.TEIE = 1 ;							// TEI割込許可
	}
	
	
	return ;
	
}


//--------------------------------------------------------------------------
//	Function Name:	IIC_TXI_Int_TfrRead
//	Description  :	受信処理用のTXI割込ハンドラ
//					IIC_TXI_Int() の割込ルーチンから呼ばれる
//	Argument     :
//	Return Value :
//--------------------------------------------------------------------------
static void		IIC_TXI_Int_TfrRead( void )
{
	
	// Increase internal transmission counter on RAM.
	gI2cDrvInf.udwTxBytes++ ;						// Tx 転送カウンタ

	// Transfer slave device address
	if( gI2cDrvInf.udwTxBytes == 1 ) {		// 1st転送
		// Transfer slave device address (Read)
		M_IIC.ICDRT = gpI2cTfrInf->ubSlvAdr & 0xfe ;// マスタ転送時 b0 は必ず'0'
//		M_IIC.ICDRT = gpI2cTfrInf->ubSlvAdr | 0x01 ;// マスタ受信時 b0 は必ず'1'
		nop( ) ;
		return ;
	}
	nop( ) ;
	
	// Transfer reg.# for read
	if( gpI2cTfrInf->ubRegBufPtr < gpI2cTfrInf->ubRegBufSize ) {	// 最終以外
		M_IIC.ICDRT = gpI2cTfrInf->ubRegBuf[gpI2cTfrInf->ubRegBufPtr++] ;
	}
	nop( ) ;
	// この後TEI割込でリスタートを掛ける
	if( gpI2cTfrInf->ubRegBufPtr == gpI2cTfrInf->ubRegBufSize ) {// データは最終
		M_IIC.ICMR3.BIT.RDRFS = 1 ;						// 8SCL-Clock High
		M_IIC.ICIER.BIT.TEIE = 1 ;						// TEI割込許可
	}
	nop( ) ;
	
	
	return ;
	
}

//-----------------------------------------------------------------------------
//	Function Name:	IIC_TEI_Int
//	Description  :	送信終了割り込みハンドラ
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
#ifdef	RIIC_CH0
	#pragma interrupt IIC_TEI_Int(vect=VECT_RIIC0_TEI0) 
#else
	#pragma interrupt IIC_TEI_Int(vect=VECT_RIIC1_TEI1) 
#endif
void	IIC_TEI_Int( void )
{
	
	// 内部ステータスによって処理振り分け
	switch( gI2cDrvInf.Mode ) {
		case I2C_MODE_TFR_WRITE :
			IIC_TEI_Int_TfrWrite( ) ;				// 書込用終了処理
			break ;
		case I2C_MODE_TFR_READ :
			IIC_TEI_Int_TfrRead( ) ;				// 読込用終了処理
			break ;
		default :
			IIC_Error( I2C_ERR_MODE_TE_INT ) ;			// Internal mode err
			break;
	}
	
	nop( ) ;
	
}


//--------------------------------------------------------------------------
//	Function Name:	IIC_TEI_Int_TfrWrite
//	Description  :	送信用のTEI割込ハンドラ
//					IIC_TEI_Int() 割込ルーチンから呼ばれる
//	Argument     :
//	Return Value :
//--------------------------------------------------------------------------
static void		IIC_TEI_Int_TfrWrite( void )
{
	
	M_IIC.ICIER.BIT.TEIE = 0 ;						// TEI割込禁止へ
	
	// Generate Stop Condition
	M_IIC.ICSR2.BIT.STOP = 0 ;						// STOP condition 未検出
	M_IIC.ICCR2.BIT.SP = 1 ;						// STOP condition 発行要求
	
}

//--------------------------------------------------------------------------
//	Function Name:	IIC_TEI_Int_TfrRead
//	Description  :	受信用のTEI割込ハンドラ
//					IIC_TEI_Int() 割込ルーチンから呼ばれる
//	Argument     :
//	Return Value :
//--------------------------------------------------------------------------
static void		IIC_TEI_Int_TfrRead( void )
{
	
	M_IIC.ICSR2.BIT.START = 0 ;		// Clear Start Condition Detection flag
	M_IIC.ICIER.BIT.STIE = 1;		// Enable Start Condition Detection Int.
	M_IIC.ICIER.BIT.TEIE = 0 ;		// TEI割込禁止へ
	
	M_IIC.ICCR2.BIT.RS = 1 ;		// Generate restart condition
	
}

//-----------------------------------------------------------------------------
//	Function Name:	IIC_BusInitialize
//	Description  :	RIICユニットをリセットしてバスを初期化する
//					Step1 - RIICをリセット (try to be SCL=High and SDA=Hgih)
//							もしリセットしてもSCL=Lowなら他のデバイスが
//							Lowにしていることになる
//					Step2 - もしSDA=LowならSCLに何クロックか出してみる
//					Step3 - SDA=HighにしてSTOPを発行
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
static void		IIC_BusInitialize( void )
{
	volatile UWORD		uwLoop ;
	
	
	M_IIC.ICCR1.BIT.IICRST = 1 ;				// RIIC内部リセット
	
	// タイムアウト検出設定
	M_IIC.ICFER.BIT.TMOE = 0 ;					// タイムアウト検出無効
	M_IIC.ICMR2.BIT.TMOL = 1 ;					// Disable SCL=Low time out
	M_IIC.ICMR2.BIT.TMOH = 1 ;					// Enable SCL=High time out
	M_IIC.ICMR2.BIT.TMOS = 0 ;					// Long mode (16bits counter),
												// PCLK=48MHz, IIC phi=48MHz,
												// Long mode is about 1.364 ms
	
	//======================================================
	// もしRIICがSDA=Lowにしていたなら内部リセット(IICRST=1)
	// で開放するはず
	//======================================================
	M_IIC.ICCR1.BIT.IICRST = 0 ;				// 内部リセット解除
	for( uwLoop = 0 ; uwLoop < 100 ; uwLoop++ ) ;	// ちょいと待つ
	
	M_IIC.ICFER.BIT.TMOE = 0 ;					// タイムアウト検出有効
	
	//======================================================
	// それでもSCL=Lowなら他のデバイスがSCL=Lowにしている
	// どうにもならないのでエラー状態にする
	//======================================================
	if ( M_IIC.ICCR1.BIT.SCLI == 0 ) {			// Check SCL level
		IIC_Error( I2C_ERR_SCL_LOCK ) ;			// Internal Error
	}
	
	
	//======================================================
	// 後の以下の操作でSTOP発行する
	// MST=1でSTOP発行するがその為にMTWP=1でプロテクト解除
	// してTRS=1で送信モードにする必要がある
	//======================================================
	M_IIC.ICMR1.BIT.MTWP = 1 ;					// MST/TRS 書込許可
	M_IIC.ICCR2.BIT.TRS = 1 ;					// 送信モードへ
	M_IIC.ICCR2.BIT.MST = 1 ;					// マスタモードへ
	
	
	//======================================================
	// もし他のデバイスがSDA=LowならRIICから何クロックか送っ
	// てみる。大抵のデバイスはこれでSDA=Lowにするのをやめる
	//======================================================
	if ( M_IIC.ICCR1.BIT.SDAI == 0 ) {			// SDA=Lowだった
		// SDA=Highになるまで10クロック送ってみる
		for( uwLoop = 0 ; uwLoop < 10 ; uwLoop++ ) {		
			if ( M_IIC.ICCR1.BIT.SDAI == 0 ) {	// SDA=Low
				M_IIC.ICCR1.BIT.CLO = 1 ;				// 1クロック出力
				while( M_IIC.ICCR1.BIT.CLO != 0 ) {		// クロック出力完了待ち
					if ( M_IIC.ICSR2.BIT.TMOF != 0 ) {	// Timeoutは無視
						M_IIC.ICSR2.BIT.TMOF = 0 ;		// Not detect timeout
						break ;
					}
				}
			} else {							// SDA=High
				// 正常に戻ったので処理中断
				break ;
			}
			
			// 10クロック送ってもSDA=Lowのままならエラー
			if ( (uwLoop == 9) && (M_IIC.ICCR1.BIT.SDAI == 0) ) {
				IIC_Error( I2C_ERR_SDA_LOCK ) ;			// Internal Error
			}
		}
	}
	
	// こんでもってバス開放状態ならSTOP発行はしない
	// バスビジーならSTOP発行する
	if ( M_IIC.ICCR2.BIT.BBSY == 0 ) {		// not Busy (Bus free)
		M_IIC.ICCR1.BIT.IICRST = 1 ;				// Reset RIIC
		M_IIC.ICCR1.BIT.IICRST = 0 ;				// Clear reset
	} else {								// Busy (Buslock)
		M_IIC.ICSR2.BIT.STOP = 0 ;					// STOP condition 未検出
		M_IIC.ICCR2.BIT.SP = 1 ;					// STOP condition 発行要求
	}
	
	
	// Enable MST/TRS Write Protect
	M_IIC.ICMR1.BIT.MTWP = 0 ;						// MST/TRS 書込禁止へ
	
	
	// タイムアウト検出設定を再度行う(あまり意味無いが)
	M_IIC.ICFER.BIT.TMOE = 1 ;					// タイムアウト検出無効
	M_IIC.ICMR2.BIT.TMOL = 0 ;					// Disable SCL=Low time out
	M_IIC.ICMR2.BIT.TMOH = 1 ;					// Enable SCL=High time out
	M_IIC.ICMR2.BIT.TMOS = 0 ;					// Long mode (16bits counter).
												// PCLK=48MHz, IIC phi=48MHz,
												// Long mode is about 1.365 ms
	M_IIC.ICFER.BIT.TMOE = 1 ;					// タイムアウト検出有効
	
}

/******************************************************************************
* Function Name: IIC_Error
* Description  : Usually this function is not called.
* Argument     : error_code
*              :    I2C_ERR_MODE_SP_INT
*              :    I2C_ERR_MODE_RX_INT_TFR_WRITE
*              :    I2C_ERR_MODE_RX_INT
*              :    I2C_ERR_MODE_TX_INT
*              :    I2C_ERR_MODE_TE_INT
*              :    I2C_ERR_SCL_LOCK
*              :    I2C_ERR_SDA_LOCK
*              :    I2C_ERR_GEN_CLK_BBSY
* Return Value : none
******************************************************************************/
static void IIC_Error( I2C_ERR_CODE ErrorCode )
{
	
	switch( ErrorCode ) {
		case I2C_ERR_MODE_SP_INT :
			break ;
		case I2C_ERR_MODE_RX_INT_TFR_WRITE :
			break ;
		case I2C_ERR_MODE_RX_INT :
			break ;
		case I2C_ERR_MODE_TX_INT :
			break ;
		case I2C_ERR_MODE_TE_INT :
			break ;
		case I2C_ERR_SCL_LOCK :
			break ;
		case I2C_ERR_SDA_LOCK :
			break ;
		case I2C_ERR_GEN_CLK_BBSY :
			break ;
		default :
			break ;
	}
	
	// RIICリセット (レジスタもリセットされる)
	M_IIC.ICCR1.BIT.ICE = 0 ;							// I2C-Bus 停止
	M_IIC.ICCR1.BIT.IICRST = 1 ;						// 内部リセット
	M_IIC.ICCR1.BIT.IICRST = 0 ;						// 内部リセット解除
//	while ( 1 ) ;
	
}

//--------------------------------------------------------------------------
