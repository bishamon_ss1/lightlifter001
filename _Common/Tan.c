//==========================================================================
//	
//	File Name	: Tan.c
//	Version		: R01.00
//	Device(s)	: RX210 64pin
//	Description	: CORDICアルゴリズムによるtan()の計算
//				  RL78用から移植
//	Date		: 2014.08.20
//	
//==========================================================================


//--------------------------------------------------------------------------
//	Includes
//--------------------------------------------------------------------------
#include	<machine.h>

#define	_TAN_DEF_
#include	"Tan.h"






//--------------------------------------------------------------------------
//	マクロ定義
//--------------------------------------------------------------------------
#define		M_PI		3.1415926						// π定義


//-----------------------------------------------------------
//	グローバル変数,関数プロトタイプ宣言
//-----------------------------------------------------------


//===========================================================================



//-----------------------------------------------------------
//	小数部17桁の固定小数点を10進整数化
//	小数５桁
//	引数
//		FIX17	fxIn			入力値
//	戻り値
//		DWORD				000.00000形式の１０進値
//-----------------------------------------------------------
DWORD	Fix2Dec( FIX17 fxIn )
{
	DWORD		dwRet ;							// 戻り値
	DWORD		dwCmp ;							// 比較値
	DWORD		dwAdd ;							// 10進加算値
	DWORD		dwWork ;						// 中間結果
	
	
	// 整数部を得る
	dwRet = (fxIn >> NUM_FIX_DEC) * 100000 ;
	
	// 小数部を得る
	dwCmp = 1L << (NUM_FIX_DEC - 1) ;						// 比較値
	dwAdd = 50000 ;											// 10進加算値
//	dwWork = fxIn << (32 - NUM_FIX_DEC) ;					// 小数部
//	dwWork = dwWork >> (32 - NUM_FIX_DEC) ;					// 小数部
	dwWork = fxIn >> NUM_FIX_DEC ;							// 小数部
	dwWork = fxIn - (dwWork << NUM_FIX_DEC) ;				// 小数部
	while ( dwAdd ) {
		if ( dwWork >= dwCmp ) {
			dwWork -= dwCmp  ;								// 小数部
			if (!dwWork ) {									// 一致終了
				break ;
			}
			dwRet += dwAdd ;								// 戻り値
		}
		// 桁を１つ下げる
		dwCmp >>= 1 ;										// 比較値
		dwAdd >>= 1 ;										// 10進加算値
	}
	
	
	return dwRet ;
	
}


//-----------------------------------------------------------
//	10進整数を小数部17桁の固定小数点化
//	小数５桁
//	引数
//		DWORD				000.00000形式の１０進値
//	戻り値
//		FIX17				小数17bitの固定小数点
//-----------------------------------------------------------
FIX17	Dec2Fix( DWORD dwIn )
{
	FIX17		fxRet ;							// 戻り値
	FIX17		fxCmp ;							// 比較値
	DWORD		dwSum ;							// 10進減算値
	DWORD		dwWork ;						// 中間結果
	
	
	
	// 整数部を得る
	fxRet = (dwIn / 100000) << NUM_FIX_DEC ;
	
	// 小数部を得る
	fxCmp = 1L << (NUM_FIX_DEC - 1) ;						// 比較値
	dwSum = 50000 ;											// 10進加算値
	dwWork = dwIn / 100000 ;								// 小数部
	dwWork = dwIn - (dwWork * 100000) ;						// 小数部
	while ( dwSum ) {
		if ( dwWork >= dwSum ) {
			fxRet += fxCmp ;								// 戻り値
			dwWork -= dwSum  ;								// 小数部
			if (!dwWork ) {									// 一致終了
				break ;
			}
		}
		// 桁を１つ下げる
		fxCmp >>= 1 ;										// 比較値
		dwSum >>= 1 ;										// 10進減算値
	}
	
	
	
	return fxRet ;
	
}


//-----------------------------------------------------------
//	CORDICアルゴリズムによるATAN計算-固定小数点版
//	Y値をゼロに持って行く方法による(比較が単純になる)
//	引数
//		FIX17	fxYin		Y値
//		FIX17	fxXin		X値
//	戻り値
//		FIX17				角度(deg)
//-----------------------------------------------------------
FIX17	cAtan2Fx( FIX17 fxYin, FIX17 fxXin )
{
	FIX17		fxAng ;							// 角度(deg)
	FIX17		*pfxAng ;						// 角度TblPtr
	FIX17		fxX, fxY ;						// 現在の座標値
	FIX17		fxX_prv, fxY_prv ;				// 1つ前の座標値
	WORD		wDiv ;							// 除数(シフト数)
	BYTE		bSect ;							// 象限
	
	
	//------------------------
	//	初期座標値セット
	//------------------------
	if ( fxYin > 0 ) {
		if ( fxXin > 0 ) {							// 第１象限
			bSect = 1 ;								// 象限
			fxX = fxYin ;							// 現座標値 X
			fxY = -fxXin ;							// 現座標値 Y
		} else if ( fxXin < 0 ) {					// 第２象限
			bSect = 2 ;								// 象限
			fxX = fxYin ;							// 現座標値 X
			fxY = fxXin ;							// 現座標値 Y
		} else {
			return (90L << NUM_FIX_DEC) ;			// 角度(deg)
		}
	} else if ( fxYin < 0 ) {
		if ( fxXin > 0 ) {							// 第４象限
			bSect = 4 ;								// 象限
			fxX = -fxYin ;							// 現座標値 X
			fxY = fxXin ;							// 現座標値 Y
		} else if ( fxXin < 0 ) {					// 第３象限
			bSect = 3 ;								// 象限
			fxX = -fxYin ;							// 現座標値 X
			fxY = -fxXin ;							// 現座標値 Y
		} else {
			return (-90L << NUM_FIX_DEC) ;			// 角度(deg)
		}
	} else {										// 0/180度
		if ( fxXin >= 0 ) {							// 0
			fxAng = 0L ;							// 角度(deg)
		} else {									// 180
			fxAng = (180L << NUM_FIX_DEC) ;			// 角度(deg)
		}
		return fxAng ;
	}
	
	
	//------------------------
	//	初期化
	//------------------------
	fxAng = -90L << NUM_FIX_DEC ;					// 角度(deg) -90度
	wDiv = 0 ;										// 除数(シフト数)
	
	
	//------------------------
	//	計算ループ
	//------------------------
	pfxAng = (FIX17 *)&gfxAngTbl[0] ;				// 角度TblPtr
	for( ; *pfxAng ; pfxAng++ ) {
		fxX_prv = (fxX >> wDiv) ;					// 1つ前の座標値 X
		fxY_prv = (fxY >> wDiv) ;					// 1つ前の座標値 Y
		if ( fxY == 0 ) {							// 角度収束
			break ;
		} else if ( fxY > 0 ) {						// 角度を減らす(右回転)
			fxAng -= *pfxAng ;						// 角度(deg)
			fxX += fxY_prv ;						// 現座標値 X
			fxY -= fxX_prv ;						// 現座標値 Y
		} else {									// 角度を増やす(左回転)
			fxAng += *pfxAng ;						// 角度(deg)
			fxX -= fxY_prv ;						// 現座標値 X
			fxY += fxX_prv ;						// 現座標値 Y
		}
		wDiv++ ;									// 次の除数(シフト数)
	}
	
	
	//------------------------
	//	角度補正
	//------------------------
	switch ( bSect ) {
		case 1 :											// 第１象限
			fxAng = -fxAng ;								// 角度(deg)
			break ;
		case 2 :											// 第２象限
			fxAng = (180L << NUM_FIX_DEC) + fxAng ;			// 角度(deg)
			break ;
		case 3 :											// 第３象限
			break ;
		case 4 :											// 第４象限
			fxAng = (-180L << NUM_FIX_DEC) - fxAng ;		// 角度(deg)
			break ;
		default :
			break ;
	}
	
	
	
//	return -fxAng ;
	return fxAng ;
	
}


//===========================================================================
