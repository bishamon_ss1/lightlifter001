//=====================================================================
//    SCI(UART) functions
//          with ring buffers and interrupt functions
//									'09.11.18 v0.2 by H.Inoue
//									'09.12.08 v0.3 by H.Inoue
//									受信エラーレジスタクリア追加
//									受信エラーバッファ追加
//          potring for R8C/2A (UART2)
//									'10.07.30 v0.31 by H.Inoue
//									'10.08.17 v0.32 by H.Inoue
//									各種Bugfix
//          potring for RX62N
//									'11.07.04 v0.4 by H.Inoue
//          potring for RX210
//									'14.07.02 v0.5 by H.Inoue
//									多チャンネル対応,RS485対応
//									XON/XOFFフロー制御は使わないので外す
//									エラーハンドラは使わないので外す
//=====================================================================
//	注：
//		MCUのピン数や使用ポートに応じて以下の関数を修正すること
//		  sSci_SetTxIOP( ), sSci_SetPinFunc( ), sSci_SetPortModePMR( )
//		使用チャンネルに応じて以下の割込関数を追加すること
//		  Int_SCIn_ERI( ), Int_SCIn_RXI( ), Int_SCIn_TXI( ), Int_SCIn_TEI( )
//		  SCIn=SCIチャンネル番号
//		必要に応じてRS485の記述を追加・修正すること
//		  次の定義のあるところ #ifdef	RS485_CHn
//		  RS485_CHn 使用チャンネル番号
//		
//=====================================================================

#include	<machine.h>					// Include machine.h
#include	<stdarg.h>					// 可変長引数サポート用
#include	"_iodefine.h"				// Include _iodefine.h
#include	"_vect.h"					// 割込み関数宣言

#define		_SCI_DRIVER_
#include	"SCI_RX210.h"


//--------------------------------
//	MCUシステム値
//--------------------------------
#define	MCU_PCLOCK	25000000				// 周辺機器Clock 25MHz

//-----------------------------------------------------------
//	SCIチャンネル選択マクロ
//-----------------------------------------------------------
// ベクタ番号
#define	_VECT_SCI( n, m ) 	VECT( SCI ## n, m ## n )
#define	VECT_SCI( n, m ) 	_VECT_SCI( n, m )

//-----------------------------------------------------------
//	SCIボーレート設定定義 PCLK=48MHz
//-----------------------------------------------------------
// CKS値
#define	SCI_CKS0			0			// PCLK   (n=0)
#define	SCI_CKS1			1			// PCLK/4 (n=1)
#define	SCI_CKS2			2			// PCLK/16(n=2)
#define	SCI_CKS3			3			// PCLK/64(n=3)

// SEMR.ABCS値
#define	SCI_ABCS_16C		0			// Trn=Pclock/16
#define	SCI_ABCS_8C			1			// Trn=Pclock/8

#if	MCU_PCLOCK == 48000000				// 周辺機器Clock 48MHz
	#define	SCI_ABCS			SCI_ABCS_16C	// Trn=Pclock/16
	#define	SCI_BRR_300			77				// BRR 300bps
	#define	SCI_CKS_300			SCI_CKS3		// CKS 3
	#define	SCI_BRR_600			155				// BRR 600bps
	#define	SCI_CKS_600			SCI_CKS2		// CKS 2
	#define	SCI_BRR_1200		77				// BRR 1200bps
	#define	SCI_CKS_1200		SCI_CKS2		// CKS 2
	#define	SCI_BRR_2400		155				// BRR 2400bps
	#define	SCI_CKS_2400		SCI_CKS1		// CKS 1
	#define	SCI_BRR_4800		77				// BRR 4800bps
	#define	SCI_CKS_4800		SCI_CKS1		// CKS 1
	#define	SCI_BRR_9600		38				// BRR 9600bps
	#define	SCI_CKS_9600		SCI_CKS1		// CKS 1
	#define	SCI_BRR_19200		77				// BRR 19200bps
	#define	SCI_CKS_19200		SCI_CKS0		// CKS 0
	#define	SCI_BRR_31250		47				// BRR 31250bps
	#define	SCI_CKS_31250		SCI_CKS0		// CKS 0
	#define	SCI_BRR_38400		38				// BRR 38400bps
	#define	SCI_CKS_38400		SCI_CKS0		// CKS 0
	#define	SCI_BRR_57600		25				// BRR 57600bps
	#define	SCI_CKS_57600		SCI_CKS0		// CKS 0
	#define	SCI_BRR_115200		12				// BRR 115200bps
	#define	SCI_CKS_115200		SCI_CKS0		// CKS 0
#elif	MCU_PCLOCK == 25000000			// 周辺機器Clock 25MHz
	#define	SCI_ABCS			SCI_ABCS_8C		// Trn=Pclock/8
	#define	SCI_BRR_300			80				// BRR 300bps
	#define	SCI_CKS_300			SCI_CKS3		// CKS 3
	#define	SCI_BRR_600			40				// BRR 600bps
	#define	SCI_CKS_600			SCI_CKS3		// CKS 3
	#define	SCI_BRR_1200		80				// BRR 1200bps
	#define	SCI_CKS_1200		SCI_CKS2		// CKS 2
	#define	SCI_BRR_2400		40				// BRR 2400bps
	#define	SCI_CKS_2400		SCI_CKS2		// CKS 2
	#define	SCI_BRR_4800		80				// BRR 4800bps
	#define	SCI_CKS_4800		SCI_CKS1		// CKS 1
	#define	SCI_BRR_9600		40				// BRR 9600bps
	#define	SCI_CKS_9600		SCI_CKS1		// CKS 1
	#define	SCI_BRR_19200		80				// BRR 19200bps
	#define	SCI_CKS_19200		SCI_CKS0		// CKS 0
	#define	SCI_BRR_31250		49				// BRR 31250bps
	#define	SCI_CKS_31250		SCI_CKS0		// CKS 0
	#define	SCI_BRR_38400		40				// BRR 38400bps
	#define	SCI_CKS_38400		SCI_CKS0		// CKS 0
	#define	SCI_BRR_57600		26				// BRR 57600bps
	#define	SCI_CKS_57600		SCI_CKS0		// CKS 0
	#define	SCI_BRR_115200		13				// BRR 115200bps
	#define	SCI_CKS_115200		SCI_CKS0		// CKS 0
#endif

//-----------------------------------------------------------
//	SCI処理用定数定義
//-----------------------------------------------------------

// 割込要求指定
#define	SCI_INT_DISABLE		0			// 割込禁止
#define	SCI_INT_ENABLE		1			// 割込許可

// 割込要求状態
#define	SCI_INT_REQ_NO		0			// 割込要求無し
#define	SCI_INT_REQ_YES		1			// 割込要求有り

// 割込の種類
#define	SCI_INTT_ERI		0			// 受信エラー
#define	SCI_INTT_RXI		1			// 受信
#define	SCI_INTT_TXI		2			// 送信データエンプティ
#define	SCI_INTT_TEI		3			// 送信終了

// ポートモードの端子指定
#define	SCI_PMR_RX			0			// 受信用端子
#define	SCI_PMR_TX			1			// 送信用端子

// ポートモードの指定
#define	SCI_PMR_IOP			0			// IOポート指定
#define	SCI_PMR_FUNC		1			// 周辺機能指定

// 割込設定値 --- SCIの全種類の共通割り込みレベル
// 注: SCI_Conf.hで定義

// 送信リトライ値
// 注: SCI_Conf.hで定義

// SCIn.SSRのエラーフラグマスク
#define	SCI_ERRF_MASK		0x38		// SSR Err-Flag Mask


// I/O端子の出力モード
#define	PORT_PDR_IN			0			// ポートは入力
#define	PORT_PDR_OUT		1			// ポートは出力

// I/O状態
#define	EXTOUT_LOW			0			// 出力 Low
#define	EXTOUT_HIGH			1			// 出力 High




//-----------------------------------------------------------
//	構造体・共用体型定義
//-----------------------------------------------------------

// チャンネル-ハンドルテーブル -> SCI_RX.hへ移動

// 送受信バッファ
typedef struct _SCI_RXB_ {
		UBYTE	ubBuf[SCI_RX_BUF_SIZE] ;	// 受信リングバッファ
		UBYTE	ubOutPtr ;					// 受信読み取りポインター
		UBYTE	ubInPtr ;					// 受信書き込みポインター
	} SCI_RXB ;
typedef struct _SCI_TXB_ {
		UBYTE	ubBuf[SCI_TX_BUF_SIZE] ;	// 送信リングバッファ
		UBYTE	ubOutPtr ;					// 送信読み取りポインター
		UBYTE	ubInPtr ;					// 送信書き込みポインター
	} SCI_TXB ;
// 内部状態フラグ -> SCI_RX.hへ移動


//-----------------------------------------------------------
//	SCIグローバル変数宣言
//-----------------------------------------------------------
// UART レジスタポインタ設定
typedef	struct st_sci0	ST_SCI0 ;			// iodefine.hの構造体に名前付け
volatile __evenaccess ST_SCI0	*gpstSci[] = {
										(ST_SCI0 *)0x8A000,	// SCI0
										(ST_SCI0 *)0x8A020,	// SCI1
										(ST_SCI0 *)0x8A040,	// SCI2
										(ST_SCI0 *)0x8A060,	// SCI3
										(ST_SCI0 *)0x8A080,	// SCI4
										(ST_SCI0 *)0x8A0A0,	// SCI5
										(ST_SCI0 *)0x8A0C0,	// SCI6
										(ST_SCI0 *)0x8A0E0,	// SCI7
										(ST_SCI0 *)0x8A100,	// SCI8
										(ST_SCI0 *)0x8A120,	// SCI9
										(ST_SCI0 *)0x8A140,	// SCI10
										(ST_SCI0 *)0x8A160	// SCI11
											} ;

// チャンネル-ハンドルテーブル -> SCI_RX.hへ移動

// 送受信バッファ
SCI_RXB				gSciRxb[UART_CH_MAX] ;			// 受信バッファ
volatile SCI_TXB	gSciTxb[UART_CH_MAX] ;			// 送信バッファ
// 内部状態フラグ	-> SCI_RX.h へ移動

// メモリ出力用Ptr  NULL:メモリ出力,その他はUARTチャンネルptr
static	UBYTE		*gpubMemOut ;



//-----------------------------------------------------------
//	SCIプロトタイプ宣言
//-----------------------------------------------------------

// 出力ストリーム関数Ptr
void	(*xFunc_Out)(UBYTE, UBYTE) ;		// Pointer to the output stream


//==========================================================================
#pragma section

//-----------------------------------------------------------
//	ストリーム又はメモリポインタに１文字出力
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		UBYTE ubChar		出力文字
//	戻り値
//-----------------------------------------------------------
void	xPutc( UBYTE ubCh, UBYTE  ubChar )
{
	UBYTE			ubHnd ;								// ハンドル
	
	
	// ハンドル取得
	ubHnd = gChTbl[ubCh].ubHnd ;						// ハンドル(Global)
	
	// CR+LF変換
	if ( (gSciF[ubHnd].fCvtCRLF == UART_CRLF_YES) && 
		 (ubChar == '\n') ) {
		xPutc( ubCh, '\r' ) ;						// CR -> CRLF(再入させる)
	}
	
	// メモリ出力指定の場合はポインタ処理
	if ( gpubMemOut != NULL ) {
		*gpubMemOut++ = (UBYTE)ubChar ;
		return;
	}
	
	// ストリーム出力の場合は関数に渡す
	if ( xFunc_Out ) {
		xFunc_Out( (UBYTE)ubCh, (UBYTE)ubChar ) ;	// 1文字出力関数
	}
	
	
}


//-----------------------------------------------------------
//	ストリーム又はメモリポインタに文字列出力
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		UBYTE *pubStr		送信データ
//	戻り値 
//-----------------------------------------------------------
void	xPuts( UBYTE ubCh, UBYTE *pubStr )
{
	
	
	while( *pubStr ){
		xPutc( ubCh, *pubStr++ ) ;					// １文字出力
	}
	
	
}


//-----------------------------------------------------------
//	ストリーム又はメモリポインタ版printf
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		CHAR *pcFmt		書式
//		可変引数
//	戻り値 
//-----------------------------------------------------------
void	xvPrintf( UBYTE ubCh, const CHAR *pcFmt, va_list arp )
{
//	FLAG			fRet ;
	CHAR			cFmt ;						// フォーマット文字
	FLAG			fPfx ;						// 前処理フラグ
	CHAR			cWidth ;					// 出力桁指定
	CHAR			cChk ;						// 書式検査文字(大文字)
	CHAR			*pcStr ;
	UBYTE			ubLen ;						// 文字数
	UBYTE			ubNum ;						// 出力数値タイプ
	UDWORD			udwNum ;					// 出力数値
	CHAR			cBuf[16] ;					// 数値出力Buffer
	CHAR			bBptr ;						// 数値出力BufferPtr
	CHAR			cNum ;
//	CHAR			cOut ;
	CHAR			cLen ;
	
	
	
	for ( ; ; ) {
		cFmt = *pcFmt++ ;						// 文字取り出し
		// 処理終了判定
		if ( !cFmt ) {							// End of format
			break ;
		}
		
		// 書式識別文字以外は直接出力
		if ( cFmt != '%' ) {					// not a % sequense
			xPutc( ubCh, cFmt ) ;				// １文字出力
			continue ;
		}
		
		// 0詰め,左詰め識別
		fPfx = 0 ;								// 前処理無し
		cFmt = *pcFmt++ ;						// 文字取り出し
		if ( cFmt == '0' ) {					// Flag: '0'詰め
			fPfx = 1 ;							// '0'詰め
			cFmt = *pcFmt++ ;					// 文字取り出し
		} else {
			if ( cFmt == '-') {					// Flag: 左詰め
				fPfx = 2 ;						// 左詰め
				cFmt = *pcFmt++ ;				// 文字取り出し
			}
		}
		// 出力桁数指定識別
		cWidth = 0 ;							// 出力桁指定無し
		for ( ; cFmt >= '0' && cFmt <= '9'; cFmt = *pcFmt++ ) {	// Min width
			cWidth = cWidth * 10 + cFmt - '0' ;	// 出力桁指定
		}
		// long int (DWORD)識別
		if ( cFmt == 'l' || cFmt == 'L') {		// Prefix: Size is long int
			fPfx |= 0x04 ;						// long int 出力
			cFmt = *pcFmt++ ;					// 文字取り出し
		}
		// 処理終了判定
		if ( !cFmt ) {							// End of format
			break ;
		}
		
		cChk = cFmt ;							// 書式検査文字(大文字)
		if ( cChk >= 'a' ) cChk -= 0x20 ;		// 大文字変換
		switch ( cChk ) {						// 書式別出力
			case 'S' :							// String
				// 文字数取得
				pcStr = va_arg( arp, CHAR * ) ;				// 引数取り出し
				for ( ubLen = 0 ; pcStr[ubLen]; ubLen++ ) ;	// 文字数取得
				// 左詰スペーシング
				while ( !(fPfx & 2) && (ubLen++ < cWidth) ) {
					xPutc( ubCh, (UBYTE)cFmt ) ;			// １文字出力
				}
				// 文字列取得
				xPuts( ubCh, (UBYTE *)pcStr ) ;				// 文字列出力
				// 右側スペーシング
				while ( ubLen++ < cWidth ) {
					xPutc( ubCh, ' ' ) ;					// １文字出力
				}
				continue ;
			case 'C' :							// Character
				xPutc( ubCh, (char)va_arg(arp, int) ) ;		// １文字出力
				continue ;
			case 'B' :							// Binary ２進数
				ubNum = 2 ;						// 出力数値タイプ
				break ;
			case 'O' :							// Octal  ８進数
				ubNum = 8 ;						// 出力数値タイプ
				break ;
			case 'D' :							// Signed decimal 10進数
			case 'U' :							// Unsigned decimal 10進数
				ubNum = 10 ;					// 出力数値タイプ
				break ;
			case 'X' :							// Hexdecimal 16進数
				ubNum = 16 ;					// 出力数値タイプ
				break ;
			default:							// Unknown type (passthrough)
				xPutc( ubCh, cFmt ) ;						// １文字出力
				continue ;
		}
		
		// 数値取り出し
		if ( fPfx & 0x04 ) {					// long int
			udwNum = va_arg( arp, long ) ;					// 引数取り出し
		} else if ( cChk == 'D' ) {
			udwNum = (UDWORD)va_arg( arp, int ) ;			// 引数取り出し
			if ( udwNum & 0x80000000 ) {					// -数値判定
				udwNum = 0 - udwNum ;
				fPfx |= 8 ;
			}
		} else {
			udwNum = (UDWORD)va_arg( arp, unsigned int ) ;	// 引数取り出し
		}
		
		//----------------
		// 数値出力
		//----------------
		bBptr = 0 ;
		do {
			cNum = (CHAR)(udwNum % ubNum) ;
			udwNum /= ubNum ;
			if ( cNum > 9 ) {								// 16進変換
				if ( cFmt == 'x' ) {
					cNum += 0x27 ;
				} else {
					cNum += 0x07 ;
				}
			}
			cBuf[bBptr++] = cNum + '0' ;
		} while ( udwNum && (bBptr < sizeof(cBuf)) ) ;
		if ( fPfx & 8 ) {									// -符号
			cBuf[bBptr++] = '-' ;
		}
		cLen = bBptr ;
		if ( fPfx & 0x01 ) {					// 左側Padding文字
			cNum = '0' ;
		} else {
			cNum = ' ' ;
		}
		// 左側Padding
		while ( !(cFmt & 2) && (cLen++ < cWidth) ) {
			xPutc( ubCh, cNum ) ;							// １文字出力
		}
		// 数値出力
		do {
			xPutc( ubCh, cBuf[--bBptr] ) ;					// １文字出力
		} while (bBptr) ;
		// 右側Padding
		while ( cLen++ < cWidth ) {
			xPutc( ubCh, ' ' ) ;							// １文字出力
		}
		
		
	}
	
	
}


//-----------------------------------------------------------
//	ストリーム版printf
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		CHAR *pcFmt		書式
//		可変引数
//	戻り値 
//-----------------------------------------------------------
void	xPrintf( UBYTE ubCh, const CHAR *pcFmt, ... )
{
	va_list			arp ;
	
	
	va_start( arp, pcFmt ) ;					// 可変引数取扱開始
	xvPrintf( ubCh, pcFmt, arp ) ;				// printf処理実態
	va_end( arp ) ;								// 可変引数取扱終了
	
	
}


//-----------------------------------------------------------
//	メモリ版printf
//	引数
//		CHAR *pcOutBuff		出力バッファptr
//		CHAR *pcFmt		書式
//		可変引数
//	戻り値 
//-----------------------------------------------------------
void	xsPrintf( 
					CHAR *pcOutBuff, 
					const CHAR *pcFmt, 
					... 
				)
{
	va_list			arp ;
	
	
	gpubMemOut = (UBYTE *)pcOutBuff ;			// 内部ptrセット
	
	va_start( arp, pcFmt ) ;					// 可変引数取扱開始
	xvPrintf( 0, pcFmt, arp ) ;					// printf処理実態
	va_end( arp ) ;								// 可変引数取扱終了
	
	*gpubMemOut = NULL ;						// 文字列最終端
	gpubMemOut = NULL ;							// 内部ptrリセット
	
	
}


//-----------------------------------------------------------
//	指定SCIチャンネルのベース割込ベクタ番号(ERI)を得る
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//	戻り値(UBYTE)
//		ベース割込ベクタ番号(ERI)
//		該当無しの場合はゼロ
//-----------------------------------------------------------
static	UBYTE	sSci_GetVectorBase( UBYTE ubCh )
{
	UBYTE			ubVectBase ;						// Vector# Base
	
	
	// ベースベクタ番号セット
	switch ( ubCh ) {
		case UART_CH_2 :
		case UART_CH_3 :
		case UART_CH_4 :
			ubVectBase = 186 + (ubCh - 2) * 4 ;			// Vector# Base
			break ;
		case UART_CH_7 :
			ubVectBase = 206 ;							// Vector# Base
			break ;
		case UART_CH_10 :
			ubVectBase = 210 ;							// Vector# Base
			break ;
		case UART_CH_0 :
		case UART_CH_1 :
			ubVectBase = 214 + (ubCh - 0) * 4 ;			// Vector# Base
			break ;
		case UART_CH_5 :
		case UART_CH_6 :
			ubVectBase = 222 + (ubCh - 5) * 4 ;			// Vector# Base
			break ;
		case UART_CH_8 :
		case UART_CH_9 :
			ubVectBase = 230 + (ubCh - 8) * 4 ;			// Vector# Base
			break ;
		case UART_CH_11 :
			ubVectBase = 250 ;							// Vector# Base
			break ;
		default :
			ubVectBase = 0 ;							// Vector# Base
	}
	
	
	return ubVectBase ;
	
}

//-----------------------------------------------------------
//	指定SCIチャンネルの割込許可/禁止処理(IER)
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//		UBYTE ubIntType		割込の種類
//								SCI_INTT_ERI		受信エラー
//								SCI_INTT_RXI		受信
//								SCI_INTT_TXI		送信データエンプティ
//								SCI_INTT_TEI		送信終了
//		UBYTE ubEnable		割込許可/禁止指定
//								SCI_INT_DISABLE		 割込禁止
//								SCI_INT_ENABLE		 割込許可
//-----------------------------------------------------------
static	void	sSci_SetIntEnableIER( 
									UBYTE ubCh, 
									UBYTE ubIntType,
									UBYTE ubEnable
								 ) 
{
	UBYTE			ubVectBase ;						// Vector# Base
	UBYTE			ubIdx ;								// IER-Idx
	UBYTE			ubBit ;								// Bit#
	UBYTE			ubData, ubMask ;
	
	
	// ベースベクタ番号セット
	ubVectBase = sSci_GetVectorBase( ubCh ) ;
	if ( ubVectBase == 0 ) return ;						// チャンネルが不正
	
	// ERI,RXI,TXI,TEIの禁止/許可をセット
	ubIdx = (ubVectBase + ubIntType) / 8 ;				// IER-Idx
	ubBit = (ubVectBase + ubIntType) - (ubIdx * 8) ;	// Bit#
	ubData = ICU.IER[ubIdx].BYTE ;						// IER Data
	ubMask = 1 << ubBit ;
	if ( ubEnable == SCI_INT_DISABLE ) {			// 禁止
		ubMask = ~ubMask ;
		ubData &= ubMask ;								// IER Data合成
	} else {										// 許可
		ubData |= ubMask ;								// IER Data合成
	}
	ICU.IER[ubIdx].BYTE = ubData ;						// IER Data
	
}


//-----------------------------------------------------------
//	指定SCIチャンネルの割込要求フラグ設定(IR)
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//		UBYTE ubIntType		割込の種類
//								SCI_INTT_ERI		受信エラー
//								SCI_INTT_RXI		受信
//								SCI_INTT_TXI		送信データエンプティ
//								SCI_INTT_TEI		送信終了
//		FLAG  fIntReq		割込要求フラグ
//			SCI_INT_REQ_NO		割込要求無し
//			SCI_INT_REQ_YES		割込要求有り
//-----------------------------------------------------------
static	void	sSci_SetIntReqIR( 
									UBYTE ubCh, 
									UBYTE ubIntType,
									FLAG  fIntReq
								 ) 
{
	UBYTE			ubIdx ;								// IR-Idx
	
	
	// ベースベクタ番号セット
	ubIdx = sSci_GetVectorBase( ubCh ) ;
	if ( ubIdx == 0 ) return ;							// チャンネルが不正
	
	// ERI,RXI,TXI,TEIの要求状態をセット
	ICU.IR[ubIdx + ubIntType].BIT.IR = fIntReq ;		// IR Data
//	while( ICU.IR[ubIdx + ubIntType].BIT.IR != fIntReq ) {	// 更新待ち
//		nop( ) ;
//	}
	
	
}


//-----------------------------------------------------------
//	指定SCIチャンネルの割込レベル設定(IPR)
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//		UBYTE ubIntLvl		割込レベル指定(0-15)
//-----------------------------------------------------------
static	void	sSci_SetIntLevelIPR( UBYTE ubCh, UBYTE ubIntLvl ) 
{
	UBYTE			ubIdx ;								// IPR-Idx
	
	
	// IPR番号セット
	ubIdx = sSci_GetVectorBase( ubCh ) ;
	if ( ubIdx == 0 ) return ;							// チャンネルが不正
	
	// 割込レベルをセット
	ICU.IPR[ubIdx].BIT.IPR = ubIntLvl & 0x0f ;			// 割込レベル
	
	
}


//-----------------------------------------------------------
//	指定SCIチャンネルのTXポート出力設定
//		注：必要に応じて変更すること
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//-----------------------------------------------------------
static	void	sSci_SetTxIOP( UBYTE ubCh ) 
{
	
	
	switch ( ubCh ) {
		case UART_CH_1 :	// 48/64pin SCI1  TXD1->P26
			PORT2.PODR.BIT.B6 = EXTOUT_HIGH ;			// P26
			PORT2.PDR.BIT.B6 = PORT_PDR_OUT ;			// P26
			break ;
		case UART_CH_5 :	// 48/64pin SCI5  TXD5->PA4
			PORTA.PODR.BIT.B4 = EXTOUT_HIGH ;			// PA4
			PORTA.PDR.BIT.B4 = PORT_PDR_OUT ;			// PA4
			break ;
		case UART_CH_6 :	// 48/64pin SCI6  TXD6->PB1
			PORTB.PODR.BIT.B1 = EXTOUT_HIGH ;			// PB1
			PORTB.PDR.BIT.B1 = PORT_PDR_OUT ;			// PB1
			break ;
		default :
			return ;
	}
	
	
}


//-----------------------------------------------------------
//	指定SCIチャンネルの端子機能選択設定(MPC)
//		注：必要に応じて変更すること
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//-----------------------------------------------------------
static	void	sSci_SetPinFunc( UBYTE ubCh ) 
{
	
	
	// PmnPFSへの書き込みを許可する
	MPC.PWPR.BIT.B0WI  = 0 ;							// PFSWE書込許可
	MPC.PWPR.BIT.PFSWE = 1 ;							// PFSreg書込許可
	
	// 端子機能選択設定
	switch ( ubCh ) {
		case UART_CH_1 :	// 48/64pin SCI1 RXD1->P30, TXD1->P26
			MPC.P30PFS.BIT.PSEL = 0x0a ;						// RXD1->P30
			MPC.P26PFS.BIT.PSEL = 0x0a ;						// TXD1->P26
			break ;
		case UART_CH_5 :	// 48/64pin SCI5 RXD5->PA3, TXD5->PA4
			MPC.PA3PFS.BIT.PSEL = 0x0a ;						// RXD5->PA3
			MPC.PA4PFS.BIT.PSEL = 0x0a ;						// TXD5->PA4
			break ;
		case UART_CH_6 :	// 48/64pin SCI6 RXD6->PB0, TXD6->PB1
			MPC.PB0PFS.BIT.PSEL = 0x0b ;						// RXD6->PB0
			MPC.PB1PFS.BIT.PSEL = 0x0b ;						// TXD6->PB1
			break ;
		default :
			return ;
	}
	
	// PmnPFSへの書き込みを禁止する
	MPC.PWPR.BIT.PFSWE = 0 ;							// PFSreg書込禁止
	MPC.PWPR.BIT.B0WI  = 1 ;							// PFSWE書込禁止
	
	
}


//-----------------------------------------------------------
//	指定SCIチャンネルの端子ポートモード設定(PMR)
//		注：必要に応じて変更すること
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//		FLAG fPort			ポートモードの端子指定
//								SCI_PMR_RX			受信用端子
//								SCI_PMR_TX			送信用端子
//		FLAG fMode			ポートモードの指定
//								SCI_PMR_IOP			IOポート指定
//								SCI_PMR_FUNC		周辺機能指定
//-----------------------------------------------------------
static	void	sSci_SetPortModePMR( 
									UBYTE ubCh,
									FLAG fPort,
									FLAG fMode
									 ) 
{
	
	
	switch ( ubCh ) {
		case UART_CH_1 :	// 48/64pin SCI1 RXD1->P30, TXD1->P26
			if ( fPort == SCI_PMR_RX ) {				// RX
				PORT3.PMR.BIT.B0 = fMode ;				// RXD1->P30
			} else {									// TX
				PORT2.PMR.BIT.B6 = fMode ;				// TXD1->P26
			}
			break ;
		case UART_CH_5 :	// 48/64pin SCI5 RXD5->PA3, TXD5->PA4
			if ( fPort == SCI_PMR_RX ) {				// RX
				PORTA.PMR.BIT.B3 = fMode ;				// RXD5->PA3
			} else {									// TX
				PORTA.PMR.BIT.B4 = fMode ;				// TXD5->PA4
			}
			break ;
		case UART_CH_6 :	// 48/64pin SCI6 RXD6->PB0, TXD6->PB1
			if ( fPort == SCI_PMR_RX ) {				// RX
				PORTB.PMR.BIT.B0 = fMode ;				// RXD6->PB0
			} else {									// TX
				PORTB.PMR.BIT.B1 = fMode ;				// TXD6->PB1
			}
			break ;
		default :
			return ;
	}
	
	
}


//-----------------------------------------------------------
//	SCIを初期化
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		UBYTE ubSpeed		通信速度指定
//								UART_BAUD_300		 300bps
//								UART_BAUD_600		 600bps
//								UART_BAUD_1200		 1200bps
//								UART_BAUD_2400		 2400bps
//								UART_BAUD_4800		 4800bps
//								UART_BAUD_9600		 9600bps
//								UART_BAUD_19200		 19200bps
//								UART_BAUD_31250		 31250bps
//								UART_BAUD_57600		 57600bps
//		UBYTE ubParity		パリティ指定
//								UART_PARITY_NONE	 パリティ無し
//								UART_PARITY_EVEN	 偶数パリティ
//								UART_PARITY_ODD		 奇数パリティ
//		UBYTE ubStopBit		STOPビット指定
//								UART_STOP_1			1bit
//								UART_STOP_2			2bit
//		UBYTE ubDataLen		データ長指定
//								UART_BIT_7			7bit
//								UART_BIT_8			8bit
//	戻り値(FLAG)
//		UART_SUCCESS(0)			正常終了
//		UART_ERR_PARM(1)		異常終了 (パラメータエラー)
//		UART_HANDLE_FULL(21)	使用可能ハンドルを使い切った
//		UART_CH_USED(20)		チャンネルは既に使用中
//-----------------------------------------------------------
FLAG	SCI_Inz(
					UBYTE ubCh,
					UBYTE ubSpeed, 
					UBYTE ubParity,
					UBYTE ubStopBit,
					UBYTE ubDataLen
			)
{
	UBYTE			ubLoop ;							// LoopCount
	UBYTE			ubUsed ;							// Used Ch Count
	UBYTE			ubHnd ;								// ハンドル
	volatile __evenaccess ST_SCI0	*pstSci ;			// 周辺Reg構造体
	volatile UDWORD	*pudwMstp ;							// ModuleStopPtr
	UDWORD			udwMstpMask ;						// ModuleStopMask
	UBYTE			ubShift ;							// ModuleStopShift
	UWORD			uwLoop ;							// LoopCount
	UWORD			uwBps ;								// BPS/100
#ifdef	RS485_USE
	UWORD			uwFj ;								// TotalBit数
#endif		// #ifdef	RS485_USE
	
	
	// パラメータチェック
	// コンパイル時に W0520186 が出ることがあるが敢えて無視
	if ( !((ubCh >= UART_CH_TOP) && (ubCh <= UART_CH_LAST)) ) {	// 使用ch
		return UART_ERR_PARM ;
	}
	if ( ubSpeed > UART_BAUD_115200 ) {					// 通信速度
		return UART_ERR_PARM ;
	}
	if ( (ubParity != UART_PARITY_NONE) &&
		 (ubParity != UART_PARITY_EVEN) &&
		 (ubParity != UART_PARITY_ODD) ) {				// パリティ
		return UART_ERR_PARM ;
	}
	if ( (ubStopBit != UART_STOP_1) &&
		 (ubStopBit != UART_STOP_2) ) {					// STOP bit
		return UART_ERR_PARM ;
	}
	if ( (ubDataLen != UART_BIT_7) &&
		 (ubDataLen != UART_BIT_8) ) {					// データ長指定
		return UART_ERR_PARM ;
	}
	
	// 使用済みチャンネルチェック
	ubUsed = 0 ;										// Used Ch Count
	for ( ubLoop = 0 ; ubLoop < UART_MCU_CHANNELS ; ubLoop++ ) {
		if ( gChTbl[ubLoop].fUse == CH_USE_YES ) {		// CH使用中
			if ( ubLoop == ubCh ) {						// 同一CH
				return UART_CH_USED ;					// チャンネルは使用中
			}
			ubUsed++ ;									// Used Ch Count
		}
	}
	if ( ubUsed >= UART_CH_MAX ) {						// 既にフル使用
		return UART_HANDLE_FULL ;						// 使用ハンドルフル
	}
	ubHnd = ubUsed ;									// ハンドル(Local)
	gChTbl[ubCh].ubHnd = ubUsed ;						// ハンドル(Global)
	gChTbl[ubCh].fUse = CH_USE_YES ;					// 使用Flag
	pstSci = gpstSci[ubCh] ;							// SCI周辺Reg構造体
	
	
	// 割込フラグクリア
	sSci_SetIntReqIR( ubCh, SCI_INTT_ERI, SCI_INT_REQ_NO ) ;		// ERI
	sSci_SetIntReqIR( ubCh, SCI_INTT_RXI, SCI_INT_REQ_NO ) ;		// RXI
	sSci_SetIntReqIR( ubCh, SCI_INTT_TXI, SCI_INT_REQ_NO ) ;		// TXI
	sSci_SetIntReqIR( ubCh, SCI_INTT_TEI, SCI_INT_REQ_NO ) ;		// TEI
	
	// 割込禁止
	nop() ;
	sSci_SetIntEnableIER( ubCh, SCI_INTT_ERI , SCI_INT_DISABLE ) ;	// 割込禁止
	sSci_SetIntEnableIER( ubCh, SCI_INTT_RXI , SCI_INT_DISABLE ) ;	// 割込禁止
	sSci_SetIntEnableIER( ubCh, SCI_INTT_TXI , SCI_INT_DISABLE ) ;	// 割込禁止
	sSci_SetIntEnableIER( ubCh, SCI_INTT_TEI , SCI_INT_DISABLE ) ;	// 割込禁止
	sSci_SetIntLevelIPR( ubCh, 0 ) ;					// 割込レベル(禁止)
	nop() ;
	
	// モジュールを有効化
	if ( ubCh <= UART_CH_7 ) {					// SCI0-7
		pudwMstp = (UDWORD *)&(SYSTEM.MSTPCRB.LONG) ;	// ModuleStopPtr
		ubShift = 31 - ubCh ;							// ModuleStopShift
	} else {									// SCI8-11
		pudwMstp = (UDWORD *)&(SYSTEM.MSTPCRC.LONG) ;	// ModuleStopPtr
		ubShift = 35 - ubCh ;							// ModuleStopShift
	}
	udwMstpMask = (UDWORD)1 << ubShift ;				// ModuleStopMask
	udwMstpMask = ~udwMstpMask ;						// ModuleStopMask
	SYSTEM.PRCR.WORD = 0xa502 ;							// MSTPmnを書込可へ
	*pudwMstp &= udwMstpMask ;							// Module Enable
	SYSTEM.PRCR.WORD = 0xa500 ;							// 書き込み不可へ
	
	// SCI停止
	pstSci->SCR.BYTE = 0 ;
	while ( (pstSci->SCR.BYTE & 0xf0) != 0x00 ) {		// 完了待ち
		nop() ;
	}
	
	// I/Oポート設定
	sSci_SetTxIOP( ubCh ) ;								// TXポート出力設定
	
	// ポートモードを一旦I/Oポートにする
	sSci_SetPortModePMR( ubCh, SCI_PMR_RX, SCI_PMR_IOP ) ;		// RX
	sSci_SetPortModePMR( ubCh, SCI_PMR_TX, SCI_PMR_IOP ) ;		// TX
	
	// 端子機能選択
	sSci_SetPinFunc( ubCh ) ;							// 端子機能選択設定
	
	// ポートモードを有効にする(受信だけ)
	sSci_SetPortModePMR( ubCh, SCI_PMR_RX, SCI_PMR_FUNC ) ;		// RX
	
	// パリティ
	switch ( ubParity ) {
		case UART_PARITY_NONE :					// None Parity
			pstSci->SMR.BIT.PE = UART_PE_NONE ;
			break ;
		case UART_PARITY_EVEN :					// Even Parity
			pstSci->SMR.BIT.PE = UART_PE_ENABLE ;
			pstSci->SMR.BIT.PM = UART_PM_EVEN ;
			break ;
		case UART_PARITY_ODD :					// Odd Parity
			pstSci->SMR.BIT.PE = UART_PE_ENABLE ;
			pstSci->SMR.BIT.PM = UART_PM_ODD ;
			break ;
		default :
			return 1 ;							// err
	}
	
	// ストップビット
	pstSci->SMR.BIT.STOP = ubStopBit ;
	
	// データ長
	pstSci->SMR.BIT.CHR = ubDataLen ;
	nop() ;

	// ボーレート設定
	pstSci->SCR.BIT.CKE = 0 ;							// 内蔵BaudGenerator
	pstSci->SEMR.BIT.ABCS = SCI_ABCS ;					// BaseClockSelect
	pstSci->SEMR.BIT.NFEN = 1 ;							// ノイズ除去有効
//	pstSci->SEMR.BIT.NFEN = 0 ;							// ノイズ除去無効
	switch ( ubSpeed ) {
		case UART_BAUD_300 :					// 300bps
			pstSci->SMR.BIT.CKS = SCI_CKS_300 ;
			pstSci->BRR = SCI_BRR_300 ;
			uwBps = 3 ;									// BPS/100
			break ;
		case UART_BAUD_600 :					// 600bps
			pstSci->SMR.BIT.CKS = SCI_CKS_600 ;
			pstSci->BRR = SCI_BRR_600 ;
			uwBps = 6 ;									// BPS/100
			break ;
		case UART_BAUD_1200 :					// 1200bps
			pstSci->SMR.BIT.CKS = SCI_CKS_1200 ;
			pstSci->BRR = SCI_BRR_1200 ;
			uwBps = 12 ;								// BPS/100
			break ;
		case UART_BAUD_2400 :					// 2400bps
			pstSci->SMR.BIT.CKS = SCI_CKS_2400 ;
			pstSci->BRR = SCI_BRR_2400 ;
			uwBps = 24 ;								// BPS/100
			break ;
		case UART_BAUD_4800 :					// 4800bps
			pstSci->SMR.BIT.CKS = SCI_CKS_4800 ;
			pstSci->BRR = SCI_BRR_4800 ;
			uwBps = 48 ;								// BPS/100
			break ;
		case UART_BAUD_9600 :					// 9600bps
			pstSci->SMR.BIT.CKS = SCI_CKS_9600 ;
			pstSci->BRR = SCI_BRR_9600 ;
			uwBps = 96 ;								// BPS/100
			break ;
		case UART_BAUD_19200 :					// 19200bps
			pstSci->SMR.BIT.CKS = SCI_CKS_19200 ;
			pstSci->BRR = SCI_BRR_19200 ;
			uwBps = 192 ;								// BPS/100
			break ;
		case UART_BAUD_31250 :					// 31250bps
			pstSci->SMR.BIT.CKS = SCI_CKS_31250 ;
			pstSci->BRR = SCI_BRR_31250 ;
			uwBps = 3125 ;								// BPS/100
			break ;
		case UART_BAUD_38400 :					// 38400bps
			pstSci->SMR.BIT.CKS = SCI_CKS_38400 ;
			pstSci->BRR = SCI_BRR_38400 ;
			uwBps = 3840 ;								// BPS/100
			break ;
		case UART_BAUD_57600 :					// 57600bps
			pstSci->SMR.BIT.CKS = SCI_CKS_57600 ;
			pstSci->BRR = SCI_BRR_57600 ;
			uwBps = 5760 ;								// BPS/100
			break ;
		case UART_BAUD_115200 :					// 115200bps
			pstSci->SMR.BIT.CKS = SCI_CKS_115200 ;
			pstSci->BRR = SCI_BRR_115200 ;
			uwBps = 11520 ;								// BPS/100
			break ;
		default :
			return 1 ;							// err
	}
	// 1bit time wait
	for( uwLoop = 0 ; uwLoop <= 10000 ; uwLoop++ ) {
		nop() ;
	}
	
	
	// リングバッファポインタ初期化
	gSciRxb[ubHnd].ubOutPtr = 0 ;
	gSciRxb[ubHnd].ubInPtr = 0 ;
	gSciTxb[ubHnd].ubOutPtr = 0 ;
	gSciTxb[ubHnd].ubInPtr = 0 ;
	
	
	// 他のフラグ初期化
//	gSciF[ubHnd].fTxBusy = SCIF_TX_BUSY_NO ;		// 送信BusyFクリア
	gSciF[ubHnd].fTxCritical = SCIF_TX_CRTICAL_NO ;	// 送信CriticalSectFクリア
	gSciF[ubHnd].ubDatalen = ubDataLen ;			// データ長(エラー時再設定)
	gSciF[ubHnd].fCvtCRLF = UART_CRLF ;				// 送信時CR+LF変換指定
	gpubMemOut = NULL ;								// メモリ出力用Ptr
	xFunc_Out = NULL ;								// 関数ptr
	
	
	// RS485レシーバ操作遅延値設定
#ifdef	RS485_USE
	uwFj = 1 + 7 ;									// TotalBit数(STOP1+LEN7)
	if ( ubParity != UART_PARITY_NONE ) {
		uwFj++ ;									// TotalBit数
	}
	if ( ubStopBit == UART_STOP_2 ) {
		uwFj++ ;									// TotalBit数
	}
	if ( ubDataLen == UART_BIT_8 ) {
		uwFj++ ;									// TotalBit数
	}
	gSciF[ubHnd].bDelay = ((1000 * uwFj)/ uwBps) + 1 ;	// RS485 CtlTimerDelay
  #ifdef	RS485_CH5
	RS485_CH5_RE_PODR = RS485_RE_ENABLE ;			// REを有効へ
	RS485_CH5_DE_PODR = RS485_DE_HIGHZ ;			// DEを停止へ
	RS485_CH5_RE_PDR = PORT_PDR_OUT ;				// REを出力へ
	RS485_CH5_DE_PDR = PORT_PDR_OUT ;				// DEを出力へ
  #endif		// #ifdef	RS485_CHx
  #ifdef	RS485_CH6
	RS485_CH6_RE_PODR = RS485_RE_ENABLE ;			// REを有効へ
	RS485_CH6_DE_PODR = RS485_DE_HIGHZ ;			// DEを停止へ
	RS485_CH6_RE_PDR = PORT_PDR_OUT ;				// REを出力へ
	RS485_CH6_DE_PDR = PORT_PDR_OUT ;				// DEを出力へ
  #endif		// #ifdef	RS485_CHx
#endif		// #ifdef	RS485_USE
	
	
	// 送受信の許可
	// 割込の許可(SCIモジュール)
	// バイトアクセスしないとうまくいかない
	// bit7:TIE, bit6:RIE, bit5:TE, bit4:RE, bit3:MPIE, bit2:TEIE
	// bit1-0:CKE
	pstSci->TDR = 0x00 ;				// 送信データ
//	pstSci->SCR.BIT.TIE = 0 ;			// 送信完了許可		B7
//	pstSci->SCR.BIT.RIE = 1 ;			// 受信割込許可		B6
//	pstSci->SCR.BIT.TE = 0 ;			// 送信許可			B5
//	pstSci->SCR.BIT.RE = 1 ;			// 受信許可			B4
//	pstSci->SCR.BIT.MPIE = 0 ;			// 					B3
//	pstSci->SCR.BIT.TEIE = 0 ;			// 送信完了割込許可	B2
	pstSci->SCR.BYTE = 0x50 ;			// 送受信と割込許可
	
	sSci_SetIntReqIR( ubCh, SCI_INTT_ERI, SCI_INT_REQ_NO ) ;	// ERI
	sSci_SetIntReqIR( ubCh, SCI_INTT_RXI, SCI_INT_REQ_NO ) ;	// RXI
	sSci_SetIntReqIR( ubCh, SCI_INTT_TXI, SCI_INT_REQ_NO ) ;	// TXI
	sSci_SetIntReqIR( ubCh, SCI_INTT_TEI, SCI_INT_REQ_NO ) ;	// TEI
	
	// 割込の許可(割り込みコントローラ) 送信以外
	sSci_SetIntLevelIPR( ubCh, SCI_INT_LVL ) ;			// 割込レベル
	sSci_SetIntEnableIER( ubCh, SCI_INTT_ERI , SCI_INT_ENABLE ) ;	// 割込禁止
	sSci_SetIntEnableIER( ubCh, SCI_INTT_RXI , SCI_INT_ENABLE ) ;	// 割込禁止
	sSci_SetIntEnableIER( ubCh, SCI_INTT_TXI , SCI_INT_DISABLE ) ;	// 割込禁止
	sSci_SetIntEnableIER( ubCh, SCI_INTT_TEI , SCI_INT_DISABLE ) ;	// 割込禁止
	nop() ;

	return UART_SUCCESS ;

}


//-----------------------------------------------------------
//	通信バッファ初期化
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//	戻り値 
//-----------------------------------------------------------
void	Sci_InzBuf( UBYTE ubCh )
{
	UBYTE			ubHnd ;								// ハンドル
	volatile __evenaccess ST_SCI0	*pstSci ;			// 周辺Reg構造体
	
	
	
	// 割込フラグクリア
	sSci_SetIntReqIR( ubCh, SCI_INTT_ERI, SCI_INT_REQ_NO ) ;		// ERI
	sSci_SetIntReqIR( ubCh, SCI_INTT_RXI, SCI_INT_REQ_NO ) ;		// RXI
	sSci_SetIntReqIR( ubCh, SCI_INTT_TXI, SCI_INT_REQ_NO ) ;		// TXI
	sSci_SetIntReqIR( ubCh, SCI_INTT_TEI, SCI_INT_REQ_NO ) ;		// TEI
	
	// 割込禁止
	nop() ;
	sSci_SetIntEnableIER( ubCh, SCI_INTT_ERI , SCI_INT_DISABLE ) ;	// 割込禁止
	sSci_SetIntEnableIER( ubCh, SCI_INTT_RXI , SCI_INT_DISABLE ) ;	// 割込禁止
	sSci_SetIntEnableIER( ubCh, SCI_INTT_TXI , SCI_INT_DISABLE ) ;	// 割込禁止
	sSci_SetIntEnableIER( ubCh, SCI_INTT_TEI , SCI_INT_DISABLE ) ;	// 割込禁止
	sSci_SetIntLevelIPR( ubCh, 0 ) ;					// 割込レベル(禁止)
	nop() ;
	
	// SCI周辺Reg構造体ポインタ取得
	pstSci = gpstSci[ubCh] ;							// SCI周辺Reg構造体
	
	// 送受信の許可
	pstSci->SCR.BYTE = 0x00 ;							// 送受信と割込禁止
	
	// ハンドル取得
	ubHnd = gChTbl[ubCh].ubHnd ;						// ハンドル(Global)
	
	// リングバッファポインタ初期化
	gSciRxb[ubHnd].ubOutPtr = 0 ;
	gSciRxb[ubHnd].ubInPtr = 0 ;
	gSciTxb[ubHnd].ubOutPtr = 0 ;
	gSciTxb[ubHnd].ubInPtr = 0 ;
	
	// 送受信の許可
	pstSci->SCR.BYTE = 0x50 ;			// 送受信と割込許可
	
	// 割込の許可(割り込みコントローラ) 送信以外
	sSci_SetIntEnableIER( ubCh, SCI_INTT_ERI , SCI_INT_ENABLE ) ;	// 割込禁止
	sSci_SetIntEnableIER( ubCh, SCI_INTT_RXI , SCI_INT_ENABLE ) ;	// 割込禁止
	sSci_SetIntEnableIER( ubCh, SCI_INTT_TXI , SCI_INT_DISABLE ) ;	// 割込禁止
	sSci_SetIntEnableIER( ubCh, SCI_INTT_TEI , SCI_INT_DISABLE ) ;	// 割込禁止
	
	
}



//-----------------------------------------------------------
//	putchar to SCI Ring Buffer
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//	戻り値 
//		UART_PUT_SUCCESS  	(0) Success
//		UART_PUT_BUSY		(-1)Busy
//		UART_PUT_OVF		(-2)BufferOverflow
//-----------------------------------------------------------
BYTE	Sci_Putc( UBYTE ubCh, UBYTE ubChar )
{
	UBYTE			ubHnd ;								// ハンドル
	WORD			wCheck ;
	volatile __evenaccess ST_SCI0	*pstSci ;			// 周辺Reg構造体
	
	
	// ハンドル取得
	ubHnd = gChTbl[ubCh].ubHnd ;						// ハンドル(Global)
	
	// クリティカルな処理中なら処理しない(BYSY)
	if ( gSciF[ubHnd].fTxCritical == SCIF_TX_BUSY_YES ) {	// NOW critical sect
		return UART_PUT_BUSY ;
	}
	
	// バッファチェック
	// バッファフルなら充填せずに戻る
	wCheck = (WORD)gSciTxb[ubHnd].ubInPtr - 
			 (WORD)gSciTxb[ubHnd].ubOutPtr ;		// 使用量
	if ( wCheck < 0 ) {								// 送信バッファが
		wCheck = SCI_TX_BUF_SIZE + wCheck ;			// 終端で折り返している
	}
	wCheck = SCI_TX_BUF_SIZE - wCheck ;				// 空き容量
	if ( wCheck <= 1 ) {							// 空きがない?
		return UART_PUT_OVF ;
	}
	
	
	// RS485トランシーバ制御 受信->送信切替
#ifdef	RS485_USE
  #ifdef	RS485_CH5
	if ( ubCh == UART_CH_5 ) {							// SCI5
		RS485_CH5_RE_PODR = RS485_RE_HIGHZ ;			// REを停止へ
		RS485_CH5_DE_PODR = RS485_DE_ENABLE ;			// DEを有効へ
		gSciF[ubHnd].fTxTransCtl = RS485_TCV_SEND ;		// TransceiverCtl
	}
  #endif		// #ifdef	RS485_CHx
  #ifdef	RS485_CH6
	if ( ubCh == UART_CH_6 ) {							// SCI6
		RS485_CH6_RE_PODR = RS485_RE_HIGHZ ;			// REを停止へ
		RS485_CH6_DE_PODR = RS485_DE_ENABLE ;			// DEを有効へ
		gSciF[ubHnd].fTxTransCtl = RS485_TCV_SEND ;		// TransceiverCtl
	}
  #endif		// #ifdef	RS485_CHx
#endif		// #ifdef	RS485_USE
	
	
	// 送信データをバッファへ格納
	gSciTxb[ubHnd].ubBuf[gSciTxb[ubHnd].ubInPtr] = ubChar ;	// 送信データ格納
	gSciTxb[ubHnd].ubInPtr++ ;								// InpPtrを進める
	if ( gSciTxb[ubHnd].ubInPtr == SCI_TX_BUF_SIZE) {		// 終端で折り返し
		gSciTxb[ubHnd].ubInPtr = 0 ;
	}
	
	// SCI周辺Reg構造体ポインタ取得
	pstSci = gpstSci[ubCh] ;							// SCI周辺Reg構造体
	
	// 送信割込設定
	pstSci->SCR.BIT.TIE = 1 ;							// 送信割込許可
	pstSci->SCR.BIT.TE = 1 ;							// 送信許可
	
	// TX端子のポートモード設定
	sSci_SetPortModePMR( ubCh, SCI_PMR_TX, SCI_PMR_FUNC ) ;		// TX
	
	// 送信割込を許可
	sSci_SetIntEnableIER( ubCh, SCI_INTT_TXI , SCI_INT_ENABLE ) ;	// 割込許可
	
	
	return UART_PUT_SUCCESS ;
	
}


//-----------------------------------------------------------
//	送信データの確認
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//	戻り値 = データバイト数
//-----------------------------------------------------------
WORD	Sci_IsTxData( UBYTE ubCh )
{
	UBYTE			ubHnd ;								// ハンドル
	WORD			wCh;
	
	
	// ハンドル取得
	ubHnd = gChTbl[ubCh].ubHnd ;						// ハンドル(Global)
	
	wCh = gSciTxb[ubHnd].ubInPtr - gSciTxb[ubHnd].ubOutPtr ;
	if ( wCh < 0 ) {						// 送信バッファが
		wCh += SCI_TX_BUF_SIZE ;			// 終端で折り返している
	}
	
	
	return wCh;
	
}


//-----------------------------------------------------------
//	UART文字列送信
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		UBYTE *ubStr		文字列Ptr
//	戻り値 
//		SCI_PUT_SUCCESS  	(0) Success
//		SCI_PUT_BUSY		(-1)Busy
//		SCI_PUT_OVF			(-2)BufferOverflow
//-----------------------------------------------------------
BYTE	Sci_Puts( UBYTE ubCh, UBYTE *ubStr )
{
	BYTE			bRet ;
	int				nErr ;
	
	
	while( *ubStr ){
		nErr = 0 ;
		// 1文字送信
		while ( (bRet = Sci_Putc( ubCh, *ubStr )) != UART_PUT_SUCCESS ) {
			if ( ++nErr > SCI_PUT_RETRY_NUM ) {				// Err Limit
				return bRet ;
			}
		}
		ubStr++ ;
	}
	
	
	return bRet ;
	
}


//-----------------------------------------------------------
//	UART文字列送信(テキストモード)
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		UBYTE *ubStr		文字列Ptr
//	戻り値 
//		SCI_PUT_SUCCESS  	(0) Success
//		SCI_PUT_BUSY		(-1)Busy
//		SCI_PUT_OVF			(-2)BufferOverflow
//-----------------------------------------------------------
BYTE	Sci_PutsText( UBYTE ubCh, UBYTE *ubStr )
{
	BYTE	bRet ;
	int		nErr ;
	
	
	while( *ubStr ){
		if( *ubStr == '\n') {
			nErr = 0 ;
			while ((bRet = Sci_Putc( ubCh, 0x0d )) != 
											UART_PUT_SUCCESS ) {	// Not Send
				if ( ++nErr > SCI_PUT_RETRY_NUM ) {					// Err Limit
					return bRet ;
				}
			}
			nErr = 0 ;
			while ((bRet = Sci_Putc( ubCh, 0x0a )) != 
											UART_PUT_SUCCESS ) {	// Not Send
				if ( ++nErr > SCI_PUT_RETRY_NUM ) {					// Err Limit
					return bRet ;
				}
			}
		} else {
			nErr = 0 ;
			while ((bRet = Sci_Putc( ubCh, *ubStr )) != 
											UART_PUT_SUCCESS ) {	// Not Send
				if ( ++nErr > SCI_PUT_RETRY_NUM ) {					// Err Limit
					return bRet ;
				}
			}
		}
		ubStr++ ;
	}
	
	
	return bRet ;
	
}


//---------------------------------------------------
//	ＣＲＬＦの出力
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//	戻り値 
//		SCI_PUT_SUCCESS  	(0) Success
//		SCI_PUT_BUSY		(-1)Busy
//		SCI_PUT_OVF			(-2)BufferOverflow
//---------------------------------------------------
BYTE	Sci_PutCRLF( UBYTE ubCh )
{
	return	Sci_Puts( ubCh, "\r\n" ) ;					// send data
}


//-----------------------------------------------------------
//	SCIポートから１キャラクタ受信
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//	戻り値 
//		受信データがなければ -1を返す
//		受信エラー制御機能がONの場合、
//			エラー発生は gUartErrF を参照し、エラー処理後クリアしておく
//			その間受信データは読み捨てられる
//			受信バッファオーバーフロー監視される
//		受信エラー制御機能がOFFの場合、
//			受信バッファオーバーフロー監視されない
//			古いデータは捨てられる
//-----------------------------------------------------------
WORD	Sci_Getc( UBYTE ubCh )
{
	UBYTE			ubHnd ;								// ハンドル
	WORD			wRet ;
	
	
	// ハンドル取得
	ubHnd = gChTbl[ubCh].ubHnd ;						// ハンドル(Global)
	
	// 受信データ取り出し
	if ( gSciRxb[ubHnd].ubOutPtr != gSciRxb[ubHnd].ubInPtr ) {	// 受信データ有
		wRet = gSciRxb[ubHnd].ubBuf[gSciRxb[ubHnd].ubOutPtr++];	// 1文字取り出し
		if (gSciRxb[ubHnd].ubOutPtr == SCI_RX_BUF_SIZE ) {		// 終端折り返し
			gSciRxb[ubHnd].ubOutPtr = 0 ;
		}
	} else {										// 受信データ無し
		wRet = -1 ;
	}
	
	
	return wRet ;
	
}


//-----------------------------------------------------------
//	受信データの確認
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//	戻り値 = データバイト数
//-----------------------------------------------------------
WORD	Sci_IsRcvData( UBYTE ubCh )
{
	UBYTE			ubHnd ;								// ハンドル
	WORD			wCh;
	
	
	// ハンドル取得
	ubHnd = gChTbl[ubCh].ubHnd ;						// ハンドル(Global)
	
	// バッファ残取得
	wCh = gSciRxb[ubHnd].ubInPtr - gSciRxb[ubHnd].ubOutPtr ;
	if ( wCh < 0 ) {						// 受信バッファが
		wCh += SCI_RX_BUF_SIZE ;			// 終端で折り返している
	}
	
	
	return wCh;
	
}


//-----------------------------------------------------------------------
//	以下　割り込みルーチン
//-----------------------------------------------------------------------
#pragma section IntPRG					// 割込処理はこのセクションへ固める

//-----------------------------------------------------------
//	SCI受信エラー割り込み処理 ERI
//	IRはレベル検出
//-----------------------------------------------------------
static	void	iSCIn_ERI( UBYTE ubCh )
{
	volatile __evenaccess ST_SCI0	*pstSci ;			// 周辺Reg構造体
	UBYTE			ubRcv ;
	
	
	// SCI周辺Reg構造体ポインタ取得
	pstSci = gpstSci[ubCh] ;							// SCI周辺Reg構造体
	
	// ダミーリード
	ubRcv = pstSci->RDR ;								// rcv data
	if ( ubRcv ) {
		nop( ) ;
	}
	
	// 一旦受信禁止
	pstSci->SCR.BIT.RE = 0 ;							// 受信禁止
	pstSci->SCR.BIT.RIE = 0 ;							// 受信割込禁止
	
    // エラーフラグをクリア
    pstSci->SSR.BYTE = (pstSci->SSR.BYTE & ~SCI_ERRF_MASK) | 0xC0;
    while ((pstSci->SSR.BYTE & SCI_ERRF_MASK) != 0x00) {
		nop( ) ;
	}
	
	// 割込要求フラグ(IR)を念の為にクリア
	sSci_SetIntReqIR( ubCh, SCI_INTT_ERI, SCI_INT_REQ_NO ) ;	// ERI
	sSci_SetIntReqIR( ubCh, SCI_INTT_RXI, SCI_INT_REQ_NO ) ;	// RXI
	
	// 受信再開始
	pstSci->SCR.BIT.RIE = 1 ;							// 受信割込禁止
	pstSci->SCR.BIT.RE = 1 ;							// 受信許可
	
	
}


//-----------------------------------------------------------
//	SCI受信割り込み処理 RXI
//	IRはエッジ検出
//	多重割込有り
//-----------------------------------------------------------
static	void	iSCIn_RXI( UBYTE ubCh )
{
	UBYTE			ubHnd ;								// ハンドル
	volatile __evenaccess ST_SCI0	*pstSci ;			// 周辺Reg構造体
	WORD			wCheck ;							// バッファ空きチェック
	UBYTE			ubRcv ;								// 受信データ
//	UBYTE		ubDmy ;
	
	
	// ハンドル取得
	ubHnd = gChTbl[ubCh].ubHnd ;						// ハンドル(Global)
	
	// バッファ使用バイト数計算
	if ( gSciRxb[ubHnd].ubInPtr >= gSciRxb[ubHnd].ubOutPtr ) {
		wCheck = gSciRxb[ubHnd].ubInPtr - gSciRxb[ubHnd].ubOutPtr ;
	} else {
		wCheck = (SCI_RX_BUF_SIZE + gSciRxb[ubHnd].ubInPtr) - 
										gSciRxb[ubHnd].ubOutPtr ;
	}
	wCheck = SCI_RX_BUF_SIZE - wCheck ;			// 空き容量
	
	// SCI周辺Reg構造体ポインタ取得
	pstSci = gpstSci[ubCh] ;							// SCI周辺Reg構造体
	
	
	// 受信データ取り出し
	ubRcv = pstSci->RDR ;								// rcv data
	
	// バッファへ格納
	gSciRxb[ubHnd].ubBuf[gSciRxb[ubHnd].ubInPtr++] = ubRcv ;	// バッファへ
	if ( gSciRxb[ubHnd].ubInPtr == SCI_RX_BUF_SIZE ) {	// 折り返し
		gSciRxb[ubHnd].ubInPtr = 0 ;
	}
	if ( wCheck < 1 ) {								// バッファオーバーフロー?
		gSciRxb[ubHnd].ubOutPtr++ ;					// 1文字捨て
		if ( gSciRxb[ubHnd].ubOutPtr == SCI_RX_BUF_SIZE ) {	// 終端で折り返し
			gSciRxb[ubHnd].ubOutPtr = 0 ;
		}
	}
	
	
}


//-----------------------------------------------------------
//	SCI 送信割り込み処理 TXI
//	IRはエッジ検出
//-----------------------------------------------------------
static	void	iSCIn_TXI( UBYTE ubCh )
{
	UBYTE			ubHnd ;								// ハンドル
	volatile __evenaccess ST_SCI0	*pstSci ;			// 周辺Reg構造体
	volatile SCI_F					*pSciF ;			// SCIフラグ構造体
	volatile SCI_TXB				*pSciTxb ;			// 送信バッファ
	
	
	// ハンドル取得
	ubHnd = gChTbl[ubCh].ubHnd ;						// ハンドル(Global)
	
	// Flag Ptrセット
	pSciF = &gSciF[ubHnd] ;								// SCIフラグ構造体
	
	// クリティカルセクション処理チェック
	if ( pSciF->fTxCritical == 
							SCIF_TX_CRTICAL_YES ) {		// NOW critical section
		nop( ) ;
		return ;
	}
	pSciF->fTxCritical = SCIF_TX_CRTICAL_YES ;			// critical sect. start
	
	// SCI周辺Reg構造体ポインタ取得
	pstSci = gpstSci[ubCh] ;							// SCI周辺Reg構造体
	
	// Buffer Ptrセット
	pSciTxb = &gSciTxb[ubHnd] ;							// 送信バッファ
	
	
	// 1byte送信
	pstSci->TDR = pSciTxb->ubBuf[pSciTxb->ubOutPtr] ;	// 送信データ
	pSciTxb->ubOutPtr++ ;								// OutPtrを次へ
	if ( pSciTxb->ubOutPtr == SCI_TX_BUF_SIZE ) {		// 終端で折り返し
		pSciTxb->ubOutPtr = 0 ;							// OutPtrを先頭へ
	}
	pSciF->fTxCritical = SCIF_TX_CRTICAL_NO ;			// critical section end
	
	// バッファ転送終了手続き
	if ( pSciTxb->ubInPtr == pSciTxb->ubOutPtr ) {
		// TXI割込終了(停止)
		sSci_SetIntEnableIER( ubCh, SCI_INTT_TXI , SCI_INT_DISABLE ) ;//割込禁止
		pstSci->SCR.BIT.TIE = 0 ;						// 送信割込禁止
		while ( pstSci->SCR.BIT.TIE != 0 ) {			// 完了待ち
			nop( ) ;
		}
		// TXI割込要求フラグをクリア
		sSci_SetIntReqIR( ubCh, SCI_INTT_TXI, SCI_INT_REQ_NO ) ;	// TXI
		// TEI(送信完了割込)発生手続き
		pstSci->SCR.BIT.TEIE = 1 ;						// 送信完了割込許可
		sSci_SetIntEnableIER( ubCh, SCI_INTT_TEI , SCI_INT_ENABLE ) ;// 割込許可
	}
	
	
	
}


//-----------------------------------------------------------
//	SCI送信バッファエンプティ割り込み処理 TEI
//	IRはレベル検出
//	多重割込有り
//-----------------------------------------------------------
static	void	iSCIn_TEI( UBYTE ubCh )
{
	volatile __evenaccess ST_SCI0	*pstSci ;			// 周辺Reg構造体
#ifdef	RS485_USE
	UBYTE			ubHnd ;								// ハンドル
	volatile SCI_F					*pSciF ;			// SCIフラグ構造体
#endif		// #ifdef	RS485_USE
	
	
	// SCI周辺Reg構造体ポインタ取得
	pstSci = gpstSci[ubCh] ;							// SCI周辺Reg構造体
	
	// ポートモードをI/Oポートにする
	sSci_SetPortModePMR( ubCh, SCI_PMR_TX, SCI_PMR_IOP ) ;		// TX
	
	// 送信モード停止
	// 送信割込設定
	pstSci->SCR.BIT.TE = 0 ;							// 送信禁止
	pstSci->SCR.BIT.TIE = 0 ;							// 送信割込禁止
	
	// TEI(送信完了割込)発生禁止手続き
	sSci_SetIntEnableIER( ubCh, SCI_INTT_TEI , SCI_INT_DISABLE ) ;// 割込禁止
	pstSci->SCR.BIT.TEIE = 0 ;							// 送信完了割込禁止
	while ( pstSci->SCR.BIT.TEIE != 0 ) {				// 完了待ち
		nop( ) ;
	}
	
	// RS485トランシーバ制御 送信->受信切替待機
#ifdef	RS485_USE
	// ハンドル取得
	ubHnd = gChTbl[ubCh].ubHnd ;						// ハンドル(Global)
	// Flag Ptrセット
	pSciF = &gSciF[ubHnd] ;								// SCIフラグ構造体
  #ifdef	RS485_CH5
	if ( ubCh == UART_CH_5 ) {							// SCI5
		pSciF->fTxTransCtl = RS485_TCV_RCV_WAIT ;		// TransceiverCtl
//		pSciF->bTimer = 0 ;								// RS485 CtlTimer
		RS485_CH5_RE_PODR = RS485_RE_ENABLE ;			// REを有効へ
		RS485_CH5_DE_PODR = RS485_DE_HIGHZ ;			// DEを停止へ
	}
  #endif		// #ifdef	RS485_CHx
  #ifdef	RS485_CH6
	if ( ubCh == UART_CH_6 ) {							// SCI6
		pSciF->fTxTransCtl = RS485_TCV_RCV_WAIT ;		// TransceiverCtl
//		pSciF->bTimer = 0 ;								// RS485 CtlTimer
		RS485_CH6_RE_PODR = RS485_RE_ENABLE ;			// REを有効へ
		RS485_CH6_DE_PODR = RS485_DE_HIGHZ ;			// DEを停止へ
	}
  #endif		// #ifdef	RS485_CHx
#endif		// #ifdef	RS485_USE
	
	
}


//=============  CH1 =====================
//-----------------------------------------------------------
//	SCI1 受信エラー割り込み処理 ERI
//	IRはレベル検出
//-----------------------------------------------------------
#pragma interrupt (Int_SCI1_ERI(vect=VECT_SCI( UART_CH_1, ERI )))
void	Int_SCI1_ERI( void )
{
	
	iSCIn_ERI( UART_CH_1 ) ;							// ERI処理
	
}


//-----------------------------------------------------------
//	SCI1 受信割り込み処理 RXI
//	IRはエッジ検出
//	多重割込有り
//-----------------------------------------------------------
#pragma interrupt (Int_SCI1_RXI(vect=VECT_SCI( UART_CH_1, RXI )))
void	Int_SCI1_RXI( void )
{
	
	iSCIn_RXI( UART_CH_1 ) ;							// RXI処理
	
}


//-----------------------------------------------------------
//	SCI1 送信割り込み処理 TXI
//	IRはエッジ検出
//-----------------------------------------------------------
#pragma interrupt (Int_SCI1_TXI(vect=VECT_SCI( UART_CH_1, TXI )))
void	Int_SCI1_TXI( void )
{
	
	iSCIn_TXI( UART_CH_1 ) ;							// TXI処理
	
}


//-----------------------------------------------------------
//	SCI1 送信バッファエンプティ割り込み処理 TEI
//	IRはレベル検出
//	多重割込有り
//-----------------------------------------------------------
#pragma interrupt (Int_SCI1_TEI(vect=VECT_SCI( UART_CH_1, TEI )), enable)
void	Int_SCI1_TEI( void )
{
	
	iSCIn_TEI( UART_CH_1 ) ;							// TEI処理
	
}


//=============  CH5 =====================
//-----------------------------------------------------------
//	SCI5 受信エラー割り込み処理 ERI
//	IRはレベル検出
//-----------------------------------------------------------
#pragma interrupt (Int_SCI5_ERI(vect=VECT_SCI( UART_CH_5, ERI )))
void	Int_SCI5_ERI( void )
{
	
	iSCIn_ERI( UART_CH_5 ) ;							// ERI処理
	
}


//-----------------------------------------------------------
//	SCI5 受信割り込み処理 RXI
//	IRはエッジ検出
//	多重割込有り
//-----------------------------------------------------------
#pragma interrupt (Int_SCI5_RXI(vect=VECT_SCI( UART_CH_5, RXI )))
void	Int_SCI5_RXI( void )
{
	
	iSCIn_RXI( UART_CH_5 ) ;							// RXI処理
	
}


//-----------------------------------------------------------
//	SCI5 送信割り込み処理 TXI
//	IRはエッジ検出
//-----------------------------------------------------------
#pragma interrupt (Int_SCI5_TXI(vect=VECT_SCI( UART_CH_5, TXI )))
void	Int_SCI5_TXI( void )
{
	
	iSCIn_TXI( UART_CH_5 ) ;							// TXI処理
	
}


//-----------------------------------------------------------
//	SCI5 送信バッファエンプティ割り込み処理 TEI
//	IRはレベル検出
//	多重割込有り
//-----------------------------------------------------------
//#pragma interrupt (Int_SCI5_TEI(vect=VECT_SCI( UART_CH_5, TEI )), enable)
//#pragma interrupt (Int_SCI5_TEI(vect=VECT_SCI( UART_CH_5, TEI )))
#pragma interrupt (Int_SCI5_TEI(vect=225))
void	Int_SCI5_TEI( void )
{
	
	iSCIn_TEI( UART_CH_5 ) ;							// TEI処理
	
}


//=============  CH6 =====================
//-----------------------------------------------------------
//	SCI6 受信エラー割り込み処理 ERI
//	IRはレベル検出
//-----------------------------------------------------------
#pragma interrupt (Int_SCI6_ERI(vect=VECT_SCI( UART_CH_6, ERI )))
void	Int_SCI6_ERI( void )
{
	
	iSCIn_ERI( UART_CH_6 ) ;							// ERI処理
	
}


//-----------------------------------------------------------
//	SCI6 受信割り込み処理 RXI
//	IRはエッジ検出
//	多重割込有り
//-----------------------------------------------------------
#pragma interrupt (Int_SCI6_RXI(vect=VECT_SCI( UART_CH_6, RXI )))
void	Int_SCI6_RXI( void )
{
	
	iSCIn_RXI( UART_CH_6 ) ;							// RXI処理
	
}


//-----------------------------------------------------------
//	SCI6 送信割り込み処理 TXI
//	IRはエッジ検出
//-----------------------------------------------------------
#pragma interrupt (Int_SCI6_TXI(vect=VECT_SCI( UART_CH_6, TXI )))
void	Int_SCI6_TXI( void )
{
	
	iSCIn_TXI( UART_CH_6 ) ;							// TXI処理
	
}


//-----------------------------------------------------------
//	SCI6 送信バッファエンプティ割り込み処理 TEI
//	IRはレベル検出
//	多重割込有り
//-----------------------------------------------------------
//#pragma interrupt (Int_SCI6_TEI(vect=VECT_SCI( UART_CH_6, TEI )), enable)
//#pragma interrupt (Int_SCI6_TEI(vect=VECT_SCI( UART_CH_6, TEI )))
#pragma interrupt (Int_SCI6_TEI(vect=229))
void	Int_SCI6_TEI( void )
{
	
	iSCIn_TEI( UART_CH_6 ) ;							// TEI処理
	
}

//---------------------------------------------------------------------
