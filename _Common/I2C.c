/******************************************************************************
* DISCLAIMER

* This software is supplied by Renesas Electronics Corp. and is only 
* intended for use with Renesas products. No other uses are authorized.

* This software is owned by Renesas Electronics Corp. and is protected under 
* all applicable laws, including copyright laws.

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED.

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORP. NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software.  
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010. Renesas Electronics Corporation. All rights reserved.
*******************************************************************************	
* File Name    : iic.c
* Version      : 1.00
* Description  : RIIC single master program
******************************************************************************
* History
* Aug 01 '11  H.Inoue	RX62Nボード用にポーティング
*         : 16.07.2010 1.00    First Release
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include	<machine.h>

#define		_I2C_DRIVER_
#include	"I2C.h"


//--------------------------------------------------------------------------
//	変数型定義
//--------------------------------------------------------------------------



//--------------------------------------------------------------------------
//	マクロ定義
//--------------------------------------------------------------------------
#define	RIIC_CH0					// When use channel0, define 'RIIC_CH0'
//#define	RIIC_CH1				// When use channel1, define 'RIIC_CH1'

// Users don't have to change the below defines.
#ifdef	RIIC_CH0
	#define RIIC			RIIC0				// I2C使用チャンネル
	#define ICEEI			ICEEI0				// 通信エラー割込
	#define ICRXI			ICRXI0				// 受信データフル割込
	#define ICTXI			ICTXI0				// 送信データエンプティ割込
	#define ICTEI			ICTEI0				// 送信終了割込
	#define	RIIC_SDA_PDR	PORT1.PDR.BIT.B3	// SDAn PDR port dir
	#define	RIIC_SDA_PODR	PORT1.PODR.BIT.B3	// SDAn port out data
	#define	RIIC_SDA_PIDR	PORT1.PIDR.BIT.B3	// SDAn port inp data
	#define	RIIC_SDA_PMR	PORT1.PMR.BIT.B3	// SDAn port mode reg
	#define	RIIC_SCL_PDR	PORT1.PDR.BIT.B2	// SCLn PDR port dir
	#define	RIIC_SCL_PODR	PORT1.PODR.BIT.B2	// SCLn port out data
	#define	RIIC_SCL_PIDR	PORT1.PIDR.BIT.B2	// SCLn port inp data
	#define	RIIC_SCL_PMR	PORT1.PMR.BIT.B2	// SCLn port mode reg
#else
	#define RIIC			RIIC1				// I2C使用チャンネル
	#define ICEEI			ICEEI1				// 通信エラー割込
	#define ICRXI			ICRXI1				// 受信データフル割込
	#define ICTXI			ICTXI1				// 送信データエンプティ割込
	#define ICTEI			ICTEI1				// 送信終了割込
	#define	RIIC_SDA_PDR	PORT2.PDR.BIT.B0	// SDAn PDR port dir
	#define	RIIC_SDA_PODR	PORT2.PODR.BIT.B0	// SDAn port out data
	#define	RIIC_SDA_PIDR	PORT2.PIDR.BIT.B0	// SDAn port inp data
	#define	RIIC_SDA_PMR	PORT2.PMR.BIT.B0	// SDAn port mode reg
	#define	RIIC_SCL_PDR	PORT2.DR.BIT.B1		// SCLn PDR port dir
	#define	RIIC_SCL_PODR	PORT2.PODR.BIT.B1	// SCLn port out data
	#define	RIIC_SCL_PIDR	PORT2.PIDR.BIT.B1	// SCLn port inp data
	#define	RIIC_SCL_PMR	PORT2.PMR.BIT.B1	// SCLn port mode reg
#endif

//#define	BUS_SPEED			_100K				// 100kbps
#define	BUS_SPEED			_400K				// 400kbps
#if		BUS_SPEED == _100K						// 100kbps
	#define	RIIC_SPEED_CKS		3					// IIC phi = PCLK/8 clock
//	#define	RIIC_SPEED_BRH		24					// 100kbps (PCLK=48MHz)
//	#define	RIIC_SPEED_BRL		26					// 100kbps (PCLK=48MHz)
	#define	RIIC_SPEED_BRH		24					// 100kbps (PCLK=48MHz)
	#define	RIIC_SPEED_BRL		26					// 100kbps (PCLK=48MHz)
#elif	BUS_SPEED == _400K						// 400kbps
	#define	RIIC_SPEED_CKS		2					// IIC phi = PCLK/4 clock
	#define	RIIC_SPEED_BRH		7					// 400kbps (PCLK=48MHz)
	#define	RIIC_SPEED_BRL		15					// 400kbps (PCLK=48MHz)
#else											// Error
	#error	I2C(RIIC) Bus Speed define error.
#endif


// 割込レベル
#define		I2C_INT_LEVEL			4			// I2C割込レベル

#define	DELAY_COUNT_25U		300					// 25uS待ち


//--------------------------------------------------------------------------
//	グローバル変数,関数プロトタイプ宣言
//--------------------------------------------------------------------------
//------------------------------
// グローバル変数
//------------------------------
static I2C_TFR_INF				*gpI2cTfrInf ;	// 転送情報
static I2C_DRV_INF				gI2cDrvInf ;	// I2Cドライバ内部情報

//------------------------------
// 関数プロトタイプ
//------------------------------
static void IIC_EEI_Int_TimeOut( void ) ;
static void IIC_EEI_Int_AL( void ) ;
static void IIC_EEI_Int_STOP( void ) ;
static void IIC_EEI_Int_START( void ) ;
static void IIC_EEI_Int_Nack( void ) ;
static void IIC_RXI_Int_TfrRead ( void ) ;
static void IIC_TXI_Int_TfrWrite( void ) ;
static void IIC_TXI_Int_TfrRead( void ) ;
static void IIC_TEI_Int_TfrWrite( void ) ;
static void IIC_TEI_Int_TfrRead( void ) ;
static void IIC_BusInitialize( void ) ;
static void IIC_Error( I2C_ERR_CODE ) ;




//-----------------------------------------------------------
//	RIICモジュールとI/Oポート初期化
//	引数
//	戻り値
//-----------------------------------------------------------
void	I2C_InzModule( void )
{
	BYTE			bTry ;								// Bus回復リトライ数
	UWORD			uwWait ;							// 待ちCounter
	
	
	
	// ** I2C バスチェック
	RIIC_SDA_DDR = PORTDIR_IN ;							// SDAn
	RIIC_SCL_DDR = PORTDIR_IN ;							// SCLn
	RIIC_SDA_ICR = INP_BUF_ON ;							// SDAn
	RIIC_SCL_ICR = INP_BUF_ON ;							// SCLn
	if ( (RIIC_SCL_PORT == PORT_IN_HIGH) && (RIIC_SDA_PORT == PORT_IN_LOW) ) {
		RIIC_SCL_ODR = PORT_PCR_ON ;					// SCLn
		RIIC_SCL_DDR = PORTDIR_OUT ;					// SCLn
		for ( bTry = 21 ; bTry-- ; ) {
			RIIC_SCL_DR ^= EXTOUT_HIGH ;				// SCLn 反転
			uwWait = DELAY_COUNT_25U ;					// 待ちCounter 25uS
			while ( uwWait-- ) {
				nop( ) ;
			}
		}
		
	}
	
	
	
	
	// ** I2C割り込み設定
#ifdef RIIC_CH0
	// 割込レベル(macro)
	IPR( RIIC0, ICEEI0 ) = I2C_INT_LEVEL ;		// RIIC0 EEI0
	IPR( RIIC0, ICRXI0 ) = I2C_INT_LEVEL ;		// RIIC0 RXI0
	IPR( RIIC0, ICTXI0 ) = I2C_INT_LEVEL ;		// RIIC0 TXI0
	IPR( RIIC0, ICTEI0 ) = I2C_INT_LEVEL ;		// RIIC0 TEI0
	// 安全の為に割込フラグをクリアする(macro)
	IR( RIIC0, ICEEI0 ) = 0 ;					// RIIC0 EEI0
	IR( RIIC0, ICRXI0 ) = 0 ;					// RIIC0 RXI0
	IR( RIIC0, ICTXI0 ) = 0 ;					// RIIC0 TXI0
	IR( RIIC0, ICTEI0 ) = 0 ;					// RIIC0 TEI0
	// 割込許可(macro)
	IEN( RIIC0, ICEEI0 ) = 1 ;					// RIIC0 EEI0
	IEN( RIIC0, ICRXI0 ) = 1 ;					// RIIC0 RXI0
	IEN( RIIC0, ICTXI0 ) = 1 ;					// RIIC0 TXI0
	IEN( RIIC0, ICTEI0 ) = 1 ;					// RIIC0 TEI0
#else		// #ifdef RIIC_CH0
	// 割込レベル(macro)
	IPR( RIIC1, ICEEI1 ) = I2C_INT_LEVEL ;		// RIIC1 EEI1
	IPR( RIIC1, ICRXI1 ) = I2C_INT_LEVEL ;		// RIIC1 RXI1
	IPR( RIIC1, ICTXI1 ) = I2C_INT_LEVEL ;		// RIIC1 TXI1
	IPR( RIIC1, ICTEI1 ) = I2C_INT_LEVEL ;		// RIIC1 TEI1
	// 安全の為に割込フラグをクリアする(macro)
	IR( RIIC1, ICEEI1 ) = 0 ;					// RIIC1 EEI1
	IR( RIIC1, ICRXI1 ) = 0 ;					// RIIC1 RXI1
	IR( RIIC1, ICTXI1 ) = 0 ;					// RIIC1 TXI1
	IR( RIIC1, ICTEI1 ) = 0 ;					// RIIC1 TEI1
	// 割込許可(macro)
	IEN( RIIC1, ICEEI1 ) = 1 ;					// RIIC1 EEI1
	IEN( RIIC1, ICRXI1 ) = 1 ;					// RIIC1 RXI1
	IEN( RIIC1, ICTXI1 ) = 1 ;					// RIIC1 TXI1
	IEN( RIIC1, ICTEI1 ) = 1 ;					// RIIC1 TEI1
#endif		// #ifdef RIIC_CH0
	
	
	// ** I2C I/Oポート設定
	RIIC_SDA_DDR = PORTDIR_IN ;					// SDAn
	RIIC_SCL_DDR = PORTDIR_IN ;					// SCLn
	RIIC_SDA_ICR = INP_BUF_ON ;					// SDAn
	RIIC_SCL_ICR = INP_BUF_ON ;					// SCLn
	
	// ** I2C モジュール起動
#ifdef RIIC_CH0
	MSTP( RIIC0 ) = 0 ;						// Wakeup RIIC0 (macro)
#else		// #ifdef RIIC_CH0
	MSTP( RIIC1 ) = 0 ;						// Wakeup RIIC1 (macro)
#endif		// #ifdef RIIC_CH0
	
	// RIICリセット (レジスタもリセットされる)
	RIIC.ICCR1.BIT.ICE = 0 ;					// I2C-Bus 停止
	RIIC.ICCR1.BIT.IICRST = 1 ;					// 内部リセット
	RIIC.ICCR1.BIT.IICRST = 0 ;					// 内部リセット解除
	
}


//=============================================================================
//	I2Cバス初期化
//	引数
//	戻り値
//=============================================================================
void	I2C_Create( void )
{
	
	
	// RIICリセット (レジスタもリセットされる)
	RIIC.ICCR1.BIT.ICE = 0 ;					// I2C-Bus 停止
	RIIC.ICCR1.BIT.IICRST = 1 ;					// 内部リセット
	RIIC.ICCR1.BIT.IICRST = 0 ;					// 内部リセット解除
	
	// Set baud rate
	RIIC.ICMR1.BIT.CKS = RIIC_SPEED_CKS ;		// IIC phi = PCLK/8 clock
	RIIC.ICBRH.BIT.BRH = RIIC_SPEED_BRH ;		// High Width
	RIIC.ICBRL.BIT.BRL = RIIC_SPEED_BRL ;		// Low Width
	
	// SDA 出力遅延 波形チェックして調整
	RIIC.ICMR2.BIT.DLCS = 1 ;			// Dlay count source is I2Cφ/2
	RIIC.ICMR2.BIT.SDDL = 0x04 ;		// Dlay count is 7-8
	

	// タイムアウト検出設定
	RIIC.ICFER.BIT.TMOE = 1 ;			// タイムアウト検出無効
	RIIC.ICMR2.BIT.TMOL = 0 ;			// Disable SCL=Low time out
	RIIC.ICMR2.BIT.TMOH = 1 ;			// Enable SCL=High time out
	RIIC.ICMR2.BIT.TMOS = 0 ;			// Long mode (16bits counter).
										// PCLK=48MHz, IIC phi=48MHz,
										// Long mode is about 1.365 ms
	RIIC.ICFER.BIT.TMOE = 1 ;			// タイムアウト検出有効
	
	
	// ノイズフィルタ設定
	RIIC.ICMR3.BIT.NF = 0 ;				// 0:1段, 1:2段, 2:3段, 3:4段
	
	// Disable all address detection. 
	// This sample code is for only master mode
	RIIC.ICSER.BYTE = 0x00 ;

	// ACKWP is protect bit for ACKBT. NAK出力の為に必要
	RIIC.ICMR3.BIT.ACKWP = 1 ;			// Disable protect for ACKBT

	// Enable/Disable each interrupt
//	RIIC.ICIER.BYTE = 0xBB ;		
		// b0:TMOIE Enable  Time Out interrupt (TMOI)
		// b1:ALIE  Enable  Arbitration Lost interrupt (ALI)
		// b2:STIE  Disable Start Condition Detection Interrupt (STI)
		// b3:SPIE  Enable  Stop condition detection interrupt (SPI)
		// b4:NAKIE Enable  NACK reception interrupt (NKAI)
		// b5:RIE   Enable  Receive Data Full Interrupt (ICRXI)
		// b6:TEIE  Disable Transmit End Interrupt (ICTEI)
		// b7:TIE   Enable  Transmit Data Empty Interrupt (ICTXI)
	RIIC.ICIER.BIT.TMOIE = 1 ;				// Level int. Timeout
	RIIC.ICIER.BIT.ALIE = 1 ;				// Level int. ArbitolationLost
	RIIC.ICIER.BIT.STIE = 0 ;				// Level int. StartCondition
	RIIC.ICIER.BIT.SPIE = 1 ;				// Level int. StopCondition
	RIIC.ICIER.BIT.NAKIE = 1 ;				// Level int. Rx NACK
	RIIC.ICIER.BIT.RIE = 1 ;				// Edge int. RxFull
	RIIC.ICIER.BIT.TEIE = 0 ;				// Level int. TxEnd
	RIIC.ICIER.BIT.TIE = 1 ;				// Edge int. TxDataEmpty
	
	// Initialize ram for RIIC
	gI2cDrvInf.Mode = I2C_MODE_IDLE ;	// Internal IIC mode
	gI2cDrvInf.Stat = I2C_STAT_IDLE ;	// I2C_GetStatus() Stat buffer
	gI2cDrvInf.udwTxBytes = 0 ;			// Clear the Tx 転送カウンタ
	gI2cDrvInf.udwRxBytes = 0 ;			// Clear the Rx 転送カウンタ

	// Enable RIIC
	RIIC.ICCR1.BIT.ICE = 1 ;			// Enable RIIC bus interface
	
}


//=============================================================================
//	I2Cバス破棄
//	引数
//	戻り値
//=============================================================================
void	I2C_Destroy( void )
{
	
	// RIICリセット (レジスタもリセットされる)
	RIIC.ICCR1.BIT.ICE = 0 ;				// RIIC disable
	RIIC.ICCR1.BIT.IICRST = 1 ;				// RIIC all reset
	RIIC.ICCR1.BIT.IICRST = 0 ;				// RIIC return reset
	
}


//=============================================================================
//	Function Name:	I2C_TfrWrite
//	Description  :	Start device write process or Acknowledge polling.
//					I2cWrtData.ubRegBufSize == I2cWrtData.ubRegBufPtr &
//					I2cWrtData.ubDataBufSize == I2cWrtData.ubDataPtr
//					になったらACKポーリングする
//	Argument     :	*pI2cWrtData
//						ubSlvAdr				Slave Device Address.
//												bit0はR/Wビットなので設定禁止
//						ubRegBufSize			実Reg.バッファサイズ
//						ubRegBuf[2]				Reg.バッファ
//						ubRegBufPtr				Reg.バッファPTR
//						ubDataBufSize 			実データバッファサイズ
//						ubDataBuf[]				データバッファ
//						ubDataPtr				NextデータバッファPTR
//	Return Value :
//					I2C_API_OK				Success
//					I2C_API_MODE_ERROR		Running I2C on other mode
//					I2C_API_PERM_ERROR		Parameter error
//					I2C_API_BUS_BUSY		I2C Bus is busy
//=============================================================================
I2C_TFR_FNC		I2C_TfrWrite( I2C_TFR_INF *pI2cWrtData )
{
	
	
	// Check the IIC mode (controled by software)
	if ( gI2cDrvInf.Mode != I2C_MODE_IDLE ) {
		return I2C_API_MODE_ERROR ;				// Running I2C on other mode
	}
	
	// Store the parameter data to ram for IIC
	gpI2cTfrInf = pI2cWrtData ;
	
	// Check parameter
	if ( (gpI2cTfrInf->ubDataBufSize == 0) || 
		 (gpI2cTfrInf->ubRegBufSize == 0) ) {
		return I2C_API_PERM_ERROR ;				// Parameter error
	}
	
	// Check IIC bus busy
	if ( RIIC.ICCR2.BIT.BBSY != 0 ) {
		return I2C_API_BUS_BUSY ;				// If I2C bus is not free
	}
	
	// Set each RAM
	gI2cDrvInf.Mode = I2C_MODE_TFR_WRITE ;		// Change the internal mode
	gI2cDrvInf.Stat = I2C_STAT_ON_COMM ;		// Change the status
	gI2cDrvInf.udwTxBytes = 0 ;					// Clear the Tx 転送カウンタ
	
	// Generate start condition
	RIIC.ICCR2.BIT.ST = 1 ;
	
	
	return I2C_API_OK ;
}


//=============================================================================
//	Function Name:	I2C_TfrRead
//	Description  :	デバイスのレジスタ読取を開始する
//	Argument     :	*pI2cWrtData
//						ubSlvAdr				Slave Device Address.
//												bit0はR/Wビットなので設定禁止
//						ubRegBufSize			実Reg.バッファサイズ
//						ubRegBuf[]				Reg.バッファ
//						ubRegBufPtr				Reg.バッファPTR
//						ubDataBufSize 			実データバッファサイズ
//						ubDataBuf[]				データバッファ
//						ubDataPtr				NextデータバッファPTR
//	Return Value :
//					I2C_API_OK				Success
//					I2C_API_MODE_ERROR		Running IIC on other mode
//					I2C_API_PERM_ERROR		Parameter error
//					I2C_API_BUS_BUSY		I2C Bus is busy
//=============================================================================
I2C_TFR_FNC		I2C_TfrRead( I2C_TFR_INF *pI2cWrtData )
{
	
	
	// Check the IIC mode (controled by software)
	if ( gI2cDrvInf.Mode != I2C_MODE_IDLE ) {
		return I2C_API_MODE_ERROR ;				// Running I2C on other mode
	}
	
	// Store the parameter data to ram for IIC
	gpI2cTfrInf = pI2cWrtData ;
	
	// Check parameter
	if ( (gpI2cTfrInf->ubDataBufSize == 0) || 
		 (gpI2cTfrInf->ubRegBufSize == 0) ) {
		return I2C_API_PERM_ERROR ;				// Parameter error
	}
	
	// Check IIC bus busy
	if ( RIIC.ICCR2.BIT.BBSY != 0 ) {
		return I2C_API_BUS_BUSY ;				// If I2C bus is not free
	}
	
	// Set each RAM
	gI2cDrvInf.Mode = I2C_MODE_TFR_READ ;		// Change the internal mode
	gI2cDrvInf.Stat = I2C_STAT_ON_COMM ;		// Change the status
	gI2cDrvInf.udwTxBytes = 0 ;					// Clear the Tx 転送カウンタ
	gI2cDrvInf.udwRxBytes = 0 ;					// Clear the Rx 転送カウンタ
	
	// Generate start condition
	RIIC.ICCR2.BIT.ST = 1 ;
	
	
	return I2C_API_OK ;
}


//=============================================================================
//	Function Name:	I2C_GetStatus
//	Description  :	Get I2C status
//	Argument     : 
//					*pI2cStat -- Pointer for I2C communication status buffer
//						I2C_STAT_IDLE			I2C is Idle mode.
//												通信開始可能
//						I2C_STAT_ON_COMM		I2C is on communication.
//												通信開始できない
//						I2C_STAT_NACK			I2C has received NACK.
//												次の通信開始可能
//					 	I2C_STAT_FAILED			I2IC has stopped.
//												次の通信開始可能だが
//												前回の通信でエラーがあった
//					*pI2cBusStat -- Pointer for I2C Bus status buffer
//						I2C_BUS_STAT_FREE		I2C bus is free
//						I2C_BUS_STAT_BUSY		I2C bus is busy
//	Return Value :
//=============================================================================
void	I2C_GetStatus( I2C_STATUS *pI2cStat, I2C_BUS_STAT *pI2cBusStat )
{
	
	// 現在の通信ステータスを返す
	*pI2cStat = gI2cDrvInf.Stat ;
	
	// 現在のバス状態を返す
	if ( RIIC.ICCR2.BIT.BBSY == 0 ) {
		*pI2cBusStat = I2C_BUS_STAT_FREE ;
	} else {
		*pI2cBusStat = I2C_BUS_STAT_BUSY ;
	}
}


//-----------------------------------------------------------------------------
//	Function Name:	IIC_EEI_Int
//	Description  :	I2C 通信エラー(EEI)割り込みハンドラ
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
//#pragma interrupt IIC_EEI_Int(vect=VECT_RIIC0_ICEEI0, enable) 
#ifdef	RIIC_CH0
	#pragma interrupt IIC_EEI_Int(vect=VECT_RIIC0_ICEEI0) 
#else
	#pragma interrupt IIC_EEI_Int(vect=VECT_RIIC1_ICEEI1) 
#endif
void	IIC_EEI_Int( void )
{
	
	
	// Check Timeout
	if ( (RIIC.ICSR2.BIT.TMOF != 0) && (RIIC.ICIER.BIT.TMOIE != 0) ) {
		IIC_EEI_Int_TimeOut( ) ;
	}
	
	// Check Arbitration Lost
	if ( (RIIC.ICSR2.BIT.AL != 0) && (RIIC.ICIER.BIT.ALIE != 0) ) {
		IIC_EEI_Int_AL( ) ;
	}
	
	// Check stop condition detection
	if ( (RIIC.ICSR2.BIT.STOP != 0) && (RIIC.ICIER.BIT.SPIE != 0) ) {
		IIC_EEI_Int_STOP( ) ;
	}
	
	// Check NACK reception
	if ( (RIIC.ICSR2.BIT.NACKF != 0 ) && (RIIC.ICIER.BIT.NAKIE != 0) ) {
		IIC_EEI_Int_Nack( ) ;
	}
	
	// Check start condition detection
	if ( (RIIC.ICSR2.BIT.START != 0 ) && (RIIC.ICIER.BIT.STIE != 0) ) {
		IIC_EEI_Int_START( ) ;
	}
	
	nop( ) ;
	
}


//-----------------------------------------------------------------------------
//	Function Name:	IIC_EEI_Int_TimeOut
//	Description  :	IIC_EEI_Int()割り込みハンドラのタイムアウト処理
//					SCL=Highのタイムアウト監視をしているので
//					他のデバイスがLowにしたままだとこのエラーが出る
//					このAPIではシングルマスターなので通常発生しない
//					考えられるのはプルアップが切れたとか...
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
static void	IIC_EEI_Int_TimeOut( void )
{
	gI2cDrvInf.Stat = I2C_STAT_IDLE ;			// バスクロックタイムアウト
	IIC_BusInitialize( ) ;
}


//-----------------------------------------------------------------------------
//	Function Name:	IIC_EEI_Int_AL
//	Description  :	IIC_EEI_Int()割り込みハンドラのバス剥奪時処理
//					送信時SDLが他から操作された
//					結果、バス占有権が剥奪された
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
static void	IIC_EEI_Int_AL( void )
{
	gI2cDrvInf.Stat = I2C_STAT_ERR_ARB_LOST ;	// バス占有権が奪われた
	IIC_BusInitialize( ) ;
}


//-----------------------------------------------------------------------------
//	Function Name:	IIC_EEI_Int_STOP
//	Description  :	IIC_EEI_Int()割り込みハンドラのSTOP検出時処理
//					通信中にSTOP検出
//					シングルマスターでは通常あり得ない
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
static void	IIC_EEI_Int_STOP( void )
{
	
	// Clear each status flag
	RIIC.ICSR2.BIT.NACKF = 0 ;			// NACK未検出へ
	RIIC.ICSR2.BIT.STOP = 0 ;			// STOPコンディション未検出へ

	// Enable/Disable each interrupt
//	RIIC.ICIER.BYTE = 0xBB ;		
		// b0:TMOIE Enable  Time Out interrupt (TMOI)
		// b1:ALIE  Enable  Arbitration Lost interrupt (ALI)
		// b2:STIE  Disable Start Condition Detection Interrupt (STI)
		// b3:SPIE  Enable  Stop condition detection interrupt (SPI)
		// b4:NAKIE Enable  NACK reception interrupt (NKAI)
		// b5:RIE   Enable  Receive Data Full Interrupt (ICRXI)
		// b6:TEIE  Disable Transmit End Interrupt (ICTEI)
		// b7:TIE   Enable  Transmit Data Empty Interrupt (ICTXI)
	RIIC.ICIER.BIT.TMOIE = 1 ;				// Level int. Timeout
	RIIC.ICIER.BIT.ALIE = 1 ;				// Level int. ArbitolationLost
	RIIC.ICIER.BIT.STIE = 0 ;				// Level int. StartCondition
	RIIC.ICIER.BIT.SPIE = 1 ;				// Level int. StopCondition
	RIIC.ICIER.BIT.NAKIE = 1 ;				// Level int. Rx NACK
	RIIC.ICIER.BIT.RIE = 1 ;				// Edge int. RxFull
	RIIC.ICIER.BIT.TEIE = 0 ;				// Level int. TxEnd
	RIIC.ICIER.BIT.TIE = 1 ;				// Edge int. TxDataEmpty
	
	
	// 内部ステータスによってI2Cステータスをセット
	switch( gI2cDrvInf.Mode ) {
		case I2C_MODE_TFR_WRITE :					// 書き込み
			if ( gpI2cTfrInf->ubDataPtr < 
				 gpI2cTfrInf->ubDataBufSize ) {		// 書き込み未完
//				gI2cDrvInf.Stat = I2C_STAT_FAILED ;			// Err:not complete
				gI2cDrvInf.Stat = I2C_STAT_ERR_DETECT_STOP ;
			} else {								// 書き込み完了
				if ( gI2cDrvInf.Stat != I2C_STAT_NACK ) {	// NACK以外
					gI2cDrvInf.Stat = I2C_STAT_IDLE ;
				}
			}
			break ;
		case I2C_MODE_TFR_READ :					// 読み込み
			if ( gpI2cTfrInf->ubDataPtr < 
				 gpI2cTfrInf->ubDataBufSize ) {			// 読み込み未完
//				gI2cDrvInf.Stat = I2C_STAT_FAILED ;			// Err:not complete
				gI2cDrvInf.Stat = I2C_STAT_ERR_DETECT_STOP ;
			} else {									// 読み込み完了
				gI2cDrvInf.Stat = I2C_STAT_IDLE ;
			}
			break ;
		case I2C_MODE_IDLE :						// アイドル中
			// 他のデバイスがSTOPを発行した
			// シングルマスターでは通常あり得ない
			gI2cDrvInf.Stat = I2C_STAT_ERR_DETECT_STOP ;
			IIC_Error( I2C_ERR_MODE_SP_INT ) ;	// Internal mode error
			break ;
		default :
			gI2cDrvInf.Stat = I2C_STAT_ERR_DETECT_STOP ;
			IIC_Error( I2C_ERR_MODE_SP_INT ) ;		// Internal mode error
			break ;
	}

	// Initialize ram for RIIC
	gI2cDrvInf.Mode = I2C_MODE_IDLE ;	// Internal IIC mode
	gI2cDrvInf.udwTxBytes = 0 ;			// Clear the Tx 転送カウンタ
	gI2cDrvInf.udwRxBytes = 0 ;			// Clear the Rx 転送カウンタ
	
}


//-----------------------------------------------------------------------------
//	Function Name:	IIC_EEI_IntSTART
//	Description  :	IIC_EEI_Int()割り込みハンドラのSTART検出時処理
//					通信中にSTART検出
//					受信処理でレジスタ値送信後リスタート掛けるので
//					その時に発生する。よってデータ受信の為にスレーブアドレス
//					を送信する。
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
static void	IIC_EEI_Int_START( void )
{
	
	// START Condition 割込を禁止させる
	RIIC.ICSR2.BIT.START = 0 ;			// STARTコンディション未検出へ
	RIIC.ICIER.BIT.STIE = 0 ;			// STARTコンディション割り込み禁止へ
	
	// Transfer slave device address (Read)
	RIIC.ICDRT = gpI2cTfrInf->ubSlvAdr | 0x01 ;	// マスタ転送時 b0 は必ず '1'
	
}


//-----------------------------------------------------------
//	Function Name:	IIC_EEI_Int_Nack
//	Description  :	IIC_EEI_Int()割り込みハンドラのNAK受信時の処理
//					通常自分(Master)が発行するのでSTOP発行して
//					トランザクションを終了する
//	引数
//	戻り値
//-----------------------------------------------------------
static	void	IIC_EEI_Int_Nack( void )
{
	
	
	gI2cDrvInf.Stat = I2C_STAT_NACK ;		// 内部モード更新
	
	RIIC.ICIER.BIT.NAKIE = 0 ;				// NAK割込禁止へ

	// STOP Condition 発行してバス開放
	RIIC.ICSR2.BIT.STOP = 0 ;				// STOP condition 未検出状態
	RIIC.ICCR2.BIT.SP = 1 ;					// STOP condition 発行要求
	
}

//-----------------------------------------------------------------------------
//	Function Name:	IIC_RXI_Int
//	Description  :	受信データフル時の割り込みハンドラ
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
//#pragma interrupt IIC_RXI_Int(vect=VECT_RIIC0_ICRXI0, enable) 
#ifdef	RIIC_CH0
	#pragma interrupt IIC_RXI_Int(vect=VECT_RIIC0_ICRXI0) 
#else
	#pragma interrupt IIC_RXI_Int(vect=VECT_RIIC1_ICRXI1) 
#endif
void	IIC_RXI_Int( void )
{
	
	// 内部ステータスによって処理振り分け
	switch( gI2cDrvInf.Mode ) {
		case I2C_MODE_TFR_READ :
			IIC_RXI_Int_TfrRead( ) ;						// get Rx data
			break ;
		case I2C_MODE_TFR_WRITE :
			IIC_Error( I2C_ERR_MODE_RX_INT_TFR_WRITE ) ;	// Internal mode err
			break ;
		default :
			IIC_Error( I2C_ERR_MODE_RX_INT ) ;				// Internal mode err
			break ;
	}
	
	nop( ) ;
	
}


//--------------------------------------------------------------------------
//	Function Name:	IIC_RXI_Int_TfrRead
//	Description  :	受信処理用のRXI割込ハンドラ
//					IIC_RXI_Int() の割込ルーチンから呼ばれる
//	Argument     :
//	Return Value :
//--------------------------------------------------------------------------
static void		IIC_RXI_Int_TfrRead( void )
{
	volatile static BYTE	bDat ;
	
	
	// Increase internal reception counter on RAM.
	// It is a receive data counter.
	gI2cDrvInf.udwRxBytes++ ;				// Rx 転送カウンタ
	
	
	if( gI2cDrvInf.udwRxBytes == 1 ) {			// 1st受信
		// 最終
		if ( (gpI2cTfrInf->ubDataPtr + 1) == gpI2cTfrInf->ubDataBufSize ) {
			// WAIT ビットが“1” のとき、1 バイト受信ごとに9 クロック目の
			// 立ち下がり以降、ICDRR レジスタの値が読み出されるまでの間 SCLn
			// ラインを Low にホールドします。(STOP condition) これにより 1 
			// バイトごとの受信動作が可能です。
			RIIC.ICMR3.BIT.WAIT = 1 ;
			
			// If RIIC sends ACK when it receives last data, EEPROM may prevent
			// RIIC generating stop condition. Because EEPROM desn't know how
			// many RIIC try to read data.
			// When RIIC sends ACK, EEPROM realizes the next rising SCL for
			// stop condition as next first bit SCL.
			RIIC.ICMR3.BIT.ACKBT = 1 ;			// Set NACK for final data.
		}
//		RIIC.ICSR2.BIT.STOP = 0 ;				// STOP condition 未検出
//		RIIC.ICCR2.BIT.SP = 1 ;					// STOP condition 発行要求
		bDat = RIIC.ICDRR ;						// dummy read
	// 最終2nd受信
	} else if ( (gpI2cTfrInf->ubDataPtr + 2) == gpI2cTfrInf->ubDataBufSize ) {
		// WAIT ビットが“1” のとき、1 バイト受信ごとに9 クロック目の
		// 立ち下がり以降、ICDRR レジスタの値が読み出されるまでの間 SCLn
		// ラインを Low にホールドします。(STOP condition) これにより 1 
		// バイトごとの受信動作が可能です。
		RIIC.ICMR3.BIT.WAIT = 1 ;
		
		// If RIIC sends ACK when it receives last data, EEPROM may prevent
		// RIIC generating stop condition. Because EEPROM desn't know how
		// many RIIC try to read data.
		// When RIIC sends ACK, EEPROM realizes the next rising SCL for
		// stop condition as next first bit SCL.
		RIIC.ICMR3.BIT.ACKBT = 1 ;			// Set NACK for final data.
		
		// Read data
		gpI2cTfrInf->ubDataBuf[gpI2cTfrInf->ubDataPtr++] = RIIC.ICDRR ;
	// 最終data
	} else if ( (gpI2cTfrInf->ubDataPtr + 1) == gpI2cTfrInf->ubDataBufSize ) {
		
		// After read final data, RIIC generates stop condtion
		gpI2cTfrInf->ubDataBuf[gpI2cTfrInf->ubDataPtr++] = RIIC.ICDRR ;
		RIIC.ICMR3.BIT.WAIT = 1 ;
		// Generate Stop Condition
		RIIC.ICSR2.BIT.STOP = 0 ;				// STOP condition 未検出
		RIIC.ICCR2.BIT.SP = 1 ;					// STOP condition 発行要求
	} else {
		// Read data
//		bDat = RIIC.ICDRR ;						// dummy read
		gpI2cTfrInf->ubDataBuf[gpI2cTfrInf->ubDataPtr++] = RIIC.ICDRR ;
		RIIC.ICMR3.BIT.WAIT = 1 ;
		nop( ) ;
	}
	
}


//-----------------------------------------------------------------------------
//	Function Name:	IIC_TXI_Int
//	Description  :	送信データエンプティ割り込みハンドラ
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
//#pragma interrupt IIC_TXI_Int(vect=VECT_RIIC0_ICTXI0, enable) 
#ifdef	RIIC_CH0
	#pragma interrupt IIC_TXI_Int(vect=VECT_RIIC0_ICTXI0) 
#else
	#pragma interrupt IIC_TXI_Int(vect=VECT_RIIC1_ICTXI1) 
#endif
void	IIC_TXI_Int( void )
{
	
	// 内部ステータスによって処理振り分け
	switch( gI2cDrvInf.Mode ) {
		case I2C_MODE_TFR_WRITE :
			IIC_TXI_Int_TfrWrite( ) ;					// 書込用データ送信
			break ;
		case I2C_MODE_TFR_READ :
			IIC_TXI_Int_TfrRead( ) ;					// 読込用データ送信
			break ;
		default:
			IIC_Error( I2C_ERR_MODE_TX_INT ) ;			// Internal mode err
			break ;
	}
	
	nop( ) ;
	
}


//--------------------------------------------------------------------------
//	Function Name:	IIC_TXI_Int_TfrWrite
//	Description  :	送信処理用のTXI割込ハンドラ
//					IIC_TXI_Int() の割込ルーチンから呼ばれる
//	Argument     :
//	Return Value :
//--------------------------------------------------------------------------
static void		IIC_TXI_Int_TfrWrite( void )
{
	
	// Increase internal transmission counter on RAM.
	gI2cDrvInf.udwTxBytes++ ;						// Tx 転送カウンタ
	
	// Transfer slave device address
	if( gI2cDrvInf.udwTxBytes == 1 ) {		// 1st転送
		// Transfer slave device address (Read)
		RIIC.ICDRT = gpI2cTfrInf->ubSlvAdr & 0xfe ;	// マスタ転送時 b0 は必ず'0'
		return ;
	}
	
	// Transfer reg.# for writting
	if( gpI2cTfrInf->ubRegBufSize > gpI2cTfrInf->ubRegBufPtr ) {	// 最終以外
		RIIC.ICDRT = gpI2cTfrInf->ubRegBuf[gpI2cTfrInf->ubRegBufPtr++] ;
		return ;
	}
	
	// Transfer data for writting
	if( gpI2cTfrInf->ubDataPtr < gpI2cTfrInf->ubDataBufSize ) {	// データは最終
		RIIC.ICDRT = gpI2cTfrInf->ubDataBuf[gpI2cTfrInf->ubDataPtr++] ;
	}
	// この後TEI割込でリスタートを掛ける
	if( gpI2cTfrInf->ubDataPtr == gpI2cTfrInf->ubDataBufSize ) {// データは最終
		RIIC.ICIER.BIT.TEIE = 1 ;							// TEI割込許可
	}
	
	
	return ;
	
}


//--------------------------------------------------------------------------
//	Function Name:	IIC_TXI_Int_TfrRead
//	Description  :	受信処理用のTXI割込ハンドラ
//					IIC_TXI_Int() の割込ルーチンから呼ばれる
//	Argument     :
//	Return Value :
//--------------------------------------------------------------------------
static void		IIC_TXI_Int_TfrRead( void )
{
	
	// Increase internal transmission counter on RAM.
	gI2cDrvInf.udwTxBytes++ ;						// Tx 転送カウンタ

	// Transfer slave device address
	if( gI2cDrvInf.udwTxBytes == 1 ) {		// 1st転送
		// Transfer slave device address (Read)
		RIIC.ICDRT = gpI2cTfrInf->ubSlvAdr & 0xfe ;	// マスタ転送時 b0 は必ず'0'
		nop( ) ;
		return ;
	}
	nop( ) ;
	
	// Transfer reg.# for read
	if( gpI2cTfrInf->ubRegBufPtr < gpI2cTfrInf->ubRegBufSize ) {	// 最終以外
		RIIC.ICDRT = gpI2cTfrInf->ubRegBuf[gpI2cTfrInf->ubRegBufPtr++] ;
	}
	nop( ) ;
	// この後TEI割込でリスタートを掛ける
	if( gpI2cTfrInf->ubRegBufPtr == gpI2cTfrInf->ubRegBufSize ) {// データは最終
		RIIC.ICMR3.BIT.RDRFS = 1 ;				// 
		RIIC.ICIER.BIT.TEIE = 1 ;				// TEI割込許可
	}
	nop( ) ;
	
	
	return ;
	
}

//-----------------------------------------------------------------------------
//	Function Name:	IIC_TEI_Int
//	Description  :	送信終了割り込みハンドラ
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
#ifdef	RIIC_CH0
	#pragma interrupt IIC_TEI_Int(vect=VECT_RIIC0_ICTEI0) 
#else
	#pragma interrupt IIC_TEI_Int(vect=VECT_RIIC1_ICTEI1) 
#endif
void	IIC_TEI_Int( void )
{
	
	// 内部ステータスによって処理振り分け
	switch( gI2cDrvInf.Mode ) {
		case I2C_MODE_TFR_WRITE :
			IIC_TEI_Int_TfrWrite( ) ;				// 書込用終了処理
			break ;
		case I2C_MODE_TFR_READ :
			IIC_TEI_Int_TfrRead( ) ;				// 読込用終了処理
			break ;
		default :
			IIC_Error( I2C_ERR_MODE_TE_INT ) ;			// Internal mode err
			break;
	}
	
	nop( ) ;
	
}


//--------------------------------------------------------------------------
//	Function Name:	IIC_TEI_Int_TfrWrite
//	Description  :	送信用のTEI割込ハンドラ
//					IIC_TEI_Int() 割込ルーチンから呼ばれる
//	Argument     :
//	Return Value :
//--------------------------------------------------------------------------
static void		IIC_TEI_Int_TfrWrite( void )
{
	
	RIIC.ICIER.BIT.TEIE = 0 ;				// TEI割込禁止へ
	
	// Generate Stop Condition
	RIIC.ICSR2.BIT.STOP = 0 ;				// STOP condition 未検出
	RIIC.ICCR2.BIT.SP = 1 ;					// STOP condition 発行要求
	
}

//--------------------------------------------------------------------------
//	Function Name:	IIC_TEI_Int_TfrRead
//	Description  :	受信用のTEI割込ハンドラ
//					IIC_TEI_Int() 割込ルーチンから呼ばれる
//	Argument     :
//	Return Value :
//--------------------------------------------------------------------------
static void		IIC_TEI_Int_TfrRead( void )
{
	
	RIIC.ICSR2.BIT.START = 0 ;		// Clear Start Condition Detection flag
	RIIC.ICIER.BIT.STIE = 1;		// Enable Start Condition Detection Int.
	RIIC.ICIER.BIT.TEIE = 0 ;		// TEI割込禁止へ
	
	RIIC.ICCR2.BIT.RS = 1 ;			// Generate restart condition
	
}

//-----------------------------------------------------------------------------
//	Function Name:	IIC_BusInitialize
//	Description  :	RIICユニットをリセットしてバスを初期化する
//					Step1 - RIICをリセット (try to be SCL=High and SDA=Hgih)
//							もしリセットしてもSCL=Lowなら他のデバイスが
//							Lowにしていることになる
//					Step2 - もしSDA=LowならSCLに何クロックか出してみる
//					Step3 - SDA=HighにしてSTOPを発行
//	Argument     :
//	Return Value :
//-----------------------------------------------------------------------------
static void		IIC_BusInitialize( void )
{
	volatile UWORD		uwLoop ;
	
	
	RIIC.ICCR1.BIT.IICRST = 1 ;				// RIIC内部リセット
	
	// タイムアウト検出設定
	RIIC.ICFER.BIT.TMOE = 0 ;				// タイムアウト検出無効
	RIIC.ICMR2.BIT.TMOL = 1 ;				// Disable SCL=Low time out
	RIIC.ICMR2.BIT.TMOH = 1 ;				// Enable SCL=High time out
	RIIC.ICMR2.BIT.TMOS = 0 ;				// Long mode (16bits counter),
											// PCLK=48MHz, IIC phi=48MHz,
											// Long mode is about 1.364 ms
	
	//======================================================
	// もしRIICがSDA=Lowにしていたなら内部リセット(IICRST=1)
	// で開放するはず
	//======================================================
	RIIC.ICCR1.BIT.IICRST = 0 ;				// 内部リセット解除
	for( uwLoop = 0 ; uwLoop < 100 ; uwLoop++ ) ;	// ちょいと待つ
	
	RIIC.ICFER.BIT.TMOE = 0 ;				// タイムアウト検出有効
	
	//======================================================
	// それでもSCL=Lowなら他のデバイスがSCL=Lowにしている
	// どうにもならないのでエラー状態にする
	//======================================================
	if ( RIIC.ICCR1.BIT.SCLI == 0 ) {			// Check SCL level
		IIC_Error( I2C_ERR_SCL_LOCK ) ;			// Internal Error
	}
	
	
	//======================================================
	// 後の以下の操作でSTOP発行する
	// MST=1でSTOP発行するがその為にMTWP=1でプロテクト解除
	// してTRS=1で送信モードにする必要がある
	//======================================================
	RIIC.ICMR1.BIT.MTWP = 1 ;			// MST/TRS 書込許可
	RIIC.ICCR2.BIT.TRS = 1 ;			// 送信モードへ
	RIIC.ICCR2.BIT.MST = 1 ;			// マスタモードへ
	
	
	//======================================================
	// もし他のデバイスがSDA=LowならRIICから何クロックか送っ
	// てみる。大抵のデバイスはこれでSDA=Lowにするのをやめる
	//======================================================
	if ( RIIC.ICCR1.BIT.SDAI == 0 ) {			// SDA=Lowだった
		// SDA=Highになるまで10クロック送ってみる
		for( uwLoop = 0 ; uwLoop < 10 ; uwLoop++ ) {		
			if ( RIIC.ICCR1.BIT.SDAI == 0 ) {	// SDA=Low
				RIIC.ICCR1.BIT.CLO = 1 ;				// 1クロック出力
				while( RIIC.ICCR1.BIT.CLO != 0 ) {		// クロック出力完了待ち
					if ( RIIC.ICSR2.BIT.TMOF != 0 ) {	// Timeoutは無視
						RIIC.ICSR2.BIT.TMOF = 0 ;		// Not detect timeout
						break ;
					}
				}
			} else {							// SDA=High
				// 正常に戻ったので処理中断
				break ;
			}
			
			// 10クロック送ってもSDA=Lowのままならエラー
			if ( (uwLoop == 9) && (RIIC.ICCR1.BIT.SDAI == 0) ) {
				IIC_Error( I2C_ERR_SDA_LOCK ) ;			// Internal Error
			}
		}
	}
	
	// こんでもってバス開放状態ならSTOP発行はしない
	// バスビジーならSTOP発行する
	if ( RIIC.ICCR2.BIT.BBSY == 0 ) {		// not Busy (Bus free)
		RIIC.ICCR1.BIT.IICRST = 1 ;					// Reset RIIC
		RIIC.ICCR1.BIT.IICRST = 0 ;					// Clear reset
	} else {								// Busy (Buslock)
		RIIC.ICSR2.BIT.STOP = 0 ;				// STOP condition 未検出
		RIIC.ICCR2.BIT.SP = 1 ;					// STOP condition 発行要求
	}
	
	
	// Enable MST/TRS Write Protect
	RIIC.ICMR1.BIT.MTWP = 0 ;			// MST/TRS 書込禁止へ
	
	
	// タイムアウト検出設定を再度行う(あまり意味無いが)
	RIIC.ICFER.BIT.TMOE = 1 ;			// タイムアウト検出無効
	RIIC.ICMR2.BIT.TMOL = 0 ;			// Disable SCL=Low time out
	RIIC.ICMR2.BIT.TMOH = 1 ;			// Enable SCL=High time out
	RIIC.ICMR2.BIT.TMOS = 0 ;			// Long mode (16bits counter).
										// PCLK=48MHz, IIC phi=48MHz,
										// Long mode is about 1.365 ms
	RIIC.ICFER.BIT.TMOE = 1 ;			// タイムアウト検出有効
	
}

/******************************************************************************
* Function Name: IIC_Error
* Description  : Usually this function is not called.
* Argument     : error_code
*              :    I2C_ERR_MODE_SP_INT
*              :    I2C_ERR_MODE_RX_INT_TFR_WRITE
*              :    I2C_ERR_MODE_RX_INT
*              :    I2C_ERR_MODE_TX_INT
*              :    I2C_ERR_MODE_TE_INT
*              :    I2C_ERR_SCL_LOCK
*              :    I2C_ERR_SDA_LOCK
*              :    I2C_ERR_GEN_CLK_BBSY
* Return Value : none
******************************************************************************/
static void IIC_Error( I2C_ERR_CODE ErrorCode )
{
	
	switch( ErrorCode ) {
		case I2C_ERR_MODE_SP_INT :
			break ;
		case I2C_ERR_MODE_RX_INT_TFR_WRITE :
			break ;
		case I2C_ERR_MODE_RX_INT :
			break ;
		case I2C_ERR_MODE_TX_INT :
			break ;
		case I2C_ERR_MODE_TE_INT :
			break ;
		case I2C_ERR_SCL_LOCK :
			break ;
		case I2C_ERR_SDA_LOCK :
			break ;
		case I2C_ERR_GEN_CLK_BBSY :
			break ;
		default :
			break ;
	}
	
	// RIICリセット (レジスタもリセットされる)
	RIIC.ICCR1.BIT.ICE = 0 ;					// I2C-Bus 停止
	RIIC.ICCR1.BIT.IICRST = 1 ;					// 内部リセット
	RIIC.ICCR1.BIT.IICRST = 0 ;					// 内部リセット解除
//	while ( 1 ) ;
	
}

//--------------------------------------------------------------------------
