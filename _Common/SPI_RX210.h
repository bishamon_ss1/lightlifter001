/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2013 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* History      : DD.MM.YYYY Version  Description
*              : 01.03.2013 1.00     First Release
*				2014.05.19	by H.Inoue
*							RX210社内用にポーティング
*							CS信号にSSL0を使用(LowActive)
*******************************************************************************/

#ifndef _SPI_DEF_
#define _SPI_DEF_

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include	"_iodefine.h"
#include	<stdint.h>
#include	<stdbool.h>

#include	"TypeDef.h"					// アプリケーション用型定義



//--------------------------------------------------------------------------
//	マクロ定義
//--------------------------------------------------------------------------
//-----------------------------------------------------------
//	関数戻り値
//-----------------------------------------------------------
#define	SPI_OK					(0x00)		// 正常終了
#define	SPI_NOT_IDLE			(0x01)		// 処理中(アイドル中でない)
#define	SPI_PARM_ERR			(0x02)		// パラメータエラー

//-----------------------------------------------------------
//	関数引数
//-----------------------------------------------------------
#define	SPI_CS_SSL0				(0x0000)	// SSL0を使用
#define	SPI_CS_SSL1				(0x0010)	// SSL1を使用
#define	SPI_CS_SSL2				(0x0020)	// SSL2を使用
#define	SPI_CS_SSL3				(0x0030)	// SSL3を使用

//-----------------------------------------------------------
//	割込
//-----------------------------------------------------------
#define	RSPI_INT_LEVEL			4			// 割込レベル


//--------------------------------------------------------------------------
//	変数型定義, 変数定義
//--------------------------------------------------------------------------
//-----------------------------------------------------------
//	コールバック関数
//-----------------------------------------------------------
typedef void	(* PcbFunck)(void) ;		// 割込処理用コールバック関数

//-----------------------------------------------------------
//	通信ステータス構造体
//-----------------------------------------------------------
typedef union _TRAN_STAT_ {
		UBYTE			ubStat ;			// 状態(一括)
		struct {
			UBYTE		ubBusy:1 ;			// 通信ビジー状態
			UBYTE		ubTrans:3 ;			// 転送状態
			UBYTE		ubErr:2 ;			// エラー状態
			UBYTE		ubDmy:2 ;			// 
		} BIT ;
	} TRAN_STAT ;
// メンバ:ubBusy
#define	SPI_STAT_BUSY_NO		(0)			// アイドル中
#define	SPI_STAT_BUSY_YES		(1)			// BUSY状態
// メンバ:ubTrans
#define	SPI_STAT_TRAN_NO		(0)			// 転送終了
#define	SPI_STAT_TRAN_START		(1)			// 転送開始
#define	SPI_STAT_TRAN_TX		(2)			// 送信中
#define	SPI_STAT_TRAN_RX_START	(3)			// 受信開始
#define	SPI_STAT_TRAN_RX		(4)			// 受信中
// メンバ:ubErr
#define	SPI_STAT_ERR_NO			(0)			// エラー無し
#define	SPI_STAT_ERR_OVR		(1)			// ERR:OverRun
#define	SPI_STAT_ERR_OTHER		(3)			// ERR:その他

//-----------------------------------------------------------
//	送受信バッファ構造体
//-----------------------------------------------------------
typedef struct _TRAN_BUF_ {
		UBYTE			*pubBuf ;			// 通信バッファPtr
		UBYTE			ubSize ;			// 通信サイズ(Bytes)
	} TRAN_BUF ;
// 注:
//		送信バッファのubSizeはコマンド(Reg#)バイトを含む


//--------------------------------------------------------------------------
//	関数プロトタイプ宣言
//--------------------------------------------------------------------------

//-----------------------------------------------------------
//	SPIを初期化
//	引数
//	戻り値
//-----------------------------------------------------------
void	SPI_Inz( void ) ;

//-----------------------------------------------------------
//	SPIへ転送指示
//	引数
//		UWORD		uwSSL		SSLn(CS)端子指定
//				SPI_CS_SSL0		SSL0を使用
//				SPI_CS_SSL1		SSL1を使用
//				SPI_CS_SSL2		SSL2を使用
//				SPI_CS_SSL3		SSL3を使用
//		TRAN_BUF	*pTxBuf		送信バッファPtr
//		TRAN_BUF	*pRxBuf		受信バッファPtr
//		PcbFunck	pcbfEnd		送受信終了コールバック関数(NULL=無し)
//		PcbFunck	pcbfErr		エラー発生時コールバック関数(NULL=無し)
//	戻り値
//				SPI_OK			正常終了
//				SPI_NOT_IDLE	処理中(アイドル中でない)
//				SPI_PARM_ERR	パラメータエラー
//-----------------------------------------------------------
UBYTE	SPI_StartTrans( 
				UWORD		uwSSL,
				TRAN_BUF	*pTxBuf,
				TRAN_BUF	*pRxBuf,
				PcbFunck	pcbfEnd,
				PcbFunck	pcbfErr
				) ;

//-----------------------------------------------------------
//	SPIの状態を取得
//	引数
//	戻り値 (TRAN_STAT)
//-----------------------------------------------------------
TRAN_STAT	SPI_GetStat( void ) ;



#endif	// #ifndef _SPI_DEF_
//--------------------------------------------------------------------------
