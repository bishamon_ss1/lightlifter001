//==========================================================================
//	
//	File Name	: FRAME_HDL.h
//	Version		: R00.10
//	Description	: UART(RS485)のデータフレームハンドラ
//				  メイン基板との通信処理
//				  MPU-6500用
//	Date		: 2014/08/21
//	Date		: 2014/11/05	データフレーム仕様変更
//	
//==========================================================================


#ifndef _APP_SB_FHDL_TYPE_DEFINED_				// 多重定義予防
#define _APP_SB_FHDL_TYPE_DEFINED_

#include	"TypeDef.h"							// 基本型定義


//------------------------------------------------------
// 加速度センサデータフレーム情報
//------------------------------------------------------
//--------------------------------
// センサへのリクエストデータフレーム
//--------------------------------
#pragma pack
// 0x01:リンク角度情報要求
// 0x02:センサデータ要求
typedef struct _ACC_REQ_ANG_ {
		UBYTE			ubSTX ;				// STX
		UBYTE			ubID ;				// SensorID
		UBYTE			ubSize ;			// データサイズ(byte)
		UBYTE			ubCmd ;				// コマンドバイト
		UWORD			uwCRC ;				// CRC値
		UBYTE			ubETX ;				// ETX
	} ACC_REQ_ANG ;
// ubSize: 1
//	ubSizeの後からuwCRCの前までのバイト数
// uwCRC:
//	ubID〜uwCRCの前までのCRC演算値

// 0x11:パラメータ値読み出し要求
typedef struct _ACC_GET_PARM_ {
		UBYTE			ubSTX ;				// STX
		UBYTE			ubID ;				// SensorID
		UBYTE			ubSize ;			// データサイズ(byte)
		UBYTE			ubCmd ;				// コマンドバイト
		UBYTE			ubIdx ;				// メモリ番号
		UBYTE			ubDmy ;				// Dummy(for Word Align)
		UWORD			uwCRC ;				// CRC値
		UBYTE			ubETX ;				// ETX
	} ACC_GET_PARM ;
// ubSize: 3

// 0x12:パラメータ値書き込み要求
typedef struct _ACC_PUT_PARM_ {
		UBYTE			ubSTX ;				// STX
		UBYTE			ubID ;				// SensorID
		UBYTE			ubSize ;			// データサイズ(byte)
		UBYTE			ubCmd ;				// コマンドバイト
		UBYTE			ubIdx ;				// メモリ番号
		UBYTE			ubDmy ;				// Dummy(for Word Align)
		WORD			wData ;				// 書き込み値
		UWORD			uwCRC ;				// CRC値
		UBYTE			ubETX ;				// ETX
	} ACC_PUT_PARM ;
// ubSize: 5

// 0x13:データをフラッシュ(書き込み)
// 0x14:データフラッシュの消去
// 0x21:センサリセット
// 0x31:センサ状態問い合わせ
typedef struct _ACC_INQ_STAT_ {
		UBYTE			ubSTX ;				// STX
		UBYTE			ubID ;				// SensorID
		UBYTE			ubSize ;			// データサイズ(byte)
		UBYTE			ubCmd ;				// コマンドバイト
		UWORD			uwCRC ;				// CRC値
		UBYTE			ubETX ;				// ETX
	} ACC_INQ_STAT ;
// ubSize: 1

// センサへのリクエストデータフレーム
typedef union _ACC_REQ_FRAME_ {
		ACC_REQ_ANG		ReqAng ;			// リンク角度,センサデータ要求
		ACC_GET_PARM	GetParm ;			// パラメータ値読み出し要求
		ACC_PUT_PARM	PutParm ;			// パラメータ値書き込み要求
		ACC_INQ_STAT	Inq ;				// センサ状態問い合わせ
	} ACC_REQ_FRAME ;
// メンバー:ubSTX
#define	ACC_STX					(0x02)		// STX
// メンバー:ubETX
#define	ACC_ETX					(0x03)		// ETX

// Req.データフレーム
typedef union _ACC_REQ_DATA_ {
		ACC_REQ_FRAME	Frame ;							// SensorReq.Frame
		BYTE			bDat[sizeof(ACC_REQ_FRAME)+1] ;	// 受信データ
	} ACC_REQ_DATA ;

#pragma packoption


//--------------------------------
// センサからのレスポンスデータフレーム
//--------------------------------
#pragma pack
// リンク角度情報要求
typedef struct _ACC_ANGLE_ {
		UBYTE			ubSTX ;				// STX
		UBYTE			ubSize ;			// データサイズ(byte)
		DWORD			dwAng ;				// 角度   小数5桁
		DWORD			dwGyro ;			// 角速度 小数4桁
		FLAG			fEq ;				// 地震検出フラグ
		FLAG			fTemp ;				// 温度異常フラグ
		UWORD			uwCRC ;				// CRC値
		UBYTE			ubETX ;				// ETX
	} ACC_ANGLE ;
// ubSize: 10
//	ubSizeの後からuwCRCの前までのバイト数
// メンバー:fEq
#define	ACC_EARTHQUAKE_NO		0			// 地震未検出
#define	ACC_EARTHQUAKE_YES		1			// 地震検出あり
// メンバー:fTemp
#define	ACC_TEMP_OK				0			// 温度正常
#define	ACC_TEMP_LOW			1			// 温度異常 低温
#define	ACC_TEMP_HIGH			2			// 温度異常 高温

// センサデータ要求
typedef struct _ACC_RAW_DATA_ {
		UBYTE			ubSTX ;				// STX
		UBYTE			ubSize ;			// データサイズ(byte)
		WORD			wDataX ;			// X軸値 0.00001度
		WORD			wDataY ;			// Y軸値 0.00001度
		WORD			wDataZ ;			// Z軸値 0.00001度
		WORD			wGyroX ;			// X角速度 0.0001度/秒
		WORD			wGyroY ;			// Y角速度 0.0001度/秒
		WORD			wGyroZ ;			// Z角速度 0.0001度/秒
		WORD			wTemp ;				// 温度  0.1度
		UWORD			uwCRC ;				// CRC値
		UBYTE			ubETX ;				// ETX
	} ACC_RAW_DATA ;
// ubSize: 14

// 0x11:パラメータ値読み出し要求
typedef struct _ACC_GET_PARMR_ {
		UBYTE			ubSTX ;				// STX
		UBYTE			ubSize ;			// データサイズ(byte)
		WORD			wData ;				// パラメータデータ
		WORD			wMax ;				// パラメータ最大値
		WORD			wMin ;				// パラメータ最小値
		FLAG			fType ;				// データ型
		UWORD			uwCRC ;				// CRC値
		UBYTE			ubETX ;				// ETX
	} ACC_GET_PARMR ;
// ubSize: 7
// メンバー:fType DF_MAINTのfType参照
//#define	DF_TYPE_SIGHNED			0			// 符号有り
//#define	DF_TYPE_UNSIGHNED		1			// 符号無し
//#define	DF_TYPE_NO_DATA			0xff		// データが存在しない

// 0x12:パラメータ値書き込み要求
// 0x13:データをフラッシュ(書き込み)
// 0x14:データフラッシュの消去
// 0x21:センサリセット
typedef struct _ACC_FLASH_DATAR_ {
		UBYTE			ubSTX ;				// STX
		UBYTE			ubSize ;			// データサイズ(byte)
		UBYTE			ubRes ;				// Response(ACK,NAK)
		UWORD			uwCRC ;				// CRC値
		UBYTE			ubETX ;				// ETX
	} ACC_FLASH_DATAR ;
// ubSize: 1
// メンバー:ubRes ACC_RESPONSE.ubRes 参照

// 0x31:センサ状態問い合わせ
typedef struct _ACC_RES_STAT_ {
		UBYTE			ubSTX ;				// STX
		UBYTE			ubSize ;			// データサイズ(byte)
		FLAG			fSens ;				// センサ状態
		FLAG			fInz ;				// 初期化状態
		UWORD			uwCRC ;				// CRC値
		UBYTE			ubETX ;				// ETX
	} ACC_RES_STAT ;
// ubSize: 2
// メンバー:fSens
#define	ACC_STAT_SENS_OK		0			// エラー無し
#define	ACC_STAT_SENS_INZ		1			// 初期化エラー
#define	ACC_STST_SENS_COMM		2			// 通信エラー
// メンバー:fInz
#define	ACC_STAT_INZ_NO			0			// 未初期化
#define	ACC_STAT_INZ_YES		1			// 初期化完了

// センサシングルレスポンス
typedef struct _ACC_RESPONSE_ {
		UBYTE			ubRes ;				// Response(ACK,NAK)
	} ACC_RESPONSE ;
// メンバー:ubRes
#define	ACC_ACK					(0x06)		// ACK
#define	ACC_NAK					(0x15)		// NAK

// センサからのレスポンスデータフレーム
typedef union _ACC_RES_FRAME_ {
		ACC_ANGLE		Ang ;				// リンク角度
		ACC_RAW_DATA	Raw ;				// センサデータ要求
		ACC_GET_PARMR	GetParm ;			// パラメータ値読み出し要求
		ACC_FLASH_DATAR	Flush ;				// データフラッシュ
		ACC_RESPONSE	Res ;				// シングルレスポンス
		ACC_RES_STAT	Stat ;				// センサ状態問い合わせ
	} ACC_RES_FRAME ;

// Res.データフレーム
typedef union _ACC_RES_DATA_ {
		ACC_RES_FRAME	Res ;							// SensorRes.Frame
		BYTE			bDat[sizeof(ACC_RES_FRAME)+1] ;	// 受信データ
	} ACC_RES_DATA ;

#pragma packoption


//--------------------------------
// 加速度センサRS485情報
//--------------------------------
// センサ基板ID
#define	ACC_ID_MAIN			0x71			// メイン(R) ID
#define	ACC_ID_SUB			0x72			// サブ(L)   ID
#define	ACC_ID_MIN			ACC_ID_MAIN		// ID最小値
#define	ACC_ID_MAX			ACC_ID_SUB		// ID最大値
#define	ACC_ID				ACC_ID_MAIN		// センサ基板ID
//#define	ACC_ID				ACC_ID_SUB		// センサ基板ID

// Req.コマンド
#define	ACC_CMD_REQ_ANG		0x01			// リンク角度情報要求
#define	ACC_CMD_REQ_RAW		0x02			// センサデータ要求
#define	ACC_CMD_GET_PARM	0x11			// パラメータ値読み出し要求
#define	ACC_CMD_PUT_PARM	0x12			// パラメータ値書き込み要求
#define	ACC_CMD_FLUSH_DATA	0x13			// データフラッシュ
#define	ACC_CMD_ERASE_FLUSH	0x14			// フラッシュメモリを消去(初期化)
#define	ACC_CMD_RESET		0x21			// センサリセット
#define	ACC_CMD_INQ_STAT	0x31			// センサ状態問い合わせ

// Req.フレームバイト数  ubID〜uwCRCの前までのバイト数
#define	ACC_CMDC_REQ_ANG	3				// リンク角度情報要求
#define	ACC_CMDC_REQ_RAW	3				// センサデータ要求
#define	ACC_CMDC_GET_PARM	5				// パラメータ値読み出し要求
#define	ACC_CMDC_PUT_PARM	7				// パラメータ値書き込み要求
#define	ACC_CMDC_FLUSH_DATA	3				// データフラッシュ
#define	ACC_CMDC_RESET		3				// センサリセット
#define	ACC_CMDC_INQ_STAT	3				// センサ状態問い合わせ
#define	ACC_CMDC_MAX		ACC_CMDC_PUT_PARM	// 最大バイト数

// 構造体サイズとリクエスト/レスポンスフレームのubSizeオフセット
#define	ACC_RES_SIZE_OFS	5				// Size Offset
#define	ACC_REQ_SIZE_OFS	6				// Size Offset

// uwCRC〜ubETX(コマンドバイトより後)の構造体バイト数
#define	ACC_REQ_TAIL_SIZE	3				// 最終部サイズ

// CRC演算サイズの構造体オフセット
#define	ACC_CEC_SIZE_OFS	4				// 構造体オフセット

//--------------------------------
// データフレーム解析
//--------------------------------

// フレーム受信タイムアウト値
//#define	FANZ_TIMEOUT		11				// 受信タイムアウト(mS)
#define	FANZ_TIMEOUT		8				// 受信タイムアウト(mS)
// フレーム解析間のインターバル
//#define	FANZ_SEQ_INTERVAL	2				// フレーム解析間Interval(mS)

typedef struct _FRAME_ANZ_ {
		FLAG			fStx ;				// STX検出Flag
//		FLAG			fEtx ;				// ETX検出Flag
		UWORD			uwRxCrc ;			// 受信CRC値
		UBYTE			ubFptrOffset ;		// FrameDataのPtrオフセット
		UBYTE			ubRxCount ;			// 受信すべきデータサイズ(byte)
		FLAG			fCmd ;				// Cmd/Res受信Flag
	} FRAME_ANZ ;
// メンバー:fStx,fEtx
#define	FANZ_XTX_NO				0			// STX/ETX未受信
#define	FANZ_XTX_YES			1			// STX/ETX受信済
// メンバー:fCmd
#define	FANZ_CMD_NO				0			// コマンド受信無し
#define	FANZ_CMD_YES			1			// コマンド受信有り
#define	FANZ_RES_NO				0			// Res.無し
#define	FANZ_RES_FRAME			1			// Frame受信
#define	FANZ_RES_ACK			2			// ACK受信
#define	FANZ_RES_NAK			3			// NAK受信
#define	FANZ_RES_TIMEOUT		11			// 受信Timeout



#define	ACC_UART_RES_DELAY		2			// Rx->Tx遅延(100uS)




#endif		// _APP_SB_FHDL_TYPE_DEFINED_
//-----------------------------------------------------------------------
