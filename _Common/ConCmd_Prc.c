//=======================================================================
//
//  FILE        : ConCmd_Prc.c
//  DATE        : 2014.10.29
//  DESCRIPTION : コンソールコマンドプロセッサ 処理
//	Device(s)	: RX210 Series
//				R0.2 2014.10.29 by H.Inoue
//					バリオスクリュー(R8/C)から移植
//
//=======================================================================

#include	<machine.h>						// Include machine.h

#include	"ConCmd_Prc.h"					// アプリケーションヘッダ




//--------------------------------------------------------------------------
//	マクロ定義
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//	定数定義
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//	型定義
//--------------------------------------------------------------------------

//--------------------------------
// 入力ラインバッファ
//--------------------------------
typedef struct _LINE_BUF_ {
		UBYTE			ubCh ;			// SCIチャンネル指定
		UBYTE			ubBufSize ;		// バッファサイズ
		CHAR			*pcLineBuf ;	// 入力ラインバッファPtr
		CHAR			*pcLinePrc ;	// 入力ライン処理Ptr
	} LINE_BUF ;




//-----------------------------------------------------------
//	グローバル変数定義
//-----------------------------------------------------------

// 入力ラインバッファ
static LINE_BUF	mLineBuf ;

// コマンドテーブル定義
static CMD_TBL		*mpCmdTbl ;


//-----------------------------------------------------------
//	関数プロトタイプ
//-----------------------------------------------------------




//===========================================================================


//------------------------------------------------------------------
//****	初期化
//------------------------------------------------------------------

//-----------------------------------------------------------
//	コンソール初期化
//	注:SCIは別途初期化しておくこと
//	引数
//		UBYTE	ubCh			SCIチャンネル指定
//		UBYTE	ubBufSize		入力バッファサイズ
//		CHAR	*pcBufExt		外部のライン入力バッファPtr
//		CMD_TBL	*pCmdTbl		外部のコマンドテーブルPtr
//	戻り値 
//-----------------------------------------------------------
void	xConCmdPrc_Init( 
						UBYTE ubCh, 
						UBYTE ubBufSize,
						CHAR *pcBufExt, 
						const CMD_TBL *pCmdTbl
						)
{
	
	// 入力ラインバッファ
	mLineBuf.ubCh = ubCh ;							// SCIチャンネル指定
	mLineBuf.ubBufSize = ubBufSize ;				// バッファサイズ
	mLineBuf.pcLineBuf = pcBufExt ;					// 入力ラインバッファPtr
	mLineBuf.pcLinePrc = mLineBuf.pcLineBuf ;		// 入力ライン処理Ptr
	
	// コマンドテーブル定義
	mpCmdTbl = (CMD_TBL *)pCmdTbl ;					// CommandTable
	
}


//------------------------------------------------------------------
//****	コンソール(SCI)入出力
//------------------------------------------------------------------

//-----------------------------------------------------------
//	コンソール１文字入力
//	引数
//		UBYTE	ubCh			SCIチャンネル指定
//		BYTE	*pbBufExt		外部のライン入力バッファPtr
//		CMD_TBL	*pCmdTbl		外部のコマンドテーブルPtr
//	戻り値
//		CHAR					入力された文字
//-----------------------------------------------------------
static CHAR	sConInput( UBYTE ubCh )
{
	WORD			wRet ;
	
	
	do {
		wRet = Sci_Getc( ubCh ) ;					// 受信
	} while( wRet == -1 ) ;
	
	
	return (CHAR)wRet ;
	
}


//-----------------------------------------------------------
//	バックスペース処理
//	引数
//		UBYTE	ubCh			SCIチャンネル指定
//	戻り値
//-----------------------------------------------------------
static void	sConBackspace( UBYTE ubCh )
{
	
	Sci_Puts( ubCh, "\b \b" ) ;
	
}


//------------------------------------------------------------------
//****	コマンドライン解析
//------------------------------------------------------------------


//-----------------------------------------------------------
//	小文字を大文字に変換
//	引数
//		CHAR	cChar			変換文字
//	戻り値
//		CHAR					変換された文字
//-----------------------------------------------------------
static CHAR	sToUpper( CHAR cChar )
{
	
	return ('a' <= cChar && cChar<= 'z' ? cChar - 'a' + 'A' : cChar ) ;
	
}


//-----------------------------------------------------------
//	文字列の比較
//	引数
//		CHAR	*pcCmpStr		比較文字列(0x00終端)Ptr
//		CHAR	*pcBuf			比較される文字列Ptr
//	戻り値
//		CHAR *					一致した場合は次の文字Ptr
//								不一致は０を返す
//-----------------------------------------------------------
CHAR	*xStrCmp( CHAR *pcCmpStr, CHAR *pcBuf )
{
	
	
	// 先頭スペースを読み飛ばす
	for( ; *pcCmpStr == ' ' ; pcCmpStr++ ) ;			// スペースを飛ばす
	for( ; *pcBuf == ' ' ; pcBuf++ ) ;					// スペースを飛ばす
	
	// 文字列pcCmpStr全体サーチ
	do {
		if ( *pcBuf++ != *pcCmpStr++ ) {
			return (CHAR *)0 ;						// 異なる
		}
	} while ( *pcCmpStr ) ;	
	
	// 区切りチェック
	if ( (*pcBuf != ' ') && 
		 (*pcBuf != '\t') &&
		 (*pcBuf != NULL) ) {
		return (CHAR *)0 ;							// 区切りでないので異なる
	}
	
	
	return pcBuf ;									// 一致,Ptrを返す
	
}


//-----------------------------------------------------------
//	ライン入力バッファへ１ライン入力
//	注:改行まで制御戻らない
//	引数
//	戻り値
//-----------------------------------------------------------
void	xConLineInput( void )
{
	CHAR			cIn ;							// 入力文字
	CHAR			*pcIn ;							// 入力Ptr
	UBYTE			ubUsedSize ;					// 使用バッファサイズ
	
	
	// 初期化
	pcIn = mLineBuf.pcLinePrc = mLineBuf.pcLineBuf ;		// 入力Ptr
	ubUsedSize = 0 ;										// 使用BufSize
	
	// ライン入力
	while( 1 ) {
		switch ( cIn = sConInput( mLineBuf.ubCh ) ){		// １文字入力
			case '\b' :								// Backspace
				if ( ubUsedSize != 0 ) {					// 行頭以外
					pcIn-- ;
					ubUsedSize-- ;
					sConBackspace( mLineBuf.ubCh ) ;
				}
				break ;
			case '\r' : case '\n' :					// 改行 (終了)
				*pcIn = '\0' ;
				Sci_Puts( mLineBuf.ubCh, "\r\n" ) ;			// 改行
				return ;
			case '\030' :							// LineDelete
				pcIn = mLineBuf.pcLineBuf ;					// 入力Ptr
				for ( ; ubUsedSize != 0 ; ubUsedSize-- ) {
					sConBackspace( mLineBuf.ubCh ) ;
				}
				break ;
			default :
				if ( (cIn >= ' ') || (cIn == '\t') ){		// 有効文字
					if( ubUsedSize < mLineBuf.ubBufSize ) {	// バッファへ格納
						*pcIn++ =  sToUpper( cIn ) ;
						ubUsedSize++ ;
					}
					Sci_Putc( mLineBuf.ubCh, cIn ) ;
				}
		}
		
	}
	
	
}


//---------------------------------------------------
//	ライン入力バッファの先頭ポインタを返す
//	引数
//	戻り値
//---------------------------------------------------
CHAR	*xConGetLineBufPtr( void )
{

	return mLineBuf.pcLineBuf ;

}


//------------------------------------------------------------------
//****	変換
//------------------------------------------------------------------


//---------------------------------------------------
//	アスキーコードを１６進に変換
//	引数
//		CHAR	bAscii			変換するAscii文字
//	戻り値
//		WORD					16進数値, エラー時は -1
//---------------------------------------------------
WORD	xAsc2Hex( CHAR cAscii )
{
	
	
	if( cAscii <= '/' ) {
		return -1 ;										// 16進文字でない
	} else if ( (cAscii -= '0') <= 9 ) {				// 0-9
		return (WORD)cAscii ;							// 16進数値
	} else if ( (cAscii >= 0x11) && (cAscii <= 0x16) ) {// A-F
		return (WORD)(cAscii - 7) ;						// 16進数値
	}
	
	
	return -1 ;											// 16進文字でない
	
}


//---------------------------------------------------
//	入力ラインバッファの中から次の１６進数を読み出す
//	書式は0xN...N
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//		DWORD					16進値
//								変換不可の場合はCMDPRC_ERR_ARGUMENT
//---------------------------------------------------
DWORD	xGetHex( CHAR **ppcCmdLine )
{
	WORD			wVal ;							// Ascii->16進変換値
	DWORD			dwRet ;							// 戻り値
	
	
	// 先頭スペースを読み飛ばす
	for( ; **ppcCmdLine == ' ' ; (*ppcCmdLine)++ ) ;	// スペースを飛ばす
	
	// 0xプリフィックス確認
	if ( (**ppcCmdLine != '0') ||
		 (sToUpper( *(*ppcCmdLine + 1) ) != 'X' ) ) {
		return CMDPRC_ERR_ARGUMENT ;
	}
	*ppcCmdLine += 2 ;
	
	// 先頭の16進値確認
	wVal = xAsc2Hex( **ppcCmdLine ) ;
	if ( wVal == -1 ) {
		return CMDPRC_ERR_ARGUMENT ;
	}
	
	// 16進変換
	dwRet = 0 ;
	while( (**ppcCmdLine != ' ') && 
		   (**ppcCmdLine != '\t') && 
		   (**ppcCmdLine != (CHAR)NULL) ) {
		wVal = xAsc2Hex( **ppcCmdLine ) ;
		if ( wVal == -1 ) {
			return CMDPRC_ERR_ARGUMENT ;
		}
		dwRet = (dwRet << 4) + wVal ;					// 桁上げ
		(*ppcCmdLine)++ ;
	}
	
	
	return dwRet ;
	
}


//---------------------------------------------------
//	16進数字かどうか調べる
//	書式は0xN...N
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//		WORD					16進値の場合は0
//								無効文字は -1
//---------------------------------------------------
WORD	xIsHex( CHAR **ppcCmdLine )
{
	CHAR	*pcBuf ;
	
	
	// スペース読み飛ばし
	xSkipSpace( ppcCmdLine ) ;
	
	// 0xプリフィックス確認
	pcBuf = *ppcCmdLine ;
	if ( (*pcBuf != '0') ||
		 (sToUpper( *(pcBuf + 1) ) != 'X' ) ) {
		return -1 ;
	}
	pcBuf += 2 ;
	
	// 16進数値チェック
	while( (*pcBuf != ' ') && 
		   (*pcBuf != (CHAR)NULL) ) {
		if ( xAsc2Hex( *pcBuf ) == -1 ) {
			return -1 ;
		}
		pcBuf++ ;
	}
	
	
	return 0 ;
	
}


//---------------------------------------------------
//	アスキーコードを10進に変換
//	引数
//		CHAR	bAscii			変換するAscii文字
//	戻り値
//		WORD					10進数値, マイナス時は -1
//								無効文字は CMDPRC_ERR_NOASCII
//---------------------------------------------------
WORD	xAsc2Dec( CHAR cAscii )
{
	
	
	if ( cAscii == '-' ) {
		return -1 ;
	}
	if ( cAscii <= '/' ) {
		return CMDPRC_ERR_NOASCII ;
	}
	if ( (cAscii -= '0') <= 9 ) {
		return (WORD)cAscii ;
	}
	
	
	return CMDPRC_ERR_NOASCII ;
	
}


//---------------------------------------------------
//	入力ラインバッファの中から次の10進数を読み出す
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//		DWORD					10進値
//								無効文字は CMDPRC_ERR_ARGUMENT
//---------------------------------------------------
DWORD	xGetDec( CHAR **ppcCmdLine )
{
	WORD			wVal ;							// Ascii->16進変換値
	WORD			wSign ;							// 符号値
	DWORD			dwRet ;							// 戻り値
	
	
	// 先頭スペースを読み飛ばす
	for( ; **ppcCmdLine == ' ' ; (*ppcCmdLine)++ ) ;	// スペースを飛ばす
	
	// 先頭の符号確認
	wVal = xAsc2Dec( **ppcCmdLine ) ;
	if ( wVal == CMDPRC_ERR_NOASCII ) {
		return CMDPRC_ERR_ARGUMENT ;
	} else if ( wVal == -1 ) {
		wSign = -1 ;									// 符号(マイナス)
		(*ppcCmdLine)++ ;								// NextPtr
	} else {
		wSign = 1 ;										// 符号(プラス)
	}
	
	// 10進数値変換
	dwRet = 0 ;											// 初期値
	while( (**ppcCmdLine != ' ') && 
		   (**ppcCmdLine != '\t') && 
		   (**ppcCmdLine != (CHAR)NULL) ) {
		wVal = xAsc2Dec( **ppcCmdLine ) ;
		if ( wVal < 0 ) {
			return CMDPRC_ERR_ARGUMENT ;
		} else {
			dwRet = dwRet * 10 + wVal ;					// 桁上げ
		}
		(*ppcCmdLine)++ ;
	}
	
	
	return dwRet * wSign ;
	
}


//---------------------------------------------------
//	10進数字かどうか調べる
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//		WORD					10進値の場合は0
//								無効文字は -1
//---------------------------------------------------
WORD	xIsDecimal( CHAR **ppcCmdLine )
{
	CHAR	*pcBuf ;
	
	
	// スペース読み飛ばし
	xSkipSpace( ppcCmdLine ) ;
	
	// 10進数値チェック
	pcBuf = *ppcCmdLine ;
	while( (*pcBuf != ' ') && 
		   (*pcBuf != '\t') && 
//		   (*pcBuf != '.') && 
		   (*pcBuf != (CHAR)NULL) ) {
		if ( xAsc2Dec( *pcBuf ) == CMDPRC_ERR_NOASCII ) {
			return -1 ;
		}
		pcBuf++ ;
	}
	
	
	return 0 ;
	
}


//---------------------------------------------------
//	入力ラインバッファのスペースをスキップ
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//---------------------------------------------------
void	xSkipSpace( CHAR **ppcCmdLine )
{
	
	for( ; (**ppcCmdLine == ' ') || (**ppcCmdLine == '\t') ; (*ppcCmdLine)++ ) ;
	
}


//---------------------------------------------------
//	スペース(ブランク), タブかチェック
//	*ppcCmdLineのポインタは進めない
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//		WORD					0:スペース又はタブである
//								-1:それ以外
//---------------------------------------------------
WORD	xIsSpace( CHAR **ppcCmdLine )
{
	
	// スペース(ブランク), タブかチェック
	if ( (**ppcCmdLine != ' ') &&
		 (**ppcCmdLine != '\t') ) {
		return -1 ;
	}
	
	
	return 0 ;
	
}


//---------------------------------------------------
//	行末かチェック
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//		WORD					0:行末である
//								-1:それ以外
//---------------------------------------------------
WORD	xIsLineEnd( CHAR **ppcCmdLine )
{
	
	// 行末チェック
	if ( **ppcCmdLine != '\0' ) {
		return -1 ;
	}
	
	return 0 ;
	
}


//---------------------------------------------------
//	入力ラインバッファの中から次の数値を読み出す
//	書式は10進数又は16進数(0xN...N)
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//		DWORD					数値
//								変換不可の場合はCMDPRC_ERR_ARGUMENT
//---------------------------------------------------
DWORD	xGetNumeric( CHAR **ppcCmdLine )
{
	DWORD			dwRet ;							// 戻り値
	WORD			wRet ;							// 戻り値
	
	
	// 数値読み取り
	dwRet = xGetHex( ppcCmdLine ) ;							// Hex読取
	if ( dwRet == CMDPRC_ERR_ARGUMENT ) {
		dwRet = xGetDec( ppcCmdLine ) ;						// Dec読取
	}
	if ( dwRet == CMDPRC_ERR_ARGUMENT ) {
		return dwRet ;
	}
	
	// ブランク/行末チェックして文字列の一部でないことを確認
	wRet = xIsSpace( ppcCmdLine ) ;
	if ( wRet != 0 ) {										// ブランクでない
		wRet = xIsLineEnd( ppcCmdLine ) ;
		if ( wRet != 0 ) {									// 行末でない
			dwRet = CMDPRC_ERR_ARGUMENT ;
		}
	}
	
	
	return dwRet ;
	
}


//------------------------------------------------------------------
//****	解析&実行
//------------------------------------------------------------------


//---------------------------------------------------
//	行末かチェック
//	引数
//		CHAR	*pcOpt			入力チェックオプション文字列
//	戻り値
//		WORD					0:オプション入力した
//								-1:オプション入力されなかった
//---------------------------------------------------
WORD	xConOptInput( CHAR *pcOpt )
{
	CHAR		*pcBuf ;						// 次の入力バッファPtr
	
	
	// オプション入力
	xConLineInput( ) ;									// ライン入力
	if ( *mLineBuf.pcLineBuf == '\0' ) {				// 行データ無し
		return -1 ;
	}
	
	// オプション値チェック
	pcBuf = xStrCmp( pcOpt, mLineBuf.pcLinePrc ) ;
	if ( pcBuf == (CHAR *)NULL ) {						// 指定した入力無し
		return -1 ;
	}
	
	
	return 0 ;
	
}


//---------------------------------------------------
//	コンソールコマンド解析&実行
//	引数
//	戻り値
//		FLAG
//				CMDPRC_EXE_EXEC_NO		無効コマンド
//				CMDPRC_EXE_EXEC_YES		コマンドが実行された
//				CMDPRC_EXE_EXIT_YES		終了(EXITコマンド発行)
//---------------------------------------------------
FLAG	xConCmdProc_Exe( void )
{
	CMD_TBL		*pCmdTbl ;						// コマンドテーブル定義
	CHAR		*pcBuf ;						// 次の入力バッファPtr
	
	
	// コマンド入力
	Sci_PutsText( mLineBuf.ubCh, ":" ) ;				// プロンプト表示
	xConLineInput( ) ;									// ライン入力
//	Sci_Puts( mLineBuf.ubCh, "\r\n" ) ;					// 改行
	if ( *mLineBuf.pcLineBuf == '\0' ) {				// 行データ無し
		return CMDPRC_EXE_EXEC_NO ;
	}
	
	// Exitコマンドチェック
	pcBuf = mLineBuf.pcLineBuf ;						// 次の入力バッファPtr
	pcBuf = xStrCmp( "EXIT", mLineBuf.pcLinePrc ) ;
	if ( pcBuf != (CHAR *)NULL ) {
		return CMDPRC_EXE_EXIT_YES ;
	}
	
	// 登録コマンド検索
	mLineBuf.pcLinePrc = mLineBuf.pcLineBuf ;			// 入力ライン処理Ptr
	pCmdTbl = mpCmdTbl ;								// コマンドテーブル定義
	for ( ; *(pCmdTbl->pcCmd) != (CHAR)NULL ; pCmdTbl++ ) {
		pcBuf=xStrCmp( pCmdTbl->pcCmd, mLineBuf.pcLinePrc ) ;
		if ( pcBuf != (CHAR *)0 ) {
			break ;		// コマンドテーブルにあった
		}
	}
	
	// コマンド実行
	if ( *pCmdTbl->pcCmd != 0 ) {
		xSkipSpace( &pcBuf ) ;
		mLineBuf.pcLinePrc = pcBuf ;					// 入力ライン処理Ptr
		(*pCmdTbl->pFunc)( &mLineBuf.pcLinePrc ) ;		// コマンド処理
		Sci_Puts( mLineBuf.ubCh, "\r\n" ) ;				// 改行
		return CMDPRC_EXE_EXEC_YES ;
	} else {
		(*pCmdTbl->pFunc)( &mLineBuf.pcLinePrc ) ;		// コマンド処理
		Sci_Puts( mLineBuf.ubCh, "\r\n" ) ;				// 改行
	}
	
	
	return CMDPRC_EXE_EXEC_NO ;
	
}


//===========================================================================
