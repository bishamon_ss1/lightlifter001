//-----------------------------------------------------------------------
//	変数型定義,定数定義
//-----------------------------------------------------------------------



#ifndef _TYPE_DEFINED_			// 多重定義予防
#define _TYPE_DEFINED_

#include	<stdint.h>					// Include stdint.h
#include	<stdbool.h>					// Include stdbool.h



//-------------------------------------------------
//	追加の型定義
//-------------------------------------------------
#ifndef	_DEFINE_BYTE_
	#define	_DEFINE_BYTE_
	typedef		signed		char 		BYTE ;		// b
	#define	_DEFINE_WORD_
	typedef		signed		short		WORD ;		// w
	#define	_DEFINE_LWORD_
	typedef		signed		long		LWORD ;		// lw
	#define	_DEFINE_DWORD_
	typedef		signed		long		DWORD ;		// dw
	#define	_DEFINE_QWORD_
	typedef		signed		long long	QWORD ;		// qw
	#define	_DEFINE_FLAG_
	typedef		unsigned	char	 	FLAG ;		// f
	#define	_DEFINE_CHAR_
	typedef		signed		char	 	CHAR ;		// c
	#define	_DEFINE_UBYTE_
	typedef		unsigned	char	 	UBYTE ;		// ub
	#define	_DEFINE_UWORD_
	typedef		unsigned	short		UWORD ;		// uw
	#define	_DEFINE_ULWORD_
	typedef		unsigned	long		ULWORD ;	// ulw
	#define	_DEFINE_UDWORD_
	typedef		unsigned	long		UDWORD ;	// udw
	#define	_DEFINE_UQWORD_
	typedef		unsigned	long long	UQWORD ;	// uqw
	#define	_DEFINE_FLOAT_
	typedef		float				 	FLOAT ;		// fl
#endif

#ifndef	_DEFINE_TRUE_
	#define	_DEFINE_TRUE_
	#define	TRUE	0
	#define	FALSE	1
#endif

#ifndef	_BOOL_
	#ifdef	bool
		#define	_BOOL_
		typedef		bool				BOOL ;		// bl
		#ifdef	_DEFINE_TRUE_
			#undef	TRUE
			#undef	FALSE
		#endif
		#define	TRUE	true
		#define	FALSE	false
	#endif
#endif

#ifndef	_DEFINE_PORTDIR_
	#define	_DEFINE_PORTDIR_
	#define	PORTDIR_OUT		1
	#define	PORTDIR_IN		0
	#define	INP_BUF_ON		1
	#define	INP_BUF_OFF		0
	#define	PORT_PMR_IOP	0			// PMR:IOポートとして使用
	#define	PORT_PMR_PHE	1			// PMR:周辺機能として使用
#endif

#ifndef	_DEFINE_NULL_
	#define	_DEFINE_NULL_ 
	#define	NULL			0
#endif




//--------------------------------
// ビットフィールド
//--------------------------------
typedef struct _ST_BIT8_ {
		UBYTE	B0:1 ;		// Bit 0 LSB
		UBYTE	B1:1 ;		// Bit 1
		UBYTE	B2:1 ;		// Bit 2
		UBYTE	B3:1 ;		// Bit 3
		UBYTE	B4:1 ;		// Bit 4
		UBYTE	B5:1 ;		// Bit 5
		UBYTE	B6:1 ;		// Bit 6
		UBYTE	B7:1 ;		// Bit 7 MSB
	} ST_BIT8 ;

typedef union _UN_BIT_ {
		UBYTE		ubData ;
		ST_BIT8		BIT ;
		UBYTE		ub07:7 ;	// チェックフィールド
		UBYTE		ub06:6 ;	// チェックフィールド
	} UN_BIT ;

typedef struct _ST_BIT16_ {
		UBYTE	B00:1 ;		// Bit 0 LSB
		UBYTE	B01:1 ;		// Bit 1
		UBYTE	B02:1 ;		// Bit 3
		UBYTE	B03:1 ;		// Bit 4
		UBYTE	B04:1 ;		// Bit 5
		UBYTE	B05:1 ;		// Bit 6
		UBYTE	B06:1 ;		// Bit 6
		UBYTE	B07:1 ;		// Bit 7
		UBYTE	B08:1 ;		// Bit 8
		UBYTE	B09:1 ;		// Bit 9
		UBYTE	B10:1 ;		// Bit10
		UBYTE	B11:1 ;		// Bit11
		UBYTE	B12:1 ;		// Bit12
		UBYTE	B13:1 ;		// Bit13
		UBYTE	B14:1 ;		// Bit14
		UBYTE	B15:1 ;		// Bit15 MSB
	} ST_BIT16 ;
typedef union _UN_BIT16_ {
		UWORD		uwData ;
		ST_BIT16	BIT ;
	} UN_BIT16 ;


//--------------------------------
// I/Oポート状態
//--------------------------------

#define	EXTOUT_OFF			0						// 出力 OFF
#define	EXTOUT_ON			1						// 出力 ON
#define	EXTOUT_LOW			0						// 出力 Low
#define	EXTOUT_HIGH			1						// 出力 High

#define	PORT_IN_LOW			0						// 入力 0
#define	PORT_IN_HIGH		1						// 入力 1



#endif		// _TYPE_DEFINED_
//-----------------------------------------------------------------------

