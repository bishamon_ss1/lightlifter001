/******************************************************************************
* DISCLAIMER
* Please refer to http://www.renesas.com/disclaimer
******************************************************************************
  Copyright (C) 2010. Renesas Electronics Corporation. All rights reserved.
*******************************************************************************
* File Name    : r_apn_iic_eep.h
* Version      : 1.00
* Description  : RIIC program header
******************************************************************************
* History
* Aug 01 '11  H.Inoue	RX62Nボード用にポーティング
*         : 15.07.2010 1.00    First Release
******************************************************************************/
#ifndef _I2C_DEF_
#define _I2C_DEF_

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include	"_iodefine.h"
#include	<stdint.h>
#include	<stdbool.h>

#include	"TypeDef.h"					// アプリケーション用型定義



//--------------------------------------------------------------------------
//	マクロ定義
//--------------------------------------------------------------------------

// 動作周波数PCLK(MHz)指定
#define		PCLK					25			// PCLK=25MHz

// I2C使用チャンネル指定
#define	RIIC_CH0					// When use channel0, define 'RIIC_CH0'

// Users don't have to change the below defines.
//#ifdef	RIIC_CH0
	#define M_IIC			RIIC0				// I2C使用チャンネル
	#define ICEEI			ICEEI0				// 通信エラー割込
	#define ICRXI			ICRXI0				// 受信データフル割込
	#define ICTXI			ICTXI0				// 送信データエンプティ割込
	#define ICTEI			ICTEI0				// 送信終了割込
	#define	RIIC_SDA_PDR	PORT1.PDR.BIT.B7	// SDAn PDR
	#define	RIIC_SCL_PDR	PORT1.PDR.BIT.B6	// SCLn PDR
	#define	RIIC_SDA_PODR	PORT1.PODR.BIT.B7	// SDAn PODR
	#define	RIIC_SCL_PODR	PORT1.PODR.BIT.B6	// SCLn PODR
	#define	RIIC_SDA_PIDR	PORT1.PIDR.BIT.B7	// SDAn PIDR
	#define	RIIC_SCL_PIDR	PORT1.PIDR.BIT.B6	// SCLn PIDR
	#define	RIIC_SDA_PMR	PORT1.PMR.BIT.B7	// SDAn PMR
	#define	RIIC_SCL_PMR	PORT1.PMR.BIT.B6	// SCLn PMR
	#define	RIIC_SDA_MPC	MPC.P17PFS.BIT.PSEL	// SDAn MPC
	#define	RIIC_SCL_MPC	MPC.P16PFS.BIT.PSEL	// SCLn MPC
/*
#else
	#define M_IIC			RIIC1				// I2C使用チャンネル
	#define ICEEI			ICEEI1				// 通信エラー割込
	#define ICRXI			ICRXI1				// 受信データフル割込
	#define ICTXI			ICTXI1				// 送信データエンプティ割込
	#define ICTEI			ICTEI1				// 送信終了割込
	#define	RIIC_SDA_PORT	PORT2.PORT.BIT.B0	// SDAn PORT
	#define	RIIC_SDA_DR		PORT2.DR.BIT.B0		// SDAn DR
	#define	RIIC_SDA_DDR	PORT2.DDR.BIT.B0	// SDAn DDR
	#define	RIIC_SDA_ICR	PORT2.ICR.BIT.B0	// SDAn ICR
	#define	RIIC_SDA_ODR	PORT2.ODR.BIT.B0	// SDAn ODR
	#define	RIIC_SCL_PORT	PORT2.PORT.BIT.B1	// SCLn PORT
	#define	RIIC_SCL_DR		PORT2.DR.BIT.B1		// SCLn DR
	#define	RIIC_SCL_DDR	PORT2.DDR.BIT.B1	// SCLn DDR
	#define	RIIC_SCL_ICR	PORT2.ICR.BIT.B1	// SCLn ICR
	#define	RIIC_SCL_ODR	PORT2.ODR.BIT.B1	// SCLn ODR
#endif
*/

// 割込レベル
#define		I2C_INT_LEVEL			4			// I2C割込レベル



//--------------------------------------------------------------------------
//	変数型定義
//--------------------------------------------------------------------------
// I2C送受信バッファサイズ 必要に応じて調整すること
#define		I2C_REG_BUF_SIZE			2	// レジスタバッファサイズ
#define		I2C_DATA_BUF_SIZE			8	// データバッファサイズ

// I2C送受信情報
typedef struct _I2C_TFR_INF_ {
		UBYTE		ubSlvAdr ;						// スレーブアドレス
													// (bit0は'0'のこと)
		UBYTE		ubRegBufSize ;					// 実Reg.バッファサイズ
		UBYTE		ubRegBuf[I2C_REG_BUF_SIZE] ;	// Reg.バッファ
		UBYTE		ubRegBufPtr ;					// Reg.バッファPTR
		UBYTE		ubDataBufSize ;					// 実データバッファサイズ
		UBYTE		ubDataBuf[I2C_DATA_BUF_SIZE] ;	// データバッファ
		UBYTE		ubDataPtr ;						// NextデータバッファPTR
	} I2C_TFR_INF ;


// 関数I2C_TfrWrite(), I2C_TfrRead()の戻り値
typedef enum _I2C_TFR_FNC_ {
		I2C_API_OK ,
		I2C_API_BUS_BUSY ,
		I2C_API_MODE_ERROR ,
		I2C_API_PERM_ERROR
	} I2C_TFR_FNC ;


// Return value for function IIC_GetStatus()
typedef enum _I2C_STATUS_ {
		I2C_STAT_IDLE ,							// アイドル中
		I2C_STAT_ON_COMM ,						// 通信中
		I2C_STAT_NACK ,							// NACK検出した
		I2C_STAT_ERR_TIMEOUT ,					// バスクロックタイムアウト
		I2C_STAT_ERR_ARB_LOST ,					// バス占有権が奪われた
		I2C_STAT_ERR_SCL_LOCK ,					// 他デバイスがSCL=Lowにしている
		I2C_STAT_ERR_SDA_LOCK ,					// 他デバイスがSDA=Lowにしている
		I2C_STAT_ERR_DETECT_STOP ,				// 通信中にSTOPを検出
		I2C_STAT_FAILED							// その他のエラー
	} I2C_STATUS ;


// Return value for function I2C_GetStatus()
typedef enum _I2C_BUS_STAT_ {
		I2C_BUS_STAT_FREE ,
		I2C_BUS_STAT_BUSY
	} I2C_BUS_STAT ;


#ifdef		_I2C_DRIVER_
	// 内部作動モード
	typedef enum _I2C_INTERNAL_MODE_ {
			I2C_MODE_IDLE ,
			I2C_MODE_TFR_READ ,
			I2C_MODE_TFR_WRITE
		} I2C_INTERNAL_MODE ;

	// I2Cドライバ内部情報
	typedef struct _I2C_DRV_INF_ {
			I2C_INTERNAL_MODE	Mode ;			// 内部作動モード
			I2C_STATUS 			Stat ;			// I2C_GetStatus() Stat buffer
			UDWORD				udwTxBytes ;	// Tx 転送カウンタ
			UDWORD				udwRxBytes ;	// Rx 転送カウンタ
		} I2C_DRV_INF ;

	// IIC_Error() の引数
	typedef enum _I2C_ERR_CODE_ {
			I2C_ERR_MODE_SP_INT ,				// 他のデバイスがSTOP発行した
			I2C_ERR_MODE_RX_INT_TFR_WRITE ,
			I2C_ERR_MODE_RX_INT ,
			I2C_ERR_MODE_TX_INT ,
			I2C_ERR_MODE_TE_INT ,
			I2C_ERR_SCL_LOCK ,					// 他デバイスがSCL=Lowにしている
			I2C_ERR_SDA_LOCK ,					// 他デバイスがSDA=Lowにしている
			I2C_ERR_GEN_CLK_BBSY
		} I2C_ERR_CODE ;

#endif		// _I2C_DRIVER_

//--------------------------------------------------------------------------
//	グローバル変数,関数プロトタイプ宣言
//--------------------------------------------------------------------------
//------------------------------
// グローバル変数
//------------------------------

//------------------------------
// 関数プロトタイプ
//------------------------------

//-----------------------------------------------------------
//	RIICモジュールとI/Oポート初期化
//	引数
//	戻り値
//-----------------------------------------------------------
void	I2C_InzModule( void ) ;

//===========================================================
//	I2Cバス初期化
//	引数
//	戻り値
//===========================================================
void			I2C_Create( void ) ;

//===========================================================
//	I2Cバス破棄
//	引数
//	戻り値
//===========================================================
void			I2C_Destroy( void ) ;

//-----------------------------------------------------------------------------
//	Function Name:	I2C_TfrWrite
//	Description  :	Start device write process or Acknowledge polling.
//					I2cWrtData.ubRegBufSize == I2cWrtData.ubRegBufPtr &
//					I2cWrtData.ubDataBufSize == I2cWrtData.ubDataPtr
//					になったらACKポーリングする
//	Argument     :	*pI2cWrtData
//						ubSlvAdr				Slave Device Address.
//												bit0はR/Wビットなので設定禁止
//						ubRegBufSize			実Reg.バッファサイズ
//						ubRegBuf[2]				Reg.バッファ
//						ubRegBufPtr				Reg.バッファPTR
//						ubDataBufSize 			実データバッファサイズ
//						ubDataBuf[]				データバッファ
//						ubDataPtr				NextデータバッファPTR
//	Return Value :
//					I2C_API_OK				Success
//					I2C_API_MODE_ERROR		Running I2C on other mode
//					I2C_API_PERM_ERROR		Parameter error
//					I2C_API_BUS_BUSY		I2C Bus is busy
//-----------------------------------------------------------------------------
I2C_TFR_FNC		I2C_TfrWrite( I2C_TFR_INF * ) ;

//-----------------------------------------------------------------------------
//	Function Name:	I2C_TfrRead
//	Description  :	デバイスのレジスタ読取を開始する
//	Argument     :	*pI2cWrtData
//						ubSlvAdr				Slave Device Address.
//												bit0はR/Wビットなので設定禁止
//						ubRegBufSize			実Reg.バッファサイズ
//						ubRegBuf[]				Reg.バッファ
//						ubRegBufPtr				Reg.バッファPTR
//						ubDataBufSize 			実データバッファサイズ
//						ubDataBuf[]				データバッファ
//						ubDataPtr				NextデータバッファPTR
//	Return Value :
//					I2C_API_OK				Success
//					I2C_API_MODE_ERROR		Running IIC on other mode
//					I2C_API_PERM_ERROR		Parameter error
//					I2C_API_BUS_BUSY		I2C Bus is busy
//-----------------------------------------------------------------------------
I2C_TFR_FNC		I2C_TfrRead( I2C_TFR_INF * ) ;

//-----------------------------------------------------------------------------
//	Function Name:	I2C_GetStatus
//	Description  :	Get I2C status
//	Argument     : 
//					*pI2cStat -- Pointer for I2C communication status buffer
//						I2C_STAT_IDLE			I2C is Idle mode.
//												通信開始可能
//						I2C_STAT_ON_COMM		I2C is on communication.
//												通信開始できない
//						I2C_STAT_NACK			I2C has received NACK.
//												次の通信開始可能
//					 	I2C_STAT_FAILED			I2IC has stopped.
//												次の通信開始可能だが
//												前回の通信でエラーがあった
//					*pI2cBusStat -- Pointer for I2C Bus status buffer
//						I2C_BUS_STAT_FREE		I2C bus is free
//						I2C_BUS_STAT_BBSY		I2C bus is busy
//	Return Value :
//-----------------------------------------------------------------------------
void			I2C_GetStatus( I2C_STATUS *, I2C_BUS_STAT * ) ;


#endif	// _I2C_DEF_

