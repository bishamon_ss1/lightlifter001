/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2013 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* History      : DD.MM.YYYY Version  Description
*              : 01.03.2013 1.00     First Release
*				2014.05.19	V1.01 by H.Inoue
*							SPIシングルマスタ8bitアクセス
*							RX210社内用にポーティング
*							CS信号にSSL0を使用(LowActive)
*							 SSLA0(CS)  :PC4
*							 RSPCKA(SCK):PC5
*							 MOSIA(DO)  :PC6
*							 MISOA(DI)  :PC7
*******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include	<machine.h>

#define		_SPI_DRIVER_
#include	"SPI_RX210.h"
#include	"SPI_RX210_Conf.h"


//--------------------------------------------------------------------------
//	マクロ定義
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//	変数型定義
//--------------------------------------------------------------------------


//--------------------------------------------------------------------------
//	グローバル変数,関数プロトタイプ宣言
//--------------------------------------------------------------------------
//------------------------------
// グローバル変数
//------------------------------
// 送受信バッファ構造体
TRAN_BUF		gTxb ;					// 送信バッファ
TRAN_BUF		gRxb ;					// 受信バッファ

// コールバック関数
PcbFunck		gpcbfEnd ;				// 転送終了処理関数
PcbFunck		gpcbfErr ;				// 受信エラー処理関数

// 通信ステータス構造体
TRAN_STAT		gTranStat ;				// 通信状況

//------------------------------
// 関数プロトタイプ
//------------------------------



//==========================================================================

//-----------------------------------------------------------
//	SPIを初期化
//	引数
//	戻り値
//-----------------------------------------------------------
void	SPI_Inz( void )
{
	volatile UBYTE		ubDummy;
	
	
	// ** RSPI割り込み設定
	// 安全の為に割込フラグをクリアする(macro)
	IR( RSPI0, SPRI0 ) = 0 ;							// RSPI0 SPRI
	while ( IR( RSPI0, SPRI0 ) != 0 ) {					// RSPI0 SPRI 更新待ち
		nop( ) ;
	}
	IR( RSPI0, SPTI0 ) = 0 ;							// RSPI0 SPTI
	while ( IR( RSPI0, SPTI0 ) != 0 ) {					// RSPI0 SPTI 更新待ち
		nop( ) ;
	}
	IR( RSPI0, SPEI0 ) = 0 ;							// RSPI0 SPEI
	while ( IR( RSPI0, SPEI0 ) != 0 ) {					// RSPI0 SPEI 更新待ち
		nop( ) ;
	}
	IR( RSPI0, SPII0 ) = 0 ;							// RSPI0 SPII
	while ( IR( RSPI0, SPII0 ) != 0 ) {					// RSPI0 SPII 更新待ち
		nop( ) ;
	}
	// 割込禁止(macro)
	IEN( RSPI0, SPRI0 ) = 0 ;							// RSPI0 SPRI
	IEN( RSPI0, SPTI0 ) = 0 ;							// RSPI0 SPTI
	IEN( RSPI0, SPEI0 ) = 0 ;							// RSPI0 SPEI
	IEN( RSPI0, SPII0 ) = 0 ;							// RSPI0 SPII
	
	// ** RSPI モジュール起動
	SYSTEM.PRCR.WORD = 0xa502 ;							// MSTPmnを書込可へ
	MSTP( RSPI0 ) = 0 ;									// Wakeup RSPI0 (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;							// 書き込み不可へ
	
	// RSPIを一旦停止
	RSPI0.SPCR.BIT.SPE = 0 ;							// RSPI停止
	while ( RSPI0.SPCR.BIT.SPE != 0 ) { nop( ) ; }		// 待ち
	
	// スレーブセレクト極性をLowActive
	RSPI0.SSLP.BYTE = 0x00 ;							// All Low-Active
	
	// MOSIA(DO)はアイドル時にHigh
	RSPI0.SPPCR.BIT.SPLP = 0 ;							// LoopBack禁止
	RSPI0.SPPCR.BIT.SPLP2 = 0 ;							// LoopBack禁止
	RSPI0.SPPCR.BIT.MOIFV = 1 ;							// Idle時MOSIA(DO)Hi
	RSPI0.SPPCR.BIT.MOIFE = 1 ;							// Idle時MOIFVに従う
	
	// ビットレートレジスタ設定
	// BPS=f(PCLKB)/(2*(n+1)*2^N n:SPBR,N:SPCMDm.BRDV BPS<=8Mbps
    // Bit Rate: 25MHz/(2*(250-1+1)*8)=6.25kbps
//	RSPI0.SPBR = 0x00 ;									// Base-BPS=12.5Mbps
//	RSPI0.SPBR = 0x05 ;									// Base-BPS=521kbps
//	RSPI0.SPBR = 250 - 1 ;								// Base-BPS=521kbps
	RSPI0.SPBR = RSPIC_SPBR ;							// Base-BPS
	
	// データコントロールレジスタ設定
	RSPI0.SPDCR.BIT.SPFC = 0x00 ;						// フレーム数=1
	RSPI0.SPDCR.BIT.SPRDTD = 0 ;						// SPDRはRXbuf読出
	RSPI0.SPDCR.BIT.SPLW = 1 ;							// SPDRはLongWord読出
	
	// シーケンス制御設定
	RSPI0.SPSCR.BIT.SPSLN = 0 ;							// SPCMD0 only
	
	// RSPCKクロック遅延
	RSPI0.SPCKD.BIT.SCKDL = 0x00 ;						// 1RSPCK
	
	// SSLi(CS)ネゲート遅延設定
	RSPI0.SSLND.BIT.SLNDL = 0 ;							// 1RSPCK
	
	// SSLi(CS)次アクセス遅延設定
//	RSPI0.SPND.BIT.SPNDL = 0x00 ;						// 1RSPCK+2PCLK
	RSPI0.SPND.BIT.SPNDL = 0x03 ;						// 4RSPCK+2PCLK
	
	// 制御レジスタ２設定
	RSPI0.SPCR2.BIT.SPPE = 0 ;							// Parity None
	RSPI0.SPCR2.BIT.SPOE = 0 ;							// Even Parity
	RSPI0.SPCR2.BIT.SPIIE = 0 ;							// Idle Int. Disable
	RSPI0.SPCR2.BIT.PTE = 0 ;							// Parity SelfCheck No.
	
	// コマンドレジスタ設定 
	// 注意：この設定は仮で最終的には SPI_StartTrans()内で設定する
	//	B0		0:偶数Edgeでデータ変化,奇数Edgeでデータサンプル
	//			1:奇数Edgeでデータ変化,偶数Edgeでデータサンプル
	RSPI0.SPCMD0.BIT.CPHA = 1 ;							// RSPCK位相設定
	//	B1		0:アイドル時RSPCK=Low
	//			1:アイドル時RSPCK=High
	RSPI0.SPCMD0.BIT.CPOL = 1 ;							// RSPCK極性設定
	//	B2-3	0:1/1*B-BPS, 1:1/2*B-BPS, 2:1/4*B-BPS, 3:1/8*B-BPS
	RSPI0.SPCMD0.BIT.BRDV = RSPIC_BRDV ;				// BPS分周設定
	//	B4-6	0:SSL0, 1:SSL1, 2;SSL2, 3:SSL3
	RSPI0.SPCMD0.BIT.SSLA = 0 ;							// SSLnアサート設定
	//	B7		0:転送終了時全SSLネゲート, 1:次アクセス開始まで保持
	RSPI0.SPCMD0.BIT.SSLKP = 1 ;						// SSLnレベル保持設定
	//	B8-11	0x04-0x07:8bit, 0x0f:16bit, 0x01:24bit, 0x02/0x03:32bit
	RSPI0.SPCMD0.BIT.SPB = 0x07 ;						// データ長
	//	B12		0:MSB-first, 1:LSB-first
	RSPI0.SPCMD0.BIT.LSBF = 0 ;							// LSB指定
	//	B13		0:1RSPCK+2PCLK, 1:SPNDの設定に従う
	RSPI0.SPCMD0.BIT.SPNDEN = 0 ;						// 次アクセス遅延
	//	B14		0:1RSPCK, 1:SSLNDの設定に従う
	RSPI0.SPCMD0.BIT.SLNDEN = 0 ;						// SSLネゲート遅延
	//	B15		0:1RSPCK, 1:SPCKDの設定に従う
	RSPI0.SPCMD0.BIT.SCKDEN = 0 ;						// RSPCK遅延
	
	
	// ** RSPI I/Oポート設定
	//	PMR:ポートモードを一旦I/Oへ
	PORTC.PMR.BIT.B4 = 0 ;								// SSLA0(PC4)-CS
	PORTC.PMR.BIT.B5 = 0 ;								// RSPCKA(PC5)-SCK
	PORTC.PMR.BIT.B6 = 0 ;								// MOSIA(PC6)-DO
	PORTC.PMR.BIT.B7 = 0 ;								// MISOA(PC7)-DI
	//	ODR0/1:オープンドレイン出力へ
	PORTC.ODR1.BIT.B0 = 1 ;								// SSLA0(PC4)-CS
	PORTC.ODR1.BIT.B2 = 1 ;								// RSPCKA(PC5)-SCK
	PORTC.ODR1.BIT.B4 = 1 ;								// MOSIA(PC6)-DO
	//	PmnPFSへの書き込みを許可する
	MPC.PWPR.BIT.B0WI  = 0 ;							// PFSWE書込許可
	MPC.PWPR.BIT.PFSWE = 1 ;							// PFSreg書込許可
	//	PmnPFS:RSPI端子割当
	MPC.PC4PFS.BIT.PSEL = 0x0d ;						// SSLA0(PC4)-CS
	MPC.PC5PFS.BIT.PSEL = 0x0d ;						// RSPCKA(PC5)-SCK
	MPC.PC6PFS.BIT.PSEL = 0x0d ;						// MOSIA(PC6)-DO
	MPC.PC7PFS.BIT.PSEL = 0x0d ;						// MISOA(PC7)-DI
	// PmnPFSへの書き込みを禁止する
	MPC.PWPR.BIT.PFSWE = 0 ;							// PFSreg書込禁止
	MPC.PWPR.BIT.B0WI  = 1 ;							// PFSWE書込禁止
	//	PMR:ポートモードを周辺機能にする
	PORTC.PMR.BIT.B4 = 1 ;								// SSLA0(PC4)-CS
	PORTC.PMR.BIT.B5 = 1 ;								// RSPCKA(PC5)-SCK
	PORTC.PMR.BIT.B6 = 1 ;								// MOSIA(PC6)-DO
	PORTC.PMR.BIT.B7 = 1 ;								// MISOA(PC7)-DI
	
	
	// ** RSPI割り込み設定
	// 割込レベル設定
	IPR(RSPI0,    ) = RSPI_INT_LEVEL ;					// 割込レベル
	// 安全の為に割込フラグをクリアする(macro)
	IR( RSPI0, SPRI0 ) = 0 ;							// RSPI0 SPRI
	IR( RSPI0, SPTI0 ) = 0 ;							// RSPI0 SPTI
	IR( RSPI0, SPEI0 ) = 0 ;							// RSPI0 SPEI
	IR( RSPI0, SPII0 ) = 0 ;							// RSPI0 SPII
	
	
	// ** RSPI制御レジスタ設定	機能設定に関するもののみ
	//	B3 MSTR:1-MasterMode
	//	B2 MODFEN:0-モードフォルトエラー検出禁止
	//	B1 TXMD:0-全２重
	//	B0 SPMS:0-SPI動作(4wire)
//	RSPI0.SPCR.BYTE = 0x08 ;							// RSPI-Ctrl
	RSPI0.SPCR.BIT.SPRIE = 0 ;							// 受信Int.禁止
	RSPI0.SPCR.BIT.SPTIE = 0 ;							// 送信Int.禁止
	RSPI0.SPCR.BIT.SPEIE = 0 ;							// ErrInt.禁止
	RSPI0.SPCR.BIT.SPE = 0 ;							// RSPI無効
	RSPI0.SPCR.BIT.SPMS = 0 ;							// SPIモード
	RSPI0.SPCR.BIT.TXMD = 0 ;							// 全二重
	RSPI0.SPCR.BIT.MODFEN = 0 ;							// ModeFaultErr検出禁止
	RSPI0.SPCR.BIT.MSTR = 1 ;							// MasterMode
	
	// 割り込み関係は転送指示時に設定する
	
	// Read RSPI control register (SPCR)
	ubDummy = RSPI0.SPCR.BYTE ;
	
	
}


//-----------------------------------------------------------
//	SPIへ転送指示
//	引数
//		UWORD		uwSSL		SSLn(CS)端子指定
//				SPI_CS_SSL0		SSL0を使用
//				SPI_CS_SSL1		SSL1を使用
//				SPI_CS_SSL2		SSL2を使用
//				SPI_CS_SSL3		SSL3を使用
//		TRAN_BUF	*pTxBuf		送信バッファPtr
//		TRAN_BUF	*pRxBuf		受信バッファPtr
//		PcbFunck	pcbfEnd		送受信終了コールバック関数(NULL=無し)
//		PcbFunck	pcbfErr		エラー発生時コールバック関数(NULL=無し)
//	戻り値
//				SPI_OK			正常終了
//				SPI_NOT_IDLE	処理中(アイドル中でない)
//				SPI_PARM_ERR	パラメータエラー
//-----------------------------------------------------------
UBYTE	SPI_StartTrans( 
				UWORD		uwSSL,
				TRAN_BUF	*pTxBuf,
				TRAN_BUF	*pRxBuf,
				PcbFunck	pcbfEnd,
				PcbFunck	pcbfErr
				)
{
	WORD			wReg ;							// レジスタ値
	volatile UBYTE	ubRx ;							// 受信データ
	
	
	// パラメータチェック uwSSL
	if ( (uwSSL != SPI_CS_SSL0) && (uwSSL != SPI_CS_SSL1) &&
		 (uwSSL != SPI_CS_SSL2) && (uwSSL != SPI_CS_SSL0) ) {
		return SPI_PARM_ERR ;
	}
	// パラメータチェック pTxBuf
	if ( (pTxBuf->pubBuf == NULL) || (pTxBuf->ubSize == 0) ) {
		return SPI_PARM_ERR ;
	}
	// パラメータチェック pRxBuf
	if ( (pRxBuf != NULL) && 
		 ((pRxBuf->pubBuf == NULL) && (pRxBuf->ubSize != 0)) ) {
		return SPI_PARM_ERR ;
	}
	
	// アイドルチェック
	if ( RSPI0.SPSR.BIT.IDLNF == 1 ) {
		return SPI_NOT_IDLE ;
	}
	
	// 初期状態格納
	gTranStat.BIT.ubBusy = SPI_STAT_BUSY_YES ;			// 通信ビジー状態
	gTranStat.BIT.ubTrans = SPI_STAT_TRAN_NO ;			// Trans Stat
	gTranStat.BIT.ubErr = SPI_STAT_ERR_NO ;				// エラー状態
	gTxb = *pTxBuf ;									// 送信バッファ
	if ( pRxBuf != NULL ) {								// RXあり
		gRxb = *pRxBuf ;								// 受信バッファ
	} else {											// RXなし
		gRxb.pubBuf = NULL ;							// RxBufPtr
		gRxb.ubSize = 0 ;								// RxCount
	}
	gpcbfEnd = pcbfEnd ;								// 転送終了処理関数
	gpcbfErr = pcbfErr ;								// 受信エラー処理関数
	
	// SSL(CS)信号割当 アサート設定
	//	B0    CPHA  : RSPCK位相設定 0=奇数エッジデータサンプル
	//							   1=奇数エッジデータ変化
	//	B1    CPOL  : RSPCK極性設定 0=Idle時Low, 1:Idle時High
	//	B2-3  BRDV  : Bitrate分周   0=1/1, 1=1/2, 2=1/4, 3=1/8
	//	B4-6  SSLA  : SSL信号アサート 0=SSL0, 1=SSL1, 2=SSL2, 3=SSL3
	//	B7    SSLKP : SSL信号保持   0=転送終了時ネゲート, 1=転送終了時保持
	//	B8-11 SPB   : データ長      4-7=8bit,15=16bit
	//	B12   LSBF  : Bit転送順     0=MSBファースト, 1=LSBファースト
	//	B13   SPNDEN: 次アクセス遅延許可 0=1RSPCK+2PCLK, 1=SPND設定に従う
	//	B14   SLNDEN: SSLネゲート遅延設定許可 0=1RSPCK, 1=SSLND設定に従う
	//	B15   SCKDEN: RSPCK遅延設定許可       0=1RSPCK, 1=SPCKD設定に従う
	RSPI0.SPCMD0.BIT.SCKDEN = 0 ;						// RSPCK遅延設定許可
	RSPI0.SPCMD0.BIT.SLNDEN = 0 ;						// SSLネゲート遅延許可
	RSPI0.SPCMD0.BIT.SPNDEN = 0 ;						// 次アクセス遅延許可
	RSPI0.SPCMD0.BIT.LSBF = 0 ;							// Bit転送順
	RSPI0.SPCMD0.BIT.SPB = 7 ;							// データ長
	RSPI0.SPCMD0.BIT.SSLKP = 1 ;						// SSL信号保持
	RSPI0.SPCMD0.BIT.BRDV = RSPIC_BRDV ;				// Bitrate分周
	RSPI0.SPCMD0.BIT.CPOL = 1 ;							// RSPCK極性設定
	RSPI0.SPCMD0.BIT.CPHA = 1 ;							// RSPCK位相設定
	wReg = RSPI0.SPCMD0.WORD ;							// レジスタ値
	wReg &= 0xff8f ;									// レジスタ値
	RSPI0.SPCMD0.WORD = wReg | uwSSL ;					// SSLA設定
	
	// RSPI制御Reg. 各種フラグのクリア
	// B7 SPRIE :RSPI受信割り込み許可
	// B6 SPE   :RSPI機能許可
	// B5 SPTIE :RSPI送信割り込み許可
	// B4 SPEIE :RSPIエラー割込許可
	RSPI0.SPCR.BIT.SPRIE = 0 ;							// RSPI-Ctr
	RSPI0.SPCR.BIT.SPTIE = 0 ;							// 送信Int.禁止
	RSPI0.SPCR.BIT.SPEIE = 0 ;							// ErrInt.禁止
	RSPI0.SPCR.BIT.SPE = 0 ;							// RSPI無効
	while ( (RSPI0.SPCR.BYTE & 0xf0) != 0 ) { nop( ) ; }		// 待ち
	
	// アイドル割込停止
	RSPI0.SPCR2.BIT.SPIIE = 0 ;							// Idle Int. Disable
	while ( RSPI0.SPCR2.BIT.SPIIE != 0 ) { nop( ) ; }	// 待ち
	
	IR( RSPI0, SPRI0 ) = 0 ;							// RSPI0 SPRI
	IR( RSPI0, SPTI0 ) = 0 ;							// RSPI0 SPTI
	IR( RSPI0, SPEI0 ) = 0 ;							// RSPI0 SPEI
	IR( RSPI0, SPII0 ) = 0 ;							// RSPI0 SPII
	
	// RSPIステータスReg. 各種フラグのクリア
	RSPI0.SPSR.BYTE = 0xa0 ;							// OVRF,IDLNF,MODF,PERF
	
	// RSPI制御Reg.2 
	//	B0    SPPE  : パリティ許可   0=パリティ無し, 1=パリティ有り
	//	B1    SPOE  : パリティモード 0=偶数パリティ, 1=奇数パリティ
	//	B2    SPIIE : Idle割り込み許可 0=禁止, 1=許可
	//	B3    PTE   : パリティ自己診断 0=無効, 1=有効
	RSPI0.SPCR2.BIT.SPPE = 0 ;							// Parity None
	RSPI0.SPCR2.BIT.SPIIE = 0 ;							// Idle Int. Disable
	
	gTranStat.BIT.ubBusy = SPI_STAT_BUSY_YES ;			// Busy Stat
	gTranStat.BIT.ubTrans = SPI_STAT_TRAN_START ;		// Trans Stat
	gTranStat.BIT.ubErr = SPI_STAT_ERR_NO ;				// Err Stat
	
	// 念の為にダミーリード(受信ovf防止)
	ubRx = (UBYTE)(RSPI0.SPDR.LONG) ;					// Dummy Read
	
	// RSPIの割込許可と開始  SPEIE,SPTIE,SPE,SPRIE
	RSPI0.SPCR.BYTE |= 0xf0 ;							// RSPI & INT start
	while ( (RSPI0.SPCR.BYTE & 0xf0) != 0xf0 ) {		// SPEIE,SPTIE,SPE,SPRIE
		nop( ) ;
	}
	
	// 割込許可(macro) --- 割り込みコントローラ
	IEN( RSPI0, SPRI0 ) = 1 ;							// RSPI0 SPRI
	IEN( RSPI0, SPTI0 ) = 1 ;							// RSPI0 SPTI
	IEN( RSPI0, SPEI0 ) = 1 ;							// RSPI0 SPEI
	
	
	return SPI_OK ;
	
}


//-----------------------------------------------------------
//	SPIの状態を取得
//	引数
//	戻り値 (TRAN_STAT)
//-----------------------------------------------------------
TRAN_STAT	SPI_GetStat( void )
{
	
	return gTranStat ;
	
}


//-----------------------------------------------------------
//	エラー割込(SPEI)実行ルーチン
//	最終処理時にSPRIを禁止して転送終了
//	引数
//	戻り値
//-----------------------------------------------------------
static void		iSPI_IntSPEI( void )
{
	volatile UBYTE		ubRx ;						// 受信データ
	
	
	// 内部通信ステータス更新
	gTranStat.BIT.ubBusy = SPI_STAT_BUSY_NO ;			// Busy Stat
	
	// Overrun Err
	if ( RSPI0.SPSR.BIT.OVRF == 1 ) {
		gTranStat.BIT.ubErr = SPI_STAT_ERR_OVR ;		// Err Stat
	} else {
		gTranStat.BIT.ubErr = SPI_STAT_ERR_OTHER ;		// Err Stat
	}
	
	// 受信バッファをダミーリードして空にする
	ubRx = (UBYTE)(RSPI0.SPDR.LONG) ;
	
	// RSPIを止める
	RSPI0.SPCR.BIT.SPE = 0 ;							// RSPI停止
	
	// 送信バッファエンプティ割込(SPTI)を停止
	IEN( RSPI0, SPTI0 ) = 0 ;							// RSPI0 SPTI
	RSPI0.SPCR.BIT.SPTIE = 0 ;							// SPTI禁止
	while ( RSPI0.SPCR.BIT.SPTIE != 0 ) { nop( ) ; }	// 待ち
	
	// 受信バッファフル割込(SPRI)を停止
	IEN( RSPI0, SPRI0 ) = 0 ;							// RSPI0 SPRI
	RSPI0.SPCR.BIT.SPRIE = 0 ;							// SPRI禁止
	while ( RSPI0.SPCR.BIT.SPRIE != 0 ) { nop( ) ; }	// 待ち
	
	// エラー割込(SPEI)を停止
	IEN( RSPI0, SPEI0 ) = 0 ;							// RSPI0 SPEI
	RSPI0.SPCR.BIT.SPEIE = 0 ;							// SPEI禁止
	while ( RSPI0.SPCR.BIT.SPEIE != 0 ) { nop( ) ; }	// 待ち
	
	// アイドル割込(SPII)を停止
	IEN( RSPI0, SPII0 ) = 0 ;							// RSPI0 SPII
	RSPI0.SPCR2.BIT.SPIIE = 0 ;							// SPII禁止
	while ( RSPI0.SPCR2.BIT.SPIIE != 0 ) { nop( ) ; }	// 待ち
	
	// エッジ割込のフラグをクリア
	IR( RSPI0, SPRI0 ) = 0 ;							// RSPI0 SPRI
	IR( RSPI0, SPTI0 ) = 0 ;							// RSPI0 SPTI
	
	// RSPIのエラーフラグをクリア
	while ( (RSPI0.SPSR.BYTE & 0x0d) != 0 ) {
		RSPI0.SPSR.BYTE = 0xa0 ;						// OVRF,IDLNF,MODF,PERF
	}
	
	// 状態更新
	gTranStat.BIT.ubBusy = SPI_STAT_BUSY_NO ;			// 通信ビジー状態
	gTranStat.BIT.ubTrans = SPI_STAT_TRAN_NO ;			// Trans Stat
	
	// エラー時コールバック処理
	if ( gpcbfErr != NULL ) {
		gpcbfErr( ) ;									// Err CallBuck
	}
	
	
}


//-----------------------------------------------------------
//	受信バッファフル割込(SPRI)実行ルーチン
//	最終処理時にSPRIを禁止して転送終了
//	引数
//	戻り値
//-----------------------------------------------------------
static void		iSPI_IntSPRI( void )
{
	volatile UBYTE		ubRx ;						// 受信データ
	
	
	// Errチェック
	if ( RSPI0.SPSR.BIT.OVRF == 1 ) {
		// オーバーランエラー 後始末はエラー割込内で行う
		gTranStat.BIT.ubErr = SPI_STAT_ERR_OVR ;			// Err Stat
		return ;
	}
	
	// 送信中はダミーリードする
	if ( gTranStat.BIT.ubTrans == SPI_STAT_TRAN_TX ) {		// Tx Now
		// ダミーリード(受信ovf防止)
		IEN( RSPI0, SPTI0 ) = 1 ;							// RSPI0 SPTI
		ubRx = (UBYTE)(RSPI0.SPDR.LONG) ;					// Dummy Read
		return ;
	}
	
	// 受信データ --> バッファ
	*(gRxb.pubBuf) = (UBYTE)(RSPI0.SPDR.LONG) ;
	gTranStat.BIT.ubTrans = SPI_STAT_TRAN_RX ;				// Trans Stat
	
	// 受信データPtrを進める
	(gRxb.pubBuf)++ ;										// RxBufPtr
	(gRxb.ubSize)-- ;										// RxCount
	
	// 指定バイト数受信した
	if ( gRxb.ubSize == 0 ) {
		// SPRI割込を停止
		IEN( RSPI0, SPRI0 ) = 0 ;							// RSPI0 SPRI
		RSPI0.SPCR.BIT.SPRIE = 0 ;							// Rx Int. Disable
		while ( RSPI0.SPCR.BIT.SPRIE != 0 ) { nop( ) ; }	// 待ち
		IR( RSPI0, SPRI0 ) = 0 ;							// RSPI0 SPRI
		// SPEI割込を停止
		IEN( RSPI0, SPEI0 ) = 0 ;							// RSPI0 SPEI
		RSPI0.SPCR.BIT.SPEIE = 0 ;							// Err Int. Disable
		while ( RSPI0.SPCR.BIT.SPEIE != 0 ) { nop( ) ; }	// 待ち
		// アイドル割込を許可
		RSPI0.SPCR2.BIT.SPIIE = 1 ;							// Idle Int. Enable
		IEN( RSPI0, SPII0 ) = 1 ;							// RSPI0 SPII
		while ( RSPI0.SPCR2.BIT.SPIIE == 0 ) { nop( ) ; }	// 待ち
		
		// この後アイドル割り込みが入る
		
	} else {
		// 受信バッファ残があるのでダミー送信して受信させる
		RSPI0.SPDR.LONG = (UDWORD)(0xff) ;			// TxData(Byte)
		// スレーブにクロックを送って受信させて再度受信割り込みさせる
	}
	
	
}


//-----------------------------------------------------------
//	送信バッファエンプティ割込(SPTI)実行ルーチン
//	最終転送時にSPTIを禁止してSPIIを許可する
//	引数
//	戻り値
//-----------------------------------------------------------
static void		iSPI_IntSPTI( void )
{
	volatile UBYTE		ubRx ;						// 受信データ
	
	
	// 送信データあり
	if ( gTxb.ubSize != 0 ) {
		
		// 送信データをSPDRに入れる
		RSPI0.SPDR.LONG = (UDWORD)*(gTxb.pubBuf) ;			// TxData(Byte)
		gTranStat.BIT.ubTrans = SPI_STAT_TRAN_TX ;			// Trans Stat
		
		// 送信データPtrを進める
		(gTxb.pubBuf)++ ;									// TxBufPtr
		(gTxb.ubSize)-- ;									// TxCount
		
		// 一旦送信割込を止めてこの後の受信割込で送信したデータを
		// ダミー受信させてバッファオーバーランを防ぐ
		IEN( RSPI0, SPTI0 ) = 0 ;							// RSPI0 SPTI
		
		// 送信データが無くなるまで繰り返す
		return ;
	}
	
	// 受信バッファ残があるとき -> ダミー送信
	if ( gRxb.ubSize != 0 ) {								// 受信有り
		// 受信バッファ残があるのでダミー送信して受信させる
		RSPI0.SPDR.LONG = (UDWORD)(0xff) ;					// TxData(Byte)
		gTranStat.BIT.ubTrans = SPI_STAT_TRAN_RX_START ;	// Trans Stat
	} else {												// 送受信完了
		// アイドル割込を許可
		RSPI0.SPCR2.BIT.SPIIE = 1 ;							// Idle割込許可
		IEN( RSPI0, SPII0 ) = 1 ;							// RSPI0 SPII
		while ( RSPI0.SPCR2.BIT.SPIIE == 0 ) { nop( ) ; }	// 待ち
	}
	
	// 送信割り込み停止処理
	RSPI0.SPCR.BIT.SPTIE = 0 ;								// Tx 割込禁止
	IR( RSPI0, SPTI0 ) = 0 ;								// RSPI0 SPTI
	IEN( RSPI0, SPTI0 ) = 0 ;								// RSPI0 SPTI
	while ( RSPI0.SPCR.BIT.SPTIE != 0 ) { nop( ) ; }		// 待ち
	
	
}


//-----------------------------------------------------------
//	アイドル割込(SPII)実行ルーチン
//	転送(Tx,Rx)完了後に発生する
//	引数
//	戻り値
//-----------------------------------------------------------
static void		iSPI_IntSPII( void )
{
	
	
	// RSPIを停止
	RSPI0.SPCR.BIT.SPE = 0 ;							// RSPI停止
	
	// アイドル割込停止
	IEN( RSPI0, SPII0 ) = 0 ;							// RSPI0 SPII
	RSPI0.SPCR2.BIT.SPIIE = 0 ;							// Idle Int. Disable
	while ( RSPI0.SPCR2.BIT.SPIIE != 0 ) { nop( ) ; }	// 待ち
	
	// 内部通信ステータス更新
	gTranStat.BIT.ubBusy = SPI_STAT_BUSY_NO ;			// Busy Stat
	gTranStat.BIT.ubTrans = SPI_STAT_TRAN_NO ;			// Trans Stat
	
	// 転送終了コールバック処理
	if ( gpcbfEnd != NULL ) {
		gpcbfEnd( ) ;									// TfrEnd CallBuck
	}
	
	
}


//-----------------------------------------------------------
//	SPI エラー割り込み処理 SPEI
//	IRはレベル検出
//-----------------------------------------------------------
#pragma interrupt (Int_RSPI0_SPEI(vect=VECT( RSPI0, SPEI0 )))
void	Int_RSPI0_SPEI( void )
{
	
	do {
		// エラー割込条件とフラグがクリア確認されるまでループ
		while ( (RSPI0.SPCR.BIT.SPEIE == 1) &&
				((RSPI0.SPSR.BYTE & 0x0f) != 0) ) {
			iSPI_IntSPEI( ) ;							// SPEI処理
		}
	} while ( IR( RSPI0, SPEI0 ) != 0 ) ;
	
}


//-----------------------------------------------------------
//	SPI 受信バッファフル割り込み処理 SPRI
//	IRはエッジ検出
//-----------------------------------------------------------
#pragma interrupt (Int_RSPI0_SPRI(vect=VECT( RSPI0, SPRI0 )))
void	Int_RSPI0_SPRI( void )
{
	
	iSPI_IntSPRI( ) ;									// SPRI処理
	
}


//-----------------------------------------------------------
//	SPI 送信バッファエンプティ割り込み処理 SPTI
//	IRはエッジ検出
//-----------------------------------------------------------
#pragma interrupt (Int_RSPI0_SPTI(vect=VECT( RSPI0, SPTI0 )))
void	Int_RSPI0_SPTI( void )
{
	
	iSPI_IntSPTI( ) ;									// SPTI処理
	
}


//-----------------------------------------------------------
//	SPI アイドル割り込み処理 SPII
//	IRはレベル検出
//-----------------------------------------------------------
#pragma interrupt (Int_RSPI0_SPII(vect=VECT( RSPI0, SPII0 )))
void	Int_RSPI0_SPII( void )
{
	
	iSPI_IntSPII( ) ;									// SPII処理
	
}



//--------------------------------------------------------------------------
