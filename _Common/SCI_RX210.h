//=====================================================================
//    SCI(UART) functions
//          with ring buffers and interrupt functions
//									09.11.18 v0.2 by H.Inoue
//          potring for R8C/2A (UART2)
//									'10.07.30 v0.31 by H.Inoue
//									'10.08.17 v0.32 by H.Inoue
//									各種Bugfix
//          potring for RX62N LQFP144pin
//									'11.07.04 v0.4 by H.Inoue
//          potring for RX210
//									'14.07.02 v0.5 by H.Inoue
//									多チャンネル対応,RS485対応
//									XON/XOFFフロー制御は使わないので外す
//									エラーハンドラは使わないので外す
//=====================================================================

// 多重定義対策
#ifndef _DEFINE_SCI_RX_
	#define _DEFINE_SCI_RX_

#include	"_iodefine.h"				// Include _iodefine.h
#include	"TypeDef.h"					// アプリケーション用型定義
#include	"SCI_Conf.h"				// SCIコンフィグレーション定数定義




//-----------------------------------------------------------
//	SCI処理用定数定義
//-----------------------------------------------------------

//	関数戻り値
#define	UART_SUCCESS		0				// 正常
#define	UART_ERR_PARM		1				// 引数が不正
#define	UART_CH_USED		20				// チャンネルは使用中
#define	UART_HANDLE_FULL	21				// 使用ハンドルフル

// SCI_Putc() 戻り値
#define	UART_PUT_SUCCESS	0				// 正常終了
#define	UART_PUT_BUSY		-1				// 割込処理中ビジー
#define	UART_PUT_OVF		-2				// バッファオーバーフロー

// 送受信バッファサイズ
// 注: RX_BUF_SIZE, TX_BUF_SIZE はSCI_Conf.hで定義
#define SCI_RX_BUF_CHK		RX_BUF_SIZE - 1	// 受信バッファチェック値
#define SCI_TX_BUF_CHK		TX_BUF_SIZE - 1	// 送信バッファチェック値

// 送信時のCR+LF変換
#define	UART_CRLF_NO		0					// CR+LF変換しない
#define	UART_CRLF_YES		1					// CR+LF変換する
#define	UART_CRLF			UART_CRLF_YES		// CR+LF変換指定(適宜変更)

// サポートチャンネル数定義 適宜変更する
// 注: UART_CH_MAX, UART_MCU_CHANNELS はSCI_Conf.hで定義

//	UARTチャンネル指定
#define	UART_CH_0			0				// CH0を使用
#define	UART_CH_1			1				// CH1を使用
#define	UART_CH_2			2				// CH2を使用
#define	UART_CH_3			3				// CH2を使用
#define	UART_CH_4			4				// CH2を使用
#define	UART_CH_5			5				// CH2を使用
#define	UART_CH_6			6				// CH2を使用
#define	UART_CH_7			7				// CH2を使用
#define	UART_CH_8			8				// CH2を使用
#define	UART_CH_9			9				// CH2を使用
#define	UART_CH_10			10				// CH2を使用
#define	UART_CH_11			11				// CH2を使用
#define	UART_CH_TOP			UART_CH_1		// 最初のチャンネル
#define	UART_CH_LAST		UART_CH_6		// 最終のチャンネル

//	SCIボーレート設定定義
#define	UART_BAUD_300		0				// 300bps
#define	UART_BAUD_600		1				// 600bps
#define	UART_BAUD_1200		2				// 1200bps
#define	UART_BAUD_2400		3				// 2400bps
#define	UART_BAUD_4800		4				// 4800bps
#define	UART_BAUD_9600		5				// 9600bps
#define	UART_BAUD_19200		6				// 19200bps
#define	UART_BAUD_31250		7				// 31250bps
#define	UART_BAUD_38400		8				// 38400bps
#define	UART_BAUD_57600		9				// 57600bps
#define	UART_BAUD_115200	10				// 115200bps

// パリティ
#define	UART_PARITY_NONE	0				// パリティ無し
#define	UART_PARITY_EVEN	1				// 偶数パリティ
#define	UART_PARITY_ODD		2				// 奇数パリティ
#define	UART_PE_NONE		0				// パリティ無し
#define	UART_PE_ENABLE		1				// パリティ有り
#define	UART_PM_EVEN		0				// 偶数パリティ
#define	UART_PM_ODD			1				// 奇数パリティ

// ストップビット
#define UART_STOP_1			0				// 1StopBit
#define UART_STOP_2			1				// 2StopBit

// データ長
#define	UART_BIT_7			1				// SCIデータ長=7bit
#define	UART_BIT_8			0				// SCIデータ長=8bit




//-----------------------------------------------------------
//	RS485処理用定数定義
//	※ 必要に応じてポート定義を修正すること
//-----------------------------------------------------------
//	RS485使用チャンネル設定
// 注: RS485_CHx はSCI_Conf.hで定義

//	RE,DE制御ポート定義
// 注: SCI_Conf.hで定義

//	RS485通信制御出力状態
#define	RS485_RE_ENABLE			0					// Receiver Enable
#define	RS485_RE_HIGHZ			1					// Receiver High-Z
#define	RS485_DE_ENABLE			1					// Transciever Enable
#define	RS485_DE_HIGHZ			0					// Transciever High-Z

//-----------------------------------------------------------
// チャンネル-ハンドルテーブル構造体定義
//-----------------------------------------------------------
typedef struct _CH_TBL_ {
		UBYTE		ubHnd ;					// ハンドル
		FLAG		fUse ;					// 使用Flag
	} CH_TBL ;
//	メンバ fUse
#define	CH_USE_NO			0				// チャンネル未使用
#define	CH_USE_YES			1				// チャンネル使用中

#ifdef	_SCI_DRIVER_
	CH_TBL				gChTbl[UART_MCU_CHANNELS] ;	// Ch->Handle Table
#else		// #ifdef	_SCI_DRIVER_
	extern CH_TBL		gChTbl[UART_MCU_CHANNELS] ;	// Ch->Handle Table
#endif		// #ifdef	_SCI_DRIVER_


//-----------------------------------------------------------
// 内部状態フラグ構造体定義
//-----------------------------------------------------------
typedef struct {
//		UBYTE	fTxBusy ;					// sending char busy
		FLAG	fTxCritical ;				// critical section flag
		UBYTE	ubDatalen ;					// データ長(エラー時再設定する為)
#ifdef	RS485_USE
		FLAG	fTxTransCtl ;				// TransceiverCtl
		UBYTE	bDelay ;					// RS485 CtlTimerDelay
		UBYTE	bTimer ;					// RS485 CtlTimer
#endif		// #ifdef	RS485_USE
		FLAG	fCvtCRLF ;					// 送信時CR+LF変換指定
	} SCI_F ;
//	メンバ fTxBusy
#define	SCIF_TX_BUSY_NO		0				// ビジーでない
#define	SCIF_TX_BUSY_YES	1				// ビジーである
//	メンバ fTxCritical
#define	SCIF_TX_CRTICAL_NO	0				// クリティカルでない
#define	SCIF_TX_CRTICAL_YES	1				// クリティカル中である
// メンバ:fTxTransCtl
#define	RS485_TCV_RCV		0				// トランシーバ:受信モード
#define	RS485_TCV_SEND		1				// トランシーバ:送信モード
#define	RS485_TCV_RCV_WAIT	2				// トランシーバ:受信切替待機
// メンバ:fCvtCRLF
//	UART_CRLF_NO 又は UART_CRLF_YES

#ifdef	_SCI_DRIVER_
	volatile SCI_F		gSciF[UART_CH_MAX] ;		// SCIフラグ構造体
#else		// #ifdef	_SCI_DRIVER_
	extern SCI_F	gSciF[] ;						// SCIフラグ構造体
#endif		// #ifdef	_SCI_DRIVER_


//-----------------------------------------------------------
//	SCIグローバル変数宣言
//-----------------------------------------------------------

// ストリーム出力指定定義
extern	void	(*xFunc_Out)(UBYTE, UBYTE) ;		// 関数ptr
//#define XDEV_OUT(func)	xFunc_Out = (void(*)(UBYTE))(func)
#define XDEV_OUT(func)	xFunc_Out = (void(*)(UBYTE, UBYTE))(func)


//-------------------------------------------------------------------
//	関数プロトタイプ宣言
//-------------------------------------------------------------------

//-----------------------------------------------------------
//	ストリーム又はメモリポインタに１文字出力
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		UBYTE ubChar		出力文字
//	戻り値
//-----------------------------------------------------------
void	xPutc( UBYTE ubCh, UBYTE  ubChar ) ;

//-----------------------------------------------------------
//	ストリーム又はメモリポインタに文字列出力
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		UBYTE *pubStr		送信データ
//	戻り値 
//-----------------------------------------------------------
void	xPuts( UBYTE ubCh, UBYTE *pubStr ) ;

//-----------------------------------------------------------
//	ストリーム又はメモリポインタ版printf
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		CHAR *pcFmt		書式
//		可変引数
//	戻り値 
//-----------------------------------------------------------
void	xvPrintf( UBYTE ubCh, const CHAR *pcFmt, va_list arp ) ;

//-----------------------------------------------------------
//	ストリーム版printf
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		CHAR *pcFmt		書式
//		可変引数
//	戻り値 
//-----------------------------------------------------------
void	xPrintf( UBYTE ubCh, const CHAR *pcFmt, ... ) ;

//-----------------------------------------------------------
//	メモリ版printf
//	引数
//		CHAR *pcOutBuff		出力バッファptr
//		CHAR *pcFmt		書式
//		可変引数
//	戻り値 
//-----------------------------------------------------------
void	xsPrintf( 
					CHAR *pcOutBuff, 
					const CHAR *pcFmt, 
					... 
				) ;

//-----------------------------------------------------------
//	SCIを初期化
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//		UBYTE ubSpeed		通信速度指定
//								UART_BAUD_300		 300bps
//								UART_BAUD_600		 600bps
//								UART_BAUD_1200		 1200bps
//								UART_BAUD_2400		 2400bps
//								UART_BAUD_4800		 4800bps
//								UART_BAUD_9600		 9600bps
//								UART_BAUD_19200		 19200bps
//								UART_BAUD_31250		 31250bps
//								UART_BAUD_57600		 57600bps
//		UBYTE ubParity		パリティ指定
//								UART_PARITY_NONE	 パリティ無し
//								UART_PARITY_EVEN	 偶数パリティ
//								UART_PARITY_ODD		 奇数パリティ
//		UBYTE ubStopBit		STOPビット指定
//								UART_STOP_1			1bit
//								UART_STOP_2			2bit
//		UBYTE ubDataLen		データ長指定
//								UART_BIT_7			7bit
//								UART_BIT_8			8bit
//	戻り値(FLAG)
//		UART_SUCCESS(0)			正常終了
//		UART_ERR_PARM(1)		異常終了 (パラメータエラー)
//		UART_HANDLE_FULL(21)	使用可能ハンドルを使い切った
//		UART_CH_USED(20)		チャンネルは既に使用中
//-----------------------------------------------------------
FLAG	SCI_Inz(
					UBYTE ubCh,
					UBYTE ubSpeed, 
					UBYTE ubParity,
					UBYTE ubStopBit,
					UBYTE ubDataLen
			) ;

//-----------------------------------------------------------
//	putchar to SCI Ring Buffer
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//	戻り値 
//		SCI_PUT_SUCCESS  	(0) Success
//		SCI_PUT_BUSY		(-1)Busy
//		SCI_PUT_OVF			(-2)BufferOverflow
//-----------------------------------------------------------
BYTE	Sci_Putc( UBYTE ubCh, UBYTE ubChar ) ;

//-----------------------------------------------------------
//	送信データの確認
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//	戻り値 = データバイト数
//-----------------------------------------------------------
WORD	Sci_IsTxData( UBYTE ubCh ) ;

//-----------------------------------------------------------
//	UART文字列送信
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		UBYTE *ubStr		文字列Ptr
//	戻り値 
//		SCI_PUT_SUCCESS  	(0) Success
//		SCI_PUT_BUSY		(-1)Busy
//		SCI_PUT_OVF			(-2)BufferOverflow
//-----------------------------------------------------------
BYTE	Sci_Puts( UBYTE ubCh, UBYTE *ubStr ) ;

//-----------------------------------------------------------
//	UART文字列送信(テキストモード)
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//		UBYTE *ubStr		文字列Ptr
//	戻り値 
//		SCI_PUT_SUCCESS  	(0) Success
//		SCI_PUT_BUSY		(-1)Busy
//		SCI_PUT_OVF			(-2)BufferOverflow
//-----------------------------------------------------------
BYTE	Sci_PutsText( UBYTE ubCh, UBYTE *ubStr ) ;

//---------------------------------------------------
//	ＣＲＬＦの出力
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//	戻り値 
//		SCI_PUT_SUCCESS  	(0) Success
//		SCI_PUT_BUSY		(-1)Busy
//		SCI_PUT_OVF			(-2)BufferOverflow
//---------------------------------------------------
BYTE	Sci_PutCRLF( UBYTE ubCh ) ;

//-----------------------------------------------------------
//	SCIポートから１キャラクタ受信
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//	戻り値 
//		受信データがなければ -1を返す
//		受信エラー制御機能がONの場合、
//			エラー発生は gUartErrF を参照し、エラー処理後クリアしておく
//			その間受信データは読み捨てられる
//			受信バッファオーバーフロー監視される
//		受信エラー制御機能がOFFの場合、
//			受信バッファオーバーフロー監視されない
//			古いデータは捨てられる
//-----------------------------------------------------------
WORD	Sci_Getc( UBYTE ubCh ) ;

//-----------------------------------------------------------
//	受信データの確認
//	引数
//		UBYTE ubCh			チャンネル指定(UART_CH_MAXによる)
//								UART_CH_0			 CH0を使用
//								UART_CH_1			 CH1を使用
//								UART_CH_2			 CH2を使用
//								etc
//	戻り値 = データバイト数
//-----------------------------------------------------------
WORD	Sci_IsRcvData( UBYTE ubCh ) ;





#endif	// _DEFINE_SCI_RX_

//--------------------------------------------------------------------------
