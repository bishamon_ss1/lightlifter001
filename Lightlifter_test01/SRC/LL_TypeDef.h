//-----------------------------------------------------------------------
//	変数型定義,定数定義
//-----------------------------------------------------------------------



#ifndef _APP_TYPE_DEFINED_					// 多重定義予防
#define _APP_TYPE_DEFINED_

#include	<stdint.h>					// Include stdint.h
#include	<stdbool.h>					// Include stdbool.h


//----------------------------------------------------------------
// インターバルタイマ処理
//----------------------------------------------------------------
typedef struct _CMT0_INT_ {
		struct {					// Timer 10mS
			UWORD	uwIndTimer ;		// インジケータタイマ
			UWORD	uwDownDlay ;		// 下降遅延タイマ
			UWORD	uwWorking ;			// 作動時間タイマ
			UWORD	uwTime ;			// 10mSec時間値
			UWORD	uwCount ;			// 10mSec計測内部カウンタ
			UWORD	uwErrMotStop ;		// Timer エラー用タイマ
		} T10M ;
		struct {					// Timer 100mS
			UWORD	uwStopCheck ;		// 停止監視
			UWORD	uwSysAct ;			// システム活動インジケータ
			UWORD	uwTime ;			// 100mSec時間値
			UWORD	uwCount ;			// 100mSec計測内部カウンタ
			UWORD	uwBrakeDelay	 ;	// ブレーキ遅延タイマ
//			UWORD	uwRunDisable ;		// 停止→次動作までのインターバルタイマ
			UDWORD	udwLEDFlashON ;		// LED点灯用タイマ
			UDWORD	udwLEDFlashOFF ;	// LED消灯用タイマ
		} T100M ;
		struct {					// Timer 1Sec
			UWORD	uwTime ;			// 1Sec時間値
			UWORD	uwCount ;			// 1Sec計測内部カウンタ
			UWORD	uwPWROFF ;			// AutoPowerOff timer
			UWORD	uwUpTimOut ;		// 上昇タイムアウト用タイマ
			UWORD	uwDownTimOut ;		// 下降タイムアウト用タイマ
		} T1S ;
		UWORD	uwScanTime ;			// Timer ScanTime
		UWORD	uwIntFb ;				// Timer Feedback
		UWORD	uwDrvCtl ;				// Timer 駆動制御用待ち
		UWORD	uwRunDisable ;			// Timer 操作禁止Timer
	} CMT0_INT ;		
typedef struct _CMT1_INT_ {
		UDWORD	udw01ms ;				// count 0.1mSec
	} CMT1_INT ;					// gCMT1にて定義
	
	
//----------------------------------------------------------------
// DIP-SW
//----------------------------------------------------------------

typedef struct _DIP8_ {
		UBYTE	S1:1 ;		// Bit 0 LSB
		UBYTE	S2:1 ;		// Bit 1
		UBYTE	S3:1 ;		// Bit 3
		UBYTE	S4:1 ;		// Bit 4
		UBYTE	S5:1 ;		// Bit 5
		UBYTE	S6:1 ;		// Bit 6
		UBYTE	S7:1 ;		// Bit 6
		UBYTE	S8:1 ;		// Bit 7 MSB
	} DIP8 ;

// DIP-SW情報
#define	DIP_SW_ON	1		// DIP-SW ON
#define	DIP_SW_OFF	0		// DIP-SW OFF



//----------------------------------------------------------------
// 入力ポートフィルタ		チャタリングフィルタより前
//----------------------------------------------------------------
//元データ
typedef union _IN_DATA_ {
			UBYTE		ubData ;
			ST_BIT8		BIT ;
			UBYTE		ubChk5:5 ;	// チェックフィールド
			UBYTE		ubChk4:4 ;	// チェックフィールド
			UBYTE		ubChk3:3 ;	// チェックフィールド
		} IN_DATA ;

typedef struct _INP_FLD_ {
		IN_DATA		I01 ;			// Inp01
		IN_DATA		I02 ;			// Inp02
		IN_DATA		I03 ;			// Inp03
		IN_DATA		I04 ;			// Inp04
		IN_DATA		I05 ;			// Inp05
//		IN_DATA		I06 ;			// Inp06
//		IN_DATA		I07 ;			// Inp07
//		IN_DATA		I08 ;			// Inp08
		IN_DATA		Inz ;			// 初期化用
	} INP_FLD ;						// gInpFld

// フィルタチェック用定数
#define	INP_FCHK_SW_VAL		0x1f			// 5bit

//----------------------------------------------------------------
// 入力ポート情報			チャタリングフィルタより後
//----------------------------------------------------------------
//元データ
typedef struct _INP_INFO_ {
		UBYTE		ubI01 ;			// Inp01
		UBYTE		ubI02 ;			// Inp02
		UBYTE		ubI03 ;			// Inp03
		UBYTE		ubI04 ;			// Inp04
		UBYTE		ubI05 ;			// Inp05
//		UBYTE		ubI06 ;			// Inp06
//		UBYTE		ubI07 ;			// Inp07
//		UBYTE		ubI08 ;			// Inp08
		UBYTE		bInz ;			// 初期化
	} INP_INFO ;					// gInpStat gInpStatGet gInpStatPrv


// I/O状態
#define	EXTINP_OFF			0		// 入力 OFF
#define	EXTINP_ON			1		// 入力 ON
#define	EXTINP_UP_EDGE		2		// 入力 UpEdge
#define	EXTINP_DWN_EDGE		3		// 入力 DownEdge

//----------------------------------------------------------------
// 接点入力デバイス情報
//----------------------------------------------------------------
//元データ
typedef struct _SW_DEV_INFO_ {
		UBYTE		ubI01 ;			// Inp01
		UBYTE		ubI02 ;			// Inp02
		UBYTE		ubI03 ;			// Inp03
		UBYTE		ubI04 ;			// Inp04
		UBYTE		ubI05 ;			// Inp05
/*外部入力が実装されたときに開放
		UBYTE		ubIR01 ;		// Inp01
		UBYTE		ubIR02 ;		// Inp02
		UBYTE		ubIR03 ;		// Inp03
*/
//元データ
//		UBYTE		ubI05 ;			// Inp05
//		UBYTE		ubI06 ;			// Inp06
	} SW_DEV_INFO ;					// Lvl Edge Long
typedef struct _SW_DEV_TMR_ {
		UWORD		uwI01 ;			// Inp01
		UWORD		uwI02 ;			// Inp02
		UWORD		uwI03 ;			// Inp03
		UWORD		uwI04 ;			// Inp04
		UWORD		uwI05 ;			// Inp05
/*外部入力が実装されたときに開放
		UWORD		ubIR01 ;		// Inp01
		UWORD		ubIR02 ;		// Inp02
		UWORD		ubIR03 ;		// Inp03
*/
// 元データ
//		UWORD		uwI05 ;			// Inp05
//		UWORD		uwI06 ;			// Inp06
	} SW_DEV_TMR ;					// Timer
typedef struct _SW_DEVICE_ {
		SW_DEV_INFO	Lvl ;			// キー(SW)レベル
		SW_DEV_INFO	Edge ;			// On/Off Edge
		SW_DEV_INFO	Long ;			// 長押し
		SW_DEV_TMR	Timer ;			// 長押しタイマ
	} SW_DEVICE ;					// gSwDev


// Level
#define	SWDEV_LVL_OFF		0		// 接点はOFF(離)
#define	SWDEV_LVL_ON		1		// 接点はON (押)
// Edge
#define	SWDEV_EDGE_NONE		0		// 接点エッジ無し
#define	SWDEV_EDGE_UP		1		// 接点 離->押
#define	SWDEV_EDGE_DOWN		2		// 接点 押->離
// 長押し
#define	SWDEV_LPUSH_NO		0		// SW長押し無し
#define	SWDEV_LPUSH_YES		1		// SW長押し有り
#define	SWDEV_LPUSH_TICK	400		// SW長押しタイマチックx10mS
	//SWDEV_LPUSH_TICKの数字を変化させると長押し待ち時間が変化
	//400は4秒で長押しと判定する設定となる
//非常停止 : B接点
#define EMM_SW_ON			0		// 接点はON(押)
#define EMM_SW_OFF			1		// 接点はOFF(離)

//----------------------------------------------------------------
//	UARTチャンネル
//----------------------------------------------------------------
// UART使用チャンネル
#define	UART_CH_DBG		UART_CH_1	// デバックポートチャンネル

//===============================================================
// フィードバック
//===============================================================

//-------------------------------------------------------------
//位置フィードバック処理
//-------------------------------------------------------------

// 目標位置待ち行列
typedef struct	_FB_PQ_ {
		BYTE	bEnable ;			// 有効F
		DWORD	dwPosTo ;			// 移動点(enc)
		DWORD	dwPosLimit ;		// 上昇端(enc)
		WORD	wVrevLimit ;		// 上限速度(rpm)
	} FB_PQ ;
	
// 位置フィードバック処理
typedef struct	_FB_P_ {
		WORD	wVrevLimit ;		// 上限速度(rpm)
		WORD	wVrevInit ;			// 初期速度(rpm)
//		WORD	wVrevNow ;			// 現在速度(rpm)
		BYTE	bEnable ;			// FB処理許可F
		BYTE	bEnablePrv ;		// FB処理許可F 前回
		BYTE	bStopState ;		// 停止ステータス
		BYTE	bStopStatePrv ;		// 停止ステータス 前回
		BYTE	bRoterStop ;		// ロータ停止Flag
//		BYTE	bStopJudgeCtl ;		// 停止判定制御Flag
//		DWORD	dwPosFrom ;			// 開始点(enc)
//		DWORD	dwPosTo ;			// 移動点(enc)
//		WORD	wDecDist ;			// 減速距離(enc)
//		DWORD	dwPosAccEnd ;		// 加速終了点(enc)
//		DWORD	dwPosDecStart ;		// 減速開始点(enc)
		DWORD	dwPdiff ;			// 偏差
		WORD	wCmp ;				// 位置比較値(enc)
//		BYTE	bInPos ;			// 目標到達F
//		WORD	wOut ;				// 現在の指示値(rpm)
//		WORD	wOutPrv ;			// 前回の指示値
		UBYTE	ubMotStat ;			// 現在の駆動ステータス
	} FB_P ;
//-----------------------------------------------
// 速度フィードバック処理
//-----------------------------------------------

typedef struct	_FB_V_ {
		DWORD	dwFbP ;				// P:比例制御値    偏差
		DWORD	dwFbI ;				// I:誤差の積分値
		DWORD	dwFbD ;				// D:偏差の微分値
//		DWORD	dwFbC ;				// C:電流比較値
		DWORD	dwLimitI ;			// I:制限値
//		DWORD	dwCurComp ;			// C:電流比較基準値
		WORD	wVdiff ;			// 偏差
		WORD	wVdiffPrv ;			// 前回の偏差
		WORD	wVdiffAbs ;			// 偏差の絶対値
//		WORD	wOut ;				// FB-V出力
		WORD	wFbStep ;			// 加減速ステップ
		WORD	wStopDecStep ;		// 停止及び減速ステップ
		WORD	wInpVal ;			// スロットル入力値(rpm) 10/5追加
		WORD	wAccStat ;			// 加減速ステータス
		WORD	wVrev ;				// 加減速処理後のFB処理速度(rpm)
		WORD	wVrevPrv ;			// 加減速処理後のFB処理速度(rpm) 前回
		WORD	wVrevAbs ;			// 加減速処理後のFB処理速度(rpm) 絶対値
		WORD	wVrevFb ;			// FB処理速度(rpm)
		WORD	wPwm ;				// 現在のPWM指示値		10/5追加
		WORD	wPwmLimit ;			// PWM制限値
//		WORD	wPwmCurLimit ;		// PWM電流制限値
//		WORD	wPwmCurAdj ;		// PWM電流補正値
		volatile __evenaccess UWORD		*puwTGR ;	// タイマTGR-ptr
	} FB_V ;						// FB内のVとして定義

// wAccStat		加減速ステータス
#define	FBV_ACDC_STOP		0		// 停止
#define	FBV_ACDC_ACC		1		// 加速
#define	FBV_ACDC_CV			2		// 等速
#define	FBV_ACDC_DEC		3		// 減速


//----------------------------------------------------------------
// フィードバック処理
//----------------------------------------------------------------
typedef struct	_FB_ {
		FB_V	V ;					// 速度フィードバック処理
		FB_PQ	Q ;					// 目標位置待ち行列
		FB_P	P ;					// 位置フィードバック処理
		UBYTE	ubFbType ;			// フィードバックタイプ
	} FB ;							// gFbにて定義

// bStopState	停止処理ステータス
#define	FB_STOP_NO			0		// 停止しない
#define	FB_STOP_NORMAL		1		// 減速停止する
#define	FB_STOP_EMM			2		// 非常停止する
#define FB_STOP_POE			3		// POEによる停止

// bRoterStop	ロータ回転&停止フラグ
#define	FB_ROTER_STOP_NO	0		// ロータ回転中
#define	FB_ROTER_STOP_WAIT	1		// ロータ停止待ち
#define	FB_ROTER_STOP_YES	2		// ロータ停止中


//----------------------------------------------------------------
// ホールセンサー情報
//----------------------------------------------------------------
//Encカウント周波数は使用の際は数値を変更のこと
//#define	ENC_COUNT_FREQ		375		// Encカウント周波数(kHz)
//25MHz/128=195312.5->195
#define	ENC_COUNT_FREQ		195		// Encカウント周波数(kHz)
//#define	ENC_COUNT_FREQ		781		// Encカウント周波数(kHz)

//#define	ENC_CMTU_COUNT		(65530 - 0)		// コンペアマッチ値
#define	ENC_CMTU_COUNT		0x8555	// コンペアマッチ値 計算式はhw&in
//#define ENC_CMTU_COUNT		0xffff	// コンペアマッチ値 計算式はhw&in
#define	ENC_RES_NORMAL		6		// 計測カウント数(電気角360度)
									// 両Edge3相分で6
#define	ENC_RES_MID			4		// 計測カウント数(電気角240度)
#define	ENC_RES_SLOW		2		// 計測カウント数(電気角120度)
#define ENC_REV_RATIO_4		4		// 機械角/電気角比
									// BMI01は360/60度=6
									// ニッセイは360/120度=3
									// MTM BL90Mは360/90度=4
//#define	ENC_RES				(ENC_RES_NORMAL * ENC_REV_RATIO_4)	// Pulse/Rev
typedef union _ENC_INP_ {		
		UBYTE		ubUVW ;
		struct {
			UBYTE	U : 1 ;			// U state
			UBYTE	V : 1 ;			// V state
			UBYTE	W : 1 ;			// Q state
			UBYTE	Rsv:5 ;			// ReserveBit
		} BIT ;
	} ENC_INP ;
/*
//	ロータ角度情報
typedef struct _ROT_ANGLE_ {
//		UDWORD		udwHsOvf ;		// ホールセンサOvfカウント
//		UDWORD		udwHsNow ;		// 現在のホールセンサTimeカウント
//		UWORD		uwTC_1deg ;		// 1度当たりのTimeカウント
//		UDWORD		udwHsAvg ;		// ホールセンサTime平均
//		UDWORD		udwHsAvgPrv ;	// ホールセンサTime平均(前回)
//		UDWORD		udwHsSum ;		// ホールセンサTime合計
//		UDWORD		udwHist[ENC_RES_NORMAL] ;// 過去のホールセンサTimeカウント
//		UWORD		uwHistPtrNow ;	// uwHist[ENC_RES_NORMAL]のPtr(今)
//		UWORD		uwHistPtr ;		// uwHist[ENC_RES_NORMAL]のPtr(次の)
//		FLAG		fHistFull ;		// uwHist[ENC_RES]充填Flag
	} ROT_ANGLE ;					// Rotにて定義
*/
//	ENC情報
typedef struct _ENC_INF_ {		
		ENC_INP		Inp ;			// Enc入力値	元データ
		ENC_INP		InpPrv ;		// Enc入力値(前回)	元データ
//		ROT_ANGLE	Rot ;			// ロータ角度情報
		WORD		wDir ;			// 回転方向
		WORD		wDirPrv ;		// 回転方向(前回)
		WORD		wPulseChk ;		// Encパルス数確認
		WORD		wPulseCount ;	// Enc計測パルス数
		WORD		wOvf ;			// OverFlow数(0x10000)
		DWORD		dwExtTimer ;	// 拡張TimerCount
		WORD		wSpeed ;		// 速度(0.1mS)
		WORD		wVrev ;			// 回転速度(rpm)
		WORD		wVrevPrv ;		// 前回回転速度(rpm)
		WORD		wVrev_Abs ;		// 絶対回転速度(rpm)
		WORD		wVrevDiff ;		// 回転速度差(rpm)
		WORD		wAccStatCount ;	// 加減速判定カウンタ
		WORD		wAccStat ;		// 加減速ステータス
		DWORD		dwPos ;			// 位置(enc)
		FLAG		fUpLimit ;		// 上昇端フラグ
//		WORD		wSpeedE ;		// 電気角速度(0.1mS)
//		WORD		wVrevE ;		// 電気角回転速度(E-rpm)
//		WORD		wVrevE_Abs ;	// 電気角絶対回転速度(E-rpm)
//		WORD		wVrevE_Prv ;	// 前回電気角回転速度(E-rpm)
	} ENC_INF ;						// gENCで定義
	


//--------------------------------
// モータ相電流指示情報
//※高所作業者からの移植分改変の必要有 9/28追加
//--------------------------------
typedef struct	_MOT_PHASE_ {
		WORD		wIu ;			// U相指示
		WORD		wIv ;			// V相指示
		WORD		wIw ;			// W相指示
	} MOT_PHASE ;					// gMph[]


//--------------------------------
// PWM処理
// 単軸の制御のためAXISからPOLEに変更
//※高所作業者からの移植分改変の必要有 9/28追加
//--------------------------------

// 1シャント電流計測ステータス
typedef enum _CR_SS_ {
		CSS_NOTHING = 0,			// 計測しない
		CSS_IUMIV = 1,				// iu,-iv計測
		CSS_IUMIW = 2,				// iu,-iw計測
		CSS_IVMIU = 3,				// iv,-iu計測
		CSS_IVMIW = 4,				// iv,-iw計測
		CSS_IWMIU = 5,				// iw,-iu計測
		CSS_IWMIV = 6				// iw,-iv計測
	} CR_SS ;						// CrSSにて定義
	
// 計測グループステータス
typedef enum _CR_STAT_ {
		CST_NOTHING = 0,			// 計測しない
		CST_GRP_A = 1,				// TADCORA(TRG4AN)対象
		CST_GRP_B = 2				// TADCORB(TRG4BN)対象
	} CR_STAT ;						// CrStatにて定義

typedef struct	_MTU_PWM_ {
		UWORD			uwPwmFull ;	// PWM周期
		UWORD			uwPwmHalf ; // PWM周期半分
		UWORD			uwPwmZero ;	// Duty50%時の値
		UWORD			uwMax ;		// PWM値Max
		UWORD			uwMin ;		// PWM値Min
		WORD			wRngMax ;	// Duty最大絶対値
		WORD			wDutyExe ;	// DutyExe BLDC用
		WORD			wDirU ;		// U相極性
		WORD			wDirV ;		// V相極性
		WORD			wDirW ;		// W相極性
//		UWORD			uwAdCmp ;	// AD変換開始要求周期
		UWORD			uwDutyU ;	// U相Duty
		UWORD			uwDutyV ;	// V相Duty
		UWORD			uwDutyW ;	// W相Duty
		UWORD			uwAdCobrA ;	// TADCOBRA
		UWORD			uwAdCobrB ;	// TADCOBRB
		UWORD			uwAdCmp ;	// AD変換開始要求周期(未使用)
		CR_SS			CrSS ;		// 1shunt計測Status
		CR_STAT			CrStat ;	// 計測状況
	} MTU_PWM ;						// gPwm	



//--------------------------------
// AD変換情報
//--------------------------------
//モータ電流	
typedef struct	_ADC_MOT_ {
		UWORD	uwAddrA ;			// AD値 TADCORA(TRG4AN)対象 相電流計測用
		UWORD	uwAddrB ;			// AD値 TADCORB(TRG4BN)対象 相電流計測用
		WORD	wIu ;				// U相実電流値(ADC)
		WORD	wIv ;				// V相実電流値(ADC)
		WORD	wIw ;				// W相実電流値(ADC)
		FLOAT	flIu ;				// U相実電流値(A)
		FLOAT	flIv ;				// V相実電流値(A)
		FLOAT	flIw ;				// W相実電流値(A)
/**/	DWORD	dwSum ;				// Motor UVW sum
/**/	WORD	uwSumCount ;			// Motor UVW sum count
/**/	DWORD	dwCur ;				// Motor UVW curr.
		UWORD	uwOfsTime ;			// Motor curr. OffsetTimer
		UWORD	uwOfs ;				// Motor curr. Offset(raw)
		UWORD	uwOfsCount ;		// Motor curr. OffsetAvgSumCount
		UDWORD	udwOfsSum ;			// Motor curr. OffsetAvgSum
		FLAG	fOfsGet ;			// Offset Measure Flag
		FLAG	fEncChg ;			// Enc.PhaseChange
		FLAG	fDbg ;				// Debug Flag
		FLAG	fSlow ;				// SlowMesure Flag
		UBYTE	ubOfsCheck ;		// PWM未出力集計相チェック
		UBYTE	ubEncCount ;		// Motor Encoder Pulse Count		 ※元
		UBYTE	ubEncCountCmp ;		// Motor Encoder Pulse Count Compair ※元
//		WORD	wEncVrevPrv ;		// Previous Motor rpm
	} ADC_MOT ;						// Motにて定義
	

// 電源電圧
typedef struct	_ADC_VAL_ {
		UWORD	uwVal ;				// ADC値
		UWORD	uwValMath ;			// ADC値*10
		UDWORD	udwSum ;			// ADC積算値
		UWORD	uwCount ;			// ADC計測回数
	} ADC_VAL ;						// V24にて定義
typedef struct	_ADC_VOLT_ {
		ADC_VAL		V24 ;			// 24V電源電圧
		FLAG		fMeas24 ;		// 24V計測Flag
		UWORD		uwV24 ;			// 24V電源電圧値(0.1V/Unit)
		UWORD		uwPoffRaw ;		// POFF_INT電源電圧値
	} ADC_VOLT ;					// Voltにて定義

typedef struct	_ADC_INF_ {			// 24V電源電圧監視用
		FLAG		fAdInt ;		// A/D割り込み状況
		ADC_MOT		Mot ;			// モータ電流計測
		ADC_VOLT	Volt ;			// 電圧計測用
	} ADC_INF ;						// gAdcにて定義 
		
// 計測Flag		fMeas
#define	ADC_MEASURE_NO		0		// 未計測
#define	ADC_MEASURE_YES		1		// 計測済



//==========================================================
// システム情報
//==========================================================

#define	BRAKE_FLAG_LOCK		1		// BrakeLockFlag
#define	BRAKE_FLAG_FREE		0		// BrakeFreeFlag

//--------------------------------
// データフラッシュ システムデータ
//--------------------------------
typedef struct	_DF_FBV_ {				// 速度FB-Parm
			BYTE	bType ;				// フィードバックタイプ
			WORD	wFbConstP ;			// P:比例定数(Kp)
			WORD	wFbConstI ;			// I:積分定数(Ki)
			WORD	wFbConstD ;			// D:微分定数(Kd)
			WORD	wFbAccelP ;			// P:比例定数加速%
			WORD	wFbAccelI ;			// I:積分定数加速%
			WORD	wFbAccelD ;			// D:微分定数加速%
			WORD	wFbDecelP ;			// P:比例定数減速%
			WORD	wFbDecelI ;			// I:積分定数減速%
			WORD	wFbDecelD ;			// D:微分定数減速%
			WORD	wRevLowOvr ;		// 低速オーバーライド回転数
			WORD	wRevLowOvrLimit ;	// 低速オーバーライド下限回転数
			WORD	wFbvLowLimit ;		// 速度FBでの指示速度下限リミッタ
//			WORD	wStartRev ;			// 開始指示回転数
			WORD	wRevMax ;			// 最高回転数
//			WORD	wRevMin ;			// 最少回転数
			WORD	wLimitI ;			// I値制限値
//**********************************
//I値制限値を現在速度から求めるための変数
/**/		WORD	wCurCompBase ;		// 電流比較値算出基数
/**/		WORD	wCCB_LowLimit ;		// 電流比較値算出基数%下限
/**/		WORD	wLimitI_Init ;		// I:制限初期値(1/1000)
/**/		WORD	wLimitI_Ratio ;		// I:制限スロープ(I/rpm)
/**/		WORD	wLimitI_RateC ;		// I:電流制限定数	
//**********************************
			WORD	wPwmRatio ;			// PWM変換係数
			WORD	wPwmOffset ;		// PWM指令Offset
	} DF_FBV ;							// FBV
	
typedef struct	_DF_PWM_ {				// PWM制限-Parm
			WORD	wLimit_Init ;		// PWM:制限初期値
			WORD	wLimit_Ratio ;		// PWM:制限Slope(100pwm/rpm)
			WORD	wLimitCur_Init ;	// PWM:電流制限初期値
			WORD	wLimitCur_Ratio ;	// PWM:電流制限Slope(100pwm/adc)
			WORD	wLimitCur_Rate ;	// PWM:電流制限率(%)
			WORD	wVfb_Ratio ;		// PWM:指示速度Slope
			WORD	wVCompWidth ;		// PWM:制限比較速度幅(rpm)
			UWORD	uwPwmTime ;			// PWMキャリア周波数1/2
			
	} DF_PWM ;							// PWM
	
typedef struct	_DF_FB_ {				// FB-Parm
			WORD	wAccStep ;			// 加速ステップ量(rpm/scan)
			WORD	wDecStep ;			// 減速ステップ量(rpm/scan)
			WORD	wEmmStep ;			// 非常停止時減速ステップ(rpm/scan)
			WORD	wStopStep ;			// 停止時の減速PWM値ステップ
//			WORD	wPOEStop ;			// POE作動時のパラメータ 10/21追加
	} DF_FB ;							// FB

typedef struct	_DF_DRV_ {				// 駆動制御-Parm
			WORD	wRpmCvtRatio ;		// rpm変換係数(0.1%)
			WORD	wUpStartSpeed ;		// 上昇開始指示値
			WORD	wDownStartSpeed ;	// 下降開始指示値
			WORD	wUpSpeedNormal ;	// 上昇速度(通常)
			WORD	wDownSpeedNormal ;	// 下降速度(通常)
			WORD	wUpSpeedHalf ;		// 上昇速度(半分)
			WORD	wDownSpeedHalf ;	// 下降速度(半分)
	} DF_DRV ;							// DRV
	
// Position-Parm
typedef struct	_DF_POS_ {				// 位置管理-Parm
			WORD	wUpDownStat ;		// 多点停止の上昇/下降停止ステータス
			DWORD	dwPos ;				// 現在位置カウント
			DWORD	dwUpLimit ;			// 上限位置
			DWORD	dwUpLmtDecArea ;	// 上限減速区間
			DWORD	dwDwLmtDecArea ;	// 下限減速区間
			DWORD	dwUpLmtAccBan ;		// 上限加速禁止区間
			DWORD	dwDwLmtAccBan ;		// 下限減速禁止区間
			DWORD	dwLev1 ;			// 多点停止1
			DWORD	dwLev2 ;			// 多点停止2
			DWORD	dwLev3 ;			// 多点停止3
			DWORD	dwLev4 ;			// 多点停止4
			DWORD	dwLev5 ;			// 多点停止5
			DWORD	dwLev6 ;			// 多点停止6
			DWORD	dwLev7 ;			// 多点停止7
			DWORD	dwLev8 ;			// 多点停止8
			DWORD	dwLev9 ;			// 多点停止9
			DWORD	dwLev10 ;			// 多点停止10
	} DF_POS ;							// POS
	

// Sys-Parm
typedef struct	_DF_SYS_ {				// Sys-Parm
			UBYTE	ubDrvMode ;			// 駆動モード : 通常 or 自己保持
/**/		WORD	wMotCurLimit ;		// モータ過電流値(ADC)
//			UWORD	uwUpTimOut ;		// 上昇タイムアウト(T1S)
//			UWORD	uwDownTimOut ;		// 下降タイムアウト(T1S)
//			WORD	wEncRevRatio ;		// 機械角/電気角比
//			WORD	wChargeEff ;		// 回生充電効率(%)

	} DF_SYS ;							// SYS
	
typedef struct	_DF_CTL_ {				// ROM-Control Inf.
			UWORD	uwCrc ;				// SYS+USR-CRC値
	} DF_CTL ;							// CTL

// MotCurrentMeasureParm
typedef struct	_DF_MCM_ {
//			WORD	wAdAdjOffset ;		// AD値補正オフセット offset
			UWORD	uwCurDutyLimit ;	// 電流ADC値最低リミット(0.01%)
			UWORD	uwCurDutyAdj ;		// 電流ADC値補正値
	} DF_MCM ;							// MCM

// Err-Parm
/*typedef struct	_ERR_SYS_ {				// Sys-Parm
//			UWORD	uwUpTimOut ;		// 上昇タイムアウト(T1S)
//			UWORD	uwDownTimOut ;		// 下降タイムアウト(T1S)
//			UWORD	uwRevOvrUpDiff ;	// 上昇時速度超過
//			UWORD	uwRevLowUpDiff ;	// 上昇時速度不足
//			UWORD	uwRevOvrDownDiff ;	// 下降時速度超過
//			UWORD	uwRevLowDownDiff ;	// 下降時速度不足
//			UWORD	uwMotStartNoneCnt ; // 指示後未動作エラー
	} ERR_SYS ;							// ERR
*/	
// データフラッシュ情報構造体			
typedef struct	_D_FLASH_ {
			DF_FBV	FBV ;				// 速度FB-Parm
			DF_PWM	PWM ;				// PWM制限-Parm
			DF_FB	FB ;				// FB-Parm
			DF_DRV	DRV ;				// 駆動制御-Parm
			DF_POS	POS ;				// 位置管理-Parm
			DF_SYS	SYS ;				// Sys-Parm
			DF_CTL	CTL ;				// ROM-Control Inf.
			DF_MCM	MCM ;				// MotCurrentMeasureParm
//			ERR_SYS ERR ;				// エラー用-Parm
	} D_FLASH ;							// gDf

// データフラッシュパラメータメンテ情報		// 10/1 追加
typedef struct	_DF_MAINT_ {
		WORD			wData ;				// パラメータデータ
		WORD			wMax ;				// パラメータ最大値
		WORD			wMin ;				// パラメータ最小値
		FLAG			fType ;				// データ型
//		UWORD			uwCmdFlag ;			// コマンドフラグ
//		ST_PARM_CMD		Cmd ;				// コマンドフラグ構造体
	} DF_MAINT ;							// Maint
	
// メンバ:fSign								// 10/1 追加
#define	DF_TYPE_SIGHNED			0			// 符号有り
#define	DF_TYPE_UNSIGHNED		1			// 符号無し
#define	DF_TYPE_NO_DATA			0xff		// データが存在しない


//-----------------------------------------------------
// システム情報
//-----------------------------------------------------

typedef struct _SYS_INF_ {
		FLAG		fPwrOff	;			// パワーオフフラグ
//		UBYTE		ubSlow:1 ;			// 低速走行フラグ 追加
//		WORD		wTorqueReq ;		// 指示トルク値 -1023 〜 +1023 追加
//		WORD		wTorqueReqPrv ;		// 指示トルク値 前回処理
		UBYTE		ubMoFlag ;			// 長押しモードフラグ
		WORD		wUpSpeed ;			// 上昇スピード
		WORD		wDownSpeed ;		// 下降スピード
//		UBYTE		fBrakeFlag ;		// ブレーキフラグ
//		UBYTE		fBrakeFlag_Prv ;	// ブレーキフラグ 前回
		FLAG		fDriveEnable ;		// 走行駆動指示許可
		BYTE		bPoffInt ;			// 電源遮断検出
		FLAG		fPOEStop ;			// POE停止Flag			※10/21追加
		FLAG		fLVDErr ;			// LVD作動フラグ
//		FLAG		fMotTrip ;			// モータ過電流トリップFlag
//		FLAG		fMotTripPrc ;		// モータ過電流トリップ処理Flag
//		UBYTE		ubMotOvc ;			// モータ過電流 Flag
		UWORD		uwMotOvrevMgn ;		// 走行モータ過回転値(rpm)
//		UBYTE		ubMotOverRev ;		// 走行モータ過回転
//		UBYTE		ubMotReverse ;		// 走行モータが指示に対して逆転
//		UBYTE		ubMovStat ;			// 動作ステータス
//		FLAG		fMotDrv ;			// モータ動作中フラグ
		FLAG		fEncErr ;			// エンコーダエラーFlag
		FLAG		fEncErrPrc ;		// エンコーダエラー処理Flag
		FLAG		fPMaint ;			// ParmMaintMode Flag
		FLAG		fPMaintChg ;		// ParmMaintChange Flag
		FLAG		fEmm ;				// 非常停止Flag
		FLAG		fModeChage ;		// モード切替フラグ
		FLAG		fStpLong ;			// 停止ボタン長押しモード
		FLAG		fUpLong ;			// 上昇ボタン長押しモード
		FLAG		fDwLong ;			// 下降ボタン長押しモード
//		UWORD		uwStat ;			// 走行駆動状態
		WORD		wVrev ;				// モータ仮想回転数 単位はrpm, 前進が+
		WORD		wTorqueOut ;		// 出力トルク値 -1023 〜 +1023 
										// 単位は無い 前進が+
		FLAG		fErrLoopFlag ;		// エラーループフラグ
		UWORD		uwErrCount ;		// エラー監視用カウンタ
		UBYTE		ubErrCode ;			// エラーコード
		UBYTE		ubErrCate ;			// エラーコードカテゴリー
		UWORD		uwMotUpStartCnt ;	// エラーカウンタ
		UWORD		uwMotDownStartCnt ;	// エラーカウンタ
		WORD		wErrVoltage	;		// 電圧エラー時の電圧(V)
		UBYTE		ubLEDStat ;			// LEDステータス
		UBYTE		ubLEDPat[ 4 ] ;		// LEDパターン判定
		BYTE		bPat ; 				// LED配列
		UBYTE		ubMotStat ;			// 
		UBYTE		ubTestMode ;		// テストモード
	} SYS_INF ;						// gSysInfにて定義

//-----------------------------------------------------
// エラー情報
// ※11/19 NXSからの移植->編集必須
//-----------------------------------------------------


typedef struct	_MC_VAL_ {					// 電流計測値構造体
		UDWORD		udwSum ;				// 電流積算値
		UWORD		uwPeak ;				// 電流ピーク値
		UWORD		uwCount ;				// 電流計測回数
	} MC_VAL ;

typedef struct	_PWM_VAL_ {					// pwm計測値構造体
		DWORD		dwSum ;					// pwm値積算値
		UWORD		uwCount ;				// pwm計測回数
	} PWM_VAL ;

// MC[2] 0:ACC 1:CV
typedef struct _MES_INF_ {					// 計測情報構造体
		MC_VAL		MC[2] ;					// 電流計測値構造体 Acc/Cv
		PWM_VAL		PWM ;					// pwm計測値構造体
	} MES_INF ;


// 計測値配列Idx定義
#define	OVCIDX_SLOW 		0				// 低速
#define	OVCIDX_HIGH 		1				// 高速

// MES[] 0:Slow 1:High
typedef struct _ERR_INF_ {					// 過電流設定情報構造体
		BYTE		bStat ;					// 現在のOvc処理モード
		BYTE		bSpeedIdx ;				// 速度Idx 0/1
		WORD		wSpeed ;				// 速度
		WORD		wStroke ;				// 上限高さ
		UWORD		uwRtrip ;				// 往復耐久回数閾値
		MES_INF		MES[2] ;				// 計測情報構造体 Slow/High
		BYTE		bSw3_Bak ;				// DIP3のバックアップ
		BYTE		bSw4_Bak ;				// DIP4のバックアップ
		BYTE		bMstopIdx_Bak ;			// 停止位置Idxバックアップ
	} ERR_INF ;

#endif		// _TYPE_DEFINED_

//-----------------------------------------------------------------------

