//=======================================================================
//
//	AvP[VRtBO[Vpwb_
//	
//
//=======================================================================


#ifndef	_DEF_APP_CONF_HDR_					// ½dè`Îô
 #define	_DEF_APP_CONF_HDR_

// _Common
#include	"TypeDef.h"						// î{^è`




//--------------------------------
// Á¬xZTRS485îñ
//--------------------------------
// ZTæ¾C^[o
#define	ACC_GET_INTERVAL		20				// ZTScanÔu(mS)
//#define	ACC_GET_INTERVAL		40				// ZTScanÔu(mS) dbg
//#define	ACC_GET_INTERVAL		80				// ZTScanÔu(mS) dbg
// t[óM^CAEgl
#define	ACC_FANZ_TIMEOUT		16				// óM^CAEg(mS)
//#define	ACC_FANZ_TIMEOUT		35				// óM^CAEg(mS) dbg
//#define	ACC_FANZ_TIMEOUT		35				// óM^CAEg(mS) dbg
//#define	ACC_FANZ_TIMEOUT		75				// óM^CAEg(mS) dbg

// ZTú»^CAEg
#define	ACC_INZ_TIMEOUT			500				// ú»^CAEg(10mS)

// ZTîÂ[gR}h^CAEg
#define	ACC_CMD_TIMEOUT			50				// Rmt-Cmd^CAEg(10mS)

//--------------------------------
// tgîñ
//--------------------------------
// ouì®x
//#define	LIFT_VDLY_VAL			20				// ouì®xÔ(10mS)
#define	LIFT_VDLY_VAL			20				// ouì®xÔ(10mS)

// º~ÜOµmF^CAEg
#define	LIFT_LOCKOUT_TIMEOUT	400				// ÜOµ^CAEg(10mS)

// I[gXgbv
//#define	LIFT_ASTOP_STD_SPEED		100		// â~»èî¬x(0.001x/b)
#define	LIFT_ASTOP_CHK_SPEED		180			// â~»èÀ¬x(0.001x/b)
#define	LIFT_ASTOP_MIN_WORK_TIME	30			// Åáì®Ô(10mS)
//#define	LIFT_ASTOP_KEEP_TIME		20			// â~»èÛÔ(100mS)
//#define	LIFT_ASTOP_KEEP_TIME_U		10			// â~»èÛÔ(100mS)
#define	LIFT_ASTOP_KEEP_TIME_U		12			// â~»èÛÔ(100mS)
#define	LIFT_ASTOP_KEEP_TIME_D		18			// â~»èÛÔ(100mS)



//--------------------------------
// tgâ~V[PXîñ
//--------------------------------
#define	LIFT_STOP_PWM	(UWORD)((DWORD)MTU_PWM_TIME * 95 / 100) 
												// â~PWMwßl(5937)
/*
//#define	LIFT_STOP_WRING_WAIT		30			// äád¥ÙÀèÒ¿(mS)
//#define	LIFT_STOP_WRING_WAIT		200			// äád¥ÙÀèÒ¿(mS)
#define	LIFT_STOP_WRING_WAIT		20			// äád¥ÙÀèÒ¿(mS)
//#define	LIFT_STOP_SHUTOFF_WAIT		24			// VbgItJúÔ(mS)
//#define	LIFT_STOP_SHUTOFF_WAIT		20			// VbgItJúÔ(mS)
#define	LIFT_STOP_SHUTOFF_WAIT		22			// VbgItJúÔ(mS)
#define	LIFT_STOP_WRING				3000		// äád¥Ùâ~ièl
*/
// 2014.11.20
#define	LIFT_STOP_WRING_WAIT		100			// äád¥ÙÀèÒ¿(mS)
#define	LIFT_STOP_SHUTOFF_WAIT		0			// VbgItJúÔ(mS)
#define	LIFT_STOP_WRING				1500		// äád¥Ùâ~ièl


//==========================================
// tB[hobN
//==========================================
// ºÀZTpx(0.001x)
//#define	FB_ANG_LOWER			13550			// ºÀÅÌZTpx
#define	FB_ANG_LOWER			13400			// ºÀÅÌZTpx

// FeedbackÔu
//#define	FBA_INTERVAL			60				// pxFBÔu(mS)
//#define	FBA_INTERVAL			50				// pxFBÔu(mS)
#define	FBA_INTERVAL			38				// pxFBÔu(mS)
//#define	FBA_INTERVAL			29				// pxFBÔu(mS)
#define	FBV_INTERVAL			20				// ¬xFBÔu(mS)

// tB[hobN®I²®
// `2014.10.03
//#define	FB_ADJ_INTERVAL			20				// Fb®I²®Ôu(10mS)
// 2014.10.06`
#define	FB_ADJ_INTERVAL			10				// Fb®I²®Ôu(10mS)

// útB[hobNèbNÔ
/*
// 2014.10.02
#define	FBV_CONST_LOCK_TIME_U	400				// úFBèLockÔ(10mS)
#define	FBV_CONST_LOCK_TIME_D	500				// úFBèLockÔ(10mS)
*/
/*
// 2014.10.08 for test
#define	FBV_CONST_LOCK_TIME_U	400				// úFBèLockÔ(10mS)
#define	FBV_CONST_LOCK_TIME_D	2990				// úFBèLockÔ(10mS)
*/
// 2014.10.20 for test
#define	FBV_CONST_LOCK_TIME_U	500				// úFBèLockÔ(10mS)
#define	FBV_CONST_LOCK_TIME_D	2990				// úFBèLockÔ(10mS)

// 2014.10.02
#define	FBV_CONST_LOCK_TIMEOUT	9999			// FBèLock^CAEgl

// 2014.11.18
//#define	FBV_CONST_LOCK_GSTEP	50				// FBèLockQCªStep

// 2014.11.20
#define	FBV_CONST_LOCK_GSTEP	53				// FBèLockQCªStep


//--------------------------------
// pxtB[hobN
//--------------------------------

// MaxNpx·§Àl 2014.10.14-
/*
//#define	FBA_ANGLEDIFF_MAX		1600			// MaxNpx·0.001x
//#define	FBA_ANGLEDIFF_FACTOR	1000			// l~bgt@N^
#define	FBA_ANGLEDIFF_MAX_U		1200			// MaxNpx·0.001x
#define	FBA_ANGLEDIFF_MAX_D		1200			// MaxNpx·0.001x
#define	FBA_ANGLEDIFF_FACTOR_U	500				// l~bgt@N^
#define	FBA_ANGLEDIFF_FACTOR_D	500				// l~bgt@N^
*/
// 2014.11.20-01
#define	FBA_ANGLEDIFF_MAX_U		1000			// MaxNpx·0.001x
#define	FBA_ANGLEDIFF_MAX_D		1200			// MaxNpx·0.001x
#define	FBA_ANGLEDIFF_FACTOR_U	1000			// l~bgt@N^
#define	FBA_ANGLEDIFF_FACTOR_D	500				// l~bgt@N^

// FBQC    TICKnÍâÎl
/*
// 2014.10.09-01
#define	FBA_CONST_PU_INIT		800					// P(äá)QC
#define	FBA_CONST_PU_END1		1200				// P(äá)QC
#define	FBA_CONST_PU_TICK1		10				// P(äá)QC
#define	FBA_CONST_PU_END2		1000				// P(äá)QC
#define	FBA_CONST_PU_TICK2		10				// P(äá)QC
#define	FBA_CONST_IU_INIT		110				// I(Ïª)QC
#define	FBA_CONST_IU_END1		150				// I(Ïª)QC
#define	FBA_CONST_IU_TICK1		2				// I(Ïª)QC
#define	FBA_CONST_IU_END2		120				// I(Ïª)QC
#define	FBA_CONST_IU_TICK2		1				// I(Ïª)QC
#define	FBA_CONST_DU			0				// D(÷ª)QC
#define	FBA_CONST_PD_INIT		400				// P(äá)QC
#define	FBA_CONST_PD_END1		1100			// P(äá)QC
#define	FBA_CONST_PD_TICK1		12				// P(äá)QC
#define	FBA_CONST_PD_END2		850				// P(äá)QC
#define	FBA_CONST_PD_TICK2		3				// P(äá)QC
#define	FBA_CONST_ID_INIT		220				// I(Ïª)QC
#define	FBA_CONST_ID_END1		600				// I(Ïª)QC
#define	FBA_CONST_ID_TICK1		10				// I(Ïª)QC
#define	FBA_CONST_ID_END2		450				// I(Ïª)QC
#define	FBA_CONST_ID_TICK2		5				// I(Ïª)QC
#define	FBA_CONST_DD			0				// D(÷ª)QC
*/
/*
// 2014.10.29-01		TickÏX
#define	FBA_CONST_PU_INIT		800					// P(äá)QC
#define	FBA_CONST_PU_END1		1200				// P(äá)QC
#define	FBA_CONST_PU_TICK1		10				// P(äá)QC
#define	FBA_CONST_PU_END2		1000				// P(äá)QC
#define	FBA_CONST_PU_TICK2		-10				// P(äá)QC
#define	FBA_CONST_IU_INIT		110				// I(Ïª)QC
#define	FBA_CONST_IU_END1		150				// I(Ïª)QC
#define	FBA_CONST_IU_TICK1		2				// I(Ïª)QC
#define	FBA_CONST_IU_END2		120				// I(Ïª)QC
#define	FBA_CONST_IU_TICK2		-1				// I(Ïª)QC
#define	FBA_CONST_DU			0				// D(÷ª)QC
#define	FBA_CONST_PD_INIT		400				// P(äá)QC
#define	FBA_CONST_PD_END1		1100			// P(äá)QC
#define	FBA_CONST_PD_TICK1		12				// P(äá)QC
#define	FBA_CONST_PD_END2		850				// P(äá)QC
#define	FBA_CONST_PD_TICK2		-3				// P(äá)QC
#define	FBA_CONST_ID_INIT		220				// I(Ïª)QC
#define	FBA_CONST_ID_END1		600				// I(Ïª)QC
#define	FBA_CONST_ID_TICK1		10				// I(Ïª)QC
#define	FBA_CONST_ID_END2		450				// I(Ïª)QC
#define	FBA_CONST_ID_TICK2		-5				// I(Ïª)QC
#define	FBA_CONST_DD			0				// D(÷ª)QC
*/
// 2014.11.20-01		TickÏX
#define	FBA_CONST_PU_INIT		1000				// P(äá)QC
#define	FBA_CONST_PU_END1		1200				// P(äá)QC
#define	FBA_CONST_PU_TICK1		10				// P(äá)QC
#define	FBA_CONST_PU_END2		1000				// P(äá)QC
#define	FBA_CONST_PU_TICK2		-10				// P(äá)QC
#define	FBA_CONST_IU_INIT		165				// I(Ïª)QC
#define	FBA_CONST_IU_END1		165				// I(Ïª)QC
#define	FBA_CONST_IU_TICK1		2				// I(Ïª)QC
#define	FBA_CONST_IU_END2		130				// I(Ïª)QC
#define	FBA_CONST_IU_TICK2		-1				// I(Ïª)QC
#define	FBA_CONST_DU			0				// D(÷ª)QC
#define	FBA_CONST_PD_INIT		500				// P(äá)QC
#define	FBA_CONST_PD_END1		1100			// P(äá)QC
#define	FBA_CONST_PD_TICK1		12				// P(äá)QC
#define	FBA_CONST_PD_END2		850				// P(äá)QC
#define	FBA_CONST_PD_TICK2		-3				// P(äá)QC
#define	FBA_CONST_ID_INIT		300				// I(Ïª)QC
#define	FBA_CONST_ID_END1		600				// I(Ïª)QC
#define	FBA_CONST_ID_TICK1		10				// I(Ïª)QC
#define	FBA_CONST_ID_END2		450				// I(Ïª)QC
#define	FBA_CONST_ID_TICK2		-5				// I(Ïª)QC
#define	FBA_CONST_DD			0				// D(÷ª)QC

// FB-Out W
/*
// 2014.10.09
#define	FBA_OUT_RATE_A			20				// FB-Out W ªq
#define	FBA_OUT_RATE_B			100				// FB-Out W ªê
*/
// 2014.10.22
#define	FBA_OUT_RATE_A_U		20				// FB-Out W ªq
#define	FBA_OUT_RATE_A_D		20				// FB-Out W ªq
#define	FBA_OUT_RATE_B			100				// FB-Out W ªê

// I§Àl I[o[t[Îô
//#define	FBA_I_MAX				220000
// 2014.09.30
//#define	FBA_I_MAX				2200000
#define	FBA_I_MAX				620000
#define	FBA_I_MIN				(-FBA_I_MAX)



//--------------------------------
// pxFBl->¬xFB
//--------------------------------

// pxFBl->¬xFB üÍQC(x/1000)
/*
// 2014.10.09-01
#define	FBV_ADJ_ANGLE_GAIN_U	330			// ¬xAdj(FBAüÍ)QC ã¸
#define	FBV_ADJ_ANGLE_GAIN_D	180			// ¬xAdj(FBAüÍ)QC º~
*/
/*
// 2014.11.14-01
#define	FBV_ADJ_ANGLE_GAIN_U	300			// ¬xAdj(FBAüÍ)QC ã¸
#define	FBV_ADJ_ANGLE_GAIN_D	1			// ¬xAdj(FBAüÍ)QC º~
*/
// 2014.11.20-01
#define	FBV_ADJ_ANGLE_GAIN_U	310			// ¬xAdj(FBAüÍ)QC ã¸
#define	FBV_ADJ_ANGLE_GAIN_D	30			// ¬xAdj(FBAüÍ)QC º~

// pxFBl->PWM _CNgüÍQC(x/100)
/*
// 2014.10.09-01
#define	FBV_ADIFF_PWM_GAIN_U	120				// Adiff->PWM  ã¸
#define	FBV_ADIFF_PWM_GAIN_D	20				// Adiff->PWM  º~
#define	FBV_FBA_PWM_GAIN_U		145				// FBA_OUT->PWM ã¸
#define	FBV_FBA_PWM_GAIN_D		80				// FBA_OUT->PWM º~
*/
/*
// 2014.11.14-01
#define	FBV_ADIFF_PWM_GAIN_U	140				// Adiff->PWM  ã¸
#define	FBV_ADIFF_PWM_GAIN_D	80				// Adiff->PWM  º~
#define	FBV_FBA_PWM_GAIN_U		110				// FBA_OUT->PWM ã¸
#define	FBV_FBA_PWM_GAIN_D		20				// FBA_OUT->PWM º~
*/
// 2014.11.20-01 x10
#define	FBV_ADIFF_PWM_GAIN_U	1300			// Adiff->PWM  ã¸
#define	FBV_ADIFF_PWM_GAIN_D	800				// Adiff->PWM  º~
#define	FBV_FBA_PWM_GAIN_U		1250			// FBA_OUT->PWM ã¸
#define	FBV_FBA_PWM_GAIN_D		220				// FBA_OUT->PWM º~

// ã¸Ì¯²ªîñ 2014.11.17`
/*
#define	FBV_SDIV_VADJ_P			50				// ÚW¬x(Vref) vX
#define	FBV_SDIV_VADJ_M			50				// ÚW¬x(Vref) }CiX
#define	FBV_SDIV_ANGPWM_P		50				// px·->PWM(Diff) vX
#define	FBV_SDIV_ANGPWM_M		50				// px·->PWM(Diff) }CiX
#define	FBV_SDIV_AFBPWM_P		50				// pxFB->PWM(Diff) vX
#define	FBV_SDIV_AFBPWM_M		50				// pxFB->PWM(Diff) }CiX
*/
// 2014.11.20-01
#define	FBV_SDIV_VADJ_P			150				// ÚW¬x(Vref) vX
#define	FBV_SDIV_VADJ_M			10				// ÚW¬x(Vref) }CiX
#define	FBV_SDIV_ANGPWM_P		150				// px·->PWM(Diff) vX
#define	FBV_SDIV_ANGPWM_M		10				// px·->PWM(Diff) }CiX
#define	FBV_SDIV_AFBPWM_P		150				// pxFB->PWM(Diff) vX
#define	FBV_SDIV_AFBPWM_M		10				// pxFB->PWM(Diff) }CiX


// FBA®IQCÏ®îñ ÁÅI,¸­JnÍ1000/1000 2014.10.04ÇÁ
/*
// 2014.10.22-01
#define FBV_AGAIN_U_INIT		800				// QC1/1kúl ã¸
#define FBV_AGAIN_U_TICK1		7				// QC1/1kÁl ã¸Jn
#define FBV_AGAIN_U_END2		1000			// QC1/1kÅIl ã¸I¹
#define FBV_AGAIN_U_TICK2		-5				// QC1/1k¸­l ã¸I¹
#define FBV_AGAIN_D_INIT		500				// QC1/1kúl º~
#define FBV_AGAIN_D_TICK1		12				// QC1/1kÁl º~Jn
#define FBV_AGAIN_D_END2		1000			// QC1/1kÅIl º~I¹
#define FBV_AGAIN_D_TICK2		-5				// QC1/1k¸­l º~I¹
*/
/*
// 2014.11.14-01
#define FBV_AGAIN_U_INIT		700				// QC1/1kúl ã¸
#define FBV_AGAIN_U_TICK1		7				// QC1/1kÁl ã¸Jn
#define FBV_AGAIN_U_END2		1000			// QC1/1kÅIl ã¸I¹
#define FBV_AGAIN_U_TICK2		-5				// QC1/1k¸­l ã¸I¹
#define FBV_AGAIN_D_INIT		500				// QC1/1kúl º~
#define FBV_AGAIN_D_TICK1		12				// QC1/1kÁl º~Jn
#define FBV_AGAIN_D_END2		700				// QC1/1kÅIl º~I¹
#define FBV_AGAIN_D_TICK2		-7				// QC1/1k¸­l º~I¹
*/
// 2014.11.20-01
#define FBV_AGAIN_U_INIT		1000			// QC1/1kúl ã¸
#define FBV_AGAIN_U_TICK1		5				// QC1/1kÁl ã¸Jn
#define FBV_AGAIN_U_END2		600				// QC1/1kÅIl ã¸I¹
#define FBV_AGAIN_U_TICK2		-10				// QC1/1k¸­l ã¸I¹
#define FBV_AGAIN_D_INIT		800				// QC1/1kúl º~
#define FBV_AGAIN_D_TICK1		5				// QC1/1kÁl º~Jn
#define FBV_AGAIN_D_END2		700				// QC1/1kÅIl º~I¹
#define FBV_AGAIN_D_TICK2		-11				// QC1/1k¸­l º~I¹

/*
#define	FBV_AGAIN_DEC_START_ANGLE_U	58000		// QC%¸­Jnpx0.001x
#define	FBV_AGAIN_DEC_START_ANGLE_D	23000		// QC%¸­Jnpx0.001x
*/
// 2014.11.20-01
#define	FBV_AGAIN_DEC_START_ANGLE_U	53000		// QC%¸­Jnpx0.001x
#define	FBV_AGAIN_DEC_START_ANGLE_D	23000		// QC%¸­Jnpx0.001x


//--------------------------------
// ¬xtB[hobN
//--------------------------------

// PWM®²®l 2014.10.02-
/*
// 2014.10.08
#define	FBV_PWM_START_INIT_U	-201			// PWM®²®úl UP
#define	FBV_PWM_START_INIT_D	-140			// PWM®²®úl DOWN
#define	FBV_PWM_START_ADJ_U		155				// PWM®²®Ï®l UP
//#define	FBV_PWM_START_ADJ_D		60				// PWM®²®Ï®l DOWN
#define	FBV_PWM_START_ADJ_D		40				// PWM®²®Ï®l DOWN
#define	FBV_PWM_START_TRATIO_U	50				// eRä²®W % UP
#define	FBV_PWM_START_TRATIO_D	150				// eRä²®W % DOWN
*/
/*
// 2014.11.14
#define	FBV_PWM_START_INIT_U	-800			// PWM®²®úl UP
#define	FBV_PWM_START_INIT_D	450				// PWM®²®úl DOWN
#define	FBV_PWM_START_ADJ_U		155				// PWM®²®Ï®l UP
#define	FBV_PWM_START_ADJ_D		15				// PWM®²®Ï®l DOWN
#define	FBV_PWM_START_TRATIO_U	50				// eRä²®W % UP
#define	FBV_PWM_START_TRATIO_D	150				// eRä²®W % DOWN
*/
// 2014.11.20
#define	FBV_PWM_START_INIT_U	-700			// PWM®²®úl UP
#define	FBV_PWM_START_INIT_D	450				// PWM®²®úl DOWN
#define	FBV_PWM_START_ADJ_U		120				// PWM®²®Ï®l UP
#define	FBV_PWM_START_ADJ_D		15				// PWM®²®Ï®l DOWN
#define	FBV_PWM_START_TRATIO_U	50				// eRä²®W % UP
#define	FBV_PWM_START_TRATIO_D	150				// eRä²®W % DOWN

// úièDutyl
/*
// `2014.11.12
#define	FBV_PWM_START_WRING_U	5000			// úièDutyl UP
#define	FBV_PWM_START_WRING_D	2750			// úièDutyl DOWN
*/
// 2014.11.12`
#define	FBV_PWM_START_WRING_U	5000			// úièDutyl UP
#define	FBV_PWM_START_WRING_D	2650			// úièDutyl DOWN

// î¬x 0.001x/b
//	ã¸    TICKnÍâÎl
/*
// 2014.10.07-02
#define	FBV_SPEED_STD_U			1450			// ã¸î¬x
//#define	FBV_SPEED_STD_U_INIT	260				// ã¸ú¬x
#define	FBV_SPEED_STD_U_INIT	220				// ã¸ú¬x
#define	FBV_SPEED_STD_U_END1	FBV_SPEED_STD_U	// ã¸ÅI¬x
#define	FBV_SPEED_STD_U_TICK1	110				// ã¸Jn¬xÁl
#define	FBV_SPEED_STD_U_END2	1200				// ã¸I¹ÅI¬x
#define	FBV_SPEED_STD_U_TICK2	-36				// ã¸I¹¬xÁl
*/
/*
// 2014.11.14-02
#define	FBV_SPEED_STD_U			1450			// ã¸î¬x
#define	FBV_SPEED_STD_U_INIT	220				// ã¸ú¬x
#define	FBV_SPEED_STD_U_END1	FBV_SPEED_STD_U	// ã¸ÅI¬x
#define	FBV_SPEED_STD_U_TICK1	110				// ã¸Jn¬xÁl
#define	FBV_SPEED_STD_U_END2	1200				// ã¸I¹ÅI¬x
#define	FBV_SPEED_STD_U_TICK2	-20				// ã¸I¹¬xÁl
*/
// 2014.11.20-02
#define	FBV_SPEED_STD_U			1200			// ã¸î¬x
#define	FBV_SPEED_STD_U_INIT	220				// ã¸ú¬x
#define	FBV_SPEED_STD_U_END1	FBV_SPEED_STD_U	// ã¸ÅI¬x
#define	FBV_SPEED_STD_U_TICK1	70				// ã¸Jn¬xÁl
#define	FBV_SPEED_STD_U_END2	1200			// ã¸I¹ÅI¬x
#define	FBV_SPEED_STD_U_TICK2	-22				// ã¸I¹¬xÁl


//	º~    TICKnÍâÎl
/*
// 2014.10.09-01
#define	FBV_SPEED_STD_D			1200			// º~î¬x
//#define	FBV_SPEED_STD_D_INIT	250				// º~ú¬x
//#define	FBV_SPEED_STD_D_INIT	350				// º~ú¬x
#define	FBV_SPEED_STD_D_INIT	450				// º~ú¬x
#define	FBV_SPEED_STD_D_END1	FBV_SPEED_STD_D	// º~ÅI¬x
#define	FBV_SPEED_STD_D_TICK1	60				// º~Jn¬xÁl
#define	FBV_SPEED_STD_D_END2	1050			// º~I¹ÅI¬x
#define	FBV_SPEED_STD_D_TICK2	-15				// º~I¹¬xÁl
*/
/*
// 2014.11.14-01
#define	FBV_SPEED_STD_D			1200			// º~î¬x
#define	FBV_SPEED_STD_D_INIT	450				// º~ú¬x
#define	FBV_SPEED_STD_D_END1	FBV_SPEED_STD_D	// º~ÅI¬x
#define	FBV_SPEED_STD_D_TICK1	60				// º~Jn¬xÁl
#define	FBV_SPEED_STD_D_END2	1100			// º~I¹ÅI¬x
#define	FBV_SPEED_STD_D_TICK2	-5				// º~I¹¬xÁl
*/
// 2014.11.14-01
#define	FBV_SPEED_STD_D			1200			// º~î¬x
#define	FBV_SPEED_STD_D_INIT	450				// º~ú¬x
#define	FBV_SPEED_STD_D_END1	FBV_SPEED_STD_D	// º~ÅI¬x
#define	FBV_SPEED_STD_D_TICK1	60				// º~Jn¬xÁl
#define	FBV_SPEED_STD_D_END2	1180			// º~I¹ÅI¬x
#define	FBV_SPEED_STD_D_TICK2	-2				// º~I¹¬xÁl

// TickÂÏI¹,JnÊu(%)
/*
// 2014.10.01-01
#define	FBV_VTICK_END_RATE		30				// TickÂÏI¹Êu(%)
#define	FBV_VTICK_START_RATE	70				// TickÂÏJnÊu(%)
*/
// 2014.11.20-01
#define	FBV_VTICK_END_RATE		30				// TickÂÏI¹Êu(%)
#define	FBV_VTICK_START_RATE	50				// TickÂÏJnÊu(%)


// ¸¬Jnpx
// 2014.10.02-01
#define	FBV_DEC_START_ANGLE_D	23000			// 0.001x º~
#define	FBV_DEC_START_ANGLE_U	59000			// 0.001x ã¸

// î¬x->PWMlÏ·Ì×Ì¬xW
/*
// 2014.10.08-01
#define	FBV_FF_MULT_MU			230				// ã¸î{¬xW
#define	FBV_FF_MULT_MD			75				// º~î{¬xW
*/
// 2014.11.14-01
#define	FBV_FF_MULT_MU			230				// ã¸î{¬xW
#define	FBV_FF_MULT_MD			60				// º~î{¬xW


// FBQC-ã¸    TICKnÍâÎl

// QCÄ²®Jnpx
/*
// 2014.09.22-01
#define	FBV_CDEC_START_ANGLE_D	26000			// 0.001x º~
#define	FBV_CDEC_START_ANGLE_U	61000			// 0.001x ã¸
*/
// 2014.11.20-01
#define	FBV_CDEC_START_ANGLE_D	26000			// 0.001x º~
#define	FBV_CDEC_START_ANGLE_U	55000			// 0.001x ã¸

/*
// 2014.10.09-01	Ix1
#define	FBV_CONST_PU_INIT		700				// PäáQC(ã¸Jn)
#define	FBV_CONST_PU_END1		900				// PäáQC(ã¸Jn®¹)
#define	FBV_CONST_PU_TICK1		18				// PäáQC(ÏXl)
#define	FBV_CONST_PU_END2		700				// PäáQC(ã¸®¹)
#define	FBV_CONST_PU_TICK2		-15				// PäáQC(ÏXl)
#define	FBV_CONST_IU_INIT		0				// IÏªQC(ã¸Jn)
#define	FBV_CONST_IU_END1		0				// IÏªQC(ã¸Jn®¹)
#define	FBV_CONST_IU_TICK1		1				// IÏªQC(ÏXl)
#define	FBV_CONST_IU_END2		0				// IÏªQC(ã¸®¹)
#define	FBV_CONST_IU_TICK2		-2				// IÏªQC(ÏXl)
#define	FBV_CONST_DU			0				// D÷ªQC(Kd)
*/
// 2014.11.20-01	Ix1
#define	FBV_CONST_PU_INIT		850				// PäáQC(ã¸Jn)
#define	FBV_CONST_PU_END1		850				// PäáQC(ã¸Jn®¹)
#define	FBV_CONST_PU_TICK1		18				// PäáQC(ÏXl)
#define	FBV_CONST_PU_END2		700				// PäáQC(ã¸®¹)
#define	FBV_CONST_PU_TICK2		-22				// PäáQC(ÏXl)
#define	FBV_CONST_IU_INIT		0				// IÏªQC(ã¸Jn)
#define	FBV_CONST_IU_END1		0				// IÏªQC(ã¸Jn®¹)
#define	FBV_CONST_IU_TICK1		1				// IÏªQC(ÏXl)
#define	FBV_CONST_IU_END2		0				// IÏªQC(ã¸®¹)
#define	FBV_CONST_IU_TICK2		-2				// IÏªQC(ÏXl)
#define	FBV_CONST_DU			0				// D÷ªQC(Kd)


// FBQC-º~    TICKnÍâÎl
/*
// 2014.10.16-01	Ix10
#define	FBV_CONST_PD_INIT		500			// PäáQC(º~Jn)
#define	FBV_CONST_PD_END1		600			// PäáQC(º~Jn®¹)
#define	FBV_CONST_PD_TICK1		5				// PäáQC(ÏXl)
#define	FBV_CONST_PD_END2		600			// PäáQC(º~®¹)
#define	FBV_CONST_PD_TICK2		-16				// PäáQC(ÏXl)
#define	FBV_CONST_ID_INIT		0				// IÏªQC(º~Jn)
#define	FBV_CONST_ID_END1		0				// IÏªQC(º~Jn®¹)
#define	FBV_CONST_ID_TICK1		1				// IÏªQC(ÏXl)
#define	FBV_CONST_ID_END2		0				// IÏªQC(º~®¹)
#define	FBV_CONST_ID_TICK2		-1				// IÏªQC(ÏXl)
#define	FBV_CONST_DD			0				// D÷ªQC(Kd)
*/
/*
// 2014.11.14-01	Ix10
#define	FBV_CONST_PD_INIT		500				// PäáQC(º~Jn)
#define	FBV_CONST_PD_END1		1000			// PäáQC(º~Jn®¹)
#define	FBV_CONST_PD_TICK1		10				// PäáQC(ÏXl)
#define	FBV_CONST_PD_END2		900				// PäáQC(º~®¹)
#define	FBV_CONST_PD_TICK2		-5				// PäáQC(ÏXl)
#define	FBV_CONST_ID_INIT		0				// IÏªQC(º~Jn)
#define	FBV_CONST_ID_END1		0				// IÏªQC(º~Jn®¹)
#define	FBV_CONST_ID_TICK1		1				// IÏªQC(ÏXl)
#define	FBV_CONST_ID_END2		0				// IÏªQC(º~®¹)
#define	FBV_CONST_ID_TICK2		-1				// IÏªQC(ÏXl)
#define	FBV_CONST_DD			0				// D÷ªQC(Kd)
*/
// 2014.11.20-01	Ix10
#define	FBV_CONST_PD_INIT		450				// PäáQC(º~Jn)
#define	FBV_CONST_PD_END1		950				// PäáQC(º~Jn®¹)
#define	FBV_CONST_PD_TICK1		10				// PäáQC(ÏXl)
#define	FBV_CONST_PD_END2		900				// PäáQC(º~®¹)
#define	FBV_CONST_PD_TICK2		-5				// PäáQC(ÏXl)
#define	FBV_CONST_ID_INIT		0				// IÏªQC(º~Jn)
#define	FBV_CONST_ID_END1		0				// IÏªQC(º~Jn®¹)
#define	FBV_CONST_ID_TICK1		1				// IÏªQC(ÏXl)
#define	FBV_CONST_ID_END2		0				// IÏªQC(º~®¹)
#define	FBV_CONST_ID_TICK2		-1				// IÏªQC(ÏXl)
#define	FBV_CONST_DD			0				// D÷ªQC(Kd)

// I§Àl I[o[t[Îô
#define	FBV_I_MAX				3000000			// I Max
#define	FBV_I_MIN				(-FBV_I_MAX)	// I Min

// ÅáDutymÛÌ×ÌDutyîñ
/*
// 2014.10.07-01
//#define	FBV_DUTY_MIN_U			900				// ÅáDuty(ã¸)
#define	FBV_DUTY_MIN_U			850				// ÅáDuty(ã¸)
#define	FBV_DUTY_MIN_D			1050			// ÅáDuty(º~)
*/
// 2014.11.20-01
#define	FBV_DUTY_MIN_U			1000			// ÅáDuty(ã¸)
#define	FBV_DUTY_MIN_D			1050			// ÅáDuty(º~)

// 2014.09.22-01
#define	FBV_DUTY_HISTERISIS		150				// DutyqXeVX
#define	FBV_SPEED_ADJ_STEP		36				// î¬x²®l0.001x/b

// PWMwß§Àl
// 2014.09.24-01
#define	FBV_PWM_LIMIT	((DWORD)MTU_PWM_TIME * 65 / 100) // PWMwß§Àl(4062)
// 2014.09.25-01
//#define	FBV_PWM_LIMIT	((DWORD)MTU_PWM_TIME * 70 / 100) // PWMwß§Àl(4375)
// 2014.10.06-01
//#define	FBV_PWM_LIMIT	((DWORD)MTU_PWM_TIME * 85 / 100) // PWMwß§Àl(5312)

// X[X^[gîñ --- äád¥ÙPWMúl
/*
// 2014.10.06-01
#define	FBV_PWM_SHIFT_INZ_MU	(4400)			// Vtgúl
#define	FBV_PWM_SHIFT_INZ_MD	(4050)			// Vtgúl
*/
// 2014.11.14-01
#define	FBV_PWM_SHIFT_INZ_MU	(4400)			// Vtgúl
#define	FBV_PWM_SHIFT_INZ_MD	(2800)			// Vtgúl


// FBî{oÍlâ³ 2014.09.26-
// 2014.10.16
#define	FBV_BASE_ADJ_VSTD_U		1					// w¦¬xâ³ UP
//#define	FBV_BASE_ADJ_VSTD_D		22					// w¦¬xâ³ DOWN
//#define	FBV_BASE_ADJ_VSTD_D		12					// w¦¬xâ³ DOWN
//#define	FBV_BASE_ADJ_VSTD_D		16					// w¦¬xâ³ DOWN
//#define	FBV_BASE_ADJ_VSTD_D		18					// w¦¬xâ³ DOWN
#define	FBV_BASE_ADJ_VSTD_D		18					// w¦¬xâ³ DOWN
#define	FBV_BASE_ADJ_TEKO_U		40					// eRäâ³ UP
//#define	FBV_BASE_ADJ_TEKO_D		175					// eRäâ³ DOWN
//#define	FBV_BASE_ADJ_TEKO_D		95					// eRäâ³ DOWN
#define	FBV_BASE_ADJ_TEKO_D		115					// eRäâ³ DOWN

// ã¸|vn®Ìäád¥ÙPWMl@VbNÎô 2014.09.19
//#define	FBV_PWM_UP_START		4300			// |vn®PWMl
//#define	FBV_PWM_UP_START		4100			// |vn®PWMl
#define	FBV_PWM_UP_START		4200			// |vn®PWMl
//#define	FBV_PWM_UP_START		4300			// |vn®PWMl

// º~tRVI 2014.10.03-
// 2014.10.06
//#define	FBV_DOWN_FLOCTL_RATIO	60				// º~ptRVI/1000
// 2014.11.14
#define	FBV_DOWN_FLOCTL_RATIO	80				// º~ptRVI/1000

// FB³µÌÌäád¥ÙPWMl 2014.09.18
// 2014.09.23-01
#define	FBV_PWM_NON_FB_U		(1500)			// FB³PWM ã¸
#define	FBV_PWM_NON_FB_D		(2550)			// FB³PWM º~
#define	FBV_PWM_NOFB_V_RATIO	205				// º~[tRVI
#define	FBV_PWM_NOFB_T_RATIO	30				// º~eRVI

// º~äád¥Ù§Àl 2014.11.13`
// 2014.11.13-01
//#define	FBV_PWM_MAX_D			3500			// º~pMax-Pwml
//#define	FBV_PWM_MAX2_D			3100			// º~Än®pMax-Pwml
// 2014.11.20-01
#define	FBV_PWM_MAX_D			3500			// º~pMax-Pwml
#define	FBV_PWM_MAX2_D			3050			// º~Än®pMax-Pwml


//--------------------------------
// d¬tB[hobN
//--------------------------------

// A/D½Ï»Wvñ
#define	FBI_ADC_SUM_COUNT		4				// Wvñ



#endif	// _DEF_APP_CONF_HDR_

//-----------------------------------------------------------------------
