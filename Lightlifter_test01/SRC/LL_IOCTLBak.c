//==========================================================================
//	
//	File Name	: MB_IOCTL.c
//	Version		: R00.10
//	Device(s)	: RX210 64pin
//	Description	: I/O制御処理
//	Date		: 2014.08.28
//	
//==========================================================================

//--------------------------------------------------------------------------
//	Includes
//--------------------------------------------------------------------------
#include	<machine.h>

#include	"SCI_RX210.h"				// SCIヘッダ

#include	"LL.h"						// アプリケーションヘッダ
#include	"LL_prototype.h"			// アプリケーションプロトタイプヘッダ

//--------------------------------------------------------------------------
//	マクロ定義
//--------------------------------------------------------------------------


//-----------------------------------------------------------
//	グローバル変数,関数プロトタイプ宣言
//-----------------------------------------------------------
//extern	FB				mFb ;					// Feedback処理情報



//===========================================================================

#pragma section

//---------------------------------------------------
// DIP-SW状態確認							テスト用
// 電源投入時に一度だけDipの状態を確認する
//---------------------------------------------------
void	xIoCtl_DipCheck( void ){
	
	if ( gDip.S1 == PORT_IN_HIGH ){
		gSysInf.ubTestMode = TEST_MODE_1 ;
	} else if ( gDip.S2 == PORT_IN_HIGH ) {
		gSysInf.ubTestMode = TEST_MODE_2 ;
	} else if ( gDip.S3 == PORT_IN_HIGH ) {
		gSysInf.ubTestMode = TEST_MODE_3 ;
	} else if ( gDip.S4 == PORT_IN_HIGH ) {
		gSysInf.ubTestMode = TEST_MODE_4 ;
	} else if ( gDip.S5 == PORT_IN_HIGH ) {
		gSysInf.ubTestMode = TEST_MODE_5 ;
	} else if ( gDip.S6 == PORT_IN_HIGH ) {
		gSysInf.ubTestMode = TEST_MODE_6 ;
	} else if ( gDip.S7 == PORT_IN_HIGH ) {
		gSysInf.ubTestMode = TEST_MODE_7 ;
	} else if ( gDip.S8 == PORT_IN_HIGH ) {
		gSysInf.ubTestMode = TEST_MODE_8 ;
	} else {
		gSysInf.ubTestMode = NORMAL_MODE ;
	}
}

//---------------------------------------------------
//	昇降停止
//	引数
//	戻り値 
//---------------------------------------------------
void	xIoCtlEMM( void ){

	//================================================
	// 非常停止確認
	if ( (gSysInf.fEmm == SYS_EMM_NO) && 
				(gSwDev.Lvl.ubI04 == EMM_SW_ON) ) {	// EMM ON
		xFeedback_Drv_Stop( FB_STOP_EMM ) ;				// EMM Stop
		gSysInf.fMotDrv = SYS_MOT_STOP ;				// 駆動状況:停止
		gSysInf.fEmm = SYS_EMM_YES ;					// 非常停止Flag
	} else if ( (gSysInf.fEmm == SYS_EMM_YES) && 
			    (gSwDev.Lvl.ubI04 == EMM_SW_OFF) &&
			      (gSysInf.fEncErrPrc == FALSE)) {	// EMM OFF
		gSysInf.fEmm = SYS_EMM_NO ;						// 非常停止Flag
	}
	
	// エラー時の停止シークエンス
	if (( gSysInf.ubErrCode ) && ( gSysInf.ubErrCate == 0 )) {
		xFeedback_Drv_Stop( FB_STOP_EMM ) ;				// EMM Stop
		gSysInf.fMotDrv = SYS_MOT_STOP ;				// 駆動状況:停止
		gSysInf.fEmm = SYS_EMM_YES ;					// 非常停止Flag
	}
	

/*	// モータ過電流トリップ検知
	if ( (gSysInf.fMotTripPrc == FALSE) &&	(gSysInf.fMotTrip == MOT_OVC_YES)) {
		gSysInf.fMotTripPrc = TRUE ;					// Mot.OVC-TripProc.Flag
		gSysInf.fEmm = SYS_EMM_YES ;						// 非常停止Flag
		xFeedback_Drv_Stop( FB_STOP_EMM ) ;					// EMM Stop
	}
	
	// モータ過電流検知
	if ( (gAdc.Mot.fOfsGet == TRUE) &&
		 (gAdc.Mot.dwCur > gDf.SYS.wMotCurLimit) ) {
		xFeedback_Drv_Stop( FB_STOP_EMM ) ;					// EMM Stop
		gSysInf.fEmm = SYS_EMM_YES ;						// 非常停止Flag
		xFeedback_Drv_Stop( FB_STOP_EMM ) ;					// EMM Stop
//		gSysInf.ubMotOvc = MOT_OVC_YES ;					// Rモータ過電流Flag
	}
	if ( (gAdc.Mot.fOfsGet == TRUE) &&
		 (gAdc.Mot.dwCur > gDf.SYS.wMotCurLimit) ) {
		xFeedback_Drv_Stop( FB_STOP_EMM ) ;					// EMM Stop
		gSysInf.fEmm = SYS_EMM_YES ;						// 非常停止Flag
		xFeedback_Drv_Stop( FB_STOP_EMM ) ;					// EMM Stop
//		gSysInf.ubMotOvc = MOT_OVC_YES ;					// Lモータ過電流Flag
}
*/	
/*	// エンコーダ断線/短絡検知
#ifndef	__ENC_ERR_BYPASS_
	if ( gDip.S7 == DIP_SW_OFF ) {						// Not Check Bypass
	if ( gSysInf.fEncErrPrc == FALSE &&
		((gEnc.Inp.ubUVW == 0x00) || (gEnc.Inp.ubUVW == 0x07))) {
		gSysInf.fEncErrPrc = TRUE ;						// Enc.Err処理Flag
		gSysInf.fEmm = SYS_EMM_YES ;						// 非常停止Flag
		xFeedback_Drv_Stop( SYS_STOP_EMM ) ;				// EMM Stop
	}
	}
#endif		// #ifndef	__ENC_ERR_BYPASS_
*/	

}

/*
//-----------------------------------------------------------
// モータ回転速度変更モード
//-----------------------------------------------------------
void	xMotSpeed( void )
{
	SYS_INF		*pSysInf ;					// システム情報
	pSysInf  =  &gSysInf ;					// システム情報Ptr

	// 停止釦が長押しの時に
	if ( gSwDev.Long.ubI03 == SWDEV_LPUSH_YES ) {

		// モータ速度が0でなかった場合にはリターン
		if(gEnc.wVrev != 0){
			Sci_PutsText( UART_CH_DBG, "MOT_NOT_LONG.\n" ) ;
			return ;
		}
		if ( gSwDev.Long.ubI01 == SWDEV_LPUSH_YES )  {
			// 上昇通常速度の場合は長押しフラグを立てる
			if( pSysInf->wUpSpeed == gDf.DRV.wUpSpeedNormal ){
				pSysInf->ubMoFlag = SW_MOFLG_UP_ON ;		// 通常→長押し
			// 上昇低速の場合は通常フラグを立てる	
			}else if ( pSysInf->wUpSpeed == gDf.DRV.wUpSpeedHalf ) {
				pSysInf->ubMoFlag = SW_MOFLG_UP_OFF ;		// 長押し→通常
			}
		}else if ( gSwDev.Long.ubI02 == SWDEV_LPUSH_YES )  {
			// 下降通常速度の場合は長押しフラグを立てる
			if( pSysInf->wDownSpeed == gDf.DRV.wDownSpeedNormal ){
				pSysInf->ubMoFlag = SW_MOFLG_DOWN_ON ;		// 通常→長押し
			// 下降低速の場合は通常フラグを立てる	
			}else if ( pSysInf->wDownSpeed == gDf.DRV.wDownSpeedHalf ) {
				pSysInf->ubMoFlag = SW_MOFLG_DOWN_OFF ;		// 長押し→通常
			}
		}
	}

	//長押しフラグ&上昇釦のダウンエッジを確認したら長押しモードへ移行
	if(gSwDev.Edge.ubI03 == SWDEV_EDGE_DOWN ){
		switch( pSysInf->ubMoFlag ) {
			case SW_MOFLG_NONE :
				Sci_PutsText( UART_CH_DBG, "NONE\n" ) ;
				break ;
			// フラグが上昇通常モードの場合	
			case SW_MOFLG_UP_OFF :	
				pSysInf->wUpSpeed = gDf.DRV.wUpSpeedNormal ;	// UpSpeedNORMAL
				Sci_PutsText( UART_CH_DBG, "UP_MODE_NORMAL\n" ) ;
				pSysInf->ubMoFlag = SW_MOFLG_NONE ;
				break ;
			// フラグが上昇長押しモードの場合
			case SW_MOFLG_UP_ON :
				pSysInf->wUpSpeed = gDf.DRV.wUpSpeedHalf	;	// UpSpeedHarf
				Sci_PutsText( UART_CH_DBG, "UP_MODE_LONG\n" ) ;
				pSysInf->ubMoFlag = SW_MOFLG_NONE ;
				break ;
			// フラグが下降通常モードの場合	
			case SW_MOFLG_DOWN_OFF :
				pSysInf->wDownSpeed = gDf.DRV.wDownSpeedNormal;//DownSpeedNORMAL
				Sci_PutsText( UART_CH_DBG, "UP_MODE_NORMAL\n" ) ;
				pSysInf->ubMoFlag = SW_MOFLG_NONE ;
				break ;
			// フラグが下降長押しモードの場合	
			case SW_MOFLG_DOWN_ON :
				pSysInf->wDownSpeed = gDf.DRV.wDownSpeedHalf ; // DownSpeedHAlf
				Sci_PutsText( UART_CH_DBG, "UP_MODE_LOMG\n" ) ;
				pSysInf->ubMoFlag = SW_MOFLG_NONE ;
				break ;
			default :
				Sci_PutsText( UART_CH_DBG, "default\n" ) ;
				break ;
		}
	}  
}
*/


//-----------------------------------------------------------
// モータ回転速度、自己保持モード変更ルーチン
//-----------------------------------------------------------
void	xModeChange( void )
{
	SYS_INF		*pSysInf ;					// システム情報
	pSysInf  =  &gSysInf ;					// システム情報Ptr

	if ( gSwDev.Long.ubI01 == SWDEV_LPUSH_YES 
					&& gSwDev.Lvl.ubI02 == SWDEV_LVL_OFF )  {
		// 上昇通常速度の場合は長押しフラグを立てる
		if( pSysInf->wUpSpeed == gDf.DRV.wUpSpeedNormal ){
			pSysInf->fSpMode = SW_MOFLG_UP_ON ;		// 通常→長押し
		// 上昇低速の場合は通常フラグを立てる	
		} else if ( pSysInf->wUpSpeed == gDf.DRV.wUpSpeedHalf ) {
			pSysInf->fSpMode = SW_MOFLG_UP_OFF ;		// 長押し→通常
		}
	} else if ( gSwDev.Long.ubI02 == SWDEV_LPUSH_YES 
					&& gSwDev.Lvl.ubI01 == SWDEV_LVL_OFF )  {
		// 下降通常速度の場合は長押しフラグを立てる
		if( pSysInf->wDownSpeed == gDf.DRV.wDownSpeedNormal ){
			pSysInf->fSpMode = SW_MOFLG_DOWN_ON ;		// 通常→長押し
		// 下降低速の場合は通常フラグを立てる	
		}else if ( pSysInf->wDownSpeed == gDf.DRV.wDownSpeedHalf ) {
			pSysInf->fSpMode = SW_MOFLG_DOWN_OFF ;		// 長押し→通常
		}
	}
	
	//上昇・下降釦のアップエッジを確認したら長押しモードクリア
	if( gSwDev.Edge.ubI01 == SWDEV_EDGE_UP 
					|| gSwDev.Edge.ubI02 == SWDEV_EDGE_UP ){
		gSysInf.fStpLong =  SWDEV_LPUSH_NO ;
	}

	//上昇・下降釦のダウンエッジを確認したら長押しモードへ移行
	if( gSwDev.Edge.ubI01 == SWDEV_EDGE_DOWN 
					|| gSwDev.Edge.ubI02 == SWDEV_EDGE_DOWN ){
		switch( pSysInf->fSpMode ) {
			case SW_MOFLG_NONE :
				Sci_PutsText( UART_CH_DBG, "NONE\n" ) ;
				break ;
			// フラグが上昇通常モードの場合	
			case SW_MOFLG_UP_OFF :	
				pSysInf->wUpSpeed = gDf.DRV.wUpSpeedNormal ;	// UpSpeedNORMAL
				Sci_PutsText( UART_CH_DBG, "UP_MODE_NORMAL\n" ) ;
				pSysInf->fSpMode = SW_MOFLG_NONE ;
				break ;
			// フラグが上昇長押しモードの場合
			case SW_MOFLG_UP_ON :
				pSysInf->wUpSpeed = gDf.DRV.wUpSpeedHalf	;	// UpSpeedHarf
				Sci_PutsText( UART_CH_DBG, "UP_MODE_LONG\n" ) ;
				pSysInf->fSpMode = SW_MOFLG_NONE ;
				break ;
			// フラグが下降通常モードの場合	
			case SW_MOFLG_DOWN_OFF :
				pSysInf->wDownSpeed = gDf.DRV.wDownSpeedNormal;//DownSpeedNORMAL
				Sci_PutsText( UART_CH_DBG, "DW_MODE_NORMAL\n" ) ;
				pSysInf->fSpMode = SW_MOFLG_NONE ;
				break ;
			// フラグが下降長押しモードの場合	
			case SW_MOFLG_DOWN_ON :
				pSysInf->wDownSpeed = gDf.DRV.wDownSpeedHalf ; // DownSpeedHAlf
				Sci_PutsText( UART_CH_DBG, "DW_MODE_LOMG\n" ) ;
				pSysInf->fSpMode = SW_MOFLG_NONE ;
				break ;
			default :
				Sci_PutsText( UART_CH_DBG, "default\n" ) ;
				break ;
		}
	}  
}
//-----------------------------------------------------------
//	I/O制御
//		リフト基本制御
//	引数
//	戻り値 
//
// EMMが押されている時はxFeedback_Drv_Setupにて動作ができないようになっている
// ※POE作動時には下降のみ行えるようコードを組み必要がある(10/21時点未実装)
//-----------------------------------------------------------
//void	xIoCtl_UP( void ){
	

//-----------------------------------------------------------
//	I/O制御
//		リフト基本制御
//	引数
//	戻り値 
//
// EMMが押されている時はxFeedback_Drv_Setupにて動作ができないようになっている
// ※POE作動時には下降のみ行えるようコードを組み必要がある(10/21時点未実装)
//-----------------------------------------------------------
void	xIoCtl( void )
{
		WORD		wInpRpm = 0	;				// 走行指示値(Rpm)
		ENC_INF		*pEnc ;						// エンコーダ

		pEnc	 =  &gEnc ;						// Enc情報 Ptr



	
	
	// モータが停止していて、停止釦が長押しの時にモード変更可能
	if ( gSwDev.Long.ubI03 == SWDEV_LPUSH_YES && gEnc.wVrev == 0 ) {
		gSysInf.fStpLong =  SWDEV_LPUSH_YES ;
	}
	if ( gSysInf.fStpLong ==  SWDEV_LPUSH_YES ) {
		xModeChange( );
	}	

	// 非常停止中は処理しない
	if (( gSysInf.fEmm == SYS_EMM_YES ) ||		// 非常停止Flag
		( gSysInf.fPOEStop == SYS_POE_YES )) {	// POE停止Flag
		return ;
	}
	
	//停止釦が押されている時は動作しない
	if ( gSwDev.Lvl.ubI03 == SWDEV_LVL_OFF ) {				// 停止釦がOFF
		// 上昇開始(Edgeトリガ) 上昇SW->ON 下降SW->OFF
		if (( gSwDev.Edge.ubI01 == SWDEV_EDGE_UP ) && 
					( gSwDev.Lvl.ubI02 == SWDEV_LVL_OFF )) {		//下降OFF
				
			// モータが逆転方向に動いていたら入力は不能
			if ( pEnc->wVrev < 0 ) { 
				Sci_PutsText( UART_CH_DBG, "MOT_REVERSE_PLEASE_WAIT.\n" ) ;
				return ;
			}	
			
			if ( gEnc.fUpLimit == UP_LIMIT ){
				Sci_PutsText( UART_CH_DBG, "UP_LIMIT.\n" ) ;
				return ;
			}	
			//駆動指示値を指定 初期値はDRV_SPEED_UP_NORMAL
			wInpRpm = gSysInf.wUpSpeed ;			// 走行駆動指示値
			gSysInf.fMotDrv = SYS_MOT_UP ;				// 駆動状況:上昇
			Sci_PutsText( UART_CH_DBG, "IoCTL_Up_Start.\n" ) ;
			xFeedback_Drv_Setup( &wInpRpm ) ;			// FB Start
				
		}

		// 上昇停止		
		// 上昇中に上昇スイッチを離す or 下降ボタンが押された場合 ->停止
		if ( (gSysInf.fMotDrv == SYS_MOT_UP) && 
			(( gSwDev.Edge.ubI01 == SWDEV_EDGE_DOWN ) ||  	// 上昇SW->OFF
			( gSwDev.Edge.ubI02 == SWDEV_EDGE_UP ) ||		// 下降SW->ON
  			( gEnc.fUpLimit == UP_LIMIT ))){			  		 

				if ( gEnc.fUpLimit == UP_LIMIT ){
					Sci_PutsText( UART_CH_DBG, "UP_LIMIT.\n" ) ;
				}	
				// 駆動指示値に0を代入する。
				wInpRpm = 0 ;								// 走行駆動指示値
				// モータ駆動状況をSTOPにする
				gSysInf.fMotDrv = SYS_MOT_STOP ;
				Sci_PutsText( UART_CH_DBG, "IoCTL_Up_Stop.\n" ) ;
//				xPrintf( UART_CH_DBG, "Spd:%d\n", gEnc.wSpeed ) ;
				
				xFeedback_Drv_Setup( &wInpRpm ) ;			// FB Start

				nop() ;

		}

		// 下降開始(Edgeトリガ)
		if ( gSwDev.Edge.ubI02 == SWDEV_EDGE_UP ){ 			//下降SW->ON
	  		if (( gSwDev.Lvl.ubI01 == SWDEV_LVL_OFF )){		//上昇はOFF

				// モータが逆転方向に動いていたら入力は不能
				if ( pEnc->wVrev > 0 ) { 
					Sci_PutsText( UART_CH_DBG, "MOT_REVERSE_PLEASE_WAIT.\n" ) ;
					return ;
				}	

				// 下限LSがONの時には下降不可
				if(gSwDev.Lvl.ubI05 == SWDEV_LVL_OFF){
					Sci_PutsText( UART_CH_DBG, "LOW_LIMIT.\n" ) ;
					return ;
				}
				//駆動指示値を指定 初期値はDRV_SPEED_DOWN_NORMAL
				wInpRpm = gSysInf.wDownSpeed ;		// 走行駆動指示値

				gSysInf.fMotDrv = SYS_MOT_DOWN ;
				Sci_PutsText( UART_CH_DBG, "IoCTL_Down.\n" ) ;	
				xFeedback_Drv_Setup( &wInpRpm ) ;			// FB Start
			}
		nop() ;

		}
	
		// 下降停止		
		// 下降中に下降スイッチを離す or 上昇ボタンが押された場合 ->停止
		// 下限LSに触れた場合停止＆位置情報をクリア
		if ( (gSysInf.fMotDrv == SYS_MOT_DOWN) && 
			((gSwDev.Edge.ubI01 == SWDEV_EDGE_UP) ||  		// 上昇SW->ON
	  		 (gSwDev.Edge.ubI02 == SWDEV_EDGE_DOWN) ||		// 下降SW->OFF
	  		 (gSwDev.Edge.ubI05 == SWDEV_EDGE_DOWN))) {		// 下限LS->ON
				
				// 下限LSがONの時には位置情報をクリアする
				if( gSwDev.Lvl.ubI05 == SWDEV_LVL_OFF){
					Sci_PutsText( UART_CH_DBG, "Pos_ZERO.\n" ) ;
				// 原位置として回転距離を0とする。
//					gFb.Q.dwPosTo = 0 ;						// 原位置合わせ FB
					gEnc.dwPos = 0 ;						// 原位置合わせ
				}
				// 駆動指示値に0を代入する。
				wInpRpm = 0 ;								// 走行駆動指示値
				// モータ駆動状況をSTOPにする
				gSysInf.fMotDrv = SYS_MOT_STOP ;
				Sci_PutsText( UART_CH_DBG, "IoCTL_Down_Stop.\n" ) ;
				xFeedback_Drv_Setup( &wInpRpm ) ;			// FB Start
	
//			gSysInf.fBrakeFlag_Prv = gSysInf.fBrakeFlag ;	// ブレーキ状態保存

		}
		
	}else if (gSwDev.Edge.ubI03 == SWDEV_EDGE_UP) {				// 停止釦がON
//	}else  {												// 停止釦がON
		if ( (gSysInf.fMotDrv == SYS_MOT_UP) || 
				 (gSysInf.fMotDrv == SYS_MOT_DOWN) ){
		
			// 駆動指示値に0を代入する。
			wInpRpm = 0 ;									// 走行駆動指示値
			// モータ駆動状況をSTOPにする
			gSysInf.fMotDrv = SYS_MOT_STOP ;
			xFeedback_Drv_Setup( &wInpRpm ) ;			// FB Start
			nop() ;
//停止中はに停止釦が押された場合は動作はしない

		}else if(gSysInf.fMotDrv == SYS_MOT_STOP ){
				xFeedback_Drv_Stop( FB_STOP_NORMAL ) ;		// 通常停止
		}
	}

	// 昇降停止のいずれかのスイッチが押されていたら
	// オートパワーオフ用タイマをクリア
	if ( gSwDev.Lvl.ubI01 || gSwDev.Lvl.ubI02 || gSwDev.Lvl.ubI03 ){
		gCMT0.T1S.uwPWROFF = 0 ;
	}
}
