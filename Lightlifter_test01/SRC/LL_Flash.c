//=======================================================================
//
//  FILE        : MB_Flash.c
//  DATE        : 2014.10.28
//  DESCRIPTION : データフラッシュ 処理
//	Device(s)	: RX210 64pin
//
//=======================================================================

#include	<machine.h>						// Include machine.h

#include	"SCI_RX210.h"					// SCIヘッダ

#include	"LL.h"							// アプリケーションヘッダ
#include	"LL_prototype.h"				// アプリケーションプロトタイプヘッダ

//-----------------------------------------------------------
//	グローバル変数,定数定義
//-----------------------------------------------------------
extern	const uint32_t	g_flash_BlockAddresses[] ;		// API var


//==============================================================
//	データフラッシュのアクセス許可設定
//	引数
//		FLAG fPermission		アクセス許可フラグ
//				DFLASH_ACC_DISABLE		アクセス禁止
//				DFLASH_ACC_READ			Read可
//				DFLASH_ACC_WRITE		Read/Write可
//	戻り値
//==============================================================
static	void	DF_AccessPermission( FLAG fPermission )
{
	uint16_t	t16ReadMask = 0 ;			// 読み取り許可マスク
	uint16_t	t16WriteMask = 0 ;			// 書き込み許可マスク
	
	
	if ( fPermission & 0x01 ) {				// 読み取り許可チェック
		t16ReadMask = 0xffff ;				// 読み取り許可
	}
	if ( fPermission & 0x02 ) {				// 書き込み許可チェック
		t16WriteMask = 0xffff ;				// 書き込み許可
	}
	R_FlashDataAreaAccess( t16ReadMask, t16WriteMask ) ;	// Access許可
	
	
}


//---------------------------------------------------
//	データフラッシュ情報構造体を初期値でセット
//	引数
//	戻り値
//---------------------------------------------------
void xDF_SetParmInz( void )
{
// 速度FB-Parm設定値
//	gDf.FBV.bType = FBV_TYPE ;					// FBタイプ
//	gDf.FBV.bType = FB_NONE ;					// FBタイプ
	gDf.FBV.bType = 0 ;					// FBタイプ
	gDf.FBV.wFbConstP = FBV_CONST_KP ;			// P:比例定数(Kp)
	gDf.FBV.wFbConstI = FBV_CONST_KI ;			// I:積分定数(Ki)
	gDf.FBV.wFbConstD = FBV_CONST_KD ;			// D:微分定数(Kd)
	gDf.FBV.wFbAccelP = FBV_ACCP_KP ;			// P:比例定数加速%
	gDf.FBV.wFbAccelI = FBV_ACCP_KI ;			// I:積分定数加速%
	gDf.FBV.wFbAccelD = FBV_ACCP_KD ;			// D:微分定数加速%
	gDf.FBV.wFbDecelP = FBV_ACCM_KP ;			// P:比例定数減速%
	gDf.FBV.wFbDecelI = FBV_ACCM_KI ;			// I:積分定数減速%
	gDf.FBV.wFbDecelD = FBV_ACCM_KD ;			// D:微分定数減速%
	gDf.FBV.wRevLowOvr = FBV_LO_OVRR ;			// 低速オーバーライド回転数
	gDf.FBV.wRevLowOvrLimit = FBV_LO_OVRR_LIMIT ;// 低速オーバーライド下限回転数
	gDf.FBV.wFbvLowLimit = FBV_LOWER_REV ;		// 速度FBでの指示速度下限Limit
//	gDf.FBV.wStartRev = FBV_START_REV ;			// 開始指示回転数
	gDf.FBV.wRevMax = FBV_REV_MAX ;				// 最高回転数
//	gDf.FBV.wREvMin = FBV_REV_MIN ;				// 最少回転数
	gDf.FBV.wLimitI = FBV_LIMIT_I ;			// I値制限値
	gDf.FBV.wCurCompBase = FBV_CUR_COMP_BASE ;	// 電流比較値算出基数
	gDf.FBV.wCCB_LowLimit = FBV_CCB_LOW_LIMIT ;	// 電流比較値算出基数下限
	gDf.FBV.wLimitI_Init = FBV_I_INIT ;			// I:制限初期値(1/1000)
	gDf.FBV.wLimitI_Ratio = FBV_I_RATIO ;		// I:制限スロープ(I/rpm)
	gDf.FBV.wLimitI_RateC = FBV_I_RATE_CUR ;	// I:電流制限定数
	gDf.FBV.wPwmRatio = FBV_PWM_RATIO ;			// PWM変換係数
	gDf.FBV.wPwmOffset = FBV_PWM_OFFSET ;		// PWM指令Offset

	gDf.POS.wUpDownStat = 0	;					// 多点停止の上昇/下降停止STAT
	gDf.POS.dwPos = 0;							// 現在位置カウント
	gDf.POS.dwUpLimit = ENC_UP_LMT ;			// 上限位置
	gDf.POS.dwUpLmtDecArea = UPLMT_DEC_AREA ;	// 上昇減速区間	
	gDf.POS.dwDwLmtDecArea = DWLMT_DEC_AREA ;	// 下限減速区間 
	gDf.POS.dwUpLmtAccBan =	UPLMT_ACC_BAN ;		// 上限加速禁止区間
	gDf.POS.dwDwLmtAccBan = DWLMT_ACC_BAN ;		// 下限加速禁止区間

//	gDf.POS.dwPosOver = gDf.POS.dwUpLimit +100 ;	// 上昇端限界値(enc)
	gDf.POS.dwLev1 = 0 ;						// 多点停止1
	gDf.POS.dwLev2 = 0 ;						// 多点停止2
	gDf.POS.dwLev3 = 0 ;						// 多点停止3
	gDf.POS.dwLev4 = 0 ;						// 多点停止4
	gDf.POS.dwLev5 = 0 ;						// 多点停止5
	gDf.POS.dwLev6 = 0 ;						// 多点停止6
	gDf.POS.dwLev7 = 0 ;						// 多点停止7
	gDf.POS.dwLev8 = 0 ;						// 多点停止8
	gDf.POS.dwLev9 = 0 ;						// 多点停止9
	gDf.POS.dwLev10  = 0 ;						// 多点停止10


	
	// PWM制限-Parm設定値
	gDf.PWM.wLimit_Init = FB_PWM_INIT ;			// PWM:制限初期値
	gDf.PWM.wLimit_Ratio = FB_PWM_RATIO ;		// PWM:制限Slope(100pwm/rpm)
/**/gDf.PWM.wLimitCur_Init = FB_PWM_CUR_INIT ;	// PWM:電流制限初期値
/**/gDf.PWM.wLimitCur_Ratio = FB_PWM_CUR_RATIO ;// 電流制限Slope(1000pwm/adc)
/**/gDf.PWM.wLimitCur_Rate = FB_PWM_CUR_RATE ;	// PWM:電流制限率(%)
	gDf.PWM.wVfb_Ratio = FB_PWM_VFB_RATIO ;		// PWM:指示速度Slope
	gDf.PWM.wVCompWidth = FB_PWM_VCOMP_WIDTH ;	// PWM:制限比較速度幅(rpm)
	
	// FB-Parm設定値
	gDf.FB.wAccStep = FB_ACC_STEP ;				// 加速ステップ量(/scan)
	gDf.FB.wDecStep = FB_DEC_STEP ;				// 減速ステップ量(/scan)
	gDf.FB.wEmmStep = FB_EMM_STEP ;				// 非常停止時減速ステップ(/scan)
	gDf.FB.wStopStep = FB_STOP_STEP ;			// 停止時の減速PWM値ステップ
//	gDf.FB.wPOEStop	= FB_POE_STOP	;			// POEストップ時のパラメータ = 0
	
	// 駆動制御-Parm設定値
	gDf.DRV.wRpmCvtRatio = DRV_RPM_CVT_RATIO ;	// rpm変換係数(0.1%)
	gDf.DRV.wUpStartSpeed = DRV_SPEED_UP_START ;	// 上昇開始速度
	gDf.DRV.wDownStartSpeed = DRV_SPEED_DOWN_START ; // 下降開始速度
	gDf.DRV.wUpSpeedNormal = DRV_SPEED_UP_NORMAL ;	// 上昇速度(通常)
	gDf.DRV.wDownSpeedNormal = DRV_SPEED_DOWN_NORMAL ;	// 下降速度(半分)
	gDf.DRV.wUpSpeedHalf = DRV_SPEED_UP_HALF;		// 上昇速度(通常)
	gDf.DRV.wDownSpeedHalf = DRV_SPEED_DOWN_HALF;	// 下降速度(半分)
/* エラー用 未実装
	gDf.DRV.uwUpTimOut = UP_TIME_OUT		;	// 上昇タイムアウト(T1S)
	gDf.DRV.uwDownTimOut = DOWN_TIME_OUT ;		// 下降タイムアウト(T1S)
	gDf.DRV.uwRevOvrUpDiff = REV_OVR_UP ;		// 上昇時速度超過
	gDf.DRV.uwRevLowUpDiff = REV_LOW_UP ;		// 上昇時速度不足
	gDf.DRV.uwRevOvrDownDiff = REV_OVR_DOWN ;	// 下降時速度超過
	gDf.DRV.uwRevLowDownDiff = REV_LOW_DOWN ;	// 下降時速度不足
	gDf.DRV.uwMotStartNoneCnt = MOT_START_NONE ;	// 指示後未動作エラー
*/

	// Sys-Parm設定値
	gDf.SYS.wMotCurLimit = SYS_MOT_OVC ;		// モータ過電流値(ADC)
	gDf.SYS.ubDrvMode = SYS_DRIVE_MODE_NORMAL ;	// 昇降は通常モード
	// Err-Parm設定値
//	gDf.ERR.uwUpTimOut	 = UP_TIME_OUT	;		// 上昇タイムアウト値(1S)
//	gDf.ERR.uwDownTimOut = DOWN_TIME_OUT ;		// 下降タイムアウト値(1S)

	// モータ電流計測定数
	gDf.MCM.uwCurDutyAdj = MCM_DUTY_ADJ ;		// 電流ADC値補正値
	gDf.MCM.uwCurDutyLimit = MCM_DUTY_LIMIT ;	// 電流ADC値最低リミット

}


//---------------------------------------------------
//	データフラッシュ情報からパラメータ計算
//	引数
//	戻り値
//---------------------------------------------------
void	xDF_ParmCalc( void )
{
	
	gSysInf.uwMotOvrevMgn = ((DWORD)gDf.FBV.wRevMax * FBV_OVREV_MGN) /
							100 ;							// Mot.OverRev(rpm)
}

//---------------------------------------------------
//	データフラッシュ初期化(消去)	※高所作業車 10/13
//	引数
//	戻り値
//---------------------------------------------------
void	xDF_Inz( void )
{
	UWORD		uwLoop ;
	WORD		wDataSize ;				// データサイズ
	UWORD		uwSize ;				// CRC演算バイト数/Block数
	
	
	DF_AccessPermission( DFLASH_ACC_WRITE ) ;				// Write Enable
	
	// パラメータデータサイズを8byte境界で切り上げる
	wDataSize = sizeof(D_FLASH) ;							// 構造体サイズ
	wDataSize >>= 3 ;										// 8byte境界
	if ( sizeof(D_FLASH) & 0x07 ) {							// 切り上げ
		wDataSize++ ;										// 8byte境界
	}
	wDataSize <<= 3 ;										// Dataサイズ(byte)
	
	// DF消去
	uwSize = (wDataSize + DF_BLOCK_SIZE - 1) / 
				DF_BLOCK_SIZE ;								// Block数
	for( uwLoop = 0 ; uwLoop < uwSize ; uwLoop++ ) {
		R_FlashErase( DF_PARM_TOP_BLOCK + uwLoop ) ;		// block format
	}
	
	DF_AccessPermission( DFLASH_ACC_DISABLE ) ;				// AccessDisable
	
}

//---------------------------------------------------
//パラメータデータのデータフラッシュへの書き込み
//---------------------------------------------------
void	xDF_ParmWrite( void )
{
	WORD		wWriteSize ;			// 書き込みサイズ
	UBYTE		*pubData ;				// CRC演算先頭アドレス
	UWORD		uwSize ;				// CRC演算バイト数/Block数
	uint32_t	t32WrtAdr ;				// 書き込み番地
	BYTE		bLoop ;
	
	
	// CRC演算値をセット 
	pubData = (UBYTE *)&gDf ;								// CRC演算TopAdr
	uwSize = sizeof(D_FLASH) - sizeof(UWORD) ;				// 演算Byte数
	gDf.CTL.uwCrc = 0;
	gDf.CTL.uwCrc = xMakeCRC( pubData, uwSize ) ;			// CRC演算(ITU-T)
	
	// パラメータデータサイズを8byte境界で切り上げる
	wWriteSize = sizeof(D_FLASH) ;							// 構造体サイズ
	wWriteSize >>= 3 ;										// 8byte境界
	if ( sizeof(D_FLASH) & 0x07 ) {							// 切り上げ
		wWriteSize++ ;										// 8byte境界
	}
	wWriteSize <<= 3 ;										// Dataサイズ(byte)
	
	// DF消去
	uwSize = (wWriteSize + DF_BLOCK_SIZE - 1) / 
				DF_BLOCK_SIZE ;								// Block数
	for( bLoop = 0 ; bLoop < uwSize ; bLoop++ ) {
		R_FlashErase( DF_PARM_TOP_BLOCK + bLoop ) ;			// block format
	}
	
	
	// ブロック書き込み
	t32WrtAdr = g_flash_BlockAddresses[DF_PARM_TOP_BLOCK] ;		// 書き込み番地
	while ( wWriteSize > 0 ) {
		// 書き込みBytes計算
		if ( wWriteSize > DF_BLOCK_SIZE ) {
			uwSize = DF_BLOCK_SIZE ;						// 書き込みByte数
		} else {
			uwSize = wWriteSize ;							// 書き込みByte数
		}
		R_FlashWrite ( t32WrtAdr, (uint32_t)pubData,
					(uint16_t)uwSize ) ;					// DF write
					
		wWriteSize -= uwSize ;								// Dataサイズ(byte)
		t32WrtAdr += DF_BLOCK_SIZE ;						// 書き込み番地
		pubData += uwSize ;									// DataWriteTopAdr
	}
	
}

//---------------------------------------------------
//	パラメータデータのデータフラッシュへの書き込み
//	引数
//	戻り値
//---------------------------------------------------
void	xDF_ParmWriteExe( void )
{
	
	DF_AccessPermission( DFLASH_ACC_WRITE ) ;				// Write Enable
	xDF_ParmWrite( ) ;										// Parm->DF
	xDF_ParmCalc( ) ;										// DF->Parm Calc
	DF_AccessPermission( DFLASH_ACC_DISABLE ) ;				// AccessDisable
	
}


#define	DFP_READ_SUCCESS		0		// 読み込み成功
#define	DFP_READ_CRC_ERR		1		// CRCエラー有り
#define	DFP_READ_NO_DATA		2		// ブロック消去されている
//---------------------------------------------------
//	データフラッシュからパラメータデータへの読み込み試行
//	引数
//	戻り値
//		FLAG					読み込み結果
//			DFP_READ_SUCCESS	読み込み成功
//			DFP_READ_CRC_ERR	CRCエラー有り
//			DFP_READ_NO_DATA	ブロック消去されている
//---------------------------------------------------
static FLAG	sDF_ParmTryRead( void )
{
	WORD		wDataSize ;				// データサイズ
	UWORD		uwSize ;				// CRC演算バイト数/Block数
	UBYTE		*pubData ;				// CRC演算Ptr/読み込み元Ptr
	UWORD		uwCrc ;					// CRC演算結果
	UWORD		uwCrcPrv ;				// CRC保存データ
	UWORD		uwLoop ;				// Loop変数
	uint32_t	t32ChkAdr ;				// チェック番地
	uint32_t	t32Ret ;				// API戻り値
	
	
	// パラメータデータサイズを8byte境界で切り上げる
	wDataSize = sizeof(D_FLASH) ;							// 構造体サイズ
	wDataSize >>= 3 ;										// 8byte境界
	if ( sizeof(D_FLASH) & 0x07 ) {							// 切り上げ
		wDataSize++ ;										// 8byte境界
	}
	wDataSize <<= 3 ;										// Dataサイズ(byte)
	
	// DFブロック消去チェック
	uwSize = (wDataSize + DF_BLOCK_SIZE - 1) / 
				DF_BLOCK_SIZE ;								// Block数
	for( uwLoop = 0 ; uwLoop < uwSize ; uwLoop++ ) {
		t32ChkAdr = g_flash_BlockAddresses[DF_PARM_TOP_BLOCK + 
									uwLoop] ;				// 該当ブロック番地
		t32Ret = R_FlashDataAreaBlankCheck( t32ChkAdr, 
							BLANK_CHECK_ENTIRE_BLOCK ) ;	// BlankCheck
		if ( t32Ret == FLASH_BLANK ) {						// DFはブランク
			Sci_PutsText( UART_CH_DBG, "DF Read No Data !\n" ) ;
			return DFP_READ_NO_DATA ;
		}
	}
	
	// データ読み込みとCRCチェック
	pubData = 
		(UBYTE *)g_flash_BlockAddresses[DF_PARM_TOP_BLOCK] ;	// 読み込み元Ptr
	gDf = *(D_FLASH *)pubData ;								// データ読込
	pubData = (UBYTE *)&gDf ;								// CRC演算TopAdr 
	uwSize = sizeof(D_FLASH) - sizeof(UWORD) ;				// CRC演算Byte数
	uwCrcPrv = gDf.CTL.uwCrc ;								// 保存されていたCRC
	gDbg.FLG.dwDbg04 = gDf.CTL.uwCrc ;
	gDf.CTL.uwCrc = 0 ;										// 演算前にクリア
	uwCrc = xMakeCRC( pubData , uwSize ) ;					// CRC演算
	if (uwCrcPrv != uwCrc ) {							// CRC Err?
		Sci_PutsText( UART_CH_DBG, "DF Read CRC Error!\n" ) ;
		return DFP_READ_CRC_ERR ;
	}
	
	return DFP_READ_SUCCESS ;								// 読み取り成功
	
	
}


//---------------------------------------------------
//	データフラッシュからパラメータデータへの読み込み
//	引数
//	戻り値
//---------------------------------------------------
void	xDF_ParmReadMain( void )
{
	FLAG			fRet ;					// 戻り値
	
	
	DF_AccessPermission( DFLASH_ACC_READ ) ;			// Read Enable
	
	fRet = sDF_ParmTryRead( ) ;							// DF読み取り試行
	if ( fRet != DFP_READ_SUCCESS ) {					// 読み取り失敗
		xDF_SetParmInz( ) ;								// Inz. DF-parm
		xDF_ParmWrite( ) ;								// DFへ書き込み
	} else {
		Sci_PutsText( UART_CH_DBG, "DF Read Success.\n" ) ;
	}
	
	xDF_ParmCalc( ) ;									// DF->Parm Calc
	
	
	DF_AccessPermission( DFLASH_ACC_DISABLE ) ;			// Access disable
	
	//要検討
	gpDf = 
		(D_FLASH *)g_flash_BlockAddresses[DF_PARM_TOP_BLOCK] ;	// 読み込み元Ptr
	
	
}


//---------------------------------------------------
//	パラメータ番号からパラメータメンテ情報取得
//	引数
//		UWORD		uwIdx		パラメータ番号
//		DF_MAINT	*pMaint		パラメータメンテ情報
//	戻り値
//		BOOL					TRUE/FALSE
//---------------------------------------------------
BOOL	xDF_GetParmInf( UWORD uwIdx, DF_MAINT *pMaint )
{
	BOOL		boRet = TRUE ;					// 戻り値
	
//オリジナル	

	DF_FBV		*pFBV ;							// 速度FB-Parm
	DF_PWM		*pPWM ;							// PWM制限-Parm
	DF_FB		*pFB ;							// FB-Parm
//	DF_DRV		*pDRV ;							// 駆動制御-Parm
	DF_SYS		*pSYS ;							// Sys-Parm
//	DF_CTL		*pCTL ;							// ROM-Control Inf.
	
	// 代表値セット
	pMaint->wData = 0 ;								// Parmデータ
	pMaint->wMax = 0x7fff ;							// Parm最大値
	pMaint->wMin = 0 ;								// Parm最小値
	pMaint->fType = DF_TYPE_UNSIGHNED ;				// データ型
	pFBV = &gDf.FBV	;								// 速度FB全体情報
	pPWM = &gDf.PWM	;								// PWM基本情報
	pFB  = &gDf.FB	;								// FBパラメータ情報
//	pDRV = &gDf.DRV	;								// 駆動システム情報
	pSYS = &gDf.SYS ;								// システムパラメータ情報
//	pCTL = &gDf.CTL ;								// ROM-Control Inf.
	
/*

	// 代表値セット
	pMaint->wData = 0 ;								// Parmデータ
	pMaint->wMax = 0x7fff ;							// Parm最大値
	pMaint->wMin = 0 ;								// Parm最小値
	pMaint->fType = DF_TYPE_UNSIGHNED ;				// データ型
	pLift = &gDf.LIFT ;								// Lift情報
	pFB = &gDf.FB ;									// フィードバック全体情報
	pFBA = &gDf.FBA ;								// 角度フィードバック情報
	pPIDA = &gDf.FBA.PID[LIDX_UP] ;					// PIDゲイン情報
	if ( uwIdx >= 61 ) {					// 下降
		pPIDA = &gDf.FBA.PID[LIDX_DOWN] ;			// PIDゲイン情報
	}
	pAIN = &gDf.FBV.AIN[LIDX_UP] ;					// FBV 角度FB入力情報
	if ( uwIdx >= 91 ) {					// 下降
		pAIN = &gDf.FBV.AIN[LIDX_DOWN] ;			// FBV 角度FB入力情報
	}
	pPWMS = &gDf.FBV.PWMS[LIDX_UP] ;				// FBV PWM初動調整
	if ( uwIdx >= 111 ) {					// 下降
		pPWMS = &gDf.FBV.PWMS[LIDX_DOWN] ;			// FBV PWM初動調整
	}
	pVSTD = &gDf.FBV.VSTD[LIDX_UP] ;				// 基準速度
	if ( uwIdx >= 131 ) {					// 下降
		pVSTD = &gDf.FBV.VSTD[LIDX_DOWN] ;			// 基準速度
	}
	pPIDV = &gDf.FBV.PID[LIDX_UP] ;					// PIDパラメータ情報
	if ( uwIdx >= 161 ) {					// 下降
		pPIDV = &gDf.FBV.PID[LIDX_DOWN] ;			// PIDパラメータ情報
	}
	pBADJ = &gDf.FBV.BADJ[LIDX_UP] ;				// FBV 基本出力補正情報
	if ( uwIdx >= 191 ) {					// 下降
		pBADJ = &gDf.FBV.BADJ[LIDX_DOWN] ;			// FBV 基本出力補正情報
	}
	pDMIN = &gDf.FBV.DMIN ;							// FBV 最低Duty確保情報
	pSDIV = &gDf.FBV.SDIV ;							// 上昇時の同調分割情報 1117
	pMISC = &gDf.FBV.MISC ;							// FBV その他の情報
*/
	// 個別情報セット
	switch( uwIdx ) {
		// 速度FB-Parm設定値
		case 0 :						// FBタイプ
			pMaint->wData = pFBV->bType ;				// Parmデータ
			pMaint->wMax = FB_PID ;						// Parm最大値
			pMaint->wMin = FB_NONE ;					// Parm最小値
			break ;
		case 1 :						// P:比例定数(Kp)
			pMaint->wData = pFBV->wFbConstP ;			// Parmデータ
			pMaint->wMax = 1000 ;						// Parm最大値
			pMaint->wMin = 0 ;					// Parm最小値
			break ;
		case 2 :						// I:積分定数(Ki)
			pMaint->wData = pFBV->wFbConstI ;			// Parmデータ
			pMaint->wMax = 1000 ;						// Parm最大値
			pMaint->wMin = 10 ;							// Parm最小値
			break ;
		case 3 :						// D:微分定数(Kd)
			pMaint->wData = pFBV->wFbConstD ;			// Parmデータ
			pMaint->wMax = 1000 ;						// Parm最大値
			pMaint->wMin = 10 ;							// Parm最小値
			break ;
		case 4 :						// P:比例定数加速%
			pMaint->wData = pFBV->wFbAccelP ;			// Parmデータ
			pMaint->wMax = 200 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 5 :						// I:積分定数加速%
			pMaint->wData = pFBV->wFbAccelI ;			// Parmデータ
			pMaint->wMax = 200 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 6 :						// D:微分定数加速%
			pMaint->wData = pFBV->wFbAccelD ;			// Parmデータ
			pMaint->wMax = 200 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 7 :						// P:比例定数減速%
			pMaint->wData = pFBV->wFbDecelP ;			// Parmデータ
			pMaint->wMax = 200 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 8 :						// I:積分定数減速%
			pMaint->wData = pFBV->wFbDecelI ;			// Parmデータ
			pMaint->wMax = 200 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 9 :						// D:微分定数減速%
			pMaint->wData = pFBV->wFbDecelD ;			// Parmデータ
			pMaint->wMax = 200 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 10 :						// 低速オーバーライド回転数
			pMaint->wData = pFBV->wRevLowOvr ;		// Parmデータ
			pMaint->wMax = 800 ;						// Parm最大値
			pMaint->wMin = 200 ;						// Parm最小値
			break ;
		case 11 :						// 低速オーバーライド下限回転数
			pMaint->wData = pFBV->wRevLowOvrLimit	;	// Parmデータ
			pMaint->wMax = 300 ;						// Parm最大値
			pMaint->wMin = 60 ;							// Parm最小値
			pMaint->fType = DF_TYPE_SIGHNED ;			// データ型
			break ;
		case 12 :						// 速度FBでの指示速度下限Limit
			pMaint->wData = pFBV->wFbvLowLimit ;		// Parmデータ
			pMaint->wMax = 200 ;						// Parm最大値
			pMaint->wMin = 40 ;							// Parm最小値
			break ;
/*		case 13 :						// 開始指示回転数
			pMaint->wData = pFBV->wStartRev ;			// Parmデータ
			pMaint->wMax = 400 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
*/		case 14 :						// 最高回転数
			pMaint->wData = pFBV->wRevMax ;			// Parmデータ
			pMaint->wMax = 4000 ;						// Parm最大値
			pMaint->wMin = 300 ;						// Parm最小値
			break ;
		case 15 :						// 電流比較値算出基数
			pMaint->wData = pFBV->wCurCompBase /10 ;	// Parmデータ
			pMaint->wMax = 1000 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 16 :						// 電流比較値算出基数下限
			pMaint->wData = pFBV->wCCB_LowLimit ;		// Parmデータ
			pMaint->wMax = 1000 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 17 :						// I:制限初期値(1/1000)
			pMaint->wData = pFBV->wLimitI_Init ;		// Parmデータ
			pMaint->wMax = 5000 ;						// Parm最大値
			pMaint->wMin = 5 ;							// Parm最小値
			break ;
		case 18 :						// I:制限スロープ(I/rpm)
			pMaint->wData = pFBV->wLimitI_Ratio ;		// Parmデータ
			pMaint->wMax = 1000 ;						// Parm最大値
			pMaint->wMin = -1000 ;						// Parm最小値
			break ;
		case 19 :						// I:電流制限定数
			pMaint->wData = pFBV->wLimitI_RateC ;		// Parmデータ
			pMaint->wMax = 500 ;							// Parm最大値
			pMaint->wMin = 0 ;								// Parm最小値
			break ;
		case 20 :						// PWM変換係数
			pMaint->wData = pFBV->wPwmRatio ;			// Parmデータ
			pMaint->wMax = 500 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 21 :						// PWM指令Offset
			pMaint->wData = pFBV->wPwmOffset ;		// Parmデータ
			pMaint->wMax = 5000 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;

// PWM制限設定値
		case 27 :						// PWM:制限初期値
			pMaint->wData = pPWM->wLimit_Init ;		// Parmデータ
			pMaint->wMax = 2000 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 28 :						// PWM:制限Slope(100pwm/rpm)
			pMaint->wData = pPWM->wLimit_Ratio ;		// Parmデータ
			pMaint->wMax = 200 ;						// Parm最大値
			pMaint->wMin = -200 ;						// Parm最小値
			pMaint->fType = DF_TYPE_SIGHNED ;			// データ型
			break ;
		case 29 :						// PWM:電流制限初期値
			pMaint->wData = pPWM->wLimitCur_Init ;	// Parmデータ
			pMaint->wMax = 2000 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 30 :						/// 電流制限Slope(1000pwm/adc)
			pMaint->wData = pPWM->wLimitCur_Ratio ;	// Parmデータ
			pMaint->wMax = 400 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 31 :						// PWM:電流制限率(%)
			pMaint->wData = pPWM->wLimitCur_Rate ;	// Parmデータ
			pMaint->wMax = 400 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 32 :						// PWM:指示速度Slope
			pMaint->wData = pPWM->wVfb_Ratio ;		// Parmデータ
			pMaint->wMax = 1000 ;						// Parm最大値
			pMaint->wMin = 0 ;							// Parm最小値
			break ;
		case 33 :						// PWM:制限比較速度幅(rpm)
			pMaint->wData = pPWM->wVCompWidth ;		// Parmデータ
			pMaint->wMax = 500 ;						// Parm最大値
			pMaint->wMin = -500 ;						// Parm最小値
			pMaint->fType = DF_TYPE_SIGHNED ;			// データ型
			break ;
			
//FB-Parm設定値
		case 34 :						// 加速ステップ量(/scan)
			pMaint->wData = pFB->wAccStep ;			// Parmデータ
			pMaint->wMax = 100 ;						// Parm最大値
			pMaint->wMin = 1 ;							// Parm最小値
			break ;
		case 35 :						// 減速ステップ量(/scan)
			pMaint->wData = pFB->wDecStep ;			// Parmデータ
			pMaint->wMax = 200 ;						// Parm最大値
			pMaint->wMin = 1 ;							// Parm最小値
			break ;
		case 36 :						// 非常停止時減速Step(/scan)
			pMaint->wData = pFB->wEmmStep ;			// Parmデータ
			pMaint->wMax = 400 ;						// Parm最大値
			pMaint->wMin = 4 ;							// Parm最小値
			break ;
		case 37 :						// 停止時の減速PWM値ステップ
			pMaint->wData = pFB->wStopStep ;			// Parmデータ
			pMaint->wMax = 200 ;						// Parm最大値
			pMaint->wMin = 5 ;							// Parm最小値
			break ;
			
// Sys-Parm設定値
		case 41 :						// モータ過電流値(ADC)
			pMaint->wData = pSYS->wMotCurLimit ;		// Parmデータ
			pMaint->wMax = 9000 ;						// Parm最大値
			pMaint->wMin = 100 ;						// Parm最小値
			break ;

// その他
		case 99 :						// 初期値セット
			pMaint->wData = 99 ;						// Parmデータ
			pMaint->wMax = 99 ;							// Parm最大値
			pMaint->wMin = 99 ;							// Parm最小値
			break ;
		default :
			boRet = FALSE ;
			break ;
	}
	
	return boRet ;

}

//---------------------------------------------------
//	指定パラメータ番号へデータを格納
//	引数
//		UWORD		uwIdx		パラメータ番号
//		WORD		wData ;		パラメータデータ
//	戻り値
//		BOOL					TRUE/FALSE
//---------------------------------------------------
BOOL	xDF_PutParmData( UWORD uwIdx, WORD wData )
{
	BOOL		boRet = TRUE ;					// 戻り値
	UWORD		uwSize ;						// CRC演算バイト数/Block数
	

	// 個別情報セット
	switch( uwIdx ) {
		// 速度FB-Parm設定値
		case 0 :						// FBタイプ
			gDf.FBV.bType = wData ;				// Parmデータ
			//高所作業者よりパラメータメンテフラグを立てる変数
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 1 :						// P:比例定数(Kp)
			gDf.FBV.wFbConstP = wData ;		// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 2 :						// I:積分定数(Ki)
			gDf.FBV.wFbConstI = wData ;			// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 3 :						// D:微分定数(Kd)
			gDf.FBV.wFbConstD = wData ;			// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 4 :						// P:比例定数加速%
			gDf.FBV.wFbAccelP = wData ;				// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 5 :						// I:積分定数加速%
			gDf.FBV.wFbAccelI = wData ;				// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 6 :						// D:微分定数加速%
			gDf.FBV.wFbAccelD = wData ;				// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 7 :						// P:比例定数減速%
			gDf.FBV.wFbDecelP = wData ;			// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 8 :						// I:積分定数減速%
			gDf.FBV.wFbDecelI = wData ;			// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 9 :						// D:微分定数減速%
			gDf.FBV.wFbDecelD = wData ;			// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 10 :						// 低速オーバーライド回転数
			gDf.FBV.wRevLowOvr = wData ;		// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 11 :						// 低速オーバーライド下限回転数
			gDf.FBV.wRevLowOvrLimit = wData ;	// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 12 :						// 速度FBでの指示速度下限Limit
			gDf.FBV.wFbvLowLimit = wData ;		// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
/*		case 13 :						// 開始指示回転数
			gDf.FBV.wStartRev = wData ;			// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
*/		case 14 :						// 最高回転数
			gDf.FBV.wRevMax = wData ;			// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 15 :						// 電流比較値算出基数 ※要検討
			gDf.FBV.wCurCompBase = wData * 10;	// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 16 :						// 電流比較値算出基数下限
			gDf.FBV.wCCB_LowLimit = wData ;		// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 17 :						// 積分値制限(x1000)
			gDf.FBV.wLimitI_Init = wData ;		// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 18 :						// I:制限スロープ(I/rpm)
			gDf.FBV.wLimitI_Ratio = wData ;		// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 19 :						// I:電流制限定数
			gDf.FBV.wLimitI_RateC = wData ;		// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 20 :						// PWM変換係数
			gDf.FBV.wPwmRatio = wData ;		// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 21 :						// PWM指令Offset
			gDf.FBV.wPwmOffset = wData ;	// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;

// PWM制限設定値
		case 27 :						// PWM:制限初期値
			gDf.PWM.wLimit_Init = wData ;	// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 28 :						// PWM:制限スロープ(100pwm/rpm)
			gDf.PWM.wLimit_Ratio = wData ;	// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 29 :						// PWM:電流制限初期値
			gDf.PWM.wLimitCur_Init = wData ; // Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 30 :						// 電流制限Slope(1000pwm/adc)
			gDf.PWM.wLimitCur_Ratio = wData ; // Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 31 :						// PWM:電流制限率(%)
			gDf.PWM.wLimitCur_Rate = wData ; // Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 32 :						// PWM:指示速度Slope
			gDf.PWM.wVfb_Ratio = wData ;	// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 33 :						// PWM:制限比較速度幅(rpm)
			gDf.PWM.wVCompWidth = wData ;	// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;

// FB-Parm設定値
		case 34 :						// 加速ステップ量(/scan)
			gDf.FB.wAccStep = wData ;		// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 35 :						// 減速ステップ量(/scan)
			gDf.FB.wDecStep = wData ;			// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 36 :						// 非常停止時減速Step(rpm/scan)
			gDf.FB.wEmmStep = wData ;	// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
		case 37 :						// 停止時の減速PWM値ステップ
			gDf.FB.wStopStep = wData ;	// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;

// Sys-Parm設定値
		case 41 :						// モータ過電流値(ADC)
			gDf.SYS.wMotCurLimit = wData ;	// Parmデータ
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;
			
// その他
		case 99 :						// 初期値セット
			xDF_SetParmInz( ) ;				// Inz. DF-parm
			gSysInf.fPMaintChg = SYS_PMAINT_CHG_YES ;		// ParmMaintChg
			break ;

		default :
			boRet = FALSE ;
			break ;
	}

	if ( gSysInf.fPMaintChg == SYS_PMAINT_CHG_YES ) {		// ParmMaintChg
		xDF_ParmCalc( ) ;									// DF->Parm Calc
	}

	// CRC計算
	if ( boRet == TRUE ) {
		uwSize = sizeof(D_FLASH) - sizeof(UWORD) ;			// CRC演算Byte数
		gDf.CTL.uwCrc = xMakeCRC( (UBYTE *)&gDf, uwSize ) ;	// CRC演算
	}
		
	return boRet ;


}


//=======================================================================
