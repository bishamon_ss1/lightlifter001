//-----------------------------------------------------------------------
//	IOフィールド定義 コンソール基板
//-----------------------------------------------------------------------


#ifndef _IO_FLD_DEF_			// 多重定義予防
#define _IO_FLD_DEF_

#include	"_iodefine.h"				// Include _iodefine.h

//--------------------------------
// ハードウェアバージョン
//--------------------------------
#define	HW_VER				1			// バージョン番号



//-----------------------------------------------------------
//	制御回路ポート定義
//-----------------------------------------------------------

//ライトリフター用基板---------------------------------------------------------

//本基板用接点入力(押しボタンスイッチ用）
#define INP01_PDR			PORTE.PDR.BIT.B2	// IN01 Dir		上昇
#define INP01_PIDR			PORTE.PIDR.BIT.B2	// IN01 InData	上昇
#define INP02_PDR			PORTE.PDR.BIT.B1	// IN02 Dir		下降
#define INP02_PIDR			PORTE.PIDR.BIT.B1	// IN02 InData	下降
#define INP03_PDR			PORTE.PDR.BIT.B0	// IN03 Dir		停止	
#define INP03_PIDR			PORTE.PIDR.BIT.B0	// IN04 InData	停止
#define INP04_PDR			PORTE.PDR.BIT.B5	// IN04 Dir 	EMM
#define INP04_PIDR			PORTE.PIDR.BIT.B5	// IN04 InData 	EMM

//本基板用接点入力(外部入力:Input Reserve ）
#define INPR01_PDR			PORT3.PDR.BIT.B7	// IN01 Dir		上昇
#define INPR01_PIDR			PORT3.PIDR.BIT.B7	// IN01 InData	上昇
#define INPR02_PDR			PORT3.PDR.BIT.B6	// IN02 Dir		下降
#define INPR02_PIDR			PORT3.PIDR.BIT.B6	// IN02 InData	下降

// P35はNMI端子であり、入力専用なので入出力設定はしなくて良い。
//#define INPR03_PDR		PORT3.PDR.BIT.B5	// IN03 Dir		停止
#define INPR03_PIDR			PORT3.PIDR.BIT.B5	// IN03 InData	停止

// DIP-SW (入力) DIP5-8は10/6時点の設定(変更可能)
#define	DIP01_PDR			PORTE.PDR.BIT.B3	// DIP01 Dir
#define	DIP01_PIDR			PORTE.PIDR.BIT.B3	// DIP01 InData
#define	DIP02_PDR			PORTE.PDR.BIT.B4	// DIP02 Dir
#define	DIP02_PIDR			PORTE.PIDR.BIT.B4	// DIP02 InData
#define	DIP03_PDR			PORTA.PDR.BIT.B0	// DIP03 Dir
#define	DIP03_PIDR			PORTA.PIDR.BIT.B0	// DIP03 InData
#define	DIP04_PDR			PORTA.PDR.BIT.B1	// DIP04 Dir
#define	DIP04_PIDR			PORTA.PIDR.BIT.B1	// DIP04 InData
#define	DIP05_PDR			PORTB.PDR.BIT.B5	// DIP05 Dir
#define	DIP05_PIDR			PORTB.PIDR.BIT.B5	// DIP05 InData
#define	DIP06_PDR			PORTB.PDR.BIT.B6	// DIP06 Dir
#define	DIP06_PIDR			PORTB.PIDR.BIT.B6	// DIP06 InData
#define	DIP07_PDR			PORTB.PDR.BIT.B7	// DIP07 Dir
#define	DIP07_PIDR			PORTB.PIDR.BIT.B7	// DIP07 InData
#define	DIP08_PDR			PORTC.PDR.BIT.B2	// DIP08 Dir
#define	DIP08_PIDR			PORTC.PIDR.BIT.B2	// DIP08 InData

//下限LS
#define INP05_PDR			PORT1.PDR.BIT.B7	// IN05 Dir 	LOW_LS
#define INP05_PIDR			PORT1.PIDR.BIT.B7	// IN05 InData 	LOW_LS

// 確認用LED (出力)
#define	LELO_PODR			PORT4.PODR.BIT.B4	// LELO OutData
#define	LELO_PDR			PORT4.PDR.BIT.B4	// LELO Dir
#define	LEMI_PODR			PORT4.PODR.BIT.B3	// LEMI OutData
#define	LEMI_PDR			PORT4.PDR.BIT.B3	// LEMI Dir
#define	LEHI_PODR			PORT4.PODR.BIT.B2	// LEHI OutData
#define	LEHI_PDR			PORT4.PDR.BIT.B2	// LEHI Dir


// 外部出力ポート (出力) ※外部出力端子として想定
// ポート等は現状不明のため後に確認のこと
// 一時的にコメントアウト
//#define	CN05_PODR			PORT3.PODR.BIT.B2	// CN05 OutData 
//#define	CN05_PDR			PORT3.PDR.BIT.B2	// CN05 Dir
#define	CN06_PODR			PORT3.PODR.BIT.B1	// CN06 OutData 
#define	CN06_PDR			PORT3.PDR.BIT.B1	// CN06 Dir
#define	CN07_PODR			PORT2.PODR.BIT.B7	// CN07 OutData	
#define	CN07_PDR			PORT2.PDR.BIT.B7	// CN07 Dir

// Brake
#define BRAKE_PODR			PORTH.PODR.BIT.B0	//モータブレーキ出力
#define BRAKE_PDR			PORTH.PDR.BIT.B0	//モータブレーキ出力

// Power ON Hold (出力)
#define	PWR_ON_PODR			PORT0.PODR.BIT.B5	// P_ON Hold
#define	PWR_ON_PDR			PORT0.PDR.BIT.B5	// P_ON Hold

// バッテリー電圧監視(入力)
#define	BAT_VOL_PIDR		PORT4.PIDR.BIT.B1	// 電源電圧検出
#define	BAT_VOL_PDR			PORT4.PDR.BIT.B1	// 電源電圧検出 

// モータ電流検出 (入力) 12bitA/Dへ 
#define	MOT_CRNT_PIDR		PORT4.PIDR.BIT.B0	// モータ電流検出
#define	MOT_CRNT_PDR		PORT4.PDR.BIT.B0	// モータ電流検出 

// モータ過電流検出 POE2機能にて強制停止 
#define	MOT_POE_PIDR		PORTA.PIDR.BIT.B6	// TRIP_CLR
#define	MOT_POE_PDR			PORTA.PDR.BIT.B6	// TRIP_CLR


//-------------------------
//	MTU (PWM)
//-------------------------
// PWM Duty register
#define	DUTY_U			MTU3.TGRB			// Duty U
#define	DUTY_UB			MTU3.TGRD			// Duty Ubuffer
#define	DUTY_V			MTU4.TGRA			// Duty V
#define	DUTY_VB			MTU4.TGRC			// Duty Vbuffer
#define	DUTY_W			MTU4.TGRB			// Duty W
#define	DUTY_WB			MTU4.TGRD			// Duty Wbuffer

// PWM Out ctl register
#define	PO_UH			MTU.TOER.BIT.OE3B	// MTIOC3B出力(UH0)
#define	PO_UL			MTU.TOER.BIT.OE3D	// MTIOC3D出力(UL0)
#define	PO_VH			MTU.TOER.BIT.OE4A	// MTIOC4A出力(VH0)
#define	PO_VL			MTU.TOER.BIT.OE4C	// MTIOC4C出力(VL0)
#define	PO_WH			MTU.TOER.BIT.OE4B	// MTIOC4B出力(WH0)
#define	PO_WL			MTU.TOER.BIT.OE4D	// MTIOC4D出力(WL0)

//-----------------------------------
//	モータセンサ(Enchoder) LowActive
//	割り込みでチェック
//-----------------------------------

//	ENC: U=IRQ4, V=IRQ5, W=IRQ6 (Vector:68,69,70)
//エンコーダU相 IRQ4の設定 (両エッジ検出を行う予定)
#define	PI_ENCU_IN		PORT1.PIDR.BIT.B4	// ENC U相 port
#define	PI_ENCU_PDR		PORT1.PDR.BIT.B4	// ENC U相 PDR
#define PI_ENCU_PIN_ISEL		MPC.P14PFS.BIT.ISEL	//ENC U相 IRQ4端子
#define PI_ENCU_IRQ_EDGE	ICU.IRQCR[4].BIT.IRQMD	//ENC U相IRQ4割込みエッジ
//エンコーダV相 IRQ5の設定
#define	PI_ENCV_IN		PORT1.PIDR.BIT.B5		// ENC V相 port
#define	PI_ENCV_PDR		PORT1.PDR.BIT.B5		// ENC V相 PDR
#define PI_ENCV_PIN_ISEL		MPC.P15PFS.BIT.ISEL	// ENC V相 IRQ5端子機能
#define	PI_ENCV_IRQ_EDGE	ICU.IRQCR[5].BIT.IRQMD	// ENC V相 IRQ5割込エッジ
//エンコーダW相 IRQ6の設定
#define	PI_ENCW_IN		PORT1.PIDR.BIT.B6		// ENC W相 port
#define	PI_ENCW_PDR		PORT1.PDR.BIT.B6		// ENC W相 PDR
#define PI_ENCW_PIN_ISEL	MPC.P16PFS.BIT.ISEL		// ENC W相 IRQ6端子機能
#define	PI_ENCW_IRQ_EDGE	ICU.IRQCR[6].BIT.IRQMD	// ENC W相 IR6割込エッジ


//--------------------------------------------------------------------

//---------------------------------------------------------------------

//--------------------------------------
// I/O設定
//--------------------------------------
#define	PORT_PDR_IN			0						// ポートは入力
#define	PORT_PDR_OUT		1						// ポートは出力


//--------------------------------
// I/Oポート状態
//--------------------------------

#define	EXTOUT_OFF			0						// 出力 OFF
#define	EXTOUT_ON			1						// 出力 ON
#define	EXTOUT_LOW			0						// 出力 Low
#define	EXTOUT_HIGH			1						// 出力 High

#define	BRAKE_LOCK			1						// BrakeLock
#define	BRAKE_FREE			0						// BrakeFree
/*
#define	PORT_IN_LOW			0						// 入力 0
#define	PORT_IN_HIGH		1						// 入力 1
*/
#define	LED_OFF				0						// LED 消灯
#define	LED_ON				1						// LED 点灯

#define	DIP_OFF				1						// DIP-SW OFF
#define	DIP_ON				0						// DIP-SW ON

// I/O動作状態
#define Input_HIGH			1
#define Input_LOW			0

// Power ON Hold
#define POWER_ON_HOLD		1						// 電源保持
#define POWER_OFF			0						// 電源遮断

// FET初期設定用 ※0を立てるとFETにゲートに電圧が印加される
#define FET_OFF				1

#endif		// _IO_FLD_DEF_

//-----------------------------------------------------------------------
