//=======================================================================
//
//	アプリケーション用割り込み関数とハードウェア処理
//	
//	_intprg.cの該当割り込みハンドラをコメントアウトしてここに記述する
//
//
//=======================================================================
#ifndef __MAIN_HEADER__						// 多重定義対策
	#define __MAIN_HEADER__
//_Common
#include	"TypeDef.h"

#include	"LL_IOdef.h"						// I/O定義
#include	"LL_TypeDef.h"
#include	"FRAME_HDL.h"					// メイン基板間通信フレームヘッダ
#include	"LL_Conf.h"					// APPコンフィグレーション定数定義
#include	"r_flash_api_rx_if.h"			// FlashAPI定義


//-----------------------------------------------------------------
//	DIP-SW
//-----------------------------------------------------------------
#ifdef _APP_DEF_
	DIP8				gDip ;					// DIP-SW情報 ON=1
#else	// _APP_DEF_
	extern	DIP8		gDip ;					// DIP-SW情報 ON=1
#endif	// _APP_DEF_


//-----------------------------------------------------------------
// 接点入力情報
//-----------------------------------------------------------------
#ifdef _APP_DEF_
	INP_FLD				gInpFld ;				// 入力ポートフィルタ
	INP_INFO			gInpStat ;				// 入力ポート状態
	INP_INFO			gInpStatGet ;			// 入力ポート状態(今回)
	INP_INFO			gInpStatPrv ;			// 入力ポート状態(前回)
#else	// _APP_DEF_
	extern	INP_FLD		gInpFld ;				// 入力ポートフィルタ
	extern	INP_INFO	gInpStat ;				// 入力ポート状態
	extern	INP_INFO	gInpStatGet ;			// 入力ポート状態(今回)
	extern	INP_INFO	gInpStatPrv ;			// 入力ポート状態(前回)
#endif	// _APP_DEF_


//-----------------------------------------------------------------
// 接点入力デバイス情報
//-----------------------------------------------------------------
#ifdef _APP_DEF_
	SW_DEVICE			gSwDev ;				// 接点入力デバイス情報
#else	// _APP_DEF_
	extern	SW_DEVICE	gSwDev ;				// 接点入力デバイス情報
#endif	// _APP_DEF_


//-----------------------------------------------------------------
//	MCUシステム値
//-----------------------------------------------------------------
#define	MCU_PCLOCK		25000000				// 周辺機器Clock 25MHz


//-----------------------------------------------------------------
//	CMT
//		CMT0:1mS, CMT1:0.1mS, CMT2:174.76ms, CMT3:1mS
//-----------------------------------------------------------------
#define	CMT0_INT_LVL	10					// 割り込みレベル
#define	CMT1_INT_LVL	12					// 割り込みレベル
#define	CMT3_INT_LVL	20					// 割り込みレベル

#ifdef _APP_DEF_
		CMT0_INT					gCMT0 ;		// インターバルタイマ処理
		volatile CMT1_INT			gCMT1 ;		// インターバルタイマ処理
	#else	// _APP_DEF_
		extern	CMT0_INT			gCMT0 ;		// インターバルタイマ処理
		extern	volatile CMT1_INT	gCMT1 ;		// インターバルタイマ処理
#endif	// _APP_DEF_


//-----------------------------------------------------------------
//	MTU2
//-----------------------------------------------------------------
#define	MTU3_INT_LVL		8				// 割り込みレベル

// PWM周期割込優先度設定
#define	MTU_PWM_INT_LVL		12				// PWM割込優先度

#define	MTU_INT_SKIP		2				// PWM割込間引き数

// 短絡防止時間 Deadtime
//#define	MTU_DEADTIME		100				// 100x20=2.0μS
//#define	MTU_DEADTIME	75				// 75x20 =1.5μS
//#define	MTU_DEADTIME	50				// 50x20 =1.0μS
#define	MTU_DEADTIME		50				// 50x40=2.0μS
//#define	MTU_DEADTIME	38				// 38x40 =1.5μS
//#define	MTU_DEADTIME	25				// 25x40 =1.0μS

// PWMキャリア周期1/2
//#define	MTU_PWM_TIME		3840			// 20.834*3840=80uS
//#define	MTU_PWM_TIME		4000			// 20*4000=80uS (2)
//#define	MTU_PWM_TIME		5208			// 40*5208=208uS 4.8KHz
//#define	MTU_PWM_TIME		3125			// 40*3125=125uS 8KHz
//#define	MTU_PWM_TIME		2500			// 20*2500=100uS 10KHz
#define	MTU_PWM_TIME		2000			// 40*2000=80uS 12.5KHz
//#define	MTU_PWM_TIME		1786			// 40*1786=72uS 14KHz
//#define	MTU_PWM_TIME		1675			// 40*1675=67uS 15KHz
//#define	MTU_PWM_TIME		1563			// 40*1563=62.5uS 16KHz

#define	MTU_MIN_MGN			140				// 最小値マージン
//#define	MTU_MAX_MGN		30				// 最大値マージン
#define	MTU_MAX_MGN			35				// 最大値マージン
#define	MTU_MGN				40				// Dutyマージン


#ifdef _APP_DEF_
	MTU_PWM					gPwm ;			// モータ PWM情報
#else	// _APP_DEF_
	extern	MTU_PWM			gPwm ;			// モータ PWM情報
#endif	// _APP_DEF_


//-----------------------------------------------------------------
//	モータエンコーダ,相電流指示情報
//-----------------------------------------------------------------
// 回転方向指示
#define	ENC_DIR_STOP		0				// 回転方向停止
#define	ENC_DIR_CW			1				// 回転方向CW
#define	ENC_DIR_CCW			-1				// 回転方向CCW

// 回転距離(上昇端) ※11/16追加
#define ENC_UP_LMT			21000			// 回転距離
#define UP_FREE				0				// 上昇端ではない
#define UP_LIMIT			1				// 上昇端

//	割込設定
#define	IRQ_ENC_INT_LVL		8				// 割り込みレベル
#define	ENC_CMT_INT_LVL		8				// 割り込みレベル

//	計測速度
//#define	ENC_CALC_HI			360				// 高速回転数(rpm)
//#define	ENC_CALC_MID		180				// 中速回転数(rpm)
#define	ENC_CALC_HI			720				// 高速回転数(rpm)
#define	ENC_CALC_MID		360				// 中速回転数(rpm)
//#define	ENC_CALC_HI_E		1080			// 高速回転数(E-rpm)
//#define	ENC_CALC_MID_E		540				// 中速回転数(E-rpm)

//	fHistFull:uwHist[ENC_RES]充填Flag
#define	ENC_HIST_FULL_YES	1				// 充填完了
#define	ENC_HIST_FULL_NO	0				// 充填未完了

#ifdef _APP_DEF_
	ENC_INF					gEnc ;			// Enc Ptr
	const MOT_PHASE			gMph[8] = {
								{  0, 0, 0 },	// dummy
								{ -1, 0, 1 },	// 1:240度(W->U)
								{ 1, -1, 0 },	// 2:120度(U->V)
								{ 0, -1, 1 },	// 3:180度(W->V)
								{ 0, 1, -1 },	// 4:  0度(V->W)
								{ -1, 1, 0 },	// 5:300度(V->U)
								{ 1, 0, -1 },	// 6: 60度(U->W)
								{  0, 0, 0 }	// 7: Enc断線時用
							} ;					// 相電流指示情報


#else	// _APP_DEF_
	extern	ENC_INF				gEnc ;			// Enc Ptr
	extern	const MOT_PHASE		gMph[] ;		// 相電流指示情報
#endif	// _APP_DEF_


//-----------------------------------------------------------------
//	AD変換情報
//-----------------------------------------------------------------
#define	ADC12_INT_LVL		8					// 割り込みレベル
#define	ADC12_OFS_TIME		3					// 電流オフセット計測タイマ値
#define	ADC12_OFS_MEASURES	32					// 電流オフセット計測回数

//#define	ADC12_I_CVT_RATE	16				// ADC値->電流値変換係数
//#define	ADC12_I_CVT_OFFSET	5				// ADC値->電流値変換オフセット

#define	ADC12_I_CVT_RATE	83					// ADC値->電流値変換係数
#define	ADC12_CVT_DELAY		7					// AD変換遅延(含マージン)

#define	ADC12_CVT_TICK		108					// AD変換tick(PCLK)I+V
#define	ADC12_CVT_MIN_DUTY	ADC12_CVT_TICK + MTU_DEADTIME	// 変換最小Duty
#define	ADC12_CORB_FULL		0xffff				// コンペアマッチさせない値

//#define	ADC12_CNV			346					// 電圧変換係数 元データ
#define	ADC12_V24_CNV1		2958				// V24電圧変換係数 分子
#define	ADC12_V24_CNV2		409600				// V24電圧変換係数 分母
//#define	ADC12_V24_COUNT	128					// V24ADC測定回数
//#define	ADC12_V24_SHIFT	7					// V24ADC測定回数 2^n
//#define	ADC12_COUNT		7					// ADC測定回数

//AD_DEV = 1/(4.9*(10/61)*4096) = 137
#define AD_DEV				137					// AD値→電圧値変換係数
#define OFFSET_AD12			26					// 電圧値オフセット
#define	ADC12_V24_COUNT		256					// V24ADC測定回数
#define	ADC12_V24_SHIFT		8					// V24ADC測定回数 2^n

#ifdef _APP_DEF_
	ADC_INF			gAdc ;						// AD変換結果(電圧監視)
#else	// _APP_DEF_
	extern	ADC_INF	gAdc ;						// AD変換結果(電圧監視)
#endif	// _APP_DEF_


//-----------------------------------------------------------------
// データフラッシュ
//-----------------------------------------------------------------
#ifdef _APP_DEF_
	D_FLASH					gDf ;				// データフラッシュ情報
	D_FLASH					*gpDf ;				// データフラッシュ情報
#else	// _APP_DEF_
	extern	D_FLASH			gDf ;				// データフラッシュ情報
	extern	D_FLASH			*gpDf ;				// データフラッシュ情報
#endif	// _APP_DEF_


//-----------------------------------------------------------------
// データフラッシュ情報
//-----------------------------------------------------------------
#define	DFLASH_ACC_DISABLE		0				// アクセス禁止
#define	DFLASH_ACC_READ			1				// Read可
#define	DFLASH_ACC_WRITE		3				// Read/Write可
#define	DF_BLOCK_SIZE			DF_BLOCK_SIZE_LARGE	// DFブロックBytes
#define	DF_PARM_TOP_BLOCK		BLOCK_DB0			// 使用DF先頭ブロック


//-----------------------------------------------------------------
// システム情報
//-----------------------------------------------------------------
#ifdef _APP_DEF_
	SYS_INF						gSysInf ;	// システム情報
#else	// _APP_DEF_
	extern	SYS_INF				gSysInf ;	// システム情報
#endif	// _APP_DEF_

// 駆動のモード
#define SYS_DRIVE_MODE_NORMAL		0		// 通常モード
#define SYS_DRIVE_MODE_SELF_HOLD	1		// 自己保持モード

// 動作停止方法
#define	SYS_STOP_NORMAL			0			// 通常の停止
#define	SYS_STOP_EMM			1			// 緊急停止

// fMotDrv			モータ動作中フラグ
#define SYS_MOT_STOP				0		// 停止中
#define SYS_MOT_UP					1		// 上昇動作中
#define SYS_MOT_DOWN				2		// 下降動作中

// fMotDrv			モータ動作中フラグ
#define STAT_MOT_STOP				0		// 完全停止中
#define STAT_MOT_STOP_EMM			1		// EMM停止中
#define STAT_MOT_DEC_EMM			2		// EMM減速中
#define STAT_MOT_POE				3		// POE停止中
#define STAT_MOT_ERR				4		// エラー発生
#define STAT_MOT_STOP_SWDEV			5		// 釦停止中
#define STAT_MOT_STOP_MODE			6		// 速度切り替えモード


#define STAT_MOT_UP					10		// 上昇中
#define STAT_MOT_UP_ACC_RESERVE		11		// 上昇加速準備中
#define STAT_MOT_UP_ACC_START		12		// 上昇加速開始
#define STAT_MOT_UP_ACC				13		// 上昇加速中
#define STAT_MOT_UP_CV				14		// 上昇等速中
#define STAT_MOT_UP_DEC_RESERVE		15		// 上昇減速準備
#define	STAT_MOT_UP_DEC				16		// 上昇減速中
#define	STAT_MOT_UP_STOP_RESERVE	17		// 上昇停止準備
#define STAT_MOT_UP_STOP			18		// 上昇停止
#define STAT_MOT_UP_DEC_AREA_1		19		// 上昇減速区間1
#define STAT_MOT_UP_DEC_AREA_2		20		// 上昇減速区間2


#define STAT_MOT_DW					30		// 下降中
#define STAT_MOT_DW_ACC_RESERVE		31		// 下降加速準備中
#define STAT_MOT_DW_ACC_START		32		// 下降加速開始
#define STAT_MOT_DW_ACC				33		// 下降加速中
#define STAT_MOT_DW_CV				34		// 下降等速中
#define STAT_MOT_DW_DEC_RESERVE		35		// 下降減速準備
#define	STAT_MOT_DW_DEC				36		// 下降減速中
#define	STAT_MOT_DW_STOP_RESERVE	37		// 下降停止準備
#define STAT_MOT_DW_STOP			38		// 下降停止
#define STAT_MOT_DW_DEC_AREA_1		39		// 下降減速区間1



// fOpEnable		作動可能Flag
#define	SYS_OPE_ENABLE				0		// 操作可能
#define	SYS_OPE_INITIAL				0xff	// 初期状態

// モータ過電流	ubMotOvc/ubMotOvc
//#define	MOT_OVC_NO					0		// 過電流でない(正常)
//#define	MOT_OVC_YES					1		// 過電流である

// モータ過回転	bMotOverRev
//#define	MOT_OVR_NO					0		// 正常
//#define	MOT_OVR_YES					1		// 過回転

// モータが指示に対して逆転	bMotReverse
//#define	MOT_REV_NO					0		// 正常
//#define	MOT_REV_YES					1		// 逆転

// fEncErr		エンコーダエラーFlag
#define	SYS_ENC_ERR_NO				0		// ENC-ERR なし
#define	SYS_ENC_ERR_YES				1		// ENC-ERR あり

// fPMaint			ParmMaint Flag
#define	SYS_PMAINT_OFF				0		// メンテモードOFF
#define	SYS_PMAINT_ON				1		// メンテモードON

// fPMaintChg		ParmMaintChange Flag 
#define	SYS_PMAINT_CHG_NO			0		// Parm修正無し
#define	SYS_PMAINT_CHG_YES			1		// Parm修正有り

// fEmm				非常停止Flag
#define	SYS_EMM_NO					0		// 非常停止OFF
#define	SYS_EMM_YES					1		// 非常停止ON

// POE2				POEフラグ&解除
#define SYS_POE_NO					0		// POE OFF
#define SYS_POE_YES					1		// POE ON
#define POE_RELEASE					0		// POE 解除

// LVD1
#define SYS_LVD1_ON					1		// LVD ON
#define SYS_LVD1_OFF				0		// LVD OFF

//長押しモードフラグ
#define SW_MOFLG_NONE				0		//長押しモード判定フラグ
#define SW_MOFLG_SPEED_OFF			1		//長押しモード判定フラグ
#define SW_MOFLG_SPEED_ON			2		//長押しモード判定フラグ
#define SW_MOFLG_MODE_OFF			3		//長押しモード判定フラグ
#define SW_MOFLG_MODE_ON			4		//長押しモード判定フラグ

//長押しモードフラグ ( 上昇と下降を別の速度にて調整する場合 )
//#define SW_MOFLG_NONE		0		//長押しモード判定フラグ ※追加 9/17
//#define SW_MOFLG_UP_OFF	1		//長押しモード判定フラグ ※追加 9/17
//#define SW_MOFLG_UP_ON	2		//長押しモード判定フラグ ※追加 9/17
//#define SW_MOFLG_DOWN_OFF	3		//長押しモード判定フラグ ※追加 9/17
//#define SW_MOFLG_DOWN_ON	4		//長押しモード判定フラグ ※追加 9/17

// モータ過電流値
//#define	SYS_MOT_OVC			294				// 過電流値(ADC)
#define	SYS_MOT_OVC				2047			// 過電流値(ADC)

#define	SYS_MOT_OVC_MIN			200				// 過電流値(ADC)
#define	SYS_MOT_OVC_MAX			4095			// 過電流値(ADC)

// 機械角/電気角比
// MTM製BL90Mモータについて 8P12S
#define	SYS_ENC_REV_RATIO		ENC_REV_RATIO_4	// 機械角/電気角比
//#define	SYS_ENC_REV_RATIO_MIN	1				// 機械角/電気角比
//#define	SYS_ENC_REV_RATIO_MAX	12				// 機械角/電気角比


//// 回生充電効率
//#define	SYS_CHARGE_EFF		90				// 回生充電効率(%)
//#define	SYS_CHARGE_EFF_MIN	0				// 回生充電効率(%)
//#define	SYS_CHARGE_EFF_MAX	100				// 回生充電効率(%)

// PWMする為の消費電流ADC値/軸当たり
#define	SYS_PWM_ADC				4				// PWM消費電流ADC値

// 電源電圧検出
#define	LVD1_INT_LVL	15					// 割り込みレベル(優先度最高)

// オートパワーオフタイマ(1Sec単位でカウント)
#define AUTO_PWR_OFF_TIMER	360					// 無操作5分で電源遮断
#define POWER_KEEP			0					// 電源入り状態を維持
#define	AUTO_POWER_OFF		1					// オートパワーオフ
#define ERR_SHUT_DOWN		2					// エラーシャットダウン
#define IWDT_SHUT_DOWN		3					// IWDT作動
//-----------------------------------------------------------------
// データフラッシュ情報
//-----------------------------------------------------------------
#define	DFLASH_ACC_DISABLE		0				// アクセス禁止
#define	DFLASH_ACC_READ			1				// Read可
#define	DFLASH_ACC_WRITE		3				// Read/Write可

#define	DF_PARM_TOP_BLOCK	BLOCK_DB0			// 先頭ブロック

//-----------------------------------------------------------------
// フィードバック処理
//-----------------------------------------------------------------
#ifdef _APP_DEF_
	FB						gFb	;				// FB情報構造体
#else	// _APP_DEF_
	extern	FB				gFb ;				// FB情報構造体	
#endif	// _APP_DEF_

// フィードバックインターバル ch間隔 2chではx2
//#define		FB_INTERVAL			10			// FB間隔(mS)
#define		FB_INTERVAL			5				// FB間隔(mS)

// フィードバック種類
#define		FB_NONE				0				// フィードバックしない
#define		FB_PROP				1				// P:比例制御
#define		FB_PI				3				// PI制御
#define		FB_PD				5				// PD制御
#define		FB_PID				7				// PID制御

// 速度フィードバック種類初期値
#define		FBV_TYPE			FB_PID			// FeedbackType

// 速度FB定数規定値
#define		FBV_CONST_KP		100				// FB比例定数 120
#define		FBV_CONST_KI		15				// FB積分定数 35
#define		FBV_CONST_KD		100				// FB微分定数 60

// 速度FB加速オーバーライド値
#define		FBV_ACCP_KP			130				// P:比例定数加速% 120
#define		FBV_ACCP_KI			10				// I:積分定数加速% 80
#define		FBV_ACCP_KD			90				// D:微分定数加速% 90
// 速度FB減速オーバーライド値
#define		FBV_ACCM_KP			90				// P:比例定数減速% 80
#define		FBV_ACCM_KI			10				// I:積分定数減速% 80
#define		FBV_ACCM_KD			100				// D:微分定数減速% 100

/*
// フィードバックのタイプなどモータの定数によって変わるものは
// テストモード時に判別できるようにする。
// 100kgタイプ 12/21追加 
// 速度FB定数規定値
#define		FBV_CONST_KP		100				// FB比例定数 120
#define		FBV_CONST_KI		15				// FB積分定数 35
#define		FBV_CONST_KD		100				// FB微分定数 60

// 速度FB加速オーバーライド値
#define		FBV_ACCP_KP			130				// P:比例定数加速% 120
#define		FBV_ACCP_KI			10				// I:積分定数加速% 80
#define		FBV_ACCP_KD			90				// D:微分定数加速% 90
// 速度FB減速オーバーライド値
#define		FBV_ACCM_KP			90				// P:比例定数減速% 80
#define		FBV_ACCM_KI			10				// I:積分定数減速% 80
#define		FBV_ACCM_KD			100				// D:微分定数減速% 100
*/

// 開始指示回転数		10/5追加
//#define		FBV_START_REV		200				// 開始指示回転数
//#define		FBV_PWM_RATIO	540				// PWM変換係数
//#define		FBV_LIMIT_I			1000			// I制限値
// ※抵抗あり
//#define		FBV_PWM_RATIO		33				// PWM変換係数
//#define		FBV_PWM_OFFSET		0			// PWM指令Offset
// ※抵抗なし 100kg
//#define		FBV_PWM_RATIO		26				// PWM変換係数
//#define		FBV_PWM_OFFSET		1500				// PWM指令Offset
// ※抵抗なし 200kg
#define		FBV_PWM_RATIO		25				// PWM変換係数
#define		FBV_PWM_OFFSET		1200				// PWM指令Offset


// 加減速定数
//#define		FBP_ACC_DIST		80				// 加速距離(enc)
//#define		FBP_DEC_DIST		120				// 減速距離(enc)
//#define		FBP_REV_MAX			2600			// 最大回転数(rpm)
//#define		FBP_DEC_CONST_MAX	3000			// 最大減速停止距離計算定数

// I:制限情報
#define		FBV_LIMIT_I			1000			// I:制限値 ※適当につけている
#define		FBV_I_INIT			5				// I:制限初期値(1/1000)
#define		FBV_I_RATIO			10				// I:制限スロープ I/rpm
#define		FBV_I_RATE_CUR		30				// I:電流制限定数

// 指示速度0保持ロックFlag値
#define		FBV_ZERO_LOCK_NO	0				// V=0ロック無し
#define		FBV_ZERO_LOCK_YES	1				// V=0ロック開始
#define		FBV_ZERO_VREV		1000			// ロック対象回転数(rpm)
#define		FBV_ZERO_WAIT_TIME	2500			// ロック待ち時間(ms)

// P:制限情報
#define		FBV_P_RATE_CUR		0				// P:電流制限定数

// 部分電流フィードバック情報
#define		FBV_CUR_COMP_BASE	2500			// 電流比較値算出基数
#define		FBV_CCB_LOW_LIMIT	400				// 電流比較値算出基数下限

// PWM制限情報
#define		FB_PWM_INIT			170				// PWM:制限初期値
#define		FB_PWM_RATIO		45				// PWM:制限スロープ(100pwm/rpm)
#define		FB_PWM_CUR_INIT		500				// PWM:電流制限初期値
#define		FB_PWM_CUR_RATIO	140				// 電流制限Slope(1000pwm/adc)
#define		FB_PWM_CUR_RATE		80				// PWM:電流制限率(%)
#define		FB_PWM_VFB_RATIO	400				// PWM:指示速度Slope
#define		FB_PWM_VCOMP_WIDTH	100				// PWM:制限比較速度幅(rpm)

// 下限速度処理情報
#define		FBV_LO_OVRR			400				// 低速オーバーライド回転数
#define		FBV_LO_OVRR_LIMIT	150				// 低速オーバーライド下限回転数
//#define		FBV_LOWER_REV		120			// 速度FBの指示速度下限リミッタ
//#define		FBV_LOWER_REV		80			// 速度FBの指示速度下限リミッタ
#define		FBV_LOWER_REV		50				// 速度FBの指示速度下限リミッタ

// 最大回転数
#define		FBV_REV_MAX			3000			// 最大回転数(rpm)

// 最小回転数 ※未使用
//#define		FBV_REV_MIN			60				// 最小回転数(rpm)

// 過回転マージン
#define		FBV_OVREV_MGN		150				// 過回転マージン(%)

// FB加減速ステップ
#define		FB_NONE_STEP		0				// スッテプ量初期値
#define		FB_ACC_STEP			10				// 加速ステップ量(/scan)
#define		FB_DEC_STEP			10				// 減速ステップ量(/scan)

// FB非常停止時の減速ステップ値
#define		FB_EMM_STEP			28				// rpm/scan

// FB停止時の減速PWM値ステップ
#define		FB_STOP_STEP		20				// pwm/scan

// POE作動時の減速パラメータ
#define 	FB_POE_STOP			0				//POE作動時は即座にストップ

// ブレーキ遅延
#define	FBP_BRAKE_DELAY			500				// ブレーキ遅延時間(mS)

// 上下限減速区間
#define	UPLMT_DEC_AREA			50				// 上昇減速区間
#define	DWLMT_DEC_AREA			50				// 下限減速区間
#define	UPLMT_ACC_BAN			20				// 上限加速禁止区間
#define	DWLMT_ACC_BAN			20				// 下限加速禁止区間

//-----------------------------------------------------------------
// 走行駆動制御
//-----------------------------------------------------------------

// 指示値->モータrpm変換係数
//#define	DRV_RPM_CVT_RATIO		160				// rpm変換係数(0.1%)
#define	DRV_RPM_CVT_RATIO		236				// rpm変換係数(0.1%)

// 上昇開始指示値
#define DRV_SPEED_UP_START		200				// 上昇開始速度
#define DRV_SPEED_DOWN_START	100				// 下降開始速度

// ブレーキ解除タイミング
#define UP_DRV_BRAKE_FREE		200				// 上昇時のタイミング(指示値)
#define DW_DRV_BRAKE_FREE		100				// 下降時のタイミング(指示値)

// 停止準備タイミング
#define UP_STOP_RESERVE_TIME	400				// 上昇停止時へ入る回転数
#define DW_STOP_RESERVE_TIME	-300			// 下降停止へ入る回転数

// ブレーキ作動タイミング
#define UP_DRV_BRAKE_LOCK		200				// 上昇時のタイミング(指示値)
#define DW_DRV_BRAKE_LOCK		-200			// 下降時のタイミング(指示値)


//指示速度初期値 10E用
//#define DRV_SPEED_UP_NORMAL		 2345			// 上昇速度(通常) rpm
//#define DRV_SPEED_DOWN_NORMAL	-2345			// 下降速度(通常) rpm
//#define DRV_SPEED_UP_HALF		 1173			// 上昇速度(半分) rpm
//#define DRV_SPEED_DOWN_HALF		-1173			// 下降速度(半分) rpm

// 指示速度初期値 20E用
#define DRV_SPEED_UP_NORMAL		 2344			// 上昇速度(通常) rpm
#define DRV_SPEED_DOWN_NORMAL	-2344			// 下降速度(通常) rpm
#define DRV_SPEED_UP_HALF		 1172			// 上昇速度(半分) rpm
#define DRV_SPEED_DOWN_HALF		-1172			// 下降速度(半分) rpm

// 指示速度初期値 テスト用低出力
//#define DRV_SPEED_UP_NORMAL	1006			// 上昇速度(通常)
//#define DRV_SPEED_DOWN_NORMAL	-1006			// 下降速度(通常)
//#define DRV_SPEED_UP_HALF		503				// 上昇速度(半分)
//#define DRV_SPEED_DOWN_HALF	-503			// 下降速度(半分)

//#define DRV_SPEED_UP_NORMAL		1272			// 上昇速度(3000rpm)
//#define DRV_SPEED_DOWN_NORMAL	-1272			// 下降速度(3000rpm)


//-----------------------------------------------------------------
// モータ定数				
//-----------------------------------------------------------------

// 電流ADC値->0.01A変換定数
#define		MOT_CUR_MULT		10000			// uA->10mA cvt-rate

//モータ電流計測定数
#define		MCM_DUTY_ADJ		5000	
#define		MCM_DUTY_LIMIT		1000	


//-----------------------------------------------------------------
// エラー関係			※数値は明確なものではないので、調整必須
//-----------------------------------------------------------------

// 数回に一度見るためのエラーカウント
#define		ERR_COUNT			100

// 電源電圧範囲
#define		ERR_BATLV_UPPER		260				// 上限 29.5V
#define		ERR_BATLV_LOWER		100				// 下限 16.0V

// 停止エラー検出用タイマー(T10M)
#define		ERR_MOT_STOP		5000

//上昇タイムアウト用タイマ(T1S)
#define		UP_TIME_OUT			60
//下降タイムアウト用タイマ(T1S)
#define		DOWN_TIME_OUT		60

//モータが回転していない
#define		MOT_START_NONE_CNT	1000


// 上昇時
// 速度偏差がマイナスに大きい：過加速
#define		REV_OVR_UP_DIFF		-400
// 速度偏差がプラスに大きい：回転不足
#define		REV_LOW_UP			400
// 加速：ソフト過電流
#define		CUR_LMT_UP_ACC		5000
// 等速：ソフト過電流
#define		CUR_LMT_UP_CV		5000
// 減速：ソフト過電流
#define		CUR_LMT_UP_DEC		5000


// 下降時
// 速度偏差がプラスに大きい：過加速
#define		REV_OVR_DOWN_DIFF	400
// 速度偏差がマイナスに大きい：回転不足
#define		REV_LOW_DOWN		-400
// 加速：ソフト過電流
#define		CUR_LMT_DOWN_ACC	5000
// 等速：ソフト過電流
#define		CUR_LMT_DOWN_CV		5000
// 減速：ソフト過電流
#define		CUR_LMT_DOWN_DEC	5000

//--------------------------------------//
// 			エラーコード一覧			//
//--------------------------------------//

//エラーループフラグ用
#define		ERR_NONE				0			// エラー無
#define		ERR_EXIST				1			// エラー有

// Cate.0:停止シークエンスが必要なもの
#define		ERR_NEED_STOP			30			// 停止シークエンスが必要
#define		ERR_UL_LLS_ON			1			// 上昇端フラグON時に下限LSON
#define		ERR_BAT_VOLTAGE			2			// 電源電圧異常
#define		ERR_LOW_LMT_OVR			3			// 下降端OVER
#define		ERR_UP_LMT_OVR			4			// 上昇端OVER
#define		ERR_MOT_STOP_DELAY		5			// モータストップ遅れ
#define		ERR_UP_TIMEOUT			6			// 上昇:タイムアウト
#define		ERR_DOWN_TIMEOUT		7			// 下降:タイムアウト
#define		ERR_REV_OVR_UP_DIFF		8			// 上昇:モータ過加速
#define		ERR_REV_OVR_DOWN_DIFF	9			// 下降:モータ過加速
#define		ERR_REV_LOW_UP			10			// 上昇:モータ回転数不足
#define		ERR_REV_LOW_DOWN		11			// 下降:モータ回転数不足
#define		ERR_CUR_LMT_UP_ACC		12			// 上昇:ソフト過電流(加速)
#define		ERR_CUR_LMT_UP_CV		13			// 上昇:ソフト過電流(等速)
#define		ERR_CUR_LMT_UP_DEC		14			// 上昇:ソフト過電流(減速)
#define		ERR_CUR_LMT_DOWN_ACC	15			// 下降:ソフト過電流(加速)
#define		ERR_CUR_LMT_DOWN_CV		16			// 下降:ソフト過電流(等速)
#define		ERR_CUR_LMT_DOWN_DEC	17			// 下降:ソフト過電流(減速)
#define		ERR_UP_LMT_OVR_REV		18			// 上昇端フラグ時に上昇加速
#define		ERR_LO_LMT_OVR_REV		19			// 下限LSON時に下降加速
#define		ERR_MOT_REVERSE			20			// モータ逆回転
#define		ERR_REV_OVR_UP_MAX		21			// 上昇:モータ過回転
#define		ERR_REV_OVR_DOWN_MAX	22			// 下降:モータ過回転

// Cate.1:ブレーキ等のみで完了できるもの(モータが回転していない)
#define		ERR_BRAKE				30			// ブレーキのみ
#define		ERR_ENC_NONE			30			// エンコーダ全断線
#define		ERR_ENC_SHORT			31			// エンコーダショート
#define		ERR_UP_MOT_START		32			// 上昇:モータが回転しない
#define		ERR_DOWN_MOT_START		33			// 下降:モータが回転しない

// Cate.2:エラーを出力だけで問題無いもの(停止シークエンスが別にある)
#define		ERR_MSG_ONLY			60			// エラー出力のみ
#define		ERR_CTL_VOLTAGE			60			// マイコン電圧低下
#define		ERR_POE_STOP			61			// 過電流ストップ

//--------------------------------------//
// 			LED表示ステータス			//
//--------------------------------------//
#define		LED_STAT_NONE			0			// LEDステータスなし
#define		LED_ERR_LMT_OVRLAP		1			// 上下限界点ON
#define		LED_ERR_BAT_VOLTAGE		2			// 電源電圧異常
#define		LED_ERR_STOP_OVR		3			// 停止点オーバー
#define		LED_ERR_TIMEOUT			4			// 昇降タイムアウト
#define		LED_ERR_ACCEL_OVER		5			// 過加速
#define		LED_ERR_REV_LOW			6			// 回転数不足
#define		LED_ERR_UP_CRTRIP		7			// 上昇ソフト過電流
#define		LED_ERR_DOWN_CRTRIP		8			// 下降ソフト過電流
#define		LED_ERR_LMT_OVER		9			// 上下限界点越え
#define		LED_ERR_ENC_ERROR		10			// エンコーダ故障
#define		LED_ERR_MOT_START		11			// モータが回転しない
#define		LED_ERR_CTL_VOL			12			// マイコン電源低下
#define		LED_ERR_POE_STOP		13			// ハード過電流
#define		LED_ERR_MOT_REVERSE		14			// モータ逆回転
#define		LED_ERR_OVER_REV		15			// 過速度

#define		LED_NORMAL				16			// 通常モード
#define		LED_HALF_SPEED_UP		17			// 上昇：1/2スピードモード
#define		LED_HALF_SPEED_DOWN		18			// 下降：1/2スピードモード
#define		LED_EMERGENCY			19			// 非常停止

#define		LED_PAT					4			// LEDパターン配列
#define		LED_STAT_BIT			4			// LEDパターン判断用
#define		LED_LONG				1			// LED点灯時間:長
#define		LED_SHORT				0			// LED点灯時間:短
#define		LED_FALSH_LONG			15			// LED長時間	1.5Sec	
#define		LED_FALSH_SHORT			5			// LED短時間	0.5Sec
#define		LED_FLASH_OFF			5			// LED消灯時間	0.5Sec
#define		LED_FLASH_INTERVAL 		5			// LED点灯休息時間 0.5Sec

//-----------------------------------------------------------------
// デバック
//-----------------------------------------------------------------
#define	_DBG_SYS_
#ifdef	_DBG_SYS_
	typedef struct _DBG_TIME_ {
			UWORD		uwT1m_01 ;				// デバック用1msタイマ
			UWORD		uwT1m_02 ;				// デバック用1msタイマ
			UWORD		uwT10m_01 ;				// デバック用10msタイマ
			UWORD		uwT1s_01 ;				// デバック用1sタイマ
		} DBG_TIME ;							// TIMにて定義	

	typedef struct _DBG_CNT_ {
			UWORD		dwCnt01	;				// デバック用カウンタ
			UWORD		dwCnt02	;				// デバック用カウンタ
			UWORD		dwCnt03	;				// デバック用カウンタ
			UWORD		dwCnt04	;				// デバック用カウンタ
			UWORD		dwCnt05	;				// デバック用カウンタ
			UWORD		dwCnt06	;				// デバック用カウンタ
			UWORD		dwCnt07	;				// デバック用カウンタ
			UWORD		dwCnt08	;				// デバック用カウンタ
		} DBG_CNT ;								// CNTにて定義	

	typedef struct _DBG_FLG_ {
			FLAG		fDbg01	;				// デバック用フラグ
			FLAG		fDbg02	;				// デバック用フラグ
			FLAG		fDbg03	;				// デバック用フラグ
			DWORD		dwDbg04	;				// デバック用フラグ
			DWORD		dwDbg05	;				// デバック用フラグ
		} DBG_FLG ;								// FLGにて定義	

	typedef struct _DBG_PRV_ {
			WORD		wDbgPrv01	;			// 前回値
			WORD		wDbgPrv02	;			// 前回値
			WORD		wDbgPrv03	;			// 前回値
			WORD		wDbgPrv04	;			// 前回値
			WORD		wDbgPrv05	;			// 前回値
		} DBG_PRV ;								// PRVにて定義	


	typedef struct	_APP_DBG_ {
			FLAG			fFB ;				// FB DBG Flag
			FLAG			fMode ;				// 実行モード
			WORD			wFbConstI ;			// I:積分定数(Ki)
			DBG_CNT			CNT ;				// デバック用タイマ構造体
			DBG_TIME		TIM ;				// デバック用カウンタ構造
			DBG_FLG			FLG ;				// デバッグ用フラグ構造体
			DBG_PRV			PRV ;				// 前回値構造体
			DWORD			dwCount ;			// 数値確認用
		} APP_DBG ;
	#ifdef _APP_DEF_
		APP_DBG						gDbg ;		// デバック構造体
	#else	// _APP_DEF_
		extern	APP_DBG				gDbg ;		// デバック構造体
	#endif	// _APP_DEF_

// ベンチテストモード用
#define		NORMAL_MODE		0
#define		TEST_MODE_1		1
#define		TEST_MODE_2		2
#define		TEST_MODE_3		3
#define		TEST_MODE_4		4
#define		TEST_MODE_5		5
#define		TEST_MODE_6		6
#define		TEST_MODE_7		7
#define		TEST_MODE_8		8

#endif //_DBG_SYS
// ----------------- End of システム情報 -----------------------



#endif	// __RX210TEST_HEADER__
