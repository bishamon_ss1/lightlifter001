//==========================================================================
//	
//	File Name	: LL_ERR.c
//	Version		: R00.10
//	Device(s)	: RX210 64pin
//	Description	: エラー管理
//	Date		: 2015.11.19
//	
//==========================================================================

//--------------------------------------------------------------------------
//	Includes
//--------------------------------------------------------------------------
#include	<machine.h>
#include	"_iodefine.h"				// Include iodefine.h
#include	"TypeDef.h"					// オリジナル構造体
#include	"LL.h"						// I/O定義
#include	"LL_prototype.h"			// アプリケーションヘッダ
#include	"SCI_RX210.h"				// SCIヘッダ

//--------------------------------------------------------------------------
//	マクロ定義
//--------------------------------------------------------------------------



//===========================================================================

#pragma section

//---------------------------------------------------
//	エラー処理
//	制御は戻さない
//---------------------------------------------------
UBYTE	xErrorStop( void )
{	
	
	ENC_INF		*pEnc ;						// エンコーダ
	MTU_PWM		*pPwm ;						// PWM固有値

	pEnc	 =  &gEnc ;						// Enc情報 Ptr
	pPwm	 =  &gPwm ;						// PWM情報 Ptr
	
	// エラーコードをカテゴリー分け 
	// Cate:0 EMM停止  Cate:1 通常停止  Cate:2 停止は別ルーチン  
	if ( gSysInf.ubErrCate < 5) {
		gSysInf.ubErrCate = (UWORD)( xDivide( gSysInf.ubErrCode , 30 )) ;
	}
	
	// エラーがなければ処理しない
	if ( !gSysInf.ubErrCode ) { 
		return gSysInf.ubErrCode ;
	}
	
	switch( gSysInf.ubErrCate ) {
		case 0 : // 停止シークエンスが必要なもの
			xIoCtlEMM( ); 							// EMM Stop
			gSysInf.ubErrCate = 5 ;					// 停止後のルーチンへ
			break ;
		case 1 : // ブレーキのみ必要なもの
			xFeedback_Drv_Stop( FB_STOP_NORMAL ) ;	// NORMAL Stop
			gSysInf.ubErrCate = 5 ;					// 停止後のルーチンへ
			break ;
		case 2 :
			gSysInf.ubErrCate = 5 ;					// 停止後のルーチンへ
			break ;
		default :
			break ;
	}
	// モータが完全停止
	if (( !pEnc->wVrev ) && ( gSysInf.ubErrCate == 5 ) &&
		( !pPwm->wDutyExe ) && ( BRAKE_PODR == BRAKE_LOCK )) {
	// 各エラーコード別にUART出力とLEDステータスの設定を行う
		switch( gSysInf.ubErrCode ) {
			case ERR_UL_LLS_ON :			// 上昇端フラグON時に下限LSON
				Sci_PutsText( UART_CH_DBG, "ERR:UpLimit&LowLs_ON\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_LMT_OVRLAP ;	// 上下限ON
					break ;
			case ERR_BAT_VOLTAGE :			// 電源電圧異常
				Sci_PutsText( UART_CH_DBG, "ERR:BAT_VOL\n" ) ;
				xPrintf( UART_CH_DBG, " VOL:%d \n",gSysInf.wErrVoltage ) ; 
				gSysInf.ubLEDStat = LED_ERR_BAT_VOLTAGE ;	// 電源電圧異常
				break ;
			case ERR_LOW_LMT_OVR :			// 下降端OVER
				Sci_PutsText( UART_CH_DBG, "ERR: LOW_LMT_OVR\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_STOP_OVR ;		// 停止点オーバー
				break ;
			case ERR_UP_LMT_OVR :			// 上昇端OVER		
				Sci_PutsText( UART_CH_DBG, "ERR: UP_LMT_OVR\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_STOP_OVR ;		// 停止点オーバー
				break ;
			case ERR_MOT_STOP_DELAY :		// モータストップ遅れ
				Sci_PutsText( UART_CH_DBG, "ERR: MOT_STOP_DELAY\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_STOP_OVR ;		// 停止点オーバー
				break ;
			case ERR_UP_TIMEOUT :			// 上昇タイムアウト
				Sci_PutsText( UART_CH_DBG, "ERR: UP_TIMEOUT\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_TIMEOUT ;		// 昇降タイムアウト
				break ;
			case ERR_DOWN_TIMEOUT :			// 下降タイムアウト	
				Sci_PutsText( UART_CH_DBG, "ERR: DOWN_TIMEOUT\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_TIMEOUT ;		// 昇降タイムアウト
				break ;
			case ERR_REV_OVR_UP_DIFF :		// 上昇:モータ過加速
				Sci_PutsText( UART_CH_DBG, "ERR: REV_OVR_UP_DIFF\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_ACCEL_OVER ;	// 過加速
				break ;
			case ERR_REV_OVR_DOWN_DIFF :	// 下降:モータ過加速
				Sci_PutsText( UART_CH_DBG, "ERR: REV_OVR_DOWN_DIFF\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_ACCEL_OVER ;	// 過加速
				break ;
			case ERR_REV_LOW_UP	:			// 上昇:モータ回転数不足
				Sci_PutsText( UART_CH_DBG, "ERR: REV_LOW_UP\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_REV_LOW ;		// 回転数不足
				break ;
			case ERR_REV_LOW_DOWN :			// 下降:モータ回転数不足
				Sci_PutsText( UART_CH_DBG, "ERR: REV_LOW_DOWN\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_REV_LOW ;		// 回転数不足
				break ;
			case ERR_CUR_LMT_UP_ACC	:		// 上昇:ソフト過電流(加速) 
				Sci_PutsText( UART_CH_DBG, "ERR:CUR_LMT_UP_ACC\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_UP_CRTRIP ;		// 上昇ソフト過電流
				break ;
			case ERR_CUR_LMT_UP_CV	:		// 上昇:ソフト過電流(等速) 
				Sci_PutsText( UART_CH_DBG, "ERR:CUR_LMT_UP_CV\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_UP_CRTRIP ;		// 上昇ソフト過電流
				break ;
			case ERR_CUR_LMT_UP_DEC	:		// 上昇:ソフト過電流(減速) 
				Sci_PutsText( UART_CH_DBG, "ERR:CUR_LMT_UP_DEC\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_UP_CRTRIP ;		// 上昇ソフト過電流
				break ;			
			case ERR_CUR_LMT_DOWN_ACC :		// 下降:ソフト過電流(加速) 
				Sci_PutsText( UART_CH_DBG, "ERR:CUR_LMT_DOWN_ACC\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_DOWN_CRTRIP ;	// 下降ソフト過電流
				break ;
			case ERR_CUR_LMT_DOWN_CV :		// 下降:ソフト過電流(等速) 
				Sci_PutsText( UART_CH_DBG, "ERR:CUR_LMT_DOWN_CV\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_DOWN_CRTRIP ;	// 下降ソフト過電流
				break ;
			case ERR_CUR_LMT_DOWN_DEC :		// 下降:ソフト過電流(減速) 
				Sci_PutsText( UART_CH_DBG, "ERR:CUR_LMT_DOWN_DEC\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_DOWN_CRTRIP ;	// 下降ソフト過電流
				break ;
			case ERR_UP_LMT_OVR_REV :		// 上昇端フラグ時に上昇加速
				Sci_PutsText( UART_CH_DBG, "ERR: UP_LMT_OVR_REV\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_LMT_OVER ;		// 上下限界値越え
				break ;
			case ERR_LO_LMT_OVR_REV	:		// 下限LSON時に下降加速
				Sci_PutsText( UART_CH_DBG, "ERR: LO_LMT_OVR_REV\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_LMT_OVER ;		// 上下限界値越え
				break ;
			
			case ERR_ENC_NONE :				// エンコーダ全断線
		 		Sci_PutsText( UART_CH_DBG, "ERR:ENC_NONE\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_ENC_ERROR ;		// エンコーダ故障
				break ;
			case ERR_ENC_SHORT :			// エンコーダ全短絡
		 		Sci_PutsText( UART_CH_DBG, "ERR:ENC_SHORT\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_ENC_ERROR ;		// エンコーダ故障 
				break ;
			case ERR_UP_MOT_START :			// 上昇:モータが回転しない
				Sci_PutsText( UART_CH_DBG, "ERR: UP_MOT_START\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_MOT_START ;		// モータ無回転
				break ;
			case ERR_DOWN_MOT_START :		// 下降:モータが回転しない
				Sci_PutsText( UART_CH_DBG, "ERR: DOWN_MOT_START\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_MOT_START ;		// モータ無回転
				break ;

			case ERR_CTL_VOLTAGE :			// マイコン電圧低下
				Sci_PutsText( UART_CH_DBG, "ERR:CTL_VOL\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_CTL_VOL ;		// マイコン電圧低下
				break ;
			case ERR_POE_STOP :				// 過電流ストップ
		 		Sci_PutsText( UART_CH_DBG, "ERR:POE_STOP\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_POE_STOP ;		// ハード過電流
				break ;
			case ERR_MOT_REVERSE :			// モータ逆回転
				Sci_PutsText( UART_CH_DBG, "ERR: MOT_REVERSE\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_MOT_REVERSE ;	// モータ逆回転
				break ;
			case ERR_REV_OVR_UP_MAX	:		// 上昇:モータ過回転
				Sci_PutsText( UART_CH_DBG, "ERR: REV_OVR_UP_MAX\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_OVER_REV ;		// モータ過回転
				break ;
			case ERR_REV_OVR_DOWN_MAX :		// 下降:モータ過回転
				Sci_PutsText( UART_CH_DBG, "ERR: REV_OVR_DOWN_MAX\n" ) ;
				gSysInf.ubLEDStat = LED_ERR_OVER_REV ;		// モータ過回転
				break ;
			default :
				return 0 ;
		}
		// 無限ループ
		// エラーコード表示(LED)
		xLedInfInz( gSysInf.ubLEDStat ) ;				// LED表示初期化
		gSysInf.fErrLoopFlag = ERR_EXIST ;				// エラーループ許可
//		xLoopInfinity( ) ;								// エラー後の無限Loop

	}
	return 0 ;
}	

//---------------------------------------------------
//	LED表示初期化(表示パターンのセット)
//	引数	ubMode	
//	戻り値
//		0		設定有効
//		1		設定無効
//	5bit目がエラーの有無を判断するbit
//	1〜4bitがLEDの点滅ステータスとなる
//	エラーがあった場合にはLEDのHIGHが点灯する
//---------------------------------------------------
void xLedInfInz( BYTE ubMode )
{
	UBYTE		ubMask ;					// 点灯bitマスク
	BYTE		bPat ;						// 

	
	ubMask = 0x01 <<  LED_STAT_BIT  ;	// 0b00010000
	bPat = LED_STAT_BIT ;					// 配列指定

	// エラーの有無を判定する
	// エラーなし
	if ( ubMode & ubMask ) {
		ubMask >>= 1 ;							// 右に1bitシフト
		LEHI_PODR = LED_OFF ;				// LED HIGH OFF
 		Sci_PutsText( UART_CH_DBG, "ERR:NONE\n" ) ;
		while ( ubMask ) {
			// nbit目が真か偽か
			if ( ubMode & ubMask ) {
				gSysInf.ubLEDPat[ bPat ] = LED_LONG ;	//長時間点灯
			} else {
				gSysInf.ubLEDPat[ bPat ] = LED_SHORT ;	//短時間点灯
			}
			bPat-- ;						// 次の配列へ
			ubMask >>= 1 ;						// 右へ1bitシフト
		}
	// エラー有り
	} else { 
		ubMask >>= 1 ;							// 右に1bitシフト
		LEHI_PODR = LED_ON ;				// LED HIGH ON
 		Sci_PutsText( UART_CH_DBG, "ERR:ANY_ERR\n" ) ;
		while ( ubMask ) {
			// nbit目が真か偽か
			if ( ubMode & ubMask ) {
				gSysInf.ubLEDPat[ bPat ] = LED_LONG ;	//長時間点灯
			} else {
				gSysInf.ubLEDPat[ bPat ] = LED_SHORT ;	//短時間点灯
			}
			bPat-- ;						// 次の配列へ
			ubMask >>= 1 ;						// 右へ1bitシフト
		}
	}
}	

/*
//---------------------------------------------------
//	エラー発生後の無限ループ
//	非常停止ON中で上昇と下降が長押しの場合でソフトウェアリセットとなる
//---------------------------------------------------
void	xLoopInfinity( void )
{

	Sci_PutsText( UART_CH_DBG, "ERR_LOOP\n" ) ;
	while(1) {

		// デバイス入力状態取得
		xSwDevStat( ) ;							// SW-Device状態取得

		// システムリセット
//		if ( gSwDev.Lvl.ubI04 == EMM_SW_ON ) {
		if ( ( gSwDev.Lvl.ubI04 == EMM_SW_ON ) &&
			 ( gSwDev.Long.ubI01 == SWDEV_LPUSH_YES ) &&
			 ( gSwDev.Long.ubI03 == SWDEV_LPUSH_NO ) &&
			 ( gSwDev.Long.ubI02 == SWDEV_LPUSH_YES )) {
			gSysInf.fPwrOff = ERR_SHUT_DOWN	;	// エラーシャットダウン	
			xPowerDown( ) ;						// Power Down
		}
		
#ifdef _WDT_									//ウォッチドッグタイマのクリア
	WDT.WDTRR = 0x00;							// リフレッシュ（WDT 開始）
	WDT.WDTRR = 0xFF;							// WDT再起動
#endif 		
		// LED 点灯
		Int_LED_Flash( ) ;
	}
}
*/
//---------------------------------------------------
//	エラー検出用 常時
//	引数
//	戻り値
//		0			エラー無し
//		それ以外	エラーあり(エラーコード)
//---------------------------------------------------

BYTE	xErr_All( void )
{
//	SYS_INF		*pSysInf ;					// システム情報
//	pSysInf  =  &gSysInf ;					// システム情報Ptr

	// エラーがあれば処理しない
	if ( gSysInf.ubErrCode ) {
		return gSysInf.ubErrCode ;
	}
	
	
	// ERR:下降/上昇時過電流検知
	if ( gSysInf.fPOEStop == SYS_POE_YES ) {			// POE STOP Flag
		gSysInf.ubErrCode = ERR_POE_STOP ;				// ハード過電流検出
		Sci_PutsText( UART_CH_DBG, "POE_STOP\n" ) ;
		return gSysInf.ubErrCode ;
	}
	
	return gSysInf.ubErrCode ;
}

//---------------------------------------------------
//	エラー検出用 数カウントに一回
//	引数
//	戻り値
//		0			エラー無し
//		それ以外	エラーあり(エラーコード)
//---------------------------------------------------

BYTE	xErr_Count( void )
{
//	SYS_INF		*pSysInf ;					// システム情報
	ENC_INF		*pEnc ;						// エンコーダ

//	pSysInf  =  &gSysInf ;					// システム情報Ptr
	pEnc	 =  &gEnc ;						// Enc情報 Ptr


	// エラーがあれば処理しない
	if ( gSysInf.ubErrCode ) {
		return gSysInf.ubErrCode ;
	}
	// ERR:上昇端フラグON時に下限LSがON
	if (( pEnc->fUpLimit == UP_LIMIT ) &&
		( gSwDev.Lvl.ubI05 == SWDEV_LVL_OFF )) {
		gSysInf.ubErrCode = ERR_UL_LLS_ON ;				// 上昇端下限LS同時ON
		Sci_PutsText( UART_CH_DBG, "LS_Both_Detect\n" ) ;
		return gSysInf.ubErrCode ;
	}

	// ERR:電源電圧異常
	if ( ( gAdc.Volt.uwV24 >= ERR_BATLV_UPPER ) ||
		 ( gAdc.Volt.uwV24 <= ERR_BATLV_LOWER ) ) {
		gSysInf.wErrVoltage = gAdc.Volt.uwV24 ;			// エラー時の電圧情報
		gSysInf.ubErrCode = ERR_BAT_VOLTAGE ;			// 制御電源電圧異常
		Sci_PutsText( UART_CH_DBG, "BATT_Voltage\n" ) ;
		return gSysInf.ubErrCode ;
	}
	
	// ERR:制御電圧異常
	if ( gSysInf.fLVDErr == SYS_LVD1_ON ){				// POE STOP Flag
		gSysInf.ubErrCode = ERR_CTL_VOLTAGE ;			// 制御電源電圧異常
		Sci_PutsText( UART_CH_DBG, "LVD1_Detect\n" ) ;
		return gSysInf.ubErrCode ;
	}

	// エンコーダエラー検出
	// ERR:エンコーダ全断線
	if ( !pEnc->Inp.ubUVW ) {
		gSysInf.ubErrCode = ERR_ENC_NONE ;				// ホールセンサ全断線
		Sci_PutsText( UART_CH_DBG, "ENC_Open\n" ) ;
		return gSysInf.ubErrCode ;
	}
	// ERR:エンコーダ短絡
	if ( pEnc->Inp.ubUVW == 7) {
		gSysInf.ubErrCode = ERR_ENC_SHORT ;				// ホールセンサ全検出
		Sci_PutsText( UART_CH_DBG, "ENC_Short\n" ) ;
		return gSysInf.ubErrCode ;
	}
		
/*	// 回転指示がゼロで一定速度以上回っている
	if (( !pFb->V.wOut ) && ( pEnc->wVrev ) &&
	 {
		gSysInf.ubErrCode = ERR_ENC_NO_STOP ;			// モータストップエラー
		return gSysInf.ubErrCode ;
	}*/
	
	return gSysInf.ubErrCode ;

}	

//---------------------------------------------------
//	エラー検出用 停止時
//	戻り値
//		0			エラー無し
//		それ以外	エラーあり(エラーコード)
//---------------------------------------------------

BYTE	xErr_Stop( void )
{
	FB 			*pFb ;						// フィードバック情報
//	SYS_INF		*pSysInf ;					// システム情報
	ENC_INF		*pEnc ;						// エンコーダ

	pFb 	 =  &gFb ;						// FB情報 Ptr
//	pSysInf  =  &gSysInf ;					// システム情報Ptr
	pEnc	 =  &gEnc ;						// Enc情報 Ptr

	// モータ指示がストップでモータが回転していない時にはタイマをクリアする
	if ( !pEnc->wVrev ){
		gCMT0.T1S.uwUpTimOut = 0 ;			// 上昇タイムアウト用タイマ
		gCMT0.T1S.uwDownTimOut = 0;			// 下降タイムアウト用タイマ
	}
	
	// モータ作動不良カウンタをクリア
	gSysInf.uwMotUpStartCnt  = 0 ;					// ErrCount Clear
	gSysInf.uwMotDownStartCnt  = 0 ;				// ErrCount Clear

	// エラーがあれば処理しない
	if ( gSysInf.ubErrCode ) {
		return gSysInf.ubErrCode ;
	}

	// モータストップの命令後一定時間経ってもモータが動き続ける
	if (( gCMT0.T10M.uwErrMotStop >= ERR_MOT_STOP ) && ( pFb->V.wVrev )){
		if ( gSwDev.Lvl.ubI05 == SWDEV_LVL_ON ){
			gSysInf.ubErrCode = ERR_LOW_LMT_OVR ;		// 下降端OVER
			Sci_PutsText( UART_CH_DBG, "Low_Limit_Over\n" ) ;
			return gSysInf.ubErrCode ;
		} else if ( gEnc.fUpLimit == UP_LIMIT ){
			gSysInf.ubErrCode = ERR_UP_LMT_OVR ;		// 上昇端OVER
			Sci_PutsText( UART_CH_DBG, "High_Limit_Over\n" ) ;
			return gSysInf.ubErrCode ;
		} else {
			gSysInf.ubErrCode = ERR_MOT_STOP_DELAY ;	// モータストップ遅れ
			Sci_PutsText( UART_CH_DBG, "MOT_OverRun\n" ) ;
			return gSysInf.ubErrCode ;
		}
	}

	return gSysInf.ubErrCode ;
}
//---------------------------------------------------
//	エラー検出用 上昇時
//	戻り値
//		0			エラー無し
//		それ以外	エラーあり(エラーコード)
//---------------------------------------------------

BYTE	xErr_Up( void )
{
	FB 			*pFb ;						// フィードバック情報
//	SYS_INF		*pSysInf ;					// システム情報
	ENC_INF		*pEnc ;						// エンコーダ
	ADC_MOT		*pMot ;						// モータ AD情報

	pFb 	 =  &gFb ;						// FB情報 Ptr
//	pSysInf  =  &gSysInf ;					// システム情報Ptr
	pEnc	 =  &gEnc ;						// Enc情報 Ptr
	pMot	 =  &gAdc.Mot ;					// モータ軸AD情報

	// エラーがあれば処理しない
	if ( gSysInf.ubErrCode ) {
		return gSysInf.ubErrCode ;
	}
		
	// 上昇タイムアウト
	if ( gCMT0.T1S.uwUpTimOut >= UP_TIME_OUT ) {			// 上昇タイムアウト
		gSysInf.ubErrCode = ERR_UP_TIMEOUT ;		
		Sci_PutsText( UART_CH_DBG, "RiseTimeOut\n" ) ;
		return gSysInf.ubErrCode ;
	}
	
	// 入力に対して逆回転している場合
	// CWへ加速中にエンコーダが逆の回転を示したら
	if ( ((pFb->V.wVrev > 160) && (pEnc->wAccStat == FBV_ACDC_ACC) ) &&
		 ((pEnc->wVrev < -160) )) {							// 逆転
		gSysInf.ubErrCode = ERR_MOT_REVERSE ;				// モータ逆転
		Sci_PutsText( UART_CH_DBG, "RotationERR\n" ) ;
		return gSysInf.ubErrCode ;
	}
		
	// モータ 過回転
	if ( pFb->V.wVrevAbs > gSysInf.uwMotOvrevMgn ) {			// 過回転
	//現在速度絶対値がモータ過回転値(定数)より大きい場合
		gSysInf.ubErrCode = ERR_REV_OVR_UP_MAX ;			// モータ過回転
		Sci_PutsText( UART_CH_DBG, "UP_Over_Rev\n" ) ;
		return gSysInf.ubErrCode ;
	}

/*	// 加速後の速度の過不足は最高速度が変化する為、絶対値の偏差で判定をする
	// pFb->V.wVdiffAbsは偏差の為、+が速度不足,-が過速度となる。
	// モータが過回転 偏差
	if ( pFb->V.wVdiffAbs <= REV_OVR_UP_DIFF ) {
		gSysInf.ubErrCode = ERR_REV_OVR_UP_DIFF ;			// モータ過加速
		return gSysInf.ubErrCode ;
	}*/
		
	// 速度ステータスを判定
	switch ( pEnc->wAccStat ) {  
		// 停止中
		case  FBV_ACDC_STOP :
			//モータ指示後 モータが回転しない
			gSysInf.uwMotUpStartCnt ++ ;
			if ( gSysInf.uwMotUpStartCnt >= MOT_START_NONE_CNT ){
				gSysInf.ubErrCode = ERR_UP_MOT_START ;		// モータ起動不可
				gSysInf.uwMotUpStartCnt  = 0 ;
				Sci_PutsText( UART_CH_DBG, "MOT_Not_Start\n" ) ;
				return gSysInf.ubErrCode ;
			}
			break ;
		// 加速中
		case  FBV_ACDC_ACC :
			gSysInf.uwMotUpStartCnt = 0 ;
			// 過電流を検出
			if ( pMot->dwCur  >= CUR_LMT_UP_ACC ){
				gSysInf.ubErrCode = ERR_CUR_LMT_UP_ACC ;	// ソフト過電流検出
				Sci_PutsText( UART_CH_DBG, "OverCurrent_ACC\n" ) ;
				return gSysInf.ubErrCode ;
			}
			// 上昇端フラグが立っている状態で上昇加速回転をする
			if ( gEnc.fUpLimit == UP_LIMIT ){
				gSysInf.ubErrCode = ERR_UP_LMT_OVR_REV ;		// 上昇端OVER
				Sci_PutsText( UART_CH_DBG, "UpLimitOverRun\n" ) ;
				return gSysInf.ubErrCode ;
			}
			break ;
		// 等速
		case  FBV_ACDC_CV :
			// 過電流を検出
			if ( pMot->dwCur  >= CUR_LMT_UP_CV ){
				gSysInf.ubErrCode = ERR_CUR_LMT_UP_CV ;	// ソフト過電流検出
				Sci_PutsText( UART_CH_DBG, "OverCurrent\n" ) ;
				return gSysInf.ubErrCode ;
			}
/*		// モータ回転数不足
			if ( pFb->V.wVdiffAbs <= REV_LOW_UP ) {
				gSysInf.ubErrCode = ERR_REV_LOW_UP ;		// モータ回転不足
				return gSysInf.ubErrCode ;
			}*/
			break ;
		// 減速
		case  FBV_ACDC_DEC :
			// 過電流を検出
			if ( pMot->dwCur  >= CUR_LMT_UP_DEC ){
				gSysInf.ubErrCode = ERR_CUR_LMT_UP_DEC ;	// ソフト過電流検出
				Sci_PutsText( UART_CH_DBG, "OverCurrent_DEC\n" ) ;
				return gSysInf.ubErrCode ;
			}
			break ;
		default :
			break ;
	}		
	return gSysInf.ubErrCode ;
}
//---------------------------------------------------
//	エラー検出用 下降時
//	戻り値
//		0			エラー無し
//		それ以外	エラーあり(エラーコード)
//---------------------------------------------------

BYTE	xErr_Down( void )
{
	FB 			*pFb ;						// フィードバック情報
//	SYS_INF		*pSysInf ;					// システム情報
	ENC_INF		*pEnc ;						// エンコーダ
	ADC_MOT		*pMot ;						// モータ AD情報

	pFb 	 =  &gFb ;						// FB情報 Ptr
//	pSysInf  =  &gSysInf ;					// システム情報Ptr
	pEnc	 =  &gEnc ;						// Enc情報 Ptr
	pMot	 =  &gAdc.Mot ;					// モータ軸AD情報

	// エラーがあれば処理しない
	if ( gSysInf.ubErrCode ) {
		return gSysInf.ubErrCode ;
	}
		
	// 下降タイムアウト
	if ( gCMT0.T1S.uwDownTimOut >= DOWN_TIME_OUT ) {	// 下降タイムアウト
		gSysInf.ubErrCode = ERR_DOWN_TIMEOUT ;		
		Sci_PutsText( UART_CH_DBG, "DownTimeOut\n" ) ;
		return gSysInf.ubErrCode ;
	}
	
	// 入力に対して逆回転している場合
	// CCWへ加速中にエンコーダが逆の回転を示したら
	if ( ((pFb->V.wVrev < -160) && (pEnc->wAccStat == FBV_ACDC_ACC) ) &&
		 ((pEnc->wVrev > 160) )) {							// 逆転
		gSysInf.ubErrCode = ERR_MOT_REVERSE ;				// モータ逆転
		Sci_PutsText( UART_CH_DBG, "RotationERR\n" ) ;
		return gSysInf.ubErrCode ;
	}
		
	// モータ 過回転
	if ( pFb->V.wVrevAbs > gSysInf.uwMotOvrevMgn ) {			// 過回転
	//現在速度絶対値がモータ過回転値(定数)より大きい場合
		gSysInf.ubErrCode = ERR_REV_OVR_DOWN_MAX ;			// モータ過加速
		Sci_PutsText( UART_CH_DBG, "DW_OverRev\n" ) ;
		return gSysInf.ubErrCode ;
	}

/*	// 加速後の速度の過不足は最高速度が変化する為、絶対値の偏差で判定をする
	// pFb->V.wVdiffAbsは偏差の為、-が速度不足,+が過速度となる。
	// モータが過回転 偏差
	if ( pFb->V.wVdiffAbs >= REV_OVR_DOWN_DIFF ) {
		gSysInf.ubErrCode = ERR_REV_OVR_DOWN_DIFF ;			// モータ過加速
		return gSysInf.ubErrCode ;
	}*/
		


	// 速度ステータスを判定
	switch ( pEnc->wAccStat ) {  
		// 停止中
		case  FBV_ACDC_STOP :
			//モータ指示後 モータが回転しない
			gSysInf.uwMotDownStartCnt ++ ;
			if ( gSysInf.uwMotDownStartCnt >= MOT_START_NONE_CNT ){
				gSysInf.ubErrCode = ERR_DOWN_MOT_START ;	// モータ起動不可
				gSysInf.uwMotUpStartCnt  = 0 ;
				Sci_PutsText( UART_CH_DBG, "MOT_Not_Start\n" ) ;
				return gSysInf.ubErrCode ;
			}
			break ;
				
		// 加速中
		case  FBV_ACDC_ACC :
			gSysInf.uwMotDownStartCnt = 0 ;
			// 過電流を検出
			if ( pMot->dwCur  >= CUR_LMT_DOWN_ACC ){
				gSysInf.ubErrCode = ERR_CUR_LMT_DOWN_ACC ;	// ソフト過電流検出
				Sci_PutsText( UART_CH_DBG, "ACC_OverCurrent\n" ) ;
				return gSysInf.ubErrCode ;
			}
			// 下限LSがONの状態で下降加速回転をする
			if ( gSwDev.Lvl.ubI05 == SWDEV_LVL_OFF ){
				gSysInf.ubErrCode = ERR_LO_LMT_OVR_REV ;	// 下降端OVER
				Sci_PutsText( UART_CH_DBG, "Lower_OverTravel\n" ) ;
				return gSysInf.ubErrCode ;
			}
			break ;
		// 等速
		case  FBV_ACDC_CV :
			// 過電流を検出
			if ( pMot->dwCur  >= CUR_LMT_DOWN_CV ){
				gSysInf.ubErrCode = ERR_CUR_LMT_DOWN_CV ;	// ソフト過電流検出
				Sci_PutsText( UART_CH_DBG, "OverCurrent\n" ) ;
				return gSysInf.ubErrCode ;
			}
/*			// モータ回転数不足
			if ( pFb->V.wVdiffAbs <= REV_LOW_DOWN ) {
				gSysInf.ubErrCode = ERR_REV_LOW_DOWN ;		// モータ回転不足
				return gSysInf.ubErrCode ;
			}*/
			break ;
		// 減速
		case  FBV_ACDC_DEC :
			// 過電流を検出
			if ( pMot->dwCur  >= CUR_LMT_DOWN_DEC ){
				gSysInf.ubErrCode = ERR_CUR_LMT_DOWN_DEC ;	// ソフト過電流検出
				Sci_PutsText( UART_CH_DBG, "DEC_OverCurrent\n" ) ;
				return gSysInf.ubErrCode ;
			}
			break ;
		default :
			break ;
	}		
	return gSysInf.ubErrCode ;
}

//=============================================================================
