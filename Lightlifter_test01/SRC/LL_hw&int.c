//=======================================================================
//
//	アプリケーション用割り込み関数とハードウェア処理
//*                                                                     */
/*  FILE        :LL_hw&int.c		                                    */
/*  DATE        :Tue , Nov 17, 2015                                     */
/*  DESCRIPTION :Main Program                                          */
/*  CPU TYPE    :                                                      */
/*                                                                     */
/*  NOTE:THIS IS A TYPICAL EXAMPLE.                                    */
/*                                                                     */
/***********************************************************************/
#include	<machine.h>					// Include machine.h
#include	"_iodefine.h"				// Include iodefine.h
#include	"_vect.h"					// 割込み関数宣言
#include	"TypeDef.h"					// オリジナル構造体
#include	"LL.h"						// I/O定義
#include	"LL_prototype.h"			// アプリケーションヘッダ
#include	"SCI_RX210.h"				// SCIヘッダ

//-------------------------------------------------------------------------

#pragma section

//-----------------------------------------------------------
//	システムクロック設定
//-----------------------------------------------------------
void	Inz_SysClock( void )
{
	WORD	wLoop ;
	
	
	// ＊クロックソースをLOCOにする(念の為)
	SYSTEM.PRCR.WORD = 0xa505 ;				// VRCR,クロック関係を書き込み可へ
	SYSTEM.VRCR = 0x00 ;					// 電圧レギュレータ制御Reg
	SYSTEM.SCKCR3.BIT.CKSEL = 0x00 ;		// LOCO選択
	SYSTEM.PRCR.WORD = 0xa500 ;				// 書き込み不可へ
	
	// ＊高速オンチップオシレータ(HOCO)を50MHzにする
	// リセット後は32MHz
	SYSTEM.PRCR.WORD = 0xa503 ;				// PRC0,PRC1を1
	// 一旦HOCO停止
	SYSTEM.HOCOCR.BIT.HCSTP = 0x01 ;		// HOCO停止
	wLoop = 1000 ;
	while( wLoop-- ) { nop( ) ; }
	// 一旦中速動作モードへ移行
	SYSTEM.OPCCR.BIT.OPCM = 0x02 ;			// 中速動作モード
	while( SYSTEM.OPCCR.BIT.OPCMTSF == 1 ) { nop( ) ; }	// 移行待ち
	// HOCO電源ON
	SYSTEM.HOCOPCR.BIT.HOCOPCNT = 0x00 ;	// HOCO-PwrON
	// 高速動作モードへ移行(リセット後は中速)
	SYSTEM.OPCCR.BIT.OPCM = 0x00 ;			// 高速動作モード
	while( SYSTEM.OPCCR.BIT.OPCMTSF == 1 ) { nop( ) ; }	// 移行待ち
	// HOCOを50MHzにする
	SYSTEM.HOCOCR2.BIT.HCFRQ = 0x03 ;		// HOCO=50MHz
	SYSTEM.HOCOWTCR2.BIT.HSTS2 = 0x03 ;		// 発振安定待機時間を50MHz用
	// 供給クロック分周設定
	SYSTEM.SCKCR.BIT.ICK = 0x00 ;			// ICLK=1/1  50MHz
	SYSTEM.SCKCR.BIT.FCK = 0x01 ;			// FCLK=1/2  25MHz
	SYSTEM.SCKCR.BIT.BCK = 0x01 ;			// BCLK=1/2  25MHz
	SYSTEM.SCKCR.BIT.PCKB = 0x01 ;			// PCLKB=1/2 25MHz
	SYSTEM.SCKCR.BIT.PCKD = 0x00 ;			// PCLKD=1/1(A/D) 50MHz
	SYSTEM.PRCR.WORD = 0xa500 ;				// 書き込み不可へ
	
	// ＊少し待つ (発振安定待ち)
	wLoop = 8000 ;
	while( wLoop-- ) { nop( ) ; }
	
	// ＊クロックソースをLOCO->HOCOに変更
	SYSTEM.PRCR.WORD = 0xa505 ;				// VRCR,クロック関係を書き込み可へ
	SYSTEM.VRCR = 0x00 ;					// 電圧レギュレータ制御Reg
	SYSTEM.SCKCR3.BIT.CKSEL = 0x01 ;		// HOCO選択
	SYSTEM.PRCR.WORD = 0xa500 ;				// 書き込み不可へ
	
	
}


//-----------------------------------------------------------
//	I/Oポート初期化
//-----------------------------------------------------------
void	Inz_IO_Port( void )
{
	
	// 接点入力 (入力)
	INP01_PDR = PORT_PDR_IN ;				// IN01 Dir UP
	INP02_PDR = PORT_PDR_IN ;				// IN02 Dir DOWN
	INP03_PDR = PORT_PDR_IN ;				// IN03 Dir STOP
	INP04_PDR = PORT_PDR_IN ;				// IN04 Dir EMM
	
	
	// 外部入力ポート
	INPR01_PDR = PORT_PDR_IN ;				// INR01 Dir UP	
	INPR02_PDR = PORT_PDR_IN ;				// INR02 Dir DOWN	
//	INPR03_PDR = PORT_PDR_IN ;				// INR03 Dir STOP	
//	INPR03用のP35はNMI端子であり、入力専用なので設定はしない。
	
	// 下限LS
	INP05_PDR = PORT_PDR_IN ;				// Low-LS Dir
	
	// DIP-SW (入力)
	DIP01_PDR = PORT_PDR_IN ;				// DIP01 Dir
	DIP02_PDR = PORT_PDR_IN ;				// DIP02 Dir
	DIP03_PDR = PORT_PDR_IN ;				// DIP03 Dir
	DIP04_PDR = PORT_PDR_IN ;				// DIP04 Dir
	DIP05_PDR = PORT_PDR_IN ;				// DIP05 Dir
	DIP06_PDR = PORT_PDR_IN ;				// DIP06 Dir
	DIP07_PDR = PORT_PDR_IN ;				// DIP07 Dir
	DIP08_PDR = PORT_PDR_IN ;				// DIP08 Dir	

	// インジケータ用LED (出力)
	LELO_PDR = PORT_PDR_OUT ;				// LED LOW Dir
	LEMI_PDR = PORT_PDR_OUT ;				// LED MIDDLE ir
	LEHI_PDR = PORT_PDR_OUT ;				// LED HIGH Dir
	LELO_PODR = LED_OFF ;					// LED LOW OFF
	LEMI_PODR = LED_OFF ;					// LED MID OFF
	LEHI_PODR = LED_OFF ;					// LED HIGH OFF

	// 外部出力ポート
//	CN05_PDR = PORT_PDR_OUT ;				// CN05 Dir
	CN06_PDR = PORT_PDR_OUT ;				// CN06 Dir
	CN07_PDR = PORT_PDR_OUT ;				// CN07 Dir

	// Power ON 保持処理			※10/23追加
	PWR_ON_PDR = PORT_PDR_OUT ;				// Port出力mode
	PWR_ON_PODR = POWER_ON_HOLD ;			// 電源保持

	// ブレーキ用出力ポート(出力)
	BRAKE_PDR = PORT_PDR_OUT ;				// Mot Brake
	BRAKE_PODR = BRAKE_LOCK ;				// Brake Lock

	// モータ電流検出(入力) 12bitA/D入力 
	MOT_CRNT_PDR = PORT_PDR_IN ;			// Mot Current
	
	// 電源電圧検出(入力) 
	BAT_VOL_PDR = PORT_PDR_IN ;				// Vatt Voltage
	
	// モータ過電流検出(入力) POE2機能
	MOT_POE_PDR	= PORT_PDR_IN ;				// Trip mode
}
//-------------------------------------------------------

//-----------------------------------------------------------
// ウォッチドッグタイマのセットアップ
//-----------------------------------------------------------
void	Inz_WDT( void ) 
{

//	WORD	wLoop ;

//	オートスタートモードでは、WDTCRの設定は無視される
//	OFS0レジスタの設定が優先されるため(mainプログラム外にマクロ記述)
	// タイムアウト期間選択ビット
//	WDT.WDTCR.BIT.TOPS = 3 ;				// 16384cycle
	// クロック分周比選択ビット
//	WDT.WDTCR.BIT.CKS = 0xF ;				// PCLK / 128
	// ウインドウ終了位置選択ビット
//	WDT.WDTCR.BIT.RPES = 3 ;				// 0% 終了位置設定なし
	// ウインドウ開始位置選択ビット
//	WDT.WDTCR.BIT.RPSS = 3 ;				// 0% 開始位置設定なし

//	オートスタートモードでは、WDTRCRの設定は無視される
	// WDT リセットコントロールレジスタの設定
//	WDT.WDTRCR.BIT.RSTIRQS = 1;				// Reset
	
	// リフレッシュレジスタ -> WDTの開始
	WDT.WDTRR = 0x00;						// リフレッシュ（WDT 開始）
	WDT.WDTRR = 0xFF;
	
}

//-----------------------------------------------------------
//	CRC演算器のセットアップ
//-----------------------------------------------------------
void	Inz_CRC( void )
{
	
	// CRC演算器を有効化
	SYSTEM.PRCR.WORD = 0xa502 ;				// MSTPmnを書込可へ
	MSTP( CRC ) = 0 ;						// Wakeup CRC (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 書き込み不可へ
	
	// 生成多項式をITU-Tのx16+x12+x5+1とする
	CRC.CRCCR.BIT.GPS = 0x03 ;				// X16 + X12 + X5 + 1
	
	// 演算をLSBファーストにする(Default)
	CRC.CRCCR.BIT.LMS = 0 ;					// CRC演算切替
	
	// CRCDORをゼロクリア
	CRC.CRCCR.BIT.DORCLR = 1 ;				// CRCDORクリア
	
}
//-----------------------------------------------------------------------
//	CMTU0_CMT0 初期化
//	1mSecインターバルタイマコンペアマッチ
//  周辺機器クロックが25MHz
//  分周比が8
//  1クロックに要する時間は1/(25*10^6/8)=3.2*10^-7=0.32us
//  コンペアマッチに要する時間は(CMCOR値)*0.32us
//  例）仮にCMCORがffffの設定だった場合 65536*0.32us=0.02097152sとなる
//  今回はCMCORが25000000/8/1000=3125なので0.32us*3125=1msとなる
//-----------------------------------------------------------------------
void	Inz_CMT0( void )
{
	
	// CMT0を有効化
	SYSTEM.PRCR.WORD = 0xa502 ;				// 動作モード変更操作を可へ
	MSTP( CMT0 ) = 0 ;						// Wakeup CMT0 (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 操作不可へ
	
	// CMT停止
	CMT.CMSTR0.BIT.STR0 = 0 ;				// Stop CMT0
	
	// インターバルセット
	CMT0.CMCR.BIT.CKS = 0x00 ;				// CKS is PCLK/8
	CMT0.CMCR.BIT.CMIE = 1 ;				// CMIE is Enable
	CMT0.CMCOR = MCU_PCLOCK / 8 / 1000 ;	// CMCOR is 1ms Count 
	
	// 割込許可,割込レベル設定
	IEN( CMT0, CMI0 ) = 1 ;					// CMI0 Enable (macro)
	IPR( CMT0, CMI0 ) = CMT0_INT_LVL ;		// CMI0 Int Level is 10 (macro)
	
	// CMT開始
	CMT.CMSTR0.BIT.STR0 = 1 ;				// Start CMT0
	
}


//-----------------------------------------------------------------------
//	CMTU0_CMT1 初期化
//	0.1mSecインターバルタイマコンペアマッチ
//-----------------------------------------------------------------------
void	Inz_CMT1( void )
{
	
	// CMT0を有効化
	SYSTEM.PRCR.WORD = 0xa502 ;				// 動作モード変更操作を可へ
	MSTP( CMT1 ) = 0 ;						// CMT1を有効化 (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 操作不可へ
	
	// CMT停止
	CMT.CMSTR0.BIT.STR1 = 0 ;				// Stop CMT1
	
	// インターバルセット MCU_PCLOCK=25000000(25MHz)
	CMT1.CMCR.BIT.CKS = 0x00 ;				// CKS is PCLK/8
	CMT1.CMCR.BIT.CMIE = 1 ;				// CMIE is Enable
	CMT1.CMCOR = MCU_PCLOCK / 8 / 10000 ;	// CMCOR is 0.1ms Count 
//	CMT1.CMCOR = MCU_PCLOCK / 8 / 20000 ;	// CMCOR is 50us Count 
//	CMT1.CMCOR = MCU_PCLOCK / 8 / 40000 ;	// CMCOR is 25us Count 
//	CMT1.CMCOR = MCU_PCLOCK / 8 / 100000 ;	// CMCOR is 10us Count 
//	CMT1.CMCOR = MCU_PCLOCK / 8 / 200000 ;	// CMCOR is 5us Count 
//	CMT1.CMCOR = MCU_PCLOCK / 8 / 400000 ;	// CMCOR is 2.5us Count 
//	CMT1.CMCOR = MCU_PCLOCK / 8 / 1000000 ;	// CMCOR is 1us Count 
	
	// 割込許可,割込レベル設定
	IEN( CMT1, CMI1 ) = 1 ;					// CMT1 Enable (macro)
	IPR( CMT1, CMI1 ) = CMT1_INT_LVL ;		// CMT1 Int Level is 11 (macro)
	
	// CMT開始
	CMT.CMSTR0.BIT.STR1 = 1 ;				// Start CMT0
	
}

//-----------------------------------------------------------------------
//	CMTU1_CMT2 初期化
//	Enc0 インターバルタイマコンペアマッチ
//	エンコーダオーバーフローのプログラムに使用
//	カウンタクロック25MHz/128=195.3125kHz(5.12us)
//	高所作業者のプログラムがCMCORを174.76msにてカウントしている。
//	そこに合わせるため0.17476*25000000/128=34132.8->34133にて1カウントとした。
//	10進数で34133->16進数で0x8555となる。
//-----------------------------------------------------------------------
void	Inz_CMT2( void )
{
	
	// CMT0を有効化
	SYSTEM.PRCR.WORD = 0xa502 ;				// 動作モード変更操作を可へ
	MSTP( CMT2 ) = 0 ;						// CMT2を有効化 (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 操作不可へ
	
	// CMT停止
	CMT.CMSTR1.BIT.STR2 = 0 ;				// Stop CMT2
	
	// インターバルセット MCU_PCLOCK=25000000(25MHz)
	CMT2.CMCR.BIT.CKS = 0x02 ;				// CKS is PCLK/128(195kHz)
//	CMT2.CMCR.BIT.CKS = 0x01 ;				// CKS is PCLK/32(781kHz)
	CMT2.CMCR.BIT.CMIE = 1 ;				// CMIE is Enable
	CMT2.CMCOR = ENC_CMTU_COUNT ; 			// CMCOR is 174.76ms Count 
	
	// 割込許可,割込レベル設定
	IEN( CMT2, CMI2 ) = 1 ;					// CMT2 Enable (macro)
	IPR( CMT2, CMI2 ) = ENC_CMT_INT_LVL ;	// CMT2 Int Level is 11 (macro)
	
	// CMT開始
	CMT.CMSTR1.BIT.STR2 = 1 ;				// Start CMT0
	
}
/*
//-----------------------------------------------------------------------
//	MTU2 ch1 フリータイマランカウンタ
//	ホールセンサ60度の時間計測用
//	
//	元はMTUのクロックはPCLK=48MHz(20.833nS)
//	PCLK=25MHz(40ns)
//-----------------------------------------------------------------------
void	Inz_MTU1( void )
{
	
	BYTE			bReg ;

	// MTUを有効化
	SYSTEM.PRCR.WORD = 0xa502 ;				// 動作モード変更操作を可へ
	MSTP( MTU ) = 0 ;						// Wakeup MTU (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 操作不可へ

	// 1.タイマカウント停止
	bReg = MTU.TSTR.BYTE ;
	nop( ) ;
	bReg &= 0xfd ;							// MTU1 mask
	MTU.TSTR.BYTE = bReg ;					// MTU1 stop
		
	// 2.カウントソース設定
	// PCLK/16を両エッジカウント
	// 元はカウント周期=20.833nS*16/2=0.167uS 実質6MHz
	// 今はカウント周期=40ns*16=0.16uS 6.25MHz
	// 両エッジ分周比を4、Upエッジにてカウントとしている
//	MTU1.TCR.BIT.TPSC = 0x02 ;				// PCLK/16
	MTU1.TCR.BIT.TPSC = 0x01 ;				// PCLK/4
//	MTU1.TCR.BIT.CKEG = 0x02 ;				// Up/DownEdge count元
	MTU1.TCR.BIT.CKEG = 0x00 ;				// UpEdge count	
	MTU1.TCR.BIT.CCLR = 0x00 ;				// Dont CLR.
	
	// 3.タイマモードの設定
	MTU1.TMDR.BIT.MD = 0x00 ;				// Normal Mode
	
	// 4.出力設定
	// 出力禁止
	MTU1.TIOR.BIT.IOA = 0x00 ;				// Ach CTL
	MTU1.TIOR.BIT.IOB = 0x00 ;				// Bch CTL
	
	// 5.タイマ値初期値設定
	MTU1.TCNT = 0 ;							// Clear Count
	
	// 6.割込許可設定
	MTU1.TIER.BIT.TCIEV = 1 ;				// OVF-Int.
	
	// 7.割込許可,割込レベル設定
	IEN( MTU1, TCIV1 ) = 0 ;				// 割込を一旦停止
	IR( MTU1, TCIV1 )  = 0 ;				// 割込フラグをクリア
	IPR( MTU1, TCIV1 ) = 13 ;				// 割込レベルセット
	IEN( MTU1, TCIV1 ) = 1 ;				// 割込許可
	
	// 8.タイマカウント開始
	bReg = MTU.TSTR.BYTE ;
	nop( ) ;
	bReg |= 0x02 ;
	MTU.TSTR.BYTE = bReg ;						// MTU0 start
	nop( ) ;
	
	
//	// 誤書き込み防止
//	MTU.TRWER.BIT.RWE = 0 ;						// MTU0 lock
	
		
	
}
*/

//-----------------------------------------------------------------------
// PWM確認用として外部出力端子OUT1を使用する
//-----------------------------------------------------------------------
void Inz_MTU0(void){

	BYTE			bReg ;

	// MTUを有効化
	SYSTEM.PRCR.WORD = 0xa502 ;				// 動作モード変更操作を可へ
	MSTP( MTU ) = 0 ;						// Wakeup MTU (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 操作不可へ

	// 1.タイマカウント停止
	bReg = MTU.TSTR.BYTE ;
	nop( ) ;
	bReg &= 0xfe ;							// MTU0 mask
	MTU.TSTR.BYTE = bReg ;					// MTU0 stop
	
	// 2.カウントソース設定
	// PCLK/1をUPEdgeカウント
	// 今はカウント周期=4ns = 25MHz
	MTU0.TCR.BIT.TPSC = 0x00 ;				// PCLK/1
	MTU0.TCR.BIT.CKEG = 0x00 ;				// UpEdge count	
	MTU0.TCR.BIT.CCLR = 0x05 ;				// TCNT CLR. by TGRC Comp.Match
	
	// 3.PWMモードの選択
	MTU0.TMDR.BIT.MD = 0x02 ;				// PWM Mode 1
	
	// 4.出力設定
	PORT3.PDR.BIT.B2 = PORT_PDR_OUT ;
	PORT3.PODR.BIT.B2 = EXTOUT_OFF ;
	PORT3.PMR.BIT.B2 = 1 ;					// P32/MTIOC0C
	MTU0.TIORL.BIT.IOC = 0x05 ;				// Cch CTL
	MTU0.TIORL.BIT.IOD = 0x02 ;				// Dch CTL

	// 端子設定
	//	PmnPFSへの書き込みを許可する
    MPC.PWPR.BIT.B0WI  = 0 ;							// PFSWE書込許可
    MPC.PWPR.BIT.PFSWE = 1 ;							// PFSreg書込許可
	MPC.P32PFS.BIT.PSEL = 1 ;							// MTIOC0C使用
	//	PmnPFSへの書き込みを禁止する
    MPC.PWPR.BIT.PFSWE = 0 ;							// PFSreg書込禁止
    MPC.PWPR.BIT.B0WI  = 1 ;							// PFSWE書込禁止
		
	// 5.タイマ値初期値設定
	MTU0.TCNT = 0 ;							// Clear Count
	MTU0.TGRC = 500 ;						// Int.Count
//	MTU0.TGRC = 0xffff ;					// Int.Count
	MTU0.TGRD = 250 ;						// Int.Count
	
	// 6.割込許可設定
	MTU0.TIER.BIT.TGIEC = 1 ;				// TGIC割込要求を許可

// 7.割込許可,割込レベル設定
	IEN( MTU0, TGIC0 ) = 0 ;				// 割込を一旦停止
	IR( MTU0, TGIC0 )  = 0 ;				// 割込フラグをクリア
	IPR( MTU0, TGIC0 ) = 14 ;				// 割込レベルセット
	IEN( MTU0, TGIC0 ) = 1 ;				// 割込許可
	
	// 9.タイマカウント開始
	bReg = MTU.TSTR.BYTE ;
	nop( ) ;
	bReg |= 0x01 ;
	MTU.TSTR.BYTE = bReg ;						// MTU0 start
	nop( ) ;
	
	
	// 誤書き込み防止
//	MTU.TRWER.BIT.RWE = 0 ;						// MTU0 lock
	
	
}


//-----------------------------------------------------------------------
//	12bitAD変換器 初期化
//	1サイクルスキャンモードで一気に変換させる
//	現状ではモータ電流の検出に使用している
//	PCKD(S12AD専用クロック)は1分周 -> 50MHz
//-----------------------------------------------------------------------
void	Inz_AD12( void )
{
		
	// AD12変換ユニットを有効化
	SYSTEM.PRCR.WORD = 0xa502 ;				// 動作モード変更操作を可へ
	MSTP( S12AD ) = 0 ;						// Wakeup AD12unit (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 操作不可へ
	
	// 端子設定
	//	PmnPFSへの書き込みを許可する
    MPC.PWPR.BIT.B0WI  = 0 ;				// PFSWE書込許可
    MPC.PWPR.BIT.PFSWE = 1 ;				// PFSreg書込許可
	MPC.P40PFS.BIT.ASEL = 1 ;				// AN000使用 電流検出
	MPC.P41PFS.BIT.ASEL = 1 ;				// AN001使用 電圧検出
	//	PmnPFSへの書き込みを禁止する
    MPC.PWPR.BIT.PFSWE = 0 ;				// PFSreg書込禁止
    MPC.PWPR.BIT.B0WI  = 1 ;				// PFSWE書込禁止

	// A/Dチャンネル選択レジスタ(ADANS)
	S12AD.ADANSA.BIT.ANSA0 = 1 ;			// CH:AN000
	S12AD.ADANSA.BIT.ANSA1 = 1 ;			// CH:AN001

	//  A/Dコントロールレジスタ(ADCSR)
	S12AD.ADCSR.BIT.DBLANS = 0 ;			// 2重化チャンネル選択
	S12AD.ADCSR.BIT.DBLE = 0 ;				// ダブルトリガ許可 しない
	S12AD.ADCSR.BIT.EXTRG = 0 ;				// ADSTRGRtimer要因
	S12AD.ADCSR.BIT.TRGE = 1 ;				// MTUトリガ許可
	S12AD.ADCSR.BIT.ADIE = 1 ;				// AD変換終了割込有り

	//  A/Dコントロール拡張レジスタ(ADCER)
	S12AD.ADCER.BIT.ACE = 0 ;				// 自動クリア許可->禁止
	S12AD.ADCER.BIT.ADRFMT = 0 ;			// 結果を右詰め
	S12AD.ADCSR.BIT.ADCS = 1 ;				// 1 Cycle Scsn Mode

	// A/D開始トリガ選択レジスタ(ADSTRGR)
	//MTU4.TADCORAとMTU4.TCNTのコンペアマッチ(割り込み間引き機能1)
	S12AD.ADSTRGR.BIT.TRSA = 0x06 ;			// TRG4AN(TRG4AN)
	
	// 割込許可,割込レベル設定
	IEN( S12AD, S12ADI0 ) = 1 ;				// AD12 Enable (macro)
	IPR( S12AD, S12ADI0 ) = ADC12_INT_LVL ;	// AD12 Int. Level is 8(macro)
	
	
}

//-----------------------------------------------------------------------
//	モータ電流オフセット計測			※10/15移植
//	モータ軸０(Axis0)		AN0
//	電源電圧(24V)			AN1
//	引数
//		BYTE bAxis			エンコーダ軸 0,1
//-----------------------------------------------------------------------
void	AD12_GetOffset( void )
{
	BYTE			bLoop ;					// LoopCounter
	UDWORD			udwOfs ;				// Motor curr. RawOffset(Axis0/1)
	UDWORD			udwVbat ;				// バッテリ電圧

	// 割込許可,割込レベル設定
		// 割り込み禁止
	IEN( S12AD, S12ADI0 ) = 0 ;				// AD12 Disable (macro)
		// 割り込みレベルを0にする
	IPR( S12AD, S12ADI0 ) = 0 ;				// AD12 Int. Level is 0(macro)
	
	// AD12変換ユニットを有効化		->初期化時に設定済み
//	MSTP( S12AD ) = 0 ;						// Wakeup AD12unit (macro)
	
	//  A/D開始トリガ選択レジスタ(ADSTRGR)
//	S12AD.ADSTRGR.BIT.ADSTRS = 0x00 ;		// 外部トリガ端子
	
	//  A/Dコントロールレジスタ(ADCSR) -> TRGE,ADIE以外初期化時に設定済み
//	S12AD.ADCSR.BIT.EXTRG = 0 ;				// ADSTRGRtimer要因
//	S12AD.ADCSR.BIT.TRGE = 0 ;				// MTUトリガ禁止
//	S12AD.ADCSR.BIT.CKS = 3 ;				// Clock=PCLK(48MHz)/1
	S12AD.ADCSR.BIT.ADIE = 0 ;				// AD変換終了割込無し
//	S12AD.ADCSR.BIT.ADCS = 0 ;				// 1 Cycle Scan Mode
	
	//  A/Dコントロール拡張レジスタ(ADCER) -> 初期化時に設定済み
//	S12AD.ADCER.BIT.ADRFMT = 0 ;			// 結果を右詰め
	
	
	// オフセット,電圧計測
	udwOfs = 0	;							// モータ電流 RawOffset		
	udwVbat = 0 ;							// バッテリ電圧
	for( bLoop = 0 ; bLoop < ADC12_OFS_MEASURES ; bLoop++ ) {	// 32回
		

		// AN0:Motor Current
		// チャンネルをAN000にセット
		S12AD.ADANSA.BIT.ANSA0 = 0x01 ;			// CH:AN0
		// AD変換を開始する→変換が完了するとADSTは0になる
		S12AD.ADCSR.BIT.ADST = 1 ;				// AD Start
		while ( S12AD.ADCSR.BIT.ADST == 1 ) { 	// 変換待ち
			nop( ) ;
		}
		//変換値を生データに加算していく
		udwOfs += S12AD.ADDR0 ;	// Motor curr. RawOffset(0)
		gCMT1.udw01ms = 0 ;						// count 0.1mSec
		while ( gCMT1.udw01ms < 4 ) {			// 0.4mS待ち
			nop( ) ;
		}
		
		
		// AN1:バッテリ電圧
		// チャンネルとAD001にセット
		S12AD.ADANSA.BIT.ANSA1 = 0x02 ;			// CH:AN1
		S12AD.ADCSR.BIT.ADST = 1 ;				// AD Start
		while ( S12AD.ADCSR.BIT.ADST == 1 ) { 	// 変換待ち
			nop( ) ;
		}
		udwVbat += S12AD.ADDR1 ;					// バッテリ電圧
//		gDbg.CNT.dwCnt05 ++ ;

		while ( gCMT1.udw01ms < 4 ) {			// 0.4mS待ち
			nop( ) ;
		}
	}
	
	// * 電流オフセットとV24電圧セット
	// AN0000:Motor 
	// 初回はオフセット電圧に計算値をセット
	// 2回目以降は合計オフセッ電圧に加算していくト
	if ( gAdc.Mot.fOfsGet != TRUE ) {			// Offset Meas. Flag
		gAdc.Mot.uwOfs = 
				udwOfs / ADC12_OFS_MEASURES ;	// Motor curr. Ofs.
	} else {
		gAdc.Mot.udwOfsSum += 
				udwOfs / ADC12_OFS_MEASURES ;	// OffsetAvgSum
		gAdc.Mot.uwOfsCount++ ;					// OffsetAvgSumCount
	}
	gAdc.Mot.uwOfsTime = 0 ;					// Motor curr. Ofs.T
	gAdc.Mot.fOfsGet = TRUE ;					// Offset Meas. Flag
	gAdc.Mot.dwCur = 0 ;						// UVWcur.
	gAdc.Mot.dwSum = 0 ;						// UVW sum
	gAdc.Mot.uwSumCount = 0 ;					// UVW sum count
	gAdc.Mot.ubEncCount = 0 ;					// Mot. Enc. Pulse Count
//	gAdc.Mot.wEncVrevPrv = 0 ;					// Previous Motor rpm

	// AN001:バッテリ電圧
	gAdc.Volt.V24.uwVal = (UWORD) (udwVbat / ADC12_OFS_MEASURES) ;// V24 ADC値
	gAdc.Volt.V24.udwSum = 0 ;							// ADC積算値
	gAdc.Volt.V24.uwCount = 0 ;							// ADC計測回数
	gAdc.Volt.fMeas24 = ADC_MEASURE_YES ;				// 24V計測Flag
	gAdc.Volt.V24.uwValMath = (UWORD)(10 * gAdc.Volt.V24.uwVal) ; // ADC値*10
	gAdc.Volt.uwV24 = (UWORD)( xDivide( gAdc.Volt.V24.uwValMath , AD_DEV ) 
										+ OFFSET_AD12 ) ;	// V24電圧(0.01V/u)
//	gAdc.Volt.uwV24 = (UWORD)(100 * (UDWORD)gAdc.Volt.V24.uwVal *
//						ADC12_V24_CNV1 / ADC12_V24_CNV2) ;	// V24電圧(0.01V/u)
	
	// A/D開始トリガ選択レジスタ(ADSTRGR)
	// Inz_AD12にて設定済み
	//MTU0〜MTU4のTRGAのインプットキャプチャ /コンペアマッチまたは
	//相補PWMモード時のMTU4.TCNTのアンダフロー (谷)
//	S12AD.ADSTRGR.BIT.ADSTRS = 0x03 ;			// TRG7AN(TRG4ABN_0)
	
	// A/Dコントロールレジスタ(ADCSR)
	// TRGEビットに1を立てるとMTUからのトリガでA/D変換をスタートできる
	S12AD.ADCSR.BIT.TRGE = 1 ;					// MTUトリガ許可
	//割り込みを許可しているので、変換後にInt_AD12( )の割り込み処理を行う。
	S12AD.ADCSR.BIT.ADIE = 1 ;					// AD変換終了割込有り
	
	// 割込許可,割込レベル設定 (元に戻す)
	IEN( S12AD, S12ADI0 ) = 1 ;					// AD12 Enable (macro)
	IPR( S12AD, S12ADI0 ) = ADC12_INT_LVL ;		// AD12 Int. Level is 8(macro)
	

}


//-----------------------------------------------------------------------
//	PWM情報の初期化
//	
//-----------------------------------------------------------------------
void	Inz_PWM_Inf( void )
{
	
	// PWM情報のセットアップ

//	gPwm.uwPwmHalf = MTU_PWM_TIME / 2 ;					// (PWM周期1/2)/2
	gPwm.uwPwmFull = MTU_PWM_TIME ;						// PWM周期1/2
	gPwm.uwMax = MTU_PWM_TIME - MTU_DEADTIME - MTU_MGN ;// PWM最大値
	gPwm.uwMin = MTU_DEADTIME + MTU_MGN ;				// PWM最小値
	gPwm.uwPwmZero = (gPwm.uwMax + gPwm.uwMin) / 2 ;	// Duty50%値
	gPwm.wRngMax = gPwm.uwMax - gPwm.uwPwmZero ;		// Duty最大絶対値

	
}

//-----------------------------------------------------------------------
//	タイマMTU3 MTU3-4 相補PWM 初期化	モータ出力軸１
//	
//	MTUのクロックはPCLK=25MHz
//  カウント周期は40ns
//	PWM周期=12.5KHz(80μS)
//-----------------------------------------------------------------------
void	Inz_PWM( void )
{
	BYTE			bReg ;

	MTU_PWM			*pPwm ;					// 軸固有値 Ptr
	pPwm = &gPwm ;

	Inz_PWM_Inf( ) ;						// PWM情報初期化

	pPwm->wDutyExe = 0 ;					// DutyExe
	pPwm->wDirU = 0 ;						// U相極性
	pPwm->wDirV = 0 ;						// V相極性
	pPwm->wDirW = 0 ;						// W相極性
	pPwm->uwAdCmp = 600 ;					// AD変換開始要求周期
	
	// MTUを有効化
	SYSTEM.PRCR.WORD = 0xa502 ;				// 動作モード変更操作を可へ
	MSTP( MTU ) = 0 ;						// Wakeup MTU3 (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 書き込み不可

	// 1.タイマカウント停止
	bReg = MTU.TSTR.BYTE ;
	nop( ) ;
	bReg &= 0x3f ;							// MTU3,4 mask
	MTU.TSTR.BYTE = bReg ;					// MTU3,4 stop
	
	// 2.端子設定
	//ライトリフターの場合PWM端子がプルアップのため
	PORTC.PODR.BIT.B5 = FET_OFF ;			// PC5/MTIOC3B(UH)
	PORTC.PODR.BIT.B4 = FET_OFF ;			// PC4/MTIOC3D(UL)
	PORTB.PODR.BIT.B3 = FET_OFF ;			// PB3/MTIOC4A(VH)
	PORTB.PODR.BIT.B1 = FET_OFF ;			// PB1/MTIOC4C(VL)
	PORT5.PODR.BIT.B4 = FET_OFF ;			// P54/MTIOC4B(WH)
	PORT5.PODR.BIT.B5 = FET_OFF ;			// P55/MTIOC4D(WL)

	PORTC.PDR.BIT.B5  = PORT_PDR_OUT ;		// PC5/MTIOC3B(UH)
	PORTC.PDR.BIT.B4  = PORT_PDR_OUT ;		// PC4/MTIOC3D(UL)
	PORTB.PDR.BIT.B3  = PORT_PDR_OUT ;		// PB3/MTIOC4A(VH)
	PORTB.PDR.BIT.B1  = PORT_PDR_OUT ;		// PB1/MTIOC4C(VL)
	PORT5.PDR.BIT.B4  = PORT_PDR_OUT ;		// P54/MTIOC4B(WH)
	PORT5.PDR.BIT.B5  = PORT_PDR_OUT ;		// P55/MTIOC4D(WL)

	// ポートモードの設定
	PORTC.PMR.BIT.B5 = 1 ;					// PC5/MTIOC3B(WH)
	PORTC.PMR.BIT.B4 = 1 ;					// PC4/MTIOC3D(WH)
	PORTB.PMR.BIT.B3 = 1 ;					// PB2/MTIOC4A(WH)
	PORTB.PMR.BIT.B1 = 1 ;					// P54/MTIOC4C(WH)
	PORT5.PMR.BIT.B4 = 1 ;					// P54/MTIOC4B(WH)
	PORT5.PMR.BIT.B5 = 1 ;					// P54/MTIOC4D(WH)

	//	PmnPFSへの書き込みを許可する
    MPC.PWPR.BIT.B0WI  = 0 ;				// PFSWE書込許可
    MPC.PWPR.BIT.PFSWE = 1 ;				// PFSreg書込許可
	MPC.PC5PFS.BIT.PSEL = 0x01 ;			// MTIOC3B使用
	MPC.PC4PFS.BIT.PSEL = 0x01 ;			// MTIOC3D使用
	MPC.PB3PFS.BIT.PSEL = 0x02 ;			// MTIOC4A使用
	MPC.PB1PFS.BIT.PSEL = 0x02 ;			// MTIOC4C使用
	MPC.P54PFS.BIT.PSEL = 0x01 ;			// MTIOC4B使用
	MPC.P55PFS.BIT.PSEL = 0x01 ;			// MTIOC4D使用
	//	PmnPFSへの書き込みを禁止する
    MPC.PWPR.BIT.PFSWE = 0 ;				// PFSreg書込禁止
    MPC.PWPR.BIT.B0WI  = 1 ;				// PFSWE書込禁止
	

	// 3.カウントソース設定
	// PCKLB=ICLK/2=25MHz
	// (1) PCLK/4を両エッジカウントして実質PCLK/2をカウント
	//     カウント周期=80nS
	// (2) PCLK/1を両エッジカウントして実質PCLK*2をカウント
	//     カウント周期=20nS
	// (3) PCLK/1をアップエッジカウントして実質PCLK*4をカウント
	//     カウント周期=40nS
	// 高所作業者のカウント周期が20.84nsのため(2)でいく->廃止
	// 現在の設定はカウント周期40nsとなっている
	
//	MTU3.TCR.BIT.TPSC = 1 ;					// PCLK/4
	MTU3.TCR.BIT.TPSC = 0 ;					// PCLK/1
//	MTU3.TCR.BIT.CKEG = 2 ;					// Up&DownEdge count
	MTU3.TCR.BIT.CKEG = 0 ;					// UpEdge count
	MTU3.TCR.BIT.CCLR = 0 ;					// Disable Clear TCNT
//	MTU4.TCR.BIT.TPSC = 1 ;					// PCLK/4
	MTU4.TCR.BIT.TPSC = 0 ;					// PCLK/1
//	MTU4.TCR.BIT.CKEG = 2 ;					// Up&DownEdge count
	MTU4.TCR.BIT.CKEG = 0 ;					// UpEdge count
	MTU4.TCR.BIT.CCLR = 0 ;					// Disable Clear TCNT
	
	
	// 4.BLDCモータ用ゲート制御  使用しない
//	MTU.TGCRA.BIT.BDC = 1 ;					// TGCRA enable
//	MTU.TGCRA.BIT.UF = 0 ;					// UF
//	MTU.TGCRA.BIT.VF = 0 ;					// VF
//	MTU.TGCRA.BIT.WF = 0 ;					// WF
//	MTU.TGCRA.BIT.FB = 1 ;					// UF,VF,WF is soft ctl
//	MTU.TGCRA.BIT.P = 1 ;					// 正相はPWM
//	MTU.TGCRA.BIT.N = 1 ;					// 逆相はPWM
	
	// 5.デッドタイム設定(タイマ初期カウンタ)
	// DT=2.0μS	2000/20.834=96
	// DT=1.5μS	1500/20.834=72
	// DT=1.0μS	1000/20.834=48
	// 変更 11/11 設定(2)
	// DT=2.0μS	2000/20=100
	// DT=1.5μS	1500/20=75
	// DT=1.0μS	1000/20=50
	// 変更 11/11 設定(3)
	// DT=2.0μS	2000/40=50
	// DT=1.5μS	1500/40=38
	// DT=1.0μS	1000/40=25

	MTU3.TCNT = MTU_DEADTIME ;				// Deadtime
	MTU4.TCNT = 0x0000 ;					// must be ZERO
	
	// 6.チャンネル間の同期設定 ※使用しない

	// 7.初期PWM-Dutyの設定 (50%)
	DUTY_U = gPwm.uwPwmZero ;		// MTU3.TGRB : Initial PWM-Duty 
	DUTY_UB = gPwm.uwPwmZero ;		// MTU3.TGRD : buffer of MTU3.TGRB
	DUTY_V = gPwm.uwPwmZero ;		// MTU4.TGRA : Initial PWM-Duty
	DUTY_VB = gPwm.uwPwmZero ;		// MTU4.TGRC : buffer of MTU4.TGRA
	DUTY_W = gPwm.uwPwmZero ;		// MTU4.TGRB : Initial PWM-Duty
	DUTY_WB = gPwm.uwPwmZero ;		// MTU4.TGRD : buffer of MTU4.TGRB
	
	// 8.デッドタイム生成の有無設定  使用しない
	// 	※デッドタイム生成なしのみ設定を行う	
	
	// 9.デッドタイムとPWM周期設定
	MTU.TDDR = MTU_DEADTIME ;				// Deadtime
	MTU.TCDR = gPwm.uwPwmFull ;				// PWM周期
	MTU.TCBR = gPwm.uwPwmFull ;				// PWM周期(Buffer)
	MTU3.TGRC = MTU.TCDR + MTU_DEADTIME ;	// PWM周期+deadtime
	MTU3.TGRA = MTU.TCDR + MTU_DEADTIME ;	// PWM周期+deadtime

	// 10.出力レベルの選択				
	MTU.TOCR1.BIT.TOCL = 0 ;				// WriteEnable
	MTU.TOCR1.BIT.TOCS = 0 ;				// TOCR1設定有効
	MTU.TOCR1.BIT.PSYE = 0 ;				// PWM同期出力禁止
	MTU.TOCR1.BIT.OLSP = 0 ;				// 正相Low-Active
	MTU.TOCR1.BIT.OLSN = 0 ;				// 逆相Low-Active
	MTU.TOCR1.BIT.TOCL = 1 ;				// WriteProtected
	
	// 11.タイマモードの設定
	// MDの設定は相補PWMの場合MTU3のみ、MTU4はMTU3の設定に準じる
	// MTU4は初期値の設定のみ行う事
//	MTU3.TMDR.BIT.MD = 0x0f ;				// 相補PWM設定 山・谷転送
	MTU3.TMDR.BIT.MD = 0x0e ;				// 相補PWM設定 谷転送
//	MTU3.TMDR.BIT.MD = 0x0d ;				// 相補PWM設定 山転送
	MTU3.TMDR.BIT.BFA = 1 ;					// TGRC is buffer of TGRA
	MTU3.TMDR.BIT.BFB = 1 ;					// TGRD is buffer of TGRB

	// 12.出力端子の許可設定
	// Int_MotExiteにてONにする為、初期設定ではOFFとする
	PO_UH = EXTOUT_OFF ;					// MTIOC3B出力(UH1)
	PO_UL = EXTOUT_OFF ;					// MTIOC3D出力(UL1)
	PO_VH = EXTOUT_OFF ;					// MTIOC4A出力(VH1)
	PO_VL = EXTOUT_OFF ;					// MTIOC4C出力(VL1)
	PO_WH = EXTOUT_OFF ;					// MTIOC4B出力(WH1)
	PO_WL = EXTOUT_OFF ;					// MTIOC4D出力(WL1)
	
	
//以下AD変換との同期設定*
	// AD開始要求設定
	// 開始要求ディレイド機能設定
	// Buf転送は山で行う --> AD計測をPWM波形の後半で行いたい
//	MTU4.TADCOBRA =  gPwm.uwPwmZero + 0 ;	// AD変換開始要求周期Buf
//	MTU4.TADCORA  =  gPwm.uwPwmZero + 0 ;	// AD変換開始要求周期
	MTU4.TADCOBRA =  0xffff ;				// AD変換開始要求周期Buf
	MTU4.TADCORA  =  0xffff ;				// AD変換開始要求周期
	MTU4.TADCOBRB =  0xffff ;				// AD変換開始要求周期Buf
	MTU4.TADCORB  =  0xffff ;				// AD変換開始要求周期

// Buffaへの転送タイミング
//	MTU4.TADCR.BIT.BF = 2 ;					// 谷でBuf転送
	MTU4.TADCR.BIT.BF = 1 ;					// 山でBuf転送
//	MTU4.TADCR.BIT.BF = 3 ;					// 山谷でBuf転送
//A/D変換の開始のタイミング->UP,DOWN共に禁止
	MTU4.TADCR.BIT.UT4AE = 0 ;				// TAG4AN UpCount
	MTU4.TADCR.BIT.DT4AE = 0 ;				// TAG4AN DownCount
	MTU4.TADCR.BIT.UT4BE = 0 ;				// TAG4BN UpCount
	MTU4.TADCR.BIT.DT4BE = 0 ;				// TAG4BN DownCount

//間引き機能の連動->なし	
	MTU4.TADCR.BIT.ITA3AE = 0 ;				// TRG4AN->TGI3A 間引き連動
	MTU4.TADCR.BIT.ITA4VE = 0 ;				// TRG4AN->TCI4V 間引き連動
	MTU4.TADCR.BIT.ITB3AE = 0 ;				// TRG4BN->TGI3A 間引き連動
	MTU4.TADCR.BIT.ITB4VE = 0 ;				// TRG4BN->TCI4V 間引き連動

//間引き機能の設定->現在は使用不可
	MTU.TITCR.BIT.T4VEN = 0 ;				// TCIV4割込間引き(禁止)
	MTU.TITCR.BIT.T4VCOR = MTU_INT_SKIP ;	// TCIV4間引き回数
	MTU.TITCR.BIT.T3AEN = 0 ;				// TGIA3割込間引き(禁止)
	MTU.TITCR.BIT.T3ACOR = MTU_INT_SKIP ;	// TGIA3間引き回数

	// 割込みを許可
	MTU4.TIER.BIT.TCIEV = 1 ;				// TCIV(OF/UF)割込許可->TCIV4
	IPR( MTU4, TCIV4 ) = MTU_PWM_INT_LVL ;	// TCIV4 Interrupt Level(macro)
	IEN( MTU4, TCIV4 ) = 1 ;				// TCIV4  IEN=1 (macro)

// 13.タイマカウント開始
	bReg = MTU.TSTR.BYTE ;
	nop( ) ;
	bReg |= 0xC0 ;
	MTU.TSTR.BYTE = bReg ;						// MTU3,4 start
	nop( ) ;

	// 誤書き込み防止 -> 使用しない 端子出力制御出来なくなる
//	MTU.TRWER.BIT.RWE = 0 ;					// MTU3,4 lock
	
}


//-----------------------------------------------------------------------
// 電圧監視１割り込み/リセット設定
// 割り込み先で無限ループに入る設定
// CPUの動作電圧が2.7〜5.5V
// 今回は電圧降下時3.70Vにて動作する仕様とする
//-----------------------------------------------------------------------
void	Inz_LVD1( void )
{

	WORD	wLoop ;
		
	SYSTEM.PRCR.WORD = 0xa508 ;				// LVD変更操作を可へ
	
	// 1.検出電圧設定
	// LVD2LVL変更前にLVDnEを一旦無効にする必要がある
	SYSTEM.LVCMPCR.BIT.LVD1E = 0 ;			// 電源検出1:無効
	SYSTEM.LVCMPCR.BIT.LVD2E = 0 ;			// 電源検出2:無効
	SYSTEM.LVDLVLR.BIT.LVD1LVL = 0x03 ;		// 検出電圧3.70V

	// 2.コンパレータA電源設定
	SYSTEM.LVCMPCR.BIT.EXVREFINP1 = 0 ;		// Vref=内部基準電圧
	SYSTEM.LVCMPCR.BIT.EXVCCINP1 = 0 ;		// Vcc=電源電圧
	SYSTEM.LVCMPCR.BIT.LVD1E = 1 ;			// 電源検出1:有効
	// ※LVD1E有効にした後に待ち時間が必要(15us) 要検討	
	wLoop = 1500 ;
	while( wLoop-- ) { nop( ) ; }
	nop( ) ;
	
	// 3.デジタルフィルタ設定
	SYSTEM.LVD1CR0.BIT.LVD1DFDIS = 0 ;		// フィルタ無効
	
	// 4.動作モード設定
	SYSTEM.LVD1CR0.BIT.LVD1RI = 0 ;			// Vdet1通過時割込
	SYSTEM.LVD1CR0.BIT.LVD1RN = 0 ;			// 検出後ネゲート
	
	// 5.-----
	// 6.割込/リセットを許可
	SYSTEM.LVD1CR0.BIT.LVD1RIE = 1 ;		// 許可
	
	SYSTEM.PRCR.WORD = 0xa500 ;				// 操作不可へ
	
	// 割込許可,割込レベル設定
	IEN( LVD, LVD1 ) = 1 ;					// LVD1 Enable (macro)
	IPR( LVD, LVD1 ) = LVD1_INT_LVL ;		// LVD1 Interrupt Level (macro)
	
	
}


//-----------------------------------------------------------
// ポートアウトプットイネーブルセットアップ
// POE2#端子(PORTA.6)に立下りエッジの入力があった場合に
// POEの作動でMTUOC3B,D 4A,C 4B,Dがハイインピーダンスになる
//
// ※POE2にDown Edgeが入るとPOE2Fレジスタに"1"が立つ
//	 その際に割り込みが発生するので、DFへの記憶を行う
//	 POEを解除するにはPOE2Fに"0"を書き込む必要がある
//-----------------------------------------------------------
void	Inz_POE( void)
{
	//POEの設定
	SYSTEM.PRCR.WORD = 0xa502 ;				// レジスタの保護を解除
	POE.ICSR1.BIT.POE2M = 0 ;				// POE2の立下りでPOE発動
	IEN( POE , OEI1 ) = 1 ;					// POE2Fに1が立った後にOEI1割込み
//	POE.ICSR1.BIT.PIE1 = 1;					// POE2Fが1になった後の割り込み許可
//	POE.OCSR1.WORD= 0x0000 ;				// 出力レベルでは監視しない
	
	SYSTEM.PRCR.WORD = 0xa500 ;				// レジスタの保護
}

//-----------------------------------------------------------
//	エンコーダ用割込み入力セットアップ
//	ENC0: U=IRQ6, V=IRQ5, W=IRQ4(Vector:70,69,68)
//-----------------------------------------------------------
void	Inz_Enc( void )
{
	
	
	// 1.割込禁止
	IEN( ICU, IRQ4 ) = 0 ;					// IRQ4  Disable (macro)
	IEN( ICU, IRQ5 ) = 0 ;					// IRQ5  Disable (macro)
	IEN( ICU, IRQ6 ) = 0 ;					// IRQ6  Disable (macro)

	// 2.端子機能制御レジスタの設定
	//	PmnPFSへの書き込みを許可する
    MPC.PWPR.BIT.B0WI  = 0 ;				// PFSWE書込許可
    MPC.PWPR.BIT.PFSWE = 1 ;				// PFSreg書込許可
	PI_ENCU_PIN_ISEL = 1 ;					// P14をIRQ4 EncU
	PI_ENCV_PIN_ISEL = 1 ;					// P15をIRQ5 EncV
	PI_ENCW_PIN_ISEL = 1 ;					// P16をIRQ6 EncV
	//	PmnPFSへの書き込みを禁止する
    MPC.PWPR.BIT.PFSWE = 0 ;				// PFSreg書込禁止
    MPC.PWPR.BIT.B0WI  = 1 ;				// PFSWE書込禁止

	// 検出方法設定
	PI_ENCU_IRQ_EDGE = 0x03 ;				// IRQ4  両エッジ EncU
	PI_ENCV_IRQ_EDGE = 0x03 ;				// IRQ5  両エッジ EncV
	PI_ENCW_IRQ_EDGE = 0x03 ;				// IRQ6  両エッジ EncW

	// 割り込みレベルの設定
	IPR( ICU, IRQ4 ) = IRQ_ENC_INT_LVL ;	// IRQ4 Interrupt Level=1 (macro)
	IPR( ICU, IRQ5 ) = IRQ_ENC_INT_LVL ;	// IRQ5 Interrupt Level=1 (macro)
	IPR( ICU, IRQ6 ) = IRQ_ENC_INT_LVL ;	// IRQ6 Interrupt Level=1 (macro)
//	IPR( ICU, IRQ4 ) = 0 ;					// IRQ4  Interrupt Level=0 (macro)
//	IPR( ICU, IRQ5 ) = 0 ;					// IRQ5  Interrupt Level=0 (macro)
//	IPR( ICU, IRQ6 ) = 0 ;					// IRQ6  Interrupt Level=0 (macro)

	// 割込みを許可
	IEN( ICU, IRQ4 ) = 1 ;					// IRQ4  IEN=1 (macro)
	IEN( ICU, IRQ5 ) = 1 ;					// IRQ5  IEN=1 (macro)
	IEN( ICU, IRQ6 ) = 1 ;					// IRQ6  IEN=1 (macro)

/*
	// ポートを入力モードへ
	PI_ENCU_PDR = PORT_PDR_IN ;				// P14 PDR  EncU
	PI_ENCV_PDR = PORT_PDR_IN ;				// P15 PDR  EncV
	PI_ENCW_PDR = PORT_PDR_IN ;				// P16 PDR  EncW
	// 割り込み要求レジスタをクリア
	IR( ICU, IRQ4 ) = 0 ;					// IRQ4  IR=0 (macro)
	IR( ICU, IRQ5 ) = 0 ;					// IRQ5  IR=0 (macro)
	IR( ICU, IRQ6 ) = 0 ;					// IRQ6  IR=0 (macro)
	IPR( ICU, IRQ4 ) = IRQ_ENC_INT_LVL ;	// IRQ4 Interrupt Level=1 (macro)
	IPR( ICU, IRQ5 ) = IRQ_ENC_INT_LVL ;	// IRQ5 Interrupt Level=1 (macro)
	IPR( ICU, IRQ6 ) = IRQ_ENC_INT_LVL ;	// IRQ6 Interrupt Level=1 (macro)
*/	
}
/*
//-----------------------------------------------------------
//	端子機能選択設定
//-----------------------------------------------------------
void	Inz_PinFunc( void )
{
	
	
	//--------------------------------------
	// PmnPFS設定
	//--------------------------------------
	// PmnPFSへの書き込みを許可する
    MPC.PWPR.BIT.B0WI  = 0 ;							// PFSWE書込許可
    MPC.PWPR.BIT.PFSWE = 1 ;							// PFSreg書込許可
	
	nop( ) ;
	
	
	// PmnPFSへの書き込みを禁止する
    MPC.PWPR.BIT.PFSWE = 0 ;							// PFSreg書込禁止
    MPC.PWPR.BIT.B0WI  = 1 ;							// PFSWE書込禁止
	
	
	//--------------------------------------
	// PMR設定
	//--------------------------------------
	nop( ) ;
	
	
	
	
}

*/


//-----------------------------------------------------------------------
//	PSW プロセッサステータスワード設定
//	プロセッサの割り込み設定をする
//-----------------------------------------------------------------------
void	Inz_PSW( void )
{
	
	// プロセッサステータスワード(PSW)設定
	// コンパイラの組み込み関数
	// I:割込許可  bit16
	// IPL:プロセッサ割り込み優先レベル 0=最低
	set_psw( 0x00010000 );					// Set I=1, IPL=0 of PSW
	
}


//---------------------------------------------------
//	ソフトリセット実行
//	引数
//	戻り値
//---------------------------------------------------
void	xSoftReset( void )
{
	
	// ソフトリセットを掛ける
	SYSTEM.PRCR.WORD = 0xa502 ;				// ソフトリセット関係を書き込み可へ
	SYSTEM.SWRR = 0xa501 ;					// ソフトリセットReg
	SYSTEM.PRCR.WORD = 0xa500 ;				// 書き込み不可へ
	

}



//---------------------------------------------------
//	データのCRC演算値をセット
//	演算はITU-T準拠のx16+x12+x5+1
//	引数
//		UBYTE *pubData			データPtr
//		UWORD uwSize			演算バイト数
//	戻り値
//		CRC演算結果
//---------------------------------------------------
UWORD	xMakeCRC( UBYTE *pubData, UWORD uwSize )
{
		
	// 演算セットアップ
	CRC.CRCCR.BIT.DORCLR = 1 ;				// CRCDORクリア
	CRC.CRCDOR = 0xffff ;					// 初期値
	
	// 指示バイト数分CRC演算
	for ( ; uwSize-- > 0 ; ) {
		CRC.CRCDIR = *pubData++  ;			// 演算データセット
	}
	
	return CRC.CRCDOR ;
	
}

//----------------------------------------------------
//  電源遮断   制御戻らない
//	オートパワーオフ時に使用する予定
//	現状では未実装
//----------------------------------------------------
void	xPowerDown( void )
{
	if ( gDbg.FLG.fDbg01 != TRUE ) {
		xDF_ParmWriteExe( ) ;						// データフラッシュ書込
	}
	
	// MTUを停止
	SYSTEM.PRCR.WORD = 0xa502 ;				// 動作モード変更操作を可へ
	MSTP( MTU ) = 1 ;						// Down MTU3 (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 操作不可へ
	
	// CMT0を停止
	SYSTEM.PRCR.WORD = 0xa502 ;				// 動作モード変更操作を可へ
	MSTP( CMT0 ) = 1 ;						// Down CMT0 (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 操作不可へ
	
	// CMT1を停止
	SYSTEM.PRCR.WORD = 0xa502 ;				// 動作モード変更操作を可へ
	MSTP( CMT1 ) = 1 ;						// Down CMT1 (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 操作不可へ
	
	// CMT2を停止
	SYSTEM.PRCR.WORD = 0xa502 ;				// 動作モード変更操作を可へ
	MSTP( CMT2 ) = 1 ;						// Down CMT2 (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 操作不可へ
	
	
	// AD12制御部を停止
	SYSTEM.PRCR.WORD = 0xa502 ;				// 動作モード変更操作を可へ
	MSTP( S12AD ) = 1 ;						// Down AD12ctl (macro)
	SYSTEM.PRCR.WORD = 0xa500 ;				// 操作不可へ
	
	// 割込停止
	IEN( ICU, IRQ6 ) = 0 ;					// IRQ6  Disable (macro)
	IEN( ICU, IRQ4 ) = 0 ;					// IRQ4  Disable (macro)
	IEN( ICU, IRQ5 ) = 0 ;					// IRQ5  Disable (macro)
	
	// 念のためブレーキをかける
	BRAKE_PODR = BRAKE_LOCK ;				// Brake Lock
	if ( gDbg.FLG.fDbg01 != TRUE ) {
		switch ( gSysInf.fPwrOff ){
			case AUTO_POWER_OFF :
				Sci_PutsText( UART_CH_DBG, "AUTO_POWER_OFF\n" ) ;
				break ;
			case ERR_SHUT_DOWN :
				Sci_PutsText( UART_CH_DBG, "ERR_SHUT_DOWN\n" ) ;
				break ;
			case IWDT_SHUT_DOWN :
				Sci_PutsText( UART_CH_DBG, "IWDT_SHUT_DOWN\n" ) ;
				break ;
			default :
				break ;
		}
		Sci_PutsText( UART_CH_DBG, "thank you ! see you ;)\n" ) ;

		gDbg.FLG.fDbg01 = TRUE ;
	}

	// 電源遮断
	PWR_ON_PODR = POWER_OFF ;			// 電源保持
	
}
//----------------------------------------------------
//  接点入力デバイス状態取得（押釦SW）
//----------------------------------------------------
void	xSwDevStat( void )
{
	
	//生情報gInpstatをgInpStatGetに置き換える
	gInpStatGet = gInpStat ;				// 入力取り込み(フィルタ処理済み)
	
//

	// 1. デバイス入力(Level)情報セット
	// 正論理  1:SW-ON(SWDEV_LVL_ON), 0:SW-OFF(SWDEV_LVL_OFF)
	//ポート情報をデバイス情報として認識
	gSwDev.Lvl.ubI01 = gInpStatGet.ubI01 ;		// Inp01 SW1	
	gSwDev.Lvl.ubI02 = gInpStatGet.ubI02 ;		// Inp02 SW2
	gSwDev.Lvl.ubI03 = gInpStatGet.ubI03 ;		// Inp03 SW3
	gSwDev.Lvl.ubI04 = gInpStatGet.ubI04 ;		// Inp04 SW4
	gSwDev.Lvl.ubI05 = gInpStatGet.ubI05 ;		// Inp05 SW5

/*	//外部入力用
	gSwDev.Lvl.ubIR01 = gInpStatGet.ubIR01 ;	// Inpr01 
	gSwDev.Lvl.ubIR02 = gInpStatGet.ubIR02 ;	// Inpr02 
	gSwDev.Lvl.ubIR03 = gInpStatGet.ubIR03 ;	// Inpr03 
*/
/*元データ
	gSwDev.Lvl.ubI05 = gInpStatGet.ubI05 ;		// Inp05 SW5
	gSwDev.Lvl.ubI06 = gInpStatGet.ubI06 ;		// Inp06 SW6
*/
	// 2. デバイス入力(Edge)情報セット
	//前回の値(Prv)と今回の値の反転値を比較
	//UP EDGEとDOWN EDGEとHIGH LEVELとLOW LEVELの4つの状態があるため
	//エッジを見る場合はif文を作る際はifとelse ifで条件を打ち込む必要がある
	//ubI01のDownEdgeを検出
	if ( gInpStatPrv.ubI01 && !gInpStatGet.ubI01 ) {	// DownEdge Inp01
		gSwDev.Edge.ubI01 = SWDEV_EDGE_DOWN ;			// 1->0
		gSwDev.Long.ubI01 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI01 = 0 ;						// 長押timer
	//ubI01のUpEdgeを検出
	} else if ( !gInpStatPrv.ubI01 && gInpStatGet.ubI01 ) {// UpEdge
		gSwDev.Edge.ubI01 = SWDEV_EDGE_UP ;				// 0->1
		gSwDev.Long.ubI01 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI01 = 0 ;						// 長押timer
	//ubI01のEdgeは検出されず
	} else {
		gSwDev.Edge.ubI01 = SWDEV_EDGE_NONE ;			// *NoChange
	//EDGEの変化がないときはtimer counterは上昇し続ける
	}
	
	if ( gInpStatPrv.ubI02 && !gInpStatGet.ubI02 ) {	// DownEdge Inp02
		gSwDev.Edge.ubI02 = SWDEV_EDGE_DOWN ;			// 1->0
		gSwDev.Long.ubI02 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI02 = 0 ;						// 長押timer
	} else if ( !gInpStatPrv.ubI02 && gInpStatGet.ubI02 ) {// UpEdge
		gSwDev.Edge.ubI02 = SWDEV_EDGE_UP ;				// 0->1
		gSwDev.Long.ubI02 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI02 = 0 ;						// 長押timer
	} else {
		gSwDev.Edge.ubI02 = SWDEV_EDGE_NONE ;			// *NoChange
	}
	
	if ( gInpStatPrv.ubI03 && !gInpStatGet.ubI03 ) {	// DownEdge Inp03
		gSwDev.Edge.ubI03 = SWDEV_EDGE_DOWN ;			// 1->0
		gSwDev.Long.ubI03 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI03 = 0 ;						// 長押timer
	} else if ( !gInpStatPrv.ubI03 && gInpStatGet.ubI03 ) {// UpEdge
		gSwDev.Edge.ubI03 = SWDEV_EDGE_UP ;				// 0->1
		gSwDev.Long.ubI03 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI03 = 0 ;						// 長押timer
	} else {
		gSwDev.Edge.ubI03 = SWDEV_EDGE_NONE ;			// *NoChange
	}
	
	if ( gInpStatPrv.ubI04 && !gInpStatGet.ubI04 ) {	// DownEdge Inp04
		gSwDev.Edge.ubI04 = SWDEV_EDGE_DOWN ;			// 1->0
		gSwDev.Long.ubI04 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI04 = 0 ;						// 長押timer
	} else if ( !gInpStatPrv.ubI04 && gInpStatGet.ubI04 ) {// UpEdge
		gSwDev.Edge.ubI04 = SWDEV_EDGE_UP ;				// 0->1
		gSwDev.Long.ubI04 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI04 = 0 ;						// 長押timer
	} else {
		gSwDev.Edge.ubI04 = SWDEV_EDGE_NONE ;			// *NoChange
	}
	
	if ( gInpStatPrv.ubI05 && !gInpStatGet.ubI05 ) {	// DownEdge Inp05
		gSwDev.Edge.ubI05 = SWDEV_EDGE_DOWN ;			// 1->0
		gSwDev.Long.ubI05 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI05 = 0 ;						// 長押timer
	} else if ( !gInpStatPrv.ubI05 && gInpStatGet.ubI05 ) {// UpEdge
		gSwDev.Edge.ubI05 = SWDEV_EDGE_UP ;				// 0->1
		gSwDev.Long.ubI05 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI05 = 0 ;						// 長押timer
	} else {
		gSwDev.Edge.ubI05 = SWDEV_EDGE_NONE ;			// *NoChange
	}

//外部入力用
/*	if ( gInpStatPrv.ubIR01 && !gInpStatGet.ubIR01 ) {	// DownEdge Inpr01
		gSwDev.Edge.ubIR01 = SWDEV_EDGE_DOWN ;			// 1->0
		gSwDev.Long.ubIR01 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwIR01 = 0 ;						// 長押timer
	} else if ( !gInpStatPrv.ubIR01 && gInpStatGet.ubIR01 ) {// UpEdge
		gSwDev.Edge.ubIR01 = SWDEV_EDGE_UP ;				// 0->1
		gSwDev.Long.ubIR01 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwIR01 = 0 ;						// 長押timer
	} else {
		gSwDev.Edge.ubIR01 = SWDEV_EDGE_NONE ;			// *NoChange
	}
	
	if ( gInpStatPrv.ubIR02 && !gInpStatGet.ubIR02 ) {	// DownEdge Inpr02
		gSwDev.Edge.ubIR02 = SWDEV_EDGE_DOWN ;			// 1->0
		gSwDev.Long.ubIR02 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwIR02 = 0 ;						// 長押timer
	} else if ( !gInpStatPrv.ubIR02 && gInpStatGet.ubIR02 ) {// UpEdge
		gSwDev.Edge.ubIR02 = SWDEV_EDGE_UP ;				// 0->1
		gSwDev.Long.ubIR02 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwIR02 = 0 ;						// 長押timer
	} else {
		gSwDev.Edge.ubIR02 = SWDEV_EDGE_NONE ;			// *NoChange
	}
	if ( gInpStatPrv.ubIR03 && !gInpStatGet.ubIR03 ) {	// DownEdge Inpr03
		gSwDev.Edge.ubIR03 = SWDEV_EDGE_DOWN ;			// 1->0
		gSwDev.Long.ubIR03 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwIR03 = 0 ;						// 長押timer
	} else if ( !gInpStatPrv.ubIR03 && gInpStatGet.ubIR03 ) {// UpEdge
		gSwDev.Edge.ubIR03 = SWDEV_EDGE_UP ;				// 0->1
		gSwDev.Long.ubIR03 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwIR03 = 0 ;						// 長押timer
	} else {
		gSwDev.Edge.ubIR03 = SWDEV_EDGE_NONE ;			// *NoChange
	}*/
/*元データ	
	if ( gInpStatPrv.ubI05 && !gInpStatGet.ubI05 ) {	// DownEdge Inp05
		gSwDev.Edge.ubI05 = SWDEV_EDGE_DOWN ;			// 1->0
		gSwDev.Long.ubI05 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI05 = 0 ;						// 長押timer
	} else if ( !gInpStatPrv.ubI05 && gInpStatGet.ubI05 ) {// UpEdge
		gSwDev.Edge.ubI05 = SWDEV_EDGE_UP ;				// 0->1
		gSwDev.Long.ubI05 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI05 = 0 ;						// 長押timer
	} else {
		gSwDev.Edge.ubI05 = SWDEV_EDGE_NONE ;			// *NoChange
	}
	
	if ( gInpStatPrv.ubI06 && !gInpStatGet.ubI06 ) {	// DownEdge Inp06
		gSwDev.Edge.ubI06 = SWDEV_EDGE_DOWN ;			// 1->0
		gSwDev.Long.ubI06 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI06 = 0 ;						// 長押timer
	} else if ( !gInpStatPrv.ubI06 && gInpStatGet.ubI06 ) {// UpEdge
		gSwDev.Edge.ubI06 = SWDEV_EDGE_UP ;				// 0->1
		gSwDev.Long.ubI06 = SWDEV_LPUSH_NO ;			// 長押reset
		gSwDev.Timer.uwI06 = 0 ;						// 長押timer
	} else {
		gSwDev.Edge.ubI06 = SWDEV_EDGE_NONE ;			// *NoChange
	}
*/	
	
	
	// 3. SW長押し(LongPush)情報セット
	//gSwDev.Lvl.ubI01:スイッチのレベルがHighでgSwDev.Timer.uwI01が400以上で
	//長押しと判断をする。
	if (gSwDev.Lvl.ubI01 && (gSwDev.Timer.uwI01 >= SWDEV_LPUSH_TICK)) {	// Inp01
		//gSwDEv.Long.UbI01長押し確認変数にYES情報を代入
		gSwDev.Long.ubI01 = SWDEV_LPUSH_YES ;			// 長押
		//Timer値が飽和しないように都度基準値を代入していく
		gSwDev.Timer.uwI01 = SWDEV_LPUSH_TICK ;			// Inp01 UP
	}
	if (gSwDev.Lvl.ubI02 && (gSwDev.Timer.uwI02 >= SWDEV_LPUSH_TICK)) {	// Inp02
		gSwDev.Long.ubI02 = SWDEV_LPUSH_YES ;			// 長押
		gSwDev.Timer.uwI02 = SWDEV_LPUSH_TICK ;			// Inp02 DOWN
	}
	if (gSwDev.Lvl.ubI03 && (gSwDev.Timer.uwI03 >= SWDEV_LPUSH_TICK)) {	// Inp03
		gSwDev.Long.ubI03 = SWDEV_LPUSH_YES ;			// 長押
		gSwDev.Timer.uwI03 = SWDEV_LPUSH_TICK ;			// Inp03 STOP
	}
	if (gSwDev.Lvl.ubI04 && (gSwDev.Timer.uwI04 >= SWDEV_LPUSH_TICK)) {	// Inp04
		gSwDev.Long.ubI04 = SWDEV_LPUSH_YES ;			// 長押
		gSwDev.Timer.uwI04 = SWDEV_LPUSH_TICK ;			// Inp04 EMM
	}
	if (gSwDev.Lvl.ubI05 && (gSwDev.Timer.uwI05 >= SWDEV_LPUSH_TICK)) {	// Inp05
		gSwDev.Long.ubI05 = SWDEV_LPUSH_YES ;			// 長押
		gSwDev.Timer.uwI05 = SWDEV_LPUSH_TICK ;			// Inp05 LowerLS(B)
	}
/*	if (gSwDev.Lvl.ubI06 && (gSwDev.Timer.uwI06 >= SWDEV_LPUSH_TICK)) {	// Inp06
		gSwDev.Long.ubI06 = SWDEV_LPUSH_YES ;			// 長押
		gSwDev.Timer.uwI06 = SWDEV_LPUSH_TICK ;			// Inp6 
	}
	if (gSwDev.Lvl.ubI07 && (gSwDev.Timer.uwI07 >= SWDEV_LPUSH_TICK)) {	// Inp06
		gSwDev.Long.ubI07 = SWDEV_LPUSH_YES ;			// 長押
		gSwDev.Timer.uwI07 = SWDEV_LPUSH_TICK ;			// Inp07
	}
	if (gSwDev.Lvl.ubI08 && (gSwDev.Timer.uwI07 >= SWDEV_LPUSH_TICK)) {	// Inp06
		gSwDev.Long.ubI08 = SWDEV_LPUSH_YES ;			// 長押
		gSwDev.Timer.uwI08 = SWDEV_LPUSH_TICK ;			// Inp08
	}
*/	//現在値Getを過去値Prvに代入する
	gInpStatPrv = gInpStatGet ;				// 入力保存(エッジ処理の為)
	
}

//-----------------------------------------------------------
//	入力ポートをフィルタ処理して取得 (チャタリング対策)
//	各入力データをビットシフトして貯めていって全て一致
//	したら値を確定する
//	インターバルタイマからコールされる(1msec毎)
//  入力から5回信号を左シフトしながら見ていく
//  一定時間ON(1f：11111:INP_FCHK_SW_VAL)になった時点で入力を判定する。
//  検出頻度はCMT0のマイコンペアマッチ毎
//-----------------------------------------------------------
static void	stSwDevGet( void )
{
	UBYTE	ubChkWork ;			// ON/OFF判定ワーク


	// 1. ビットシフト して現在のポート状態を加える -> 入力ポートフィルタ
	//													(INP_FLD)
	// IN01-IN06
	// 入力:ONで0,OFFで1なのでbitを反転させる
	//gInpFld.I01にubData(UBYTE:符号無8bitの領域が確保される
	gInpFld.I01.ubData <<= 1 ;					// Inp01
	//ビットシフトしたI01に論理和が与えられる
	gInpFld.I01.ubData |= !INP01_PIDR ;			// Inp01 SW1
	gInpFld.I02.ubData <<= 1 ;					// Inp02
	gInpFld.I02.ubData |= !INP02_PIDR ;			// Inp02 SW2
	gInpFld.I03.ubData <<= 1 ;					// Inp03
	gInpFld.I03.ubData |= !INP03_PIDR ;			// Inp03 SW3
	gInpFld.I04.ubData <<= 1 ;					// Inp04
	gInpFld.I04.ubData |= !INP04_PIDR ;			// Inp04 SW4
	gInpFld.I05.ubData <<= 1 ;					// Inp05
	gInpFld.I05.ubData |= !INP05_PIDR ;			// Inp05 SW5
// 外部入力用		10/6追加
/*	gInpFld.IR01.ubData <<= 1 ;					// Inp05
	gInpFld.IR01.ubData |= !INPR01_PIDR ;		// Inp05 SW5
	gInpFld.IR02.ubData <<= 1 ;					// Inp06
	gInpFld.IR02.ubData |= !INPR02_PIDR ;		// Inp06 SW6
	gInpFld.IR03.ubData <<= 1 ;					// Inp05
	gInpFld.IR03.ubData |= !INPR03_PIDR ;		// Inp05 SW5
*/
/*元データ
	gInpFld.I05.ubData <<= 1 ;					// Inp05
	gInpFld.I05.ubData |= !INP05_PIDR ;			// Inp05 SW5
	gInpFld.I06.ubData <<= 1 ;					// Inp06
	gInpFld.I06.ubData |= !INP06_PIDR ;			// Inp06 SW6
*/	
	// 2. ON/OFF判定 & 判定結果代入 -> 入力ポート情報(INP_INFO)
	//ビットフィールドを下位5bitにしてマスク処理をかけた値を判定ワークとする
	ubChkWork = gInpFld.I01.ubChk5 & INP_FCHK_SW_VAL ;	// Inp01 SW1
	//00000になった段階でOFF判定、デバイス情報にOFFを代入
	if ( !ubChkWork ) {								// offした
		gInpStat.ubI01 = EXTINP_OFF ;
	//11111になった状態でON判定、デバイス情報にONを代入
	} else if ( ubChkWork == INP_FCHK_SW_VAL ) {	// onした
		gInpStat.ubI01 = EXTINP_ON ;
	}
	ubChkWork = gInpFld.I02.ubChk5 & INP_FCHK_SW_VAL ;	// Inp02 SW2
	if ( !ubChkWork ) {								// offした
		gInpStat.ubI02 = EXTINP_OFF ;
	} else if ( ubChkWork == INP_FCHK_SW_VAL ) {	// onした
		gInpStat.ubI02 = EXTINP_ON ;
	}
	ubChkWork = gInpFld.I03.ubChk5 & INP_FCHK_SW_VAL ;	// Inp03 SW3
	if ( !ubChkWork ) {								// offした
		gInpStat.ubI03 = EXTINP_OFF ;
	} else if ( ubChkWork == INP_FCHK_SW_VAL ) {	// onした
		gInpStat.ubI03 = EXTINP_ON ;
	}
	ubChkWork = gInpFld.I04.ubChk5 & INP_FCHK_SW_VAL ;	// Inp04 SW4
	if ( !ubChkWork ) {								// offした
		gInpStat.ubI04 = EXTINP_OFF ;
	} else if ( ubChkWork == INP_FCHK_SW_VAL ) {	// onした
		gInpStat.ubI04 = EXTINP_ON ;
	}
	ubChkWork = gInpFld.I05.ubChk5 & INP_FCHK_SW_VAL ;	// Inp05 SW5
	if ( !ubChkWork ) {								// offした
		gInpStat.ubI05 = EXTINP_OFF ;
	} else if ( ubChkWork == INP_FCHK_SW_VAL ) {	// onした
		gInpStat.ubI05 = EXTINP_ON ;
	}

//外部入力用	10/6追加
/*		ubChkWork = gInpFld.IR01.ubChk5 & INP_FCHK_SW_VAL ;	// Inpr01 SW5
	if ( !ubChkWork ) {								// offした
		gInpStat.ubIR01 = EXTINP_OFF ;
	} else if ( ubChkWork == INP_FCHK_SW_VAL ) {	// onした
		gInpStat.ubIR01 = EXTINP_ON ;
	}
	ubChkWork = gInpFld.IR02.ubChk5 & INP_FCHK_SW_VAL ;	// Inpr02 SW6
	if ( !ubChkWork ) {								// offした
		gInpStat.ubIR02 = EXTINP_OFF ;
	} else if ( ubChkWork == INP_FCHK_SW_VAL ) {	// onした
		gInpStat.ubIR02 = EXTINP_ON ;
	}	
	ubChkWork = gInpFld.IR03.ubChk5 & INP_FCHK_SW_VAL ;	// Inrp03 SW5
	if ( !ubChkWork ) {								// offした
		gInpStat.ubIR03 = EXTINP_OFF ;
	} else if ( ubChkWork == INP_FCHK_SW_VAL ) {	// onした
		gInpStat.ubIR03 = EXTINP_ON ;
	}
*/	
/*元データ	
	ubChkWork = gInpFld.I05.ubChk5 & INP_FCHK_SW_VAL ;	// Inp05 SW5
	if ( !ubChkWork ) {								// offした
		gInpStat.ubI05 = EXTINP_OFF ;
	} else if ( ubChkWork == INP_FCHK_SW_VAL ) {	// onした
		gInpStat.ubI05 = EXTINP_ON ;
	}
	ubChkWork = gInpFld.I06.ubChk5 & INP_FCHK_SW_VAL ;	// Inp06 SW6
	if ( !ubChkWork ) {								// offした
		gInpStat.ubI06 = EXTINP_OFF ;
	} else if ( ubChkWork == INP_FCHK_SW_VAL ) {	// onした
		gInpStat.ubI06 = EXTINP_ON ;
	}
*/

	
}

// ---------------------------------------------------
// 割り算処理
//	引数	UWORD 割られる値(分子)	UWORD 割る値(分母)
//	戻り値	UWORD 結果(商)
// ---------------------------------------------------
UWORD xDivide( UWORD uwdata , UWORD uwdiv )
{
	UWORD uwans ;						// 結果(商)
	UWORD uwdammy ;						// 割られた値
	UWORD uwsyou ;						// 各ループ内での結果(商)
	uwans = 0;

// 分子が分母より小さくなるまで割り算を繰り返して答えを得る
 	while( uwdata >= uwdiv ){
    	uwdammy = uwdiv ;
    	uwsyou = 1;
	// ビットシフトで割り切れる分だけ繰り返す
    	while( uwdata >= uwdammy ){   	// 割られる数を超えるまで割る数をシフト
        	uwdammy = uwdammy << 1;		// 割られた値
        	uwsyou  = uwsyou  << 1;		// 割った値
    	}
    uwdammy = uwdammy >> 1;             // 行き過ぎた分1bitシフトして戻す
    uwsyou = uwsyou >> 1;
    // 割り切れていない分を求めて再計算
    uwdata -= uwdammy;					// 割り切れていない分
    uwans += uwsyou;					// 割った回数を積算(これが答え)
 	}
 	return uwans ;						// 結果を返す
}

//---------------------------------------------------
//	Enc情報リセット
//	引数
//		ENC_INF *pEnc		エンコーダ構造体ptr
//	戻り値
//---------------------------------------------------
void	xEnc_ResetInf(  volatile ENC_INF *pEnc )
{
//	BYTE		bIdx ;								// AllayIdx
	
	pEnc->Inp.ubUVW = 0 ;								// Enc UVWリセット
	pEnc->Inp.BIT.U = !PI_ENCU_IN ;						// ENC0 U相 port
	pEnc->Inp.BIT.V = !PI_ENCV_IN ;						// ENC0 V相 port
	pEnc->Inp.BIT.W = !PI_ENCW_IN ;						// ENC0 W相 port

	pEnc->InpPrv = pEnc->Inp ;							// 前回のEnc入力値
	pEnc->wDirPrv = pEnc->wDir ;						// 前回の回転方向
	pEnc->wDir = ENC_DIR_STOP ;							// 回転方向
	pEnc->wPulseChk = ENC_RES_SLOW ;					// パルス数確認
	pEnc->wPulseCount = ENC_RES_SLOW ;					// カウンタリセット
	pEnc->wOvf = 0 ;									// OverFlow数
	pEnc->dwExtTimer = 0 ;								// 拡張TimerCount
	pEnc->wSpeed = 0 ;									// 速度 (0.1mS)
	pEnc->wVrev = 0 ;									// 仮想回転数(rpm)
//	pEnc->wSpeedE = 0 ;									// 電気角速度(0.1mS)
//	pEnc->wVrevE = 0 ;									// 電気角回転速度(E-rpm)
	pEnc->wVrevPrv = 0 ;								// 前回回転速度(rpm)
	pEnc->wVrevDiff = 0 ;								// 回転速度差(rpm)
//	pEnc->wVrevE_Abs = 0 ;								// 電気角絶対回転速度
	pEnc->wAccStatCount = 0 ;							// 加減速判定カウンタ
	pEnc->wAccStat = FBV_ACDC_STOP ;					// 加減速ステータス
//	pEnc->Rot.udwHsOvf = 0 ;							// H/S-Ovfカウント
//	pEnc->Rot.udwHsNow = 0 ;							// H/S-Timeカウント
//	pEnc->Rot.udwHsAvg = 0 ;							// H/S-Time平均
//	pEnc->Rot.udwHsAvgPrv = 0 ;							// H/S-Time平均(前回)
//	pEnc->Rot.udwHsSum = 0 ;							// H/S-Time合計
//	pEnc->Rot.uwHistPtrNow = 0 ;						// uwHist[]のPtr(今)
//	pEnc->Rot.uwHistPtr = 0 ;							// uwHist[]のPtr(次)
//	pEnc->Rot.fHistFull = ENC_HIST_FULL_NO ;			// uwHist[]充填Flag
//	for ( bIdx = 0 ; bIdx < ENC_RES_NORMAL ; bIdx++ ) {
//		pEnc->Rot.udwHist[bIdx] = 0 ;					// H/S-TCNT Hist.
//	}

}

/*
//---------------------------------------------------
//	Enc60度の平均時間を計算
//	引数
//		ENC_INF *pEnc		エンコーダ構造体ptr
//---------------------------------------------------
static void	xEnc_Calc60( volatile ENC_INF *pEnc )
{
	UBYTE	ubHistPtr	;								// uwHist[]のPtr
	
	// 電気角360度分の合計からEnc60度分の移動平均値を求める
	pEnc->Rot.udwHsSum = pEnc->Rot.udwHsSum - pEnc->Rot.udwHist[ubHistPtr] + 									pEnc->Rot.udwHsNow ;		// H/S Time合計
	pEnc->Rot.udwHist[ubHistPtr] = pEnc->Rot.udwHsNow ;	// H/S Time Hist
	pEnc->Rot.udwHsAvg = pEnc->Rot.udwHsSum / ENC_RES_NORMAL ;	// H/S Time平均
	ubHistPtr++ ;										// uwHist[]のPtr
	if ( ubHistPtr >= ENC_RES_NORMAL ) {				// uwHist[]のPtr
		ubHistPtr = 0 ;									// uwHist[]のPtr
	}

}

*/
//-----------------------------------------------------------------------
// MTM BL90Mでは8P12S->電気角360°でモータ軸1/4rev(機械角90°)
// よって 電気角60°=1/24rev
//-----------------------------------------------------------------------

void	Int_EncCore( void )
{
	ENC_INF			*pEnc ;							// エンコーダ
	ADC_MOT			*pMot ;							// モータ情報
	WORD			wDir ;							// 回転方向
	DWORD			dwExtTimer ;					// 拡張TimerCount gEncとは別
	WORD			wAcDcRate ;						// 加減速率

	pEnc = &gEnc ;									// Enc情報 Ptr
	pMot = &gAdc.Mot ;								// 電流情報情報 Ptr
/*
	// H/S-Timeカウント	ホールセンサカウント 
	// MTU1はホールセンサ60度あたりの時間計測に使用
	MTU.TSTR.BIT.CST1 = 0 ;							// MTU1 stop
	// 実際の時間=MTU1の現在地+60度に達するまでにMTUがオーバーフローした回数
	pEnc->Rot.udwHsNow = (UDWORD)MTU1.TCNT +
							pEnc->Rot.udwHsOvf ;	// H/S-Timeカウント
	pEnc->Rot.udwHsOvf = 0 ;						// H/S-Ovfカウント
	MTU1.TCNT = 0 ;									// Clear Count
	MTU.TSTR.BIT.CST1 = 1 ;							// MTU1 start
	xEnc_Calc60( pEnc ) ;							// Enc60度の平均時間計算

	// 1度割込のコンペア値セット
	// 注:MTU1とMTU0は同じカウントソース設定になっている
	// HS60度あたりの平均時間を60で割り、1度の時間を求めている
	pEnc->Rot.uwTC_1deg = pEnc->Rot.udwHsAvg / 60 ;	// TGRAコンペア値
	if ( pEnc->Rot.uwTC_1deg ) {
		//MTU0.TGRCとMUTU0.TCNTが一致した時にA/D変換開始要求ができる
		MTU0.TGRC = pEnc->Rot.uwTC_1deg ;			// Int.Count
	}
*/	
	// 入力取り込み
	//gEnc.inp.BITの各層にエンコーダの接続されているポートの状態を代入
		pEnc->Inp.BIT.U = !PI_ENCU_IN ;				// ENC U相 port
		pEnc->Inp.BIT.V = !PI_ENCV_IN ;				// ENC V相 port
		pEnc->Inp.BIT.W = !PI_ENCW_IN ;				// ENC W相 port

	if ( pEnc->Inp.ubUVW != pEnc->InpPrv.ubUVW ) {	// 相変化有り
		// 相変化で回転方向を判定
		if ( (pEnc->InpPrv.BIT.U == 0) && 
			 (pEnc->Inp.BIT.U == 1) ) {				// U up
			if ( pEnc->Inp.BIT.V == 1 ) {			// CCW
				pEnc->wDir = ENC_DIR_CCW ;			// CCW回転
			} else {								// CW
				pEnc->wDir = ENC_DIR_CW ;			// CW回転
			}
		} else if ( (pEnc->InpPrv.BIT.V == 0) && 
					(pEnc->Inp.BIT.V == 1) ) {		// V up
			if ( pEnc->Inp.BIT.W == 1 ) {			// CCW
				pEnc->wDir = ENC_DIR_CCW ;			// CCW回転
			} else {								// CW
				pEnc->wDir = ENC_DIR_CW ;			// CW回転
			}
		} else if ( (pEnc->InpPrv.BIT.W == 0) && 
					(pEnc->Inp.BIT.W == 1) ) {		// W up
			if ( pEnc->Inp.BIT.U == 1 ) {			// CCW
				pEnc->wDir = ENC_DIR_CCW ;			// CCW回転
			} else {								// CW
				pEnc->wDir = ENC_DIR_CW ;			// CW回転
			}
		} else if ( (pEnc->InpPrv.BIT.U == 1) && 
					(pEnc->Inp.BIT.U == 0) ) {		// U down
			if ( pEnc->Inp.BIT.V == 0 ) {			// CCW
				pEnc->wDir = ENC_DIR_CCW ;			// CCW回転
			} else {								// CW
				pEnc->wDir = ENC_DIR_CW ;			// CW回転
			}
		} else if ( (pEnc->InpPrv.BIT.V == 1) && 
					(pEnc->Inp.BIT.V == 0) ) {		// V down
			if ( pEnc->Inp.BIT.W == 0 ) {			// CCW
				pEnc->wDir = ENC_DIR_CCW ;			// CCW回転
			} else {								// CW
				pEnc->wDir = ENC_DIR_CW ;			// CW回転
			}
		} else if ( (pEnc->InpPrv.BIT.W == 1) && 
					(pEnc->Inp.BIT.W == 0) ) {		// W down
			if ( pEnc->Inp.BIT.U == 0 ) {			// CCW
				pEnc->wDir = ENC_DIR_CCW ;			// CCW回転
			} else {								// CW
				pEnc->wDir = ENC_DIR_CW ;			// CW回転
			}
		}

		// 位置更新 正方向なら+1,逆方向なら-1
//		gFb.Q.dwPosTo += pEnc->wDir ;				// 位置(enc) FB
		gEnc.dwPos += pEnc->wDir ;					// 位置(enc) FB
		gDf.POS.dwPos += pEnc->wDir ;				// 位置(enc) FB
		
		// 回転数が一定以上になったら上昇端(上昇不可)
//		if( gFb.Q.dwPosTo >= ENC_UP_LMT ){
//		if( gEnc.dwPos >= gDf.POS.dwUpLimit ){
		if( gDf.POS.dwPos >= gDf.POS.dwUpLimit ){
			gEnc.fUpLimit = UP_LIMIT ;				// 上昇不可
		}else{
			gEnc.fUpLimit = UP_FREE ;				// 上昇可
		}
			

		// 方向反転したら情報リセット(速度をゼロ状態へ)
		// 前回値が停止ではない、かつ回転方向が前回と異なる場合
		if ( (pEnc->wDirPrv != ENC_DIR_STOP) && 
			 (pEnc->wDir != pEnc->wDirPrv) ) {		// 方向反転
			Sci_PutsText( UART_CH_DBG, "Enc DirCHNG\n" ) ;	
			wDir = pEnc->wDir ;							// 回転方向
		// 逆回転になったらEncの各項目をリセットする
			xEnc_ResetInf( pEnc ) ;					// Enc.Inf Reset
		// 回転方向の情報だけは保持する
			pEnc->wDir = wDir ;						// 回転方向
		// 方向が同じなら速度計算
		} else {									// 方向維持

		// 速度カウント計測数をデクリメントしていき、0になったら速度計測
			if ( !(--pEnc->wPulseChk) ) {			// 指定電気角回転
				// タイマ値取得
				//pEnc->dwExtTimerにCMT2.CNTの値を1bitずつorしていく
				//※dwExtTimerはローカル変数でpEnc->dwExtTimerとは別物
				//現段階では1割込時間分の値しか算出できていない
				CMT.CMSTR1.BIT.STR2 = 0 ;		// Stop CMT2
				dwExtTimer = pEnc->dwExtTimer | CMT2.CMCNT ;// 拡張TmrCount
				CMT2.CMCNT = 0 ;
				CMT.CMSTR1.BIT.STR2 = 1 ;		// Start CMT2
				pEnc->wOvf = 0 ;				// OverFlow数
				pEnc->dwExtTimer = 0 ;			// 拡張TimerCount
// 高所作業者からのデータdoがよくわからない
/*				dwExtTimer = pEnc->dwExtTimer | CMT2.CMCNT ;// 拡張TmrCount
				do {							// タイマクリア
					CMT2.CMCNT = 0 ;
				} while ( CMT2.CMCNT > 100 ) ;
				pEnc->wOvf = 0 ;				// OverFlow数
				pEnc->dwExtTimer = 0 ;			// 拡張TimerCount
*/
		// 電気角360度分の時間換算をする
		// ここで求めるのはローカル変数のdwExtTimerの値
		// 上記で求めた1割込の数値に計測カウントとENC計測パルス数を含めて再計算
		// wPulseCountの値は現在の計測ステータスと同じ
				dwExtTimer = ((dwExtTimer * 10 * ENC_RES_NORMAL) + 
								5) / (pEnc->wPulseCount * 10) ;// 拡張TimerCount
				// Enc速度(0.1mS) 計算
				// 1周期=uwTcnt*ENC_REV_RATIO*1/195*10 (0.1mS/unit)
				//       uwTcnt:電気角360度分の所要時間カウント
				//       ENC_REV_RATIO:機械角/電気角比(3)
				//       1/195:1カウント時間(mS/unit) タイマの設定でPCLOK/128
				//		 25MHz/128=195312.5->195KHz
				// wSpeed :回転速度(0.1ms)
				// ENC_COUNT_FREQ:Encカウント周波数(kHz)->定数 375
//				pEnc->wSpeedE = pEnc->wDir * (WORD)((
//					 dwExtTimer * 10) / ENC_COUNT_FREQ ) ;	// 電気角速度(0.1mS)
				//ENC_REV_RATIO_4:機械角/電気角比 ※変更があるので注意
				pEnc->wSpeed = pEnc->wDir * (WORD)(((DWORD)ENC_REV_RATIO_4 *
					 dwExtTimer * 10) / ENC_COUNT_FREQ ) ;	// 速度(0.1mS)
//				pEnc->wSpeed = pEnc->wDir * 
//					(WORD)(((DWORD)gDf.SYS.wEncRevRatio *
//					 dwExtTimer * 10) / ENC_COUNT_FREQ ) ;	// 速度(0.1mS)
				// Enc 回転速度(rev/min) 計算
				if ( pEnc->wSpeed ) {						// 値有り
					pEnc->wVrev = (WORD)((DWORD)600000 / 
									(DWORD)pEnc->wSpeed) ;	// 速度(rev/min)
//					pEnc->wVrevE = (WORD)((DWORD)600000 / 
//									(DWORD)pEnc->wSpeedE) ;	// 速度(e-rev/min)
//					if ( pEnc->wVrevE >= 0 ) {
//						pEnc->wVrevE_Abs = pEnc->wVrevE ;	// 絶対速度 Erev/min
//					} else {
//						pEnc->wVrevE_Abs = -(pEnc->wVrevE) ;// 絶対速度 Erev/min
//					}
					if ( pEnc->wVrev >= 0 ) {
						pEnc->wVrev_Abs = pEnc->wVrev * 4 ;	// 絶対速度 rev/min
					} else {
						pEnc->wVrev_Abs = -(pEnc->wVrev*4);// 絶対速度 rev/min
					}
				} else {									// 値無し
					pEnc->wVrev = 0 ;						// 速度(rev/min)
					pEnc->wVrev_Abs = 0 ;					// 絶対速度 rev/min
//					pEnc->wVrevE = 0 ;						// 速度(E-rev/min)
//					pEnc->wVrevE_Abs = 0 ;					// 絶対速度 Erev/min
				}
				
				//数回に1度加減速判定を行う
				if ( ++pEnc->wAccStatCount > pEnc->wPulseChk ) { // 加減速判定
//				if ( ++pEnc->wAccStatCount > 6 ) { // 加減速判定 元データ
					pEnc->wAccStatCount = 0 ;				// 加減速判定Cnt.
					pEnc->wVrevDiff = 
						pEnc->wVrev - pEnc->wVrevPrv ;		// 回転速度差(rpm)
					wAcDcRate = (pEnc->wVrevDiff * 100) / 
							pEnc->wVrev ;					// 加減速率
					if ( wAcDcRate > 2 ) {					// 加速
						pEnc->wAccStat = FBV_ACDC_ACC ;		// 加減速ステータス
//						Sci_PutsText( UART_CH_DBG, "ACC\n" ) ;
					} else if ( wAcDcRate < -2 ) {			// 減速
						pEnc->wAccStat = FBV_ACDC_DEC ;		// 加減速ステータス
//						Sci_PutsText( UART_CH_DBG, "DEC\n" ) ;
					} else {								// 等速
						pEnc->wAccStat = FBV_ACDC_CV ;		// 加減速ステータス
//						Sci_PutsText( UART_CH_DBG, "CV\n" ) ;
					}
					pEnc->wVrevPrv = pEnc->wVrev ;			// 前回回転速度(rpm)
				}

				// 次の速度計測パルス数を電気角速度に応じてセット
				// 電気角速度が高速範囲外であったら
/*				if ( (pEnc->wVrevE <= -ENC_CALC_HI_E) || 
					 (pEnc->wVrevE >= ENC_CALC_HI_E) ) {
					pEnc->wPulseChk = ENC_RES_NORMAL ;		// パルス数確認
					pEnc->wPulseCount = ENC_RES_NORMAL ;	// 計測パルス数
				} else if ( (pEnc->wVrevE <= -ENC_CALC_MID_E) || 
							(pEnc->wVrevE >= ENC_CALC_MID_E) ) {
					pEnc->wPulseChk = ENC_RES_MID ;			// パルス数確認
					pEnc->wPulseCount = ENC_RES_MID ;		// 計測パルス数
				} else {
					pEnc->wPulseChk = ENC_RES_SLOW ;		// パルス数確認
					pEnc->wPulseCount = ENC_RES_SLOW ;		// 計測パルス数
				}
*/			
				if ( (pEnc->wVrev <= -ENC_CALC_HI) || 
					 (pEnc->wVrev >= ENC_CALC_HI) ) {
					pEnc->wPulseChk = ENC_RES_NORMAL ;		// パルス数確認
					pEnc->wPulseCount = ENC_RES_NORMAL ;	// 計測パルス数
				} else if ( (pEnc->wVrev <= -ENC_CALC_MID) || 
							(pEnc->wVrev >= ENC_CALC_MID) ) {
					pEnc->wPulseChk = ENC_RES_MID ;			// パルス数確認
					pEnc->wPulseCount = ENC_RES_MID ;		// 計測パルス数
				} else {
					pEnc->wPulseChk = ENC_RES_SLOW ;		// パルス数確認
					pEnc->wPulseCount = ENC_RES_SLOW ;		// 計測パルス数
				}
			
			}
		}
		
		// モータ電流AD値計算処理
		// 電気角360度分で平均化する 
		pMot->ubEncCount++ ;							// Mot. Enc. Pulse Count
		if ( (++pMot->ubEncCount >= 6) &&
//			 (pEnc->wVrevE_Abs > 20) ) {					// 計測Timing
			 (pEnc->wVrev_Abs > 20) ) {					// 計測Timing
			pMot->dwCur = pMot->dwSum / pMot->uwSumCount ;
//			pMot->dwCur -= pMot->uwOfs ;					// UVWcur.
			pMot->dwSum = 0 ;							// UVW sum
			pMot->uwSumCount = 0 ;						// UVW sum count
			pMot->ubEncCount = 0 ;						// Mot. Enc. Pulse Count
		}

		// オフセット計測タイマをリセット
		pMot->uwOfsTime = 0 ;						// Mot. cr. OffsetTimer
		// AD処理用エンコーダ相変化フラグをセット
		pMot->fEncChg = TRUE ;							// Enc.PhaseChange
				
	}					// 相変化有り
	nop( ) ;

	// 前回値保存
		pEnc->wDirPrv = pEnc->wDir ;					// Enc 回転方向(前回)
		pEnc->InpPrv.ubUVW = pEnc->Inp.ubUVW ;			// Enc UVW前回値
//		pEnc->wVrevE_Prv = pEnc->wVrevE ;				// 速度(e-rev/min)(前回)

}



//-----------------------------------------------------------------------
//	モータ励磁
//	引数
//-----------------------------------------------------------------------
void	Int_MotExite( void )
{
	ENC_INF			*pEnc ;									// エンコーダptr
//	UBYTE			ubUVW ;									// Enc UVW State
	MTU_PWM			*pPwm ;									// PWM Ptr
	UWORD			uwDutyU ;								// U相Duty
	UWORD			uwDutyV ;								// V相Duty
	UWORD			uwDutyW ;								// W相Duty
	BYTE			bReg ;

	
	// 各相のDuty値計算
	pEnc = &gEnc ;											// Enc情報 Ptr
	pPwm = &gPwm ;											// PWM Ptr
	// エンコーダの現在値をwDirに代入する
	pPwm->wDirU = gMph[pEnc->Inp.ubUVW].wIu ;				// U相極性
	pPwm->wDirV = gMph[pEnc->Inp.ubUVW].wIv ;				// V相極性
	pPwm->wDirW = gMph[pEnc->Inp.ubUVW].wIw ;				// W相極性
	nop( ) ;
	// 実際のDuty=現在のエンコーダ情報×指示Duty比+Duty50%時の値
	// gPwm.uwPwmZero = (gPwm.uwMax + gPwm.uwMin) / 2 ;		// Duty50%値
	uwDutyU = (UWORD)(pPwm->wDirU * pPwm->wDutyExe + 
				(WORD)gPwm.uwPwmZero) ;						// Duty-U
	uwDutyV = (UWORD)(pPwm->wDirV * pPwm->wDutyExe + 
				(WORD)gPwm.uwPwmZero) ;						// Duty-V
	uwDutyW = (UWORD)(pPwm->wDirW * pPwm->wDutyExe + 
				(WORD)gPwm.uwPwmZero) ;						// Duty-W
	pPwm->uwDutyU = uwDutyU ;								// U duty
	pPwm->uwDutyV = uwDutyV ;								// V duty
	pPwm->uwDutyW = uwDutyW ;								// W duty



#define	_TOER_LIMIT_	1				// TOER使用制限対策 0:なし,1:あり
	
	// 励磁相の切り替えとDutyセット
	#if	_TOER_LIMIT_ == 1				// TOER使用制限対策 0:なし,1:あり
//		MTU.TSTR.BIT.CST3 = 0 ;								// MTU3 stop
//		MTU.TSTR.BIT.CST4 = 0 ;								// MTU4 stop
		bReg = MTU.TSTR.BYTE ;
		nop( ) ;
		bReg &= 0x3f ;										// MTU3,4 mask
		MTU.TSTR.BYTE = bReg ;								// MTU3,4 stop
	#endif	// TOER使用制限対策
	// U相出力指示
	//Dir?が0に場合(無回転)は各相のタイマアウトプットマスタ許可レジスタ
	//								(出力許可レジスタ)をOFFにする	
	if ( !pPwm->wDirU ) {									// Off
		PO_UH = EXTOUT_OFF ;								// MTIOC3B出力(UH0)
		PO_UL = EXTOUT_OFF ;								// MTIOC3D出力(UL0)
	} else {												// On
	//Dir?が0でない場合(回転)は各相のタイマアウトプットマスタ許可レジスタ
	//								(出力許可レジスタ)をONにする	
		PO_UH = EXTOUT_ON ;									// MTIOC3B出力(UH0)
		PO_UL = EXTOUT_ON ;									// MTIOC3D出力(UL0)
	}
	// V相出力指示
	if ( !pPwm->wDirV ) {									// Off
		PO_VH = EXTOUT_OFF ;								// MTIOC4A出力(VH0)
		PO_VL = EXTOUT_OFF ;								// MTIOC4C出力(VL0)
	} else {												// On
		PO_VH = EXTOUT_ON ;									// MTIOC4A出力(VH0)
		PO_VL = EXTOUT_ON ;									// MTIOC4C出力(VL0)
	}
	// W相出力指示
	if ( !pPwm->wDirW ) {									// Off
		PO_WH = EXTOUT_OFF ;								// MTIOC4B出力(WH0)
		PO_WL = EXTOUT_OFF ;								// MTIOC4D出力(WL0)
	} else {												// On
		PO_WH = EXTOUT_ON ;									// MTIOC4B出力(WH0)
		PO_WL = EXTOUT_ON ;									// MTIOC4D出力(WL0)
	}
	#if	_TOER_LIMIT_ == 1						// TOER使用制限対策 0:なし,1:あり
//		MTU.TCSYSTR.BYTE = 0x18 ;							// MTU3,4 restart
		MTU.TSYR.BYTE = 0xC0	;							// MTU3,4 同期
//		MTU.TSTR.BIT.CST3 = 1 ;								// MTU3 restart
//		MTU.TSTR.BIT.CST4 = 1 ;								// MTU4 restart
		bReg = MTU.TSTR.BYTE ;
		nop( ) ;
		bReg |= 0xC0 ;
		MTU.TSTR.BYTE = bReg ;								// MTU3,4 start
		nop( ) ;
	#endif	// TOER使用制限対策
	nop( ) ;
	//上記で計算したuwDutyを各相のバッファレジスタに格納する。
	//コンペアマッチが発生するとバッファレジスタより
	//TGR(タイマジェネラルレジスタ)にデータが転送される
	DUTY_UB = uwDutyU ;										// U duty
	DUTY_VB = uwDutyV ;										// V duty
	DUTY_WB = uwDutyW ;										// W duty
	nop( ) ;
	
}
//-----------------------------------------------------------------------
//	サーボロック
//	引数
//-----------------------------------------------------------------------
void	Int_ServoLock( void )
{
/*
	MTU_PWM			*pPwm ;									// PWM Ptr
	BYTE			bReg ;

	
	// 各相のDuty値計算
	pPwm = &gPwm ;											// PWM Ptr

	// エンコーダの状況によらずDutyを50%にて固定
	pPwm->uwDutyU = gPwm.uwPwmZero ;						// U duty
	pPwm->uwDutyV = gPwm.uwPwmZero ;						// V duty
	pPwm->uwDutyW = gPwm.uwPwmZero ;						// W duty


#define	_TOER_LIMIT_	1				// TOER使用制限対策 0:なし,1:あり
	
	// 励磁相の切り替えとDutyセット
	#if	_TOER_LIMIT_ == 1				// TOER使用制限対策 0:なし,1:あり
//		MTU.TSTR.BIT.CST3 = 0 ;								// MTU3 stop
//		MTU.TSTR.BIT.CST4 = 0 ;								// MTU4 stop
		bReg = MTU.TSTR.BYTE ;
		nop( ) ;
		bReg &= 0x3f ;										// MTU3,4 mask
		MTU.TSTR.BYTE = bReg ;								// MTU3,4 stop
	#endif	// TOER使用制限対策

	// エンコーダの状況によらず、全FETに出力許可
		PO_UH = EXTOUT_ON ;									// MTIOC3B出力(UH0)
		PO_UL = EXTOUT_ON ;									// MTIOC3D出力(UL0)
		PO_VH = EXTOUT_ON ;									// MTIOC4A出力(VH0)
		PO_VL = EXTOUT_ON ;									// MTIOC4C出力(VL0)
		PO_WH = EXTOUT_ON ;									// MTIOC4B出力(WH0)
		PO_WL = EXTOUT_ON ;									// MTIOC4D出力(WL0)

	#if	_TOER_LIMIT_ == 1						// TOER使用制限対策 0:なし,1:あり
//		MTU.TCSYSTR.BYTE = 0x18 ;							// MTU3,4 restart
		MTU.TSYR.BYTE = 0xC0	;							// MTU3,4 同期
//		MTU.TSTR.BIT.CST3 = 1 ;								// MTU3 restart
//		MTU.TSTR.BIT.CST4 = 1 ;								// MTU4 restart
		bReg = MTU.TSTR.BYTE ;
		nop( ) ;
		bReg |= 0xC0 ;
		MTU.TSTR.BYTE = bReg ;								// MTU3,4 start
		nop( ) ;
	#endif	// TOER使用制限対策
	nop( ) ;
	//上記で計算したuwDutyを各相のバッファレジスタに格納する。
	//コンペアマッチが発生するとバッファレジスタより
	//TGR(タイマジェネラルレジスタ)にデータが転送される
	DUTY_UB = gPwm.uwPwmZero ;								// U duty
	DUTY_VB = gPwm.uwPwmZero ;								// V duty
	DUTY_WB = gPwm.uwPwmZero ;								// W duty
	nop( ) ;
	

*/

}
//---------------------------------------------------
//	走行用電流フィードバック処理
//	入力:pFb->wPwm
//	出力:gPwm.wDutyExe (pPwm->wDutyExe)
//	引数
//		FB			*pFb		該当軸のFB構造体ptr
//		ENC_INF		*pEnc		エンコーダ構造体ptr
//	戻り値
//---------------------------------------------------
void	xFeedback_Pwm( 	FB *pFb, ENC_INF *pEnc )
{
	MTU_PWM			*pPwm ;					// PWM値
	WORD			wDuty ;
	
	//---------------------------------------
	// Ptrセットアップ
	//---------------------------------------
	pPwm = &gPwm ;							// PWM値Ptr
	
	//---------------------------------------
	// 操作量(PWM値)計算
	//---------------------------------------
		wDuty = pFb->V.wPwm ;
	
	//---------------------------------------
	// 操作量範囲オーバー時の制限,PWM設定値計算
	//---------------------------------------
	if ( wDuty > gPwm.wRngMax ) {			// Max Limit
		wDuty = gPwm.wRngMax ;				// 実PWM指示値
	} else if ( wDuty < -gPwm.wRngMax ) {	// Min Limit
		wDuty = -gPwm.wRngMax ;				// 実PWM指示値
	}
	
	//---------------------------------------
	// PWM値反映
	//---------------------------------------
	pPwm->wDutyExe = wDuty ;				// Duty
	
}

//---------------------------------------------------
//	AD変換値を読んで各相の電流,電圧値をセット	
//	引数
//	戻り値
// PMSM モータの単一シャントセンサレスベクトル制御のP8を参照
// 参考ソースコード：motorcontrol.cのvoid MC_readc_SS()
//---------------------------------------------------
static void	Int_ReadAdc( void )
{
	ADC_MOT					*pMot ;						// モータ AD情報
	MTU_PWM					*pPwm ;						// PWM Ptr
	WORD					wIa ;						// A相実電流値(ADC)
	WORD					wIb ;						// B相実電流値(ADC)
	FLOAT					flIa ;						// A相実電流値(A)
	FLOAT					flIb ;						// B相実電流値(A)
	
	pMot = &gAdc.Mot ;
	pPwm = &gPwm ;

	// ADC値->実電流変換 UwAddeA:Iaad	uwAddrB:Ibad
	wIa = (pMot->uwAddrA - pMot->uwOfs) ;				// A相実電流値(ADC)
	wIb = (pMot->uwOfs - pMot->uwAddrB) ;				// B相実電流値(ADC)
	flIa = (ADC12_I_CVT_RATE * (FLOAT)wIa) / 10 ;		// A相実電流値(A)
	flIb = (ADC12_I_CVT_RATE * (FLOAT)wIb) / 10 ;		// B相実電流値(A)
	
	// uvw電流値に分解
	switch( pPwm->CrSS ) {
		case CSS_IUMIW :								// iu,-iw (du>=dv>=dw)
			pMot->flIu = flIa ;							// U相実電流値(A)
			pMot->flIv = -flIa -flIb ;					// V相実電流値(A)
			pMot->flIw = flIb ;							// W相実電流値(A)
			pMot->wIu = wIa ;							// U相実電流値(ADC)
			pMot->wIv = -wIa -wIb ;						// V相実電流値(ADC)
			pMot->wIw = wIb ;							// W相実電流値(ADC)
			break ;
		case CSS_IUMIV :								// iu,-iv (du>=dw>dv)
			pMot->flIu = flIa ;							// U相実電流値(A)
			pMot->flIv = flIb ;							// V相実電流値(A)
			pMot->flIw = -flIa -flIb ;					// W相実電流値(A)
			pMot->wIu = wIa ;							// U相実電流値(ADC)
			pMot->wIv = wIb ;							// V相実電流値(ADC)
			pMot->wIw = -wIa -wIb ;						// W相実電流値(ADC)
			break ;
		case CSS_IWMIV :								// iw,-iv (dw>du>dv)
			pMot->flIu = -flIa -flIb ;					// U相実電流値(A)
			pMot->flIv = flIb ;							// V相実電流値(A)
			pMot->flIw = flIa ;							// W相実電流値(A)
			pMot->wIu = -wIa -wIb ;						// U相実電流値(ADC)
			pMot->wIv = wIb ;							// V相実電流値(ADC)
			pMot->wIw = wIa ;							// W相実電流値(ADC)
			break ;
		case CSS_IVMIW :								// iv,-iw (dv>du>=dw)
			pMot->flIu = -flIa -flIb ;					// U相実電流値(A)
			pMot->flIv = flIa ;							// V相実電流値(A)
			pMot->flIw = flIb ;							// W相実電流値(A)
			pMot->wIu = -wIa -wIb ;						// U相実電流値(ADC)
			pMot->wIv = wIa ;							// V相実電流値(ADC)
			pMot->wIw = wIb ;							// W相実電流値(ADC)
			break ;
		case CSS_IVMIU :								// iv,-iu (dv>=dw>du)
			pMot->flIu = flIb ;							// U相実電流値(A)
			pMot->flIv = flIa ;							// V相実電流値(A)
			pMot->flIw = -flIa -flIb ;					// W相実電流値(A)
			pMot->wIu = wIb ;							// U相実電流値(ADC)
			pMot->wIv = wIa ;							// V相実電流値(ADC)
			pMot->wIw = -wIa -wIb ;						// W相実電流値(ADC)
			break ;
		case CSS_IWMIU :								// iw,-iu (dw>dv>du)
			pMot->flIu = flIb ;							// U相実電流値(A)
			pMot->flIv = -flIa -flIb ;					// V相実電流値(A)
			pMot->flIw = flIa ;							// W相実電流値(A)
			pMot->wIu = wIb ;							// U相実電流値(ADC)
			pMot->wIv = -wIa -wIb ;						// V相実電流値(ADC)
			pMot->wIw = wIa ;							// W相実電流値(ADC)
			break ;
		case CSS_NOTHING :								// no conversion
		default :
			pPwm->CrSS = CSS_NOTHING ;					// 計測しない
			break ;
	}
	

}


//---------------------------------------------------
//	AD変換変換開始タイミングセット		
//	TCNTとのコンペアマッチで変換起動させる
//	引数
//	戻り値
// 参考ソースコード：motorcontrol.cのMC_Set_SS()
//---------------------------------------------------
static void	Int_SetAdStart( void )
{
	MTU_PWM					*pPwm ;						// PWM Ptr
	UWORD					uwBufA, uwBufB ;			// COBRA,COBRBの値
	UWORD					uwOffset ;					// Duty (Max-Mid)/2
	UWORD					uwDutyRef ;					// 計測基準Duty
	
	pPwm = &gPwm ;

	//DutyUがDutyVよりも大きい場合
	if ( pPwm->uwDutyU >= pPwm->uwDutyV ) {				// du >= dv
		//Dutyの関係性が du>=dv>=dwの時
		if ( pPwm->uwDutyV >= pPwm->uwDutyW ) {			// du>=dv>=dw
			uwOffset = ((DWORD)(pPwm->uwDutyU - pPwm->uwDutyV) * 6)
						/ 10 ;							//(Max-Mid)xN/10
			
			// 設定不可(Duty幅が小さすぎる)
			// ( du - dv ) or ( dv - dw )が最少Duty変換値より小さいとき
			if(((pPwm->uwDutyU - pPwm->uwDutyV) < ADC12_CVT_MIN_DUTY) ||
			   ((pPwm->uwDutyV - pPwm->uwDutyW) < ADC12_CVT_MIN_DUTY)) {
				//1シャント電流計測ステータスを計測しないに設定
				pPwm->CrSS = CSS_NOTHING ;				// 計測しない
				uwDutyRef = pPwm->uwDutyV ;				// 計測基準Duty
			// 設定可能
			} else {	//最少変換値より大きい為設定可能
			//シャントステータスをiw,-iu計測とする
				pPwm->CrSS = CSS_IWMIU ;				// +iw,-iu
				//タイマ A/D 変換開始要求周期設定バッファレジスタの設定
				//オフセットDUTYより変換開始要求周期を求めて代入
				uwBufA = pPwm->uwDutyV - 
								uwOffset + ADC12_CVT_DELAY ;	// +iw  計測
				uwBufB = pPwm->uwDutyU - 
								uwOffset + ADC12_CVT_DELAY ;	// -iu 計測
			}
			
		//Dutyの関係性が du>=dw>dvの時
		} else if ( pPwm->uwDutyU >= pPwm->uwDutyW ) {	// du>=dw>dv
			uwOffset = ((DWORD)(pPwm->uwDutyU - pPwm->uwDutyW) * 6)
						/ 10 ;							//(Max-Mid)xN/10
			// 設定不可(Duty幅が小さすぎる)
			if(((pPwm->uwDutyU - pPwm->uwDutyW) < ADC12_CVT_MIN_DUTY) ||
			   ((pPwm->uwDutyW - pPwm->uwDutyV) < ADC12_CVT_MIN_DUTY)) {
				pPwm->CrSS = CSS_NOTHING ;				// 計測しない
				uwDutyRef = pPwm->uwDutyW ;				// 計測基準Duty
			// 設定可能
			} else {
				pPwm->CrSS = CSS_IVMIU ;				// +iv,-iu
				uwBufA = pPwm->uwDutyW - 
								uwOffset + ADC12_CVT_DELAY ;	// +iv 計測
				uwBufB = pPwm->uwDutyU - 
								uwOffset + ADC12_CVT_DELAY ;	// -iu 計測
			}
			
		//Dutyの関係性が dw>du>dvの時
		} else {										// dw>du>dv
//			uwOffset = (pPwm->uwDutyW - pPwm->uwDutyU) 
//						>> 1 ;							// (Max-Mid)/2
			uwOffset = ((DWORD)(pPwm->uwDutyW - pPwm->uwDutyU) * 6)
						/ 10 ;							//(Max-Mid)xN/10
			// 設定不可(Duty幅が小さすぎる)
			if(((pPwm->uwDutyW - pPwm->uwDutyU) < ADC12_CVT_MIN_DUTY) ||
			   ((pPwm->uwDutyU - pPwm->uwDutyV) < ADC12_CVT_MIN_DUTY)) {
				pPwm->CrSS = CSS_NOTHING ;				// 計測しない
				uwDutyRef = pPwm->uwDutyU ;				// 計測基準Duty
			// 設定可能
			} else {
				pPwm->CrSS = CSS_IVMIW ;				// +iw,-iv
				uwBufA = pPwm->uwDutyU - 
								uwOffset + ADC12_CVT_DELAY ;	// +iv 計測
				uwBufB = pPwm->uwDutyW - 
								uwOffset + ADC12_CVT_DELAY ;	// -iw 計測
			}
		}
	//DutyVがDutyUよりも大きい場合
	} else {											// du < dv
		//Dutyの関係性が dv>du>=dwの時
		if ( pPwm->uwDutyU >= pPwm->uwDutyW ) {			// dv>du>=dw
			uwOffset = ((DWORD)(pPwm->uwDutyV - pPwm->uwDutyU) * 6)
						/ 10 ;							//(Max-Mid)xN/10
			// 設定不可(Duty幅が小さすぎる)
			if(((pPwm->uwDutyV - pPwm->uwDutyU) < ADC12_CVT_MIN_DUTY) ||
			   ((pPwm->uwDutyU - pPwm->uwDutyW) < ADC12_CVT_MIN_DUTY)) {
				pPwm->CrSS = CSS_NOTHING ;				// 計測しない
				uwDutyRef = pPwm->uwDutyU ;				// 計測基準Duty
			// 設定可能
			} else {
				pPwm->CrSS = CSS_IWMIV ;				// +iw,-iv
				uwBufA = pPwm->uwDutyU - 
								uwOffset + ADC12_CVT_DELAY ;	// +iw 計測
				uwBufB = pPwm->uwDutyV - 
								uwOffset + ADC12_CVT_DELAY ;	// -iv 計測
			}
		//Dutyの関係性が dv>=dw>duの時
		} else if ( pPwm->uwDutyV >= pPwm->uwDutyW ) {	// dv>=dw>du
			uwOffset = ((DWORD)(pPwm->uwDutyV - pPwm->uwDutyW) * 6)
						/ 10 ;							//(Max-Mid)xN/10
			// 設定不可(Duty幅が小さすぎる)
			if(((pPwm->uwDutyV - pPwm->uwDutyW) < ADC12_CVT_MIN_DUTY) ||
			   ((pPwm->uwDutyW - pPwm->uwDutyU) < ADC12_CVT_MIN_DUTY)) {
				pPwm->CrSS = CSS_NOTHING ;				// 計測しない
				uwDutyRef = pPwm->uwDutyW ;				// 計測基準Duty
			// 設定可能
			} else {
				pPwm->CrSS = CSS_IUMIV ;				// +iu,-iv
				uwBufA = pPwm->uwDutyW - 
								uwOffset + ADC12_CVT_DELAY ;	// +iu 計測
				uwBufB = pPwm->uwDutyV - 
								uwOffset + ADC12_CVT_DELAY ;	// -iv 計測
			}
		//Dutyの関係性が dw>dv>duの時	
		} else {										// dw>dv>du
			uwOffset = ((DWORD)(pPwm->uwDutyW - pPwm->uwDutyV) * 6)
						/ 10 ;							//(Max-Mid)xN/10
			// 設定不可(Duty幅が小さすぎる)
			if(((pPwm->uwDutyW - pPwm->uwDutyV) < ADC12_CVT_MIN_DUTY) ||
			   ((pPwm->uwDutyV - pPwm->uwDutyU) < ADC12_CVT_MIN_DUTY)) {
				pPwm->CrSS = CSS_NOTHING ;				// 計測しない
				uwDutyRef = pPwm->uwDutyV ;				// 計測基準Duty
			// 設定可能
			} else {
				pPwm->CrSS = CSS_IUMIW ;				// +iw,-iu
				uwBufA = pPwm->uwDutyV - 
								uwOffset + ADC12_CVT_DELAY ;	// +iu  計測
				uwBufB = pPwm->uwDutyW - 
								uwOffset + ADC12_CVT_DELAY ;	// -iw 計測
			}
		}
	}

	// Duty小の時はTADCOBRAで電流ピークを計測してTADCOBRBはダミー計測させる
	if ( pPwm->CrSS == CSS_NOTHING ) {					// 計測しない
//		uwBufA = uwDutyRef + ADC12_CVT_DELAY ;			// TADCOBRA
//		uwBufA = uwDutyRef + ADC12_CVT_DELAY + 40 ;		// TADCOBRA
		uwBufA = uwDutyRef + ADC12_CVT_DELAY + 60 ;		// TADCOBRA
		uwBufB = (MTU_PWM_TIME * 3 / 4) + ADC12_CVT_DELAY ;		// TADCOBRB
	}
	
	
	// AD変換開始タイミングをセットする
	// 必ず MTU4.TADCOBRA < MTU4.TADCOBRB なのでまずBを計測対象とする
	//	モータ電流				AN0 ADANS bit0(0x01)
	//	電源電圧(24V)			AN1 ADANS bit1(0x02)
	S12AD.ADANSA.BIT.ANSA0 = 1 ;						// CH:AN000
	S12AD.ADANSA.BIT.ANSA1 = 1 ;						// CH:AN001
	pPwm->uwAdCobrA = uwBufA ;							// TADCOBRA
	pPwm->uwAdCobrB = uwBufB ;							// TADCOBRB
	MTU4.TADCOBRA = pPwm->uwAdCobrA ;					// Aタイミング
	MTU4.TADCOBRB = pPwm->uwAdCobrB ;					// Bタイミング
	// MTU4.TCNTのダウンカウント時にA/D変換の開始要求を許可
	MTU4.TADCR.BIT.DT4AE = 1 ;							// TRG4AN DwnCnt
	MTU4.TADCR.BIT.DT4BE = 1 ;							// TRG4BN DwnCnt
	// MTU4.TADCORAとMTU4.TCNTのコンペアマッチ(割り込み間引き機能1)
	S12AD.ADSTRGR.BIT.TRSA = 0x06 ;						// TRG7AN(TRG4ABN_0)

	S12AD.ADCSR.BIT.ADIE = 1 ;							// AD変換終了割込有
	// TADCOBRB -> TADCOBRAの順で計測する
	pPwm->CrStat = CST_GRP_B ;							// B対象
	
}

//-------------------------------------------------------------------------
// LED点滅用プログラム
// 現在はエラー時のみであるが、通常時の点灯もあるので、
// 将来的には割り込みを利用する可能性がある
//
// MIDのLEDを点滅させている。
// 長短の点灯時間を変更、消灯時間とインターバルの時間は定数とした
//-------------------------------------------------------------------------

void Int_LED_Flash( void )
{
	
	UDWORD	udwLEDInterval ;				// LED点灯インターバル
	UDWORD	udwLEDFlashON ;					// LED点灯時間
	UDWORD	udwLEDFlashOFF ;				// LED消灯時間
	

	// 1サイクルの時間を決定
	udwLEDInterval = LED_FLASH_INTERVAL ;	// LED点灯休息時間
	udwLEDFlashON = 0 ;						// 点灯時間をクリア
	udwLEDFlashOFF = LED_FLASH_OFF ;		// 消灯時間初期値
	
	// 配列が真なら長時間点灯 偽なら短時間点灯		
	if ( gSysInf.ubLEDPat[ gSysInf.bPat ] ){
		udwLEDFlashON = LED_FALSH_LONG ;		// 点灯時間:長
	} else {
		udwLEDFlashON = LED_FALSH_SHORT ;		// 点灯時間:短
	}
	
	// 配列の中身が1以上の時
	if ( gSysInf.bPat > 0 ) {
		// カウンタが点灯時間に達するまでLEDは点灯し続ける
		if ( udwLEDFlashON >= gCMT0.T100M.udwLEDFlashON ) {
			LEMI_PODR = LED_ON ;				// LED MID ON
			gCMT0.T100M.udwLEDFlashOFF = 0 ;	// 消灯タイマをクリア
		// 点灯時間を超え、消灯カウンタの消灯時間に達するまで消灯し続ける	
		} else if ( udwLEDFlashOFF >= gCMT0.T100M.udwLEDFlashOFF ){
		LEMI_PODR = LED_OFF ; 				// LED MID OFF
		// 1サイクルが終了したら次の配列へ
		} else {	  
			gCMT0.T100M.udwLEDFlashON = 0 ;		// LED点灯用タイマ
			gCMT0.T100M.udwLEDFlashOFF = 0 ;	// LED消灯用タイマ
			gSysInf.bPat-- ;					// 次の配列へ
		}
	// 配列の中身がなくなった時にはインターバルを置いて繰り返し
	}else {
		if ( udwLEDInterval >= gCMT0.T100M.udwLEDFlashOFF ){
			LEMI_PODR = LED_OFF ; 				// LED MID OFF
		} else {
			gSysInf.bPat = LED_STAT_BIT ;
			gCMT0.T100M.udwLEDFlashON = 0 ;		// LED点灯用タイマ
		}
	}
		
}


//=======================================================================
//	これ以下は割り込みハンドラ
//	プロトタイプ宣言は_vect.hに記述されているので
//	APPヘッダに記述する必要無し
//	標準ではセクションをIntPRGにしているが特に指定しなくても
//	良いと思う。(動作上の問題無し)
//=======================================================================

//-------------------------------------------------------------------------------//以下割り込み処理
//-------------------------------------------------------------------------------
#pragma section IntPRG


//-----------------------------------------------------------------------
//	12bitAD変換器(S12AD)							※移植候補※
//	1サイクルスキャンモードで一気に変換させる
//	変換時間は約1.3μSec
//
//-----------------------------------------------------------------------
#pragma interrupt Int_AD12(vect=VECT_S12AD_S12ADI0)
void	Int_AD12( void )
{
	ADC_MOT					*pMot ;					// モータ情報
	MTU_PWM					*pPwm ;					// PWM情報
	
		pMot = &gAdc.Mot ;
		pPwm = &gPwm ;

	
		// Duty小(pPwmAxis->CrSS != CSS_NOTHING)時のCST_GRP_B計測は捨てる
		// Enc相変化直後の計測は捨てる


//*******後でオフセットを引くパターン
/*		// 監視するエンコーダの相を変化させない
		pMot->fEncChg = FALSE ;						// Enc.PhaseChange
		//計測状況がTRG4ANが対象の時
		if ( pPwm->CrStat == CST_GRP_A ) {			// TADCORA(TRG4AN)対象
			// A側のAD値をAN000とする
			pMot->uwAddrA = S12AD.ADDR0 ;			// AD値 TADCORA(TRG4AN)対象
			// dwSumに現在に測定値を加えていく
			pMot->dwSum += S12AD.ADDR0  ;	// AD値合計
			// AD値の加算回数をカウントアップする
			pMot->uwSumCount++ ;					// AD値合計カウント
		} // 計測状況がTRG4BNが対象の時
		else if ( pPwm->CrStat == CST_GRP_B ) {		// TADCORB(TRG4BN)対象
			pMot->uwAddrB = S12AD.ADDR0 ;			// AD値 TADCORB(TRG4BN)対象
			if ( pPwm->CrSS != CSS_NOTHING ) {		// 計測あり
			// dwSumに現在に測定値を加えていく
				pMot->dwSum += S12AD.ADDR0  ;		// AD値合計
				pMot->uwSumCount++ ;				// AD値合計カウント
			}
		}	//計測対象がなかった場合
		 else {										// 対象無し
			pPwm->CrStat = CST_NOTHING ;			// 対象なし
		}
*/
//*******すぐにオフセットを引くパターン
		// 監視するエンコーダの相を変化させない
		pMot->fEncChg = FALSE ;						// Enc.PhaseChange
		//計測状況がTRG4ANが対象の時
		if ( pPwm->CrStat == CST_GRP_A ) {			// TADCORA(TRG4AN)対象
			// A側のAD値をAN000とする
			pMot->uwAddrA = S12AD.ADDR0 ;			// AD値 TADCORA(TRG4AN)対象
			// dwSumに現在に測定値を加えていく
			pMot->dwSum += ( S12AD.ADDR0 - pMot->uwOfs ) ;	// AD値合計
			// AD値の加算回数をカウントアップする
			pMot->uwSumCount++ ;					// AD値合計カウント
		} // 計測状況がTRG4BNが対象の時
		else if ( pPwm->CrStat == CST_GRP_B ) {		// TADCORB(TRG4BN)対象
			pMot->uwAddrB = S12AD.ADDR0 ;			// AD値 TADCORB(TRG4BN)対象
			if ( pPwm->CrSS != CSS_NOTHING ) {		// 計測あり
			// dwSumに現在に測定値を加えていく
				pMot->dwSum += ( S12AD.ADDR0 - pMot->uwOfs )  ;		// AD値合計
				pMot->uwSumCount++ ;				// AD値合計カウント
			}
		}	//計測対象がなかった場合
		 else {										// 対象無し
			pPwm->CrStat = CST_NOTHING ;			// 対象なし
		}


	// 変換相の切り換え
	if ( pPwm->CrStat == CST_GRP_B ) {				// B対象
		pPwm->CrStat = CST_GRP_A ;					// A対象
	}


	// 電源電圧測定
	gAdc.Volt.V24.uwVal = S12AD.ADDR1 ;				// V24 ADC値
	gAdc.Volt.V24.udwSum += gAdc.Volt.V24.uwVal ;	// V24 ADC積算値
	gAdc.Volt.V24.uwCount++ ;						// V24 計測回数
	if ( gAdc.Volt.V24.uwCount >= ADC12_V24_COUNT ) {	// 規程回数計測
		gAdc.Volt.V24.uwVal = 
			gAdc.Volt.V24.udwSum >> ADC12_V24_SHIFT ;	// V24 ADC値
		gAdc.Volt.V24.uwValMath = (UWORD)(10 * gAdc.Volt.V24.uwVal) ;
		gAdc.Volt.uwV24 = (UWORD)( xDivide( gAdc.Volt.V24.uwValMath , AD_DEV ) 
															+ OFFSET_AD12 ) ;
//		gAdc.Volt.uwV24 = (UWORD)(100 * (UDWORD)gAdc.Volt.V24.uwVal *
//						ADC12_V24_CNV1 / ADC12_V24_CNV2) ;	// V24電圧(0.01V/u)
//		gAdc.Volt.uwV24 = (UWORD)(100 * (UDWORD)gAdc.Volt.V24.uwVal / 
//						ADC12_CNV) ;					// V24電圧値(0.1V/u)
		gAdc.Volt.fMeas24 = ADC_MEASURE_YES ;			// 24V計測Flag
		gAdc.Volt.V24.udwSum = 0 ;						// ADC積算値
		gAdc.Volt.V24.uwCount = 0 ;						// ADC計測回数

	}

}

//-----------------------------------------------------------------------
//	CMTU0_CMT0
//	1mSecインターバルタイマコンペアマッチ
//-----------------------------------------------------------------------
#pragma interrupt (Int_CMT0(vect=VECT_CMT0_CMI0))
void	Int_CMT0( void )
{
	
	gCMT0.uwIntFb++ ;							// Feedback
	gCMT0.uwDrvCtl++ ;							// 駆動制御用待ち
	gCMT0.uwRunDisable++ ;						// 操作禁止Timer
	gCMT0.uwScanTime++ ;						// ScanTime
	gAdc.Mot.uwOfsTime++ ;						// Mot. cr. OffsetTimer
//	gDbg.TIM.uwT1m_01++ ;						// デバッグ用
//	gDbg.TIM.uwT1m_02++ ;						// デバッグ用

/*	// 電源遮断検出
	gSysInf.bPoffInt <<= 1 ;					// 電源遮断検出
	gSysInf.bPoffInt |= POFF_INT_PORT ;			// 電源遮断検出
*/
	if ( ++gCMT0.T10M.uwCount >= 10 ) {			// 1x10=10mS
		// １秒タイマ
		switch(++gCMT0.T1S.uwCount) {
			case 50 :							// 500mS
				break;
			case 100 :							// 1000mS
				gCMT0.T1S.uwCount = 0 ;			// 1Sec計測内部カウンタ
				gCMT0.T1S.uwTime++ ;			// 1Sec時間値
				gCMT0.T1S.uwPWROFF++ ;			// オートパワーオフ用タイマ
				gCMT0.T1S.uwUpTimOut++ ;		// 上昇タイムアウト用タイマ
				gCMT0.T1S.uwDownTimOut++ ;		// 下降タイムアウト用タイマ
//				gDbg.TIM.uwT1s_01++ ;
				break;
			default:
				;
		}
		if ( ++gCMT0.T100M.uwCount >= 10 ) {	// 10x10=100mS
			gCMT0.T100M.uwCount = 0 ;			// 100mSec計測内部カウンタ
			gCMT0.T100M.uwTime++ ;				// 100mSec時間値
			gCMT0.T100M.uwBrakeDelay++ ;		// ブレーキ遅延タイマ F
			gCMT0.T100M.udwLEDFlashON ++ ;		// LED点滅用タイマ
			gCMT0.T100M.udwLEDFlashOFF ++ ;		// LED点滅用タイマ
			if ( gCMT0.T100M.uwBrakeDelay > 100 ) {	// Max制限
				gCMT0.T100M.uwBrakeDelay = 100 ;
			}
		// システム活動インジケータ
			gCMT0.T100M.uwSysAct++ ;			// システム活動インジケータ
			if ( gCMT0.T100M.uwSysAct >= 10 ) {
//				LEHI_PODR ^= LED_ON ;			// Active Indicator
				gCMT0.T100M.uwSysAct = 0 ;		// システム活動インジケータ
			}
	
		}
		
		gCMT0.T10M.uwCount = 0 ;				// 10mSec計測内部カウンタ
		gCMT0.T10M.uwTime++ ;					// 10mSec時間値
//		gDbg.TIM.uwT10m_01++ ;					// デバック用タイマ
		
		// Feedback駆動制御待ちタイマ
		gCMT0.uwDrvCtl++ ;						// 駆動制御用待ち
		
		// 停止エラー検出用タイマ
		gCMT0.T10M.uwErrMotStop ++ ;			// 停止エラー検出
		
		// キー入力タイマ
		stSwDevGet( ) ;							// INポートをフィルタ処理・取得
		gSwDev.Timer.uwI01++ ;					// Inp01 SW1
		gSwDev.Timer.uwI02++ ;					// Inp02 SW2
		gSwDev.Timer.uwI03++ ;					// Inp03 SW3
		gSwDev.Timer.uwI04++ ;					// Inp04 SW4
		gSwDev.Timer.uwI05++ ;					// Inp05 SW5
//		gSwDev.Timer.uwI06++ ;					// Inp06 SW6
//		gSwDev.Timer.uwI07++ ;					// Inp07 SW7
//		gSwDev.Timer.uwI08++ ;					// Inp08 SW8
	
		
		// タイマ、オーバーフロー防止
		if ( gCMT0.T10M.uwErrMotStop >= 60000 ) {
			gCMT0.T10M.uwErrMotStop = 0 ;
		}
		if (( gCMT0.T100M.udwLEDFlashON > 100000 ) || 
					( gCMT0.T100M.udwLEDFlashOFF > 100000)){ 
			gCMT0.T100M.udwLEDFlashON = 0 ;		// LED点滅用タイマ
			gCMT0.T100M.udwLEDFlashOFF = 0 ;	// LED点滅用タイマ
		}
		
	}
}

//-----------------------------------------------------------------------
//	CMTU0_CMT1
//	0.1mSecインターバルタイマコンペアマッチ
//-----------------------------------------------------------------------
#pragma interrupt (Int_CMT1(vect=VECT_CMT1_CMI1))
void	Int_CMT1( void )
{
//	UBYTE	*pubDat ;
	
	gCMT1.udw01ms++ ;							// count 0.1mSec
	
	
//	pubDat = (UBYTE *)0x112 ;
//	if (*pubDat == 0x0b) {
//		nop( ) ;
//		nop( ) ;
//	}
	
	
}


//-----------------------------------------------------------------------
//	電圧監視1割り込み/リセット設定
//	電圧低下検出した時に不意に再起動しないようにする
//-----------------------------------------------------------------------
#pragma interrupt (Int_LVD1(vect=VECT_LVD_LVD1))
void	Int_LVD1( void )
{

	gSysInf.fLVDErr = SYS_LVD1_ON ;				// POE STOP Flag
//	xErr_All( ) ;						// 常時監視
	Sci_PutsText( UART_CH_DBG, "Low_BAT_ERR.\n" ) ;
	while( 1 ) {
//				CN16_PODR = 1 ;

		nop( ) ;
	}
	
	
}

//-----------------------------------------------------------------------
//	CMTU1_CMT2
//	Enc0 インターバルタイマコンペアマッチ
//	エンコーダオーバーフロー処理
//-----------------------------------------------------------------------
#pragma interrupt Int_CMT2_Enc0_Ovf(vect=VECT_CMT2_CMI2)
void	Int_CMT2_Enc0_Ovf( void )
{
	ENC_INF			*pEnc ;							// エンコーダ情報
	pEnc = &gEnc ;
	
	// 計測オーバーフロー処理
//	if ( pEnc->wOvf >= 2 ) {						// OverFlowカウント有り
	if ( pEnc->wOvf >= 1 ) {						// OverFlowカウント有り
		xEnc_ResetInf( pEnc );
	} else {										// OverFlowカウント無し
		pEnc->wOvf++ ;								// OverFlow数
		pEnc->dwExtTimer += 0x00010000 ;			// 拡張TimerCount
	}

}


//------------------------------------------------------------------------
// POE2F OIE1
// ポートアウトイネーブル用割り込み
// POE2Fに1が立った際にモータドライバへの信号が停止するので
// ブレーキ及びDF記憶を行う
//------------------------------------------------------------------------

#pragma interrupt (Excep_POE_OEI1(vect=VECT_POE_OEI1))
void Int_POE_OEI1(void)
{
	Sci_PutsText( UART_CH_DBG, "POE_ON.\n" ) ;
	gSysInf.fPOEStop = SYS_POE_YES ;				// POE STOP Flag
	xFeedback_Drv_Stop( FB_STOP_POE ) ;				// POE Stop
//	gSysInf.fMotDrv = SYS_MOT_UP ;					// 駆動状況:上昇
}


//-----------------------------------------------------------------------
//	IRQ6
//	Enc-U 両エッジ
//-----------------------------------------------------------------------
#pragma interrupt Int_IRQ6_EncW(vect=VECT_ICU_IRQ6)
void	Int_IRQ6_EncW( void )
{

//	Sci_PutsText( UART_CH_DBG, "ENC W High.\n" ) ;
	Int_EncCore(  ) ;								// エンコーダ処理コア
	Int_MotExite(  ) ;								// モータ励磁
	
}


//-----------------------------------------------------------------------
//	IRQ5
//	Enc1-W 両エッジ
//-----------------------------------------------------------------------
#pragma interrupt Int_IRQ5_EncV(vect=VECT_ICU_IRQ5)
void	Int_IRQ5_EncV( void )
{
	
//	Sci_PutsText( UART_CH_DBG, "ENC V High.\n" ) ;
	Int_EncCore(  ) ;								// エンコーダ処理コア
	Int_MotExite(  ) ;								// モータ励磁
	
}

//-----------------------------------------------------------------------
//	IRQ4
//	Enc1-V 両エッジ
//-----------------------------------------------------------------------
#pragma interrupt Int_IRQ4_EncU(vect=VECT_ICU_IRQ4)
void	Int_IRQ4_EncU( void )
{

//	Sci_PutsText( UART_CH_DBG, "ENC U High.\n" ) ;
	Int_EncCore(  ) ;								// エンコーダ処理コア
	Int_MotExite(  ) ;								// モータ励磁
	
}



#pragma section

//-----------------------------------------------------------------------

/*
//-----------------------------------------------------------------------
//	MTU1 TGIV オーバーフロー割込
//	ロータ60度時間計測
//-----------------------------------------------------------------------
#pragma interrupt Int_MTU1_TCIV(vect=VECT_MTU1_TCIV1)
void	Int_MTU1_TCIV( void )
{
	
	
//	gEnc.Rot.udwHsOvf += (UDWORD)0x10000 ;// H/S-Ovfカウント
	
	
}

*/
//-----------------------------------------------------------------------
//	MTU PWM割込 MTU4.TCNTのアンダーフローで割り込み (TCIV4)		
//	A/D結果処理と次の変換タイミング設定
//-----------------------------------------------------------------------
#pragma interrupt Int_MTU4_TCIV4(vect=VECT_MTU4_TCIV4)
void	Int_MTU4_TCIV4( void )
{

	// PWM端子に出力するDuty値決定 (gPwm..wDutyExe)
	xFeedback_Pwm( &gFb,(ENC_INF *)&gEnc ) ;	// FB処理
	
	//	AD変換値を読んで各相の電流,電圧値をセット
	// この時点でTADCORA,TADCORBによる変換が終わっている
	Int_ReadAdc( ) ;								// ivw電流値決定
	Int_SetAdStart( ) ;								// 変換タイミング設定
	nop() ;


}

//-----------------------------------------------------------------------
//	MTU0 PWMコンペアマッチ割込
//	変数内容確認用 MTIOC0にPWM出力
//-----------------------------------------------------------------------
#pragma interrupt Int_MTU0_TGIC(vect=VECT_MTU0_TGIC0)
void	Int_MTU0_TGIC( void )
{
	UWORD			uwWork ;
	
	
//    MTU0.TIER.BIT.TGIEC = 0 ;					// TGIA割込要求を禁止
//	uwWork = gEnc.Rot.udwHsNow / 200 ;
//	uwWork = gEnc.Rot.udwHsAvg / 200 ;
//	uwWork = gPwm.uwDutyU / 16 ;
//	uwWork = gPwm.wDutyExe ;
//	uwWork = gDbg.TIM.uwT1m_01 ;
//	uwWork = gDbg.TIM.uwT10m_01 ;
	uwWork = 250 ;
//	uwWork = gPwm.wDutyExe ;
//	uwWork = gFb.V.wVdiff ;
	
	if ( MTU0.TGRC < uwWork ) {
		MTU0.TGRD = MTU0.TGRC ;
	} else {
		MTU0.TGRD = uwWork ;
	}
//  MTU0.TIER.BIT.TGIEC = 1 ;					// TGIA割込要求を許可
	
	
}		
// End of File 


