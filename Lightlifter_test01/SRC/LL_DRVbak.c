/***********************************************************************/
/*                                                                     */
/*  FILE        : Feedback Program                                     */
/*  DATE        : Nov 18, 2015                                    	   */
/*  DESCRIPTION : Main Program                                         */
/*  CPU TYPE    : R5F52106BxFM										   */
/*                                                                     */
/*  NOTE:THIS IS A TYPICAL EXAMPLE.                                    */
/*                                                                     */
/***********************************************************************/

#include	<machine.h>					// Include machine.h
#include	"_iodefine.h"				// Include _iodefine.h

#include	"LL.h"						// アプリケーションヘッダ
#include	"SCI_RX210.h"				// SCIヘッダ
#include	"LL_prototype.h"			// アプリケーションプロトタイプヘッダ

//---------------------------------------------------
//	フィードバック情報のリセット
//	引数
//		FB  *pFb				FB構造体ptr
//
//	戻り値
//---------------------------------------------------
void	xFB_ResetInf(  volatile FB *pFb )
{
	// 目標位置用 ※未実装		
		pFb->Q.bEnable = FALSE ;					// 有効F
	// 速度フィードバック用
		pFb->V.wOut = 0 ;							// FB-V出力
		pFb->V.wInpVal = 0 ;						// スロットル入力値
		pFb->V.bFbLock = TRUE ;						// Feedback Lock
		pFb->V.dwFbP = 0 ;							// P:比例制御値    偏差
		pFb->V.dwFbI = 0 ;							// I:誤差の積分値
		pFb->V.dwFbD = 0 ;							// D:偏差の微分値
		pFb->V.wVdiff = 0 ;							// 偏差
		pFb->V.wVdiffPrv = 0 ;						// 前回の偏差
		gFb.V.wVdiffAbs = 0 ;						// 絶対値の偏差
		pFb->V.wPwm = 0 ;							// 現在のPWM指示値
		pFb->V.wPwmLimit = 0 ;						// PWM制限値
		pFb->V.wPwmCurLimit = 0 ;					// PWM電流制限値
		pFb->V.wPwmCurAdj = 0 ;						// PWM電流補正値
		pFb->V.fZeroLock = FBV_ZERO_LOCK_NO ;		// 速度0保持ロックFlag
		pFb->V.wAccStat = FBV_ACDC_STOP ;			// 加減速ステータス
		pFb->V.wVrevFb = 0 ;						// FB処理速度(rpm)
		pFb->V.wVrev = 0 ;							// 加減速処理後処理速度
		pFb->V.wVrevPrv = 0 ;						// 加減速処理後処理速度 前回
		gFb.V.wVrevAbs = 0 ;						// 加減速処理後処理速度の偏差
	// その他＋位置FB用
		pFb->P.wVrevLimit = 0 ;						// 上限速度(rpm)
		pFb->P.wVrevInit = 0 ;						// 初期速度(rpm)
		pFb->P.wVrevNow = 0 ;						// 現在速度(rpm)
		pFb->P.bEnable = FALSE ;					// FB処理許可F
		pFb->P.bEnablePrv = FALSE ;					// FB処理許可F 前回
		pFb->P.bStopState = FB_STOP_NO ;			// 停止ステータス
		pFb->P.bStopStatePrv = 0 ;					// 停止ステータス 前回
		pFb->P.bRoterStop = FB_ROTER_STOP_YES ;		// ロータ停止Flag
//		pFb->P.dwPosTo = 0 ;						// 移動点(enc)
//		pFb->P.dwPosFrom = 0 ;						// 開始点(enc)
//		pFb->P.dwPosAccEnd = 0 ;					// 加速終了点(enc)
//		pFb->P.dwPosDecStart = 0 ;					// 減速開始点(enc)
//		pFb->P.wOut = 0 ;							// 現在の指示値(rpm)
//		pFb->P.wOutPrv = 0 ;						// 前回の指示値

}

//---------------------------------------------------
//	走行用速度フィードバック情報セットアップ処理
//	引数
//		BYTE    bStopState			停止方法
//			FB_STOP_NORMAL			減速停止する
//			FB_STOP_EMM				非常停止する
//			FB_STOP_POE				POEによる停止
//	戻り値
//---------------------------------------------------
void	xFeedback_Drv_Stop( BYTE bStopState )
{
	// 緊急停止手続き
	gFb.P.bStopState = bStopState ;					// 停止方法
	gCMT0.uwIntFb = FB_INTERVAL ;					// 強制FB

	switch(bStopState){
		case 0 :
			Sci_PutsText( UART_CH_DBG, "StpStat: NO.\n" ) ;
			break;
		case 1 :
			Sci_PutsText( UART_CH_DBG, "StpStat: NORMAL.\n" ) ;
			break;
		case 2 :
			Sci_PutsText( UART_CH_DBG, "StpStat: EMM.\n" ) ;
			break;
		case 4 :
			Sci_PutsText( UART_CH_DBG, "StpStat: POE.\n" ) ;
			break;
		default :
			break ;
	}


}


//---------------------------------------------------
//	走行用速度フィードバック情報セットアップ処理
//	引数
//		WORD    *pwInp		走行指令値(-1023〜+1023)
//							＋は前進,−値はバック
//	戻り値
//---------------------------------------------------

void	xFeedback_Drv_Setup( WORD *pwInp )
{
	WORD			wRpm ;						// 指示回転数
	
	// 非常停止中は処理しない
	if (( gSysInf.fEmm == SYS_EMM_YES ) ||			// 非常停止Flag
		( gSysInf.fPOEStop == SYS_POE_YES )) {	// POE停止Flag
		return ;
	}
	
	
	// ロックはFeedbackルーチン内の停止処理で行う
//	BRAKE_PODR = BRAKE_FREE ;					// Brake Free

	// 指示値(±1023max)->指示速度rpm変換
	// 入力速度(rpm)範囲チェック
	// 指示回転数=((走行指令値*rpm変換係数(0.1%)*10)+5)/1000
	wRpm = (((DWORD)*pwInp * gDf.DRV.wRpmCvtRatio * 10) 
					+ 5) / 1000 ;				// 駆動回転数

	// (正転)指示速度が最高回転数よりも大きかったら最高回転数に合わせる
	if ( wRpm > gDf.FBV.wRevMax ) {				// 前進 LimitOver
		wRpm = gDf.FBV.wRevMax ;				// 指示速度(rpm)
	} else if ( wRpm < -gDf.FBV.wRevMax ) {		// 後進 LimitOver
		wRpm = -gDf.FBV.wRevMax ;				// 指示速度(rpm)
	} 
	//指示速度下限よりも下回った場合は指示速度をゼロにする
	 else if ( (wRpm >=0) && 
				(wRpm < gDf.FBV.wFbvLowLimit) ) {	// 前進 LimitUnder
		wRpm = 0 ;								// 指示速度(rpm)
	} else if ( ( wRpm  <0) && 
				( wRpm > -gDf.FBV.wFbvLowLimit) ) { // 後進 LimitUnder
		wRpm = 0 ;								// 指示速度(rpm)
	}
	
	// 入力速度がゼロ(指示速度下限未満)なら停止手続き
	if (( wRpm < gDf.FBV.wFbvLowLimit ) &&
		  ( wRpm > -gDf.FBV.wFbvLowLimit )) {	// ゼロ判定
		// 停止シークエンス(通常停止)を呼び出す
		gCMT0.T10M.uwErrMotStop = 0 ;			// エラー用タイマ	
		Sci_PutsText( UART_CH_DBG, "FB_STP_NML.\n" ) ;
		xFeedback_Drv_Stop( FB_STOP_NORMAL ) ;	// NORMAL Stop
		return ;
	}


	// FB指示値(rpm)セット
	gFb.V.wInpVal = wRpm ;						// スロットル入力値

//	xPrintf( UART_CH_DBG, "wRm:%d, ",wRpm ) ; 

	
	// フィードバック開始手続き
	gCMT0.uwDrvCtl = 0 ;						// 駆動制御用待ち
	gFb.P.bEnable = TRUE ;						// FB処理許可F
	gFb.V.fZeroLock = FBV_ZERO_LOCK_NO ;		// 速度0保持ロックFlag
	gFb.V.bFbLock = FALSE ;						// Feedback Lock No
	gFb.P.bStopState = FB_STOP_NO ;				// 駆動指示
	if ( gFb.V.wInpVal ) {						// 指示有り
		BRAKE_PODR = BRAKE_FREE ;				//  Brake Free
	}

//	xPrintf( UART_CH_DBG, "gFb.V.wInpVal:%d\t\n", gFb.V.wInpVal) ;
//	xPrintf( UART_CH_DBG, "gPwm.uwPwmZero:%05d\t\n", gPwm.uwPwmZero) ;



}

//---------------------------------------------------
//	走行用速度フィードバック処理
//	引数
//		FB			*pFb		FB構造体ptr
//		ENC_INF		*pEnc		エンコーダ構造体ptr
//		ADC_MOT		*pMot		A/D変換軸固有値Ptr
//	戻り値
//---------------------------------------------------
void	xFeedback_Drv(	FB *pFb, ENC_INF *pEnc, ADC_MOT *pMot)
{
	WORD			wFbStep ;							// 加減速ステップ
	WORD			wVrevNow ;							// 現在速度
	WORD			wFbConstP, wFbConstI, wFbConstD ;	// PID係数
	WORD			wVrevAbs ;							// 実回転数絶対値
	DWORD			dwCtlBase ;							// 基本PWM値
	MTU_PWM			*pPwm ;								// PWM軸固有値


	//---------------------------------------
	// 処理実行判定
	// 停止処理
	//---------------------------------------
	// 現在FB禁止
	if ( pFb->P.bEnable != TRUE ) {						// 作動禁止
		// 前回がFB許可だった場合
		if ( pFb->P.bEnablePrv == TRUE ) {				// 停止立ち上がり
			xEnc_ResetInf( pEnc ) ;						// Reset EncInf
			// POE時はロータの回転がすでに止まっているため即座にブレーキをかける
			// 待ち時間を飛ばし、急停止を行う。
			if ( pFb->P.bStopState == FB_STOP_POE ) {
				gCMT0.uwDrvCtl = FBP_BRAKE_DELAY ;		// 駆動制御用待ち
			}else{
				gCMT0.uwDrvCtl = 0 ;					// 駆動制御用待ち
			}
			pFb->P.bRoterStop = FB_ROTER_STOP_WAIT ;	// ロータ停止待ち
		}
		pFb->P.bEnablePrv = pFb->P.bEnable ;			// FB処理許可F 前回
		// 加減速ステータスを停止中にする
		pFb->V.wAccStat = FBV_ACDC_STOP ;				// 停止中
		
		// Feedback主要データをクリアさせる
		if ( (gCMT0.uwDrvCtl >= 5) &&
			 (pFb->P.bRoterStop == FB_ROTER_STOP_WAIT) ) {	// ロータ停止待ち
			xFB_ResetInf( pFb );						// FB情報リセット
			pFb->P.bRoterStop = FB_ROTER_STOP_WAIT ;	// ロータ停止待ち
			gPwm.wDutyExe = 0 ;							// 指示Duty比をにする。
			Int_MotExite( ) ;							// モータ励磁
		}
		
		// 完全停止時の処理
		// uwDrvCtlカウンタがブレーキ遅延時間より大きく
		// かつ、Rotorstopが停止待ちの時
		if ( (gCMT0.uwDrvCtl >= FBP_BRAKE_DELAY) &&
				 (pFb->P.bRoterStop == FB_ROTER_STOP_WAIT)) { // ロータ停止待ち	
				  pFb->P.bRoterStop = FB_ROTER_STOP_YES ;     // ロータ停止中
			Int_MotExite( ) ;							// モータ励磁
			BRAKE_PODR = BRAKE_LOCK ;					// Brake Lock
		}
		return ;
	}
			
	//---------------------------------------
	// 入力速度->FB処理速度加減速処理
	//  急加減速しないようにスロープを付ける
	//  指示速度下限リミッタ以下は停止させる
	// 加速,減速フラグのセット
	//---------------------------------------

	
	// POEにて停止した時->既にロータはメカ式のブレーキでストップしている
	//					POE部分は10/21追加
	if ( pFb->P.bStopState == FB_STOP_POE ) {				// POE停止指示あり
		pFb->V.wInpVal = 0 ;								// ThrottleVal = 0
		pFb->V.wAccStat = FBV_ACDC_STOP ;					// AccStat=停止状態
		// wPOEStop:POE作動時は即座にFB処理速度に0を代入
		// 既にロータの回転は止まっているので即座に停止の信号を出して問題ない
		Sci_PutsText( UART_CH_DBG, "FB_STP_POE\n" ) ;
		pFb->V.wVrev = FB_POE_STOP ;					// FB制御値
	
	// StopstateがEMMになったとき
	}else if ( pFb->P.bStopState == FB_STOP_EMM ) {			// 非常停止指示あり
		pFb->V.wInpVal = 0 ;								// ThrottleVal = 0
		pFb->V.wAccStat = FBV_ACDC_DEC ;					// AccStat=減速状態
		// FB処理速度が0以上の時(正転)
		if ( pFb->V.wVrev > 0 ) {							// +(CW)制御中
			// wEmmStep:緊急停止減速ステップ※正転は減算
			Sci_PutsText( UART_CH_DBG, "FB_STP_EMM\n" ) ;
			pFb->V.wVrev -= gDf.FB.wEmmStep ;				// FB制御値
			// 減速していき0以下になった時点でbStopStateを通常停止にする
			if ( pFb->V.wVrev <= 0 ) {						// Over?
				pFb->V.wVrev = 0 ;							// FB制御値
				pFb->P.bStopState = FB_STOP_NORMAL ;		// 通常停止
			}
		// FB処理速度が0以下の時(逆転)	
		} else if ( pFb->V.wVrev < 0 ) {					// -(CCW)制御中
			// wEmmStep:緊急停止減速ステップ※逆転は加算
			Sci_PutsText( UART_CH_DBG, "FB_STP_EMM\n" ) ;
			pFb->V.wVrev += gDf.FB.wEmmStep ;				// FB制御値
			if ( pFb->V.wVrev >= 0 ) {						// Over?
				pFb->V.wVrev = 0 ;							// FB制御値
				pFb->P.bStopState = FB_STOP_NORMAL ;		// 通常停止
			}
		}
	} else {												// 通常運転 or 停止
		// 加減速処理ステップの決定
		// 停止時制御限界時は停止フラグセット(FeedbackやめてPWM値操作して止める)
		// 停止statが通常停止or (入力速度が正転逆転共に下回ったとき)
		if ( (pFb->P.bStopState == FB_STOP_NORMAL) ||
			 ((pFb->V.wInpVal < gDf.FBV.wFbvLowLimit) &&
			  (pFb->V.wInpVal > -gDf.FBV.wFbvLowLimit)) ) {	// 通常停止
			pFb->V.wInpVal = 0 ;							// ThrottleVal
			// 減速ステップに減速ステップ量(Flashにてメンテ可能)を代入
			wFbStep = gDf.FB.wDecStep ;						// 減速step
			pFb->P.bStopState = FB_STOP_NORMAL ;			// 通常停止指示
		} else {
			// そうでないときは加速ステップ量を代入する
			wFbStep = gDf.FB.wAccStep ;						// 加速step
		}
/*		
//正転
		if ( pFb->V.wInpVal >= pFb->V.wVrev ) {				// +(CW方向)
			// 回転速度に加減速ステップ量を加える
			pFb->V.wVrev += wFbStep ;						// FB制御値
			// 速度ゼロクロス判定してクロスしたらエンコーダリセットする
			// 速度が0になったタイミングでエンコーダのリセットを行う
			if ( (pFb->V.wVrevPrv < 0) &&
					 (pFb->V.wVrev >= 0) ) {				// -から0クロス
				xEnc_ResetInf( pEnc ) ;						// Reset EncInf
			}
			// 入力上限制限と加減速フラグセット
			// 回転指示速度がスロットル入力速度より速いとき
			if ( pFb->V.wInpVal < pFb->V.wVrev ) {			// Over?
				pFb->V.wVrev = pFb->V.wInpVal ;				// FB制御値
				pFb->V.wAccStat = FBV_ACDC_CV ;				// 等速状態
			} else {
				// スロットル入力より遅く、0以上のときは加速（正転）
				if ( pFb->V.wVrev > 0 ) {					// CW
					pFb->V.wAccStat = FBV_ACDC_ACC ;		// 加速状態
				}else {										// CCW
				//スロットル入力より遅く0以下の時は減速（逆転方向）
					pFb->V.wAccStat = FBV_ACDC_DEC ;		// 減速状態
				}
			}
//逆転
		}else if ( pFb->V.wInpVal < pFb->V.wVrev ) {		// -(CCW方向)
			pFb->V.wVrev -= wFbStep ;						// FB制御値
			if ( (pFb->V.wVrevPrv > 0) &&
				 (pFb->V.wVrev <= 0) ) {					// +から0クロス
				xEnc_ResetInf(pEnc ) ;						// Reset EncInf
			}
			if ( pFb->V.wInpVal > pFb->V.wVrev ) {			// Over?
				pFb->V.wVrev = pFb->V.wInpVal ;				// FB制御値
				pFb->V.wAccStat = FBV_ACDC_CV ;				// 等速状態
			} else {
				if ( pFb->V.wVrev > 0 ) {					// CW
					pFb->V.wAccStat = FBV_ACDC_DEC ;		// 減速状態
				} else {									// CCW
					pFb->V.wAccStat = FBV_ACDC_ACC ;		// 加速状態
				}
			}
		}
*/

	// 加減速スロープ処理(pFb->wInpVal -> pFb->V.wVrevFb)とフラグセット
	if( wFbStep == gDf.FB.wAccStep){
		if ( pFb->V.wInpVal >= pFb->V.wVrev ) {				// +(CW方向)
				// 回転速度に加減速ステップ量を加える
				pFb->V.wVrev += wFbStep ;						// FB制御値
				// 速度ゼロクロス判定してクロスしたらエンコーダリセットする
				// 速度が0になったタイミングでエンコーダのリセットを行う
				if ( (pFb->V.wVrevPrv < 0) &&
					 (pFb->V.wVrev >= 0) ) {					// -から0クロス
					xEnc_ResetInf( pEnc ) ;						// Reset EncInf
				}
				// 入力上限制限と加減速フラグセット
				// 回転指示速度がスロットル入力速度より速いとき
				if ( pFb->V.wInpVal < pFb->V.wVrev ) {			// Over?
					pFb->V.wVrev = pFb->V.wInpVal ;				// FB制御値
					pFb->V.wAccStat = FBV_ACDC_CV ;				// 等速状態
				} else {
					// スロットル入力より遅く、0以上のときは加速（正転）
					if ( pFb->V.wVrev > 0 ) {					// CW
						pFb->V.wAccStat = FBV_ACDC_ACC ;		// 加速状態
					}else {										// CCW
					//スロットル入力より遅く0以下の時は減速（逆転方向）
						pFb->V.wAccStat = FBV_ACDC_DEC ;		// 減速状態
					}
				}
			

		}else if ( pFb->V.wInpVal < pFb->V.wVrev ) {		// -(CCW方向)
				pFb->V.wVrev -= wFbStep ;						// FB制御値
				if ( (pFb->V.wVrevPrv > 0) &&
					 (pFb->V.wVrev <= 0) ) {					// +から0クロス
					xEnc_ResetInf(pEnc ) ;						// Reset EncInf
				}
				if ( pFb->V.wInpVal > pFb->V.wVrev ) {			// Over?
					pFb->V.wVrev = pFb->V.wInpVal ;				// FB制御値
					pFb->V.wAccStat = FBV_ACDC_CV ;				// 等速状態
				} else {
					if ( pFb->V.wVrev > 0 ) {					// CW
						pFb->V.wAccStat = FBV_ACDC_DEC ;		// 減速状態
					} else {									// CCW
						pFb->V.wAccStat = FBV_ACDC_ACC ;		// 加速状態
					}
				}
			}
	}else if( wFbStep == gDf.FB.wDecStep ){
			if ( pFb->V.wInpVal < pFb->V.wVrev ) {		// (CW方向の減速)
				pFb->V.wVrev -= wFbStep ;						// FB制御値
				pFb->V.wAccStat = FBV_ACDC_DEC ;		// 減速状態
				if ( (pFb->V.wVrevPrv > 0) &&
					 (pFb->V.wVrev <= 0) ) {					// +から0クロス
					xEnc_ResetInf(pEnc ) ;						// Reset EncInf
				}
			} else if ( pFb->V.wInpVal > pFb->V.wVrev ) {	// (CCW方向の減速)
				pFb->V.wVrev += wFbStep ;						// FB制御値
				pFb->V.wAccStat = FBV_ACDC_DEC ;		// 減速状態
				if ( (pFb->V.wVrevPrv < 0) &&
					 (pFb->V.wVrev >= 0) ) {					// -から0クロス
					xEnc_ResetInf( pEnc ) ;						// Reset EncInf
				}
			}
		}

	}
	pFb->V.wVrevPrv = pFb->V.wVrev ;					// FB処理速度(rpm) 前回
	
	//---------------------------------------
	// エラーチェック
	//---------------------------------------
	
	// エンコーダの速度を現在速度として代入する
	// POE作動時はエンコーダの速度が0なのでwVrevNowは0となる
	wVrevNow = pEnc->wVrev ;								// 現在速度
	// 現在速度絶対値を作成
	if ( pEnc->wVrev >= 0 ) {								// CW	正転
		// 現在速度絶対値に代入
		wVrevAbs = wVrevNow ;
	} else {												// CCW	逆転
		// 逆転の場合マイナス符号を消すためにマイナスをかけて代入				
		wVrevAbs = -wVrevNow ;
	}

	//フィードバック許可時
	if ( pFb->P.bEnable == TRUE ) {							// FB作動時

		//現在速度絶対値がモータ過回転値(定数)より大きい場合
		if ( wVrevAbs > gSysInf.uwMotOvrevMgn ) {			// 過回転
			// モータが過回転か判断する変数
			gSysInf.ubMotOverRev = MOT_OVR_YES ;			// 走行モータ過回転
			//モータの停止手続きに入る(停止ステータスは非常停止)
			xFeedback_Drv_Stop( FB_STOP_EMM ) ;				// EMM Stop
			Sci_PutsText( UART_CH_DBG, "MotRevOvr_STP.\n" ) ;
			return ;
		}
		// 入力に対して逆回転している場合
		if ( (wVrevAbs > 160) && 
			 (pEnc->wAccStat == FBV_ACDC_ACC) ) {			// 加速回転中
			if ( ((wVrevNow > 160) && (pFb->V.wVrev < -160)) ||
				 ((wVrevNow < -160) && (pFb->V.wVrev > 160)) ) {	// 逆転
				//モータが逆回転か判断する変数
				gSysInf.ubMotReverse = MOT_REV_YES ;			// 走行Mot.逆転
				//モータの停止手続きに入る(停止ステータスは非常停止)
				xFeedback_Drv_Stop( FB_STOP_EMM ) ;			// EMM Stop
				Sci_PutsText( UART_CH_DBG, "MotRvrsERR.\n" ) ;
				return ;
			}
		}
	}
	
	
	
	//==============================================
	// 速度 PID-Feedback
	//==============================================
	//加減速処理後のFB処理速度(rpm)をFB処理速度(rpm)に代入
	pFb->V.wVrevFb = pFb->V.wVrev ;							// FB処理速度(rpm)

	//加減速処理後のFB処理速度を絶対値に変換
	if ( pFb->V.wVrev >= 0 ) {								// CW	正転
		// 指示速度絶対値に代入
		pFb->V.wVrevAbs = pFb->V.wVrev ;
	} else {												// CCW	逆転
		// 逆転の場合マイナス符号を消すためにマイナスをかけて代入				
		pFb->V.wVrevAbs = -pFb->V.wVrev ;
	}

	//---------------------------------------
	// 指示速度がゼロ維持でモータ回転
	// し続けたらブレーキを掛ける
	//---------------------------------------
	//速度0保持ロックがない状態
	if ( pFb->V.fZeroLock == FBV_ZERO_LOCK_NO ) {			// V=0 HoldLock Flag
		//FB処理速度が0かつロック対象回転数(1000）よりも遅い回転数の場合
		if ( (pFb->V.wVrevFb == 0) && 
			 (wVrevAbs < FBV_ZERO_VREV) ) {					// ロック対象
			gCMT0.uwDrvCtl = 0 ;							// 駆動制御用待ち
			//速度0保持ロックをかける
			pFb->V.fZeroLock = FBV_ZERO_LOCK_YES ;			// V=0 HoldLock Flag
			Sci_PutsText( UART_CH_DBG, "Drv 502 STOP.\n" ) ;
		}
	//速度0保持ロックが無くない状態
	} else {												// ロック待ち
		//FB処理速度が0でない場合
		if ( pFb->V.wVrevFb != 0 ) {						// ロック除外
			//動作中のため速度ゼロロックを解除
			pFb->V.fZeroLock = FBV_ZERO_LOCK_NO ;			// V=0 HoldLock Flag
		} else {
			//速度がゼロでuwDrvCtlカウンタがロック待ち時間より多い場合
			if ( gCMT0.uwDrvCtl >= FBV_ZERO_WAIT_TIME ) {	//Brake Lock
				//停止ステータスに通常停止を代入
				gFb.P.bStopState = FB_STOP_NORMAL ;			// 通常停止
				//フィードバックを禁止
				gFb.P.bEnable = FALSE ;						// FB処理許可F
				Sci_PutsText( UART_CH_DBG, "Drv 518 STOP.\n" ) ;
				//ブレーキをロックする
				BRAKE_PODR = BRAKE_LOCK ;					// Brake Lock
			}
		}
	}
	
	//---------------------------------------
	// 加速,減速時のPID定数補正
	//---------------------------------------
	//加速ステーションが加速か等速か減速か判断をする。
	switch( pFb->V.wAccStat ) {
		case FBV_ACDC_ACC :					// 加速
			wFbConstP=((DWORD)gDf.FBV.wFbConstP * gDf.FBV.wFbAccelP) / 100 ;
			wFbConstI=((DWORD)gDf.FBV.wFbConstI * gDf.FBV.wFbAccelI) / 100 ;
			wFbConstD=((DWORD)gDf.FBV.wFbConstD * gDf.FBV.wFbAccelD) / 100 ;
			break ;
		case FBV_ACDC_DEC :					// 減速
			wFbConstP=((DWORD)gDf.FBV.wFbConstP * gDf.FBV.wFbDecelP) / 100 ;
			wFbConstI=((DWORD)gDf.FBV.wFbConstI * gDf.FBV.wFbDecelI) / 100 ;
			wFbConstD=((DWORD)gDf.FBV.wFbConstD * gDf.FBV.wFbDecelD) / 100 ;
			break ;
		case FBV_ACDC_CV :					// 等速
			wFbConstP = gDf.FBV.wFbConstP ;
			wFbConstI = gDf.FBV.wFbConstI ;
			wFbConstD = gDf.FBV.wFbConstD ;
			break ;
		default :
			wFbConstP = 0 ;
			wFbConstI = 0 ;
			wFbConstD = 0 ;
			break ;
	}


	//---------------------------------------
	// C:電流比較値計算
	//---------------------------------------
	//FB処理速度が0以上の時(モータ正転時)
	if ( pFb->V.wVrevFb >= 0 ) {							// 正転
	//電流比較値算出基数下限+(FB処理速度*電流比較算出基数)/1000
												//*モータ過電流値/10000
		pFb->V.dwCurComp = gDf.FBV.wCCB_LowLimit +
							(((DWORD)pFb->V.wVrevFb * 
							(DWORD)gDf.FBV.wCurCompBase) / 1000) * 
							(DWORD)gDf.SYS.wMotCurLimit / 
							10000 ;							// C:電流比較基準値
	} else {												// 逆転
	//逆転時は最後の10000に負記号をつける
		pFb->V.dwCurComp = gDf.FBV.wCCB_LowLimit +
							(((DWORD)pFb->V.wVrevFb * 
							(DWORD)gDf.FBV.wCurCompBase) / 1000) * 
							(DWORD)gDf.SYS.wMotCurLimit / 
							-10000 ;						// C:電流比較基準値
	}
	//モータ電流がモータ電流比較基準値より大きい場合
	if ( pMot->dwCur > pFb->V.dwCurComp ) {
		//電流比較値=モータ電流値−モータ電流比較基準値
		pFb->V.dwFbC = (pMot->dwCur - pFb->V.dwCurComp) ;	// C:電流比較値
	} else {
		pFb->V.dwFbC = 0 ;									// C:電流比較値
	}

	//---------------------------------------
	// I:制限値計算
	//---------------------------------------
	// 上記dwCurCompを求めることで、現在速度の電流基準値を求めることが可能
	// しかしその他プログラムにおいてはdwLimitIに定数を与えているため
	// 今回はデータフラッシュを絡めた定数を設定してく。

//	if ( pFb->V.wVrevFb >= 0 ) {							//正転時
//	//制限値=制限初期値*1000+(制限値スロープ*FB処理速度)/10
//								// -電流比較値*電流制限定数
//		pFb->V.dwLimitI = (DWORD)gDf.FBV.wLimitI_Init * 1000 + 
//			((DWORD)gDf.FBV.wLimitI_Ratio * pFb->V.wVrevFb) / 10 -
//			pFb->V.dwFbC * gDf.FBV.wLimitI_RateC ;			// I:制限値
//	} else {												//逆転時
//	//制限値=制限初期値*1000-(制限値スロープ*FB処理速度)/10
//								// -電流比較値*電流制限定数
//		pFb->V.dwLimitI = (DWORD)gDf.FBV.wLimitI_Init * 1000 - 
//			((DWORD)gDf.FBV.wLimitI_Ratio * pFb->V.wVrevFb) / 10 -
//			pFb->V.dwFbC * gDf.FBV.wLimitI_RateC ;			// I:制限値
//	}


	// 計算ではなく定数で求めたIの制限値
	// wLimitIの中身も倍率も適当なのでチューンが必要
	pFb->V.dwLimitI = gDf.FBV.wLimitI * 1000 ;
	

	//---------------------------------------
	// PID値の計算
	//---------------------------------------
	// 低速時は制御定数をオーバーライド(減らす)
	if ( wVrevAbs <= gDf.FBV.wRevLowOvr ) {			// 低速OverRide
		if ( wVrevAbs < gDf.FBV.wRevLowOvrLimit ) {	// Override制限
			wVrevAbs = gDf.FBV.wRevLowOvrLimit ;
		}
	//各FB定数を算出する
		wFbConstP = ((DWORD)wFbConstP * 
				(DWORD)wVrevAbs) / gDf.FBV.wRevLowOvr ;
		wFbConstI = ((DWORD)wFbConstI * 
				(DWORD)wVrevAbs) / gDf.FBV.wRevLowOvr ;
		wFbConstD = ((DWORD)wFbConstD * 
				(DWORD)wVrevAbs) / gDf.FBV.wRevLowOvr ;
	}
	// rpmベースの比例制御値
	pFb->V.wVdiff = pFb->V.wVrevFb - wVrevNow ;		// 偏差
	pFb->V.wVdiffAbs = pFb->V.wVrevAbs - wVrevAbs ;	// 偏差の絶対値


	// P(比例) 計算
	// P:現在と目標の差(偏差)
	if ( gDf.FBV.bType & 0x01 ) {	// P(比例)制御有 bit0
		if ( pFb->P.bEnable == TRUE ) {						// FB作動時
			if ( pFb->V.wVdiff >= 0 ) {
				pFb->V.dwFbP = (((DWORD)pFb->V.wVdiff * wFbConstP) + 5) / 10 ;
			} else {
				pFb->V.dwFbP = (((DWORD)pFb->V.wVdiff * wFbConstP) - 5) / 10 ;
			}
		} else {											// FB停止
			pFb->V.dwFbP = 0 ;
			pFb->V.wVdiff = pFb->V.wVdiffPrv = 0 ;
		}
	} else {												// FB無し
		pFb->V.dwFbP = 0 ;
		pFb->V.wVdiff = pFb->V.wVdiffPrv = 0 ;
	}
	
	// I:誤差の積分値
	if ( gDf.FBV.bType & 0x02 ) {	// I(積分)制御有 bit1
		if ( pFb->P.bEnable == TRUE ) {						// 作動時
			if ( pFb->V.wVdiff >= 0 ) {
				pFb->V.dwFbI += (((DWORD)pFb->V.wVdiff * wFbConstI) + 5) / 10  ;
			} else {
				pFb->V.dwFbI += (((DWORD)pFb->V.wVdiff * wFbConstI) - 5) / 10  ;
			}
			// I制限
			// dwLimitIを求める方法がいくつかあり、高所作業車のドライブ側は
			// 速度により比較値が変わる方法を行っている。
			// しかしNXSやSTDは定数を入れているため、そちらの方法を参考とする。
			if ( (pFb->V.wVrevFb >= 0) && 
				 (pFb->V.dwFbI > pFb->V.dwLimitI) ) {		// +limit over
				pFb->V.dwFbI = pFb->V.dwLimitI ;
			} else if ( (pFb->V.wVrevFb < 0) &&
					(pFb->V.dwFbI < -pFb->V.dwLimitI) ) {	// -limit over
				pFb->V.dwFbI = -pFb->V.dwLimitI ;
			}
		} else {											// FB停止
			pFb->V.dwFbI = 0 ;
		}
	} else {
		pFb->V.dwFbI = 0 ;
	}
	
	// D:偏差の微分値
	if ( gDf.FBV.bType & 0x04 ) {	// D(微分)制御有 bit2
		if ( pFb->P.bEnable == TRUE ) {						// 作動時
			// rpmベースの偏差の微分値
			pFb->V.dwFbD = ((DWORD)(pFb->V.wVdiff - pFb->V.wVdiffPrv) * 
								wFbConstD) / 10 ;
		} else {											// FB停止
			pFb->V.dwFbD = 0 ;
		}
	} else {
		pFb->V.dwFbD = 0 ;
	}

//	gDbg.CNT.dwCnt05 ++ ;
//	if(gDbg.CNT.dwCnt05 >= 50){
//		xPrintf( UART_CH_DBG, " FbP:%d\n FbI:%d\n FbD:%d\n "
//							,pFb->V.dwFbP,pFb->V.dwFbI,pFb->V.dwFbD ) ; 
//		xPrintf( UART_CH_DBG, " Diff:%d ",pFb->V.wVdiff) ; 
//		xPrintf( UART_CH_DBG, " FB:%d Now:%d\n"
//							,pFb->V.wVrevFb,wVrevNow ) ; 
//		xPrintf( UART_CH_DBG, " FbP:%d FbI:%d Diff:%d\n"
//							,pFb->V.dwFbP,pFb->V.dwFbI,pFb->V.wVdiff ) ; 
//		gDbg.CNT.dwCnt05 = 0;
//	}

	//---------------------------------------
	// 操作量(PWM値)計算
	//---------------------------------------
	if ( pFb->P.bEnable == TRUE ) {							// 作動時
		if ( pFb->V.wVrevFb >= 0 ) {						// CW 正転時
			//基本PWM値=FB処理速度*PWM変換係数+PWM指令オフセット
			dwCtlBase = ((DWORD)pFb->V.wVrevFb * gDf.FBV.wPwmRatio) +
						gDf.FBV.wPwmOffset ;
			//FB-V出力=(基本PWM値+比例制御値+誤差の積分値+偏差の微分値)/100
			pFb->V.wOut = (WORD)((dwCtlBase + pFb->V.dwFbP + 
						 pFb->V.dwFbI + pFb->V.dwFbD) / 100) ;
//			pFb->V.wOut -= pFb->V.dwFbC ;
		} else {											// CCW 逆転時
			dwCtlBase = ((DWORD)pFb->V.wVrevFb * gDf.FBV.wPwmRatio) -
						gDf.FBV.wPwmOffset ;
			pFb->V.wOut = (WORD)((dwCtlBase + pFb->V.dwFbP + 
						 pFb->V.dwFbI + pFb->V.dwFbD) / 100) ;
//			pFb->V.wOut += pFb->V.dwFbC ;
		}
	} else {
		//FBが禁止
		//フィードバックロックが有効の場合、FB-V出力はしない
		if ( pFb->V.bFbLock == TRUE ) {						// Feedback Lock YES
			pFb->V.wOut = 0 ;
		} else {											// Feedback Lock NO
		//FBが禁止かつFBロックが無効の場合、少量のみ出力をする
			pFb->V.wOut = (WORD)(((DWORD)pFb->V.wOut * 94) / gPwm.wRngMax 
							/ 100) ;
		}
	}
	
	
	//-------------- 速度制御終わり ----------------
	
	
	//---------------------------------------
	// 操作量(PWM値)制限
	//---------------------------------------
	//求めたFBの出力量をPWM指示値に代入
	pFb->V.wPwm = pFb->V.wOut ;								// 現在のPWM指示値
	if ( pFb->P.bEnable == TRUE ) {							// 作動時
		if ( pFb->V.wVrevFb >= 0 ) {						// CW 正転
		//*************************************//
			//PWM電流制限値=電流制限初期値+電流制限スロープ*FB処理速度/1000
			// 制限電流値は通常モータ電流値を上回らないよう定数を設定
			pFb->V.wPwmCurLimit = (WORD)(gDf.PWM.wLimitCur_Init + 
						((DWORD)gDf.PWM.wLimitCur_Ratio * pFb->V.wVrevFb) / 
						1000) ;								// PWM電流制限値
			// PWM電流補正値=電流制限率*(モータ電流-PWM制限電流)/1000
			// 
			
			pFb->V.wPwmCurAdj = (WORD)((DWORD)gDf.PWM.wLimitCur_Rate * 
								(pMot->dwCur - pFb->V.wPwmCurLimit) / 
								1000) ;						// PWM電流補正値
			//電流補正値が0未満だった場合は0とする
			if ( pFb->V.wPwmCurAdj < 0 ) {
				pFb->V.wPwmCurAdj = 0 ;						// PWM電流補正値
			}
		//**************************************//	
			
			//目標値が現在速度-PWM制限比較速度幅より大きかった場合
			if ( pFb->V.wVrevFb >= (wVrevNow - gDf.PWM.wVCompWidth) ) {	// 制限?
				//PWM制限値=PWM制限初期値+指示速度スロープ
									//*FB処理速度+500/100 - PWM電流補正値
				pFb->V.wPwmLimit = gDf.PWM.wLimit_Init + 
					((DWORD)gDf.PWM.wVfb_Ratio * pFb->V.wVrevFb + 500)/1000 +
					((DWORD)gDf.PWM.wLimit_Ratio * wVrevNow + 50) / 100 -
					pFb->V.wPwmCurAdj ;						// PWM制限値
				//PWM指示値がPWMの上限より大きい場合は上限に合わせる
				if ( pFb->V.wPwm > pFb->V.wPwmLimit ) {		// 制限する
					pFb->V.wPwm = pFb->V.wPwmLimit ;		// 現在のPWM指示値
				}
			}
		} else {											// CCW 逆転も同じく
		//*************************************//
			pFb->V.wPwmCurLimit = (WORD)(gDf.PWM.wLimitCur_Init - 
						((DWORD)gDf.PWM.wLimitCur_Ratio * pFb->V.wVrevFb) / 
						1000) ;								// PWM電流制限値
			pFb->V.wPwmCurAdj = (WORD)((DWORD)gDf.PWM.wLimitCur_Rate * 
								(pMot->dwCur - pFb->V.wPwmCurLimit) / 
								1000) ;						// PWM電流補正値
			if ( pFb->V.wPwmCurAdj < 0 ) {
				pFb->V.wPwmCurAdj = 0 ;						// PWM電流補正値
			}
		//*************************************//
			if ( pFb->V.wVrevFb <= (wVrevNow + gDf.PWM.wVCompWidth) ) {	// 制限?
				pFb->V.wPwmLimit = -gDf.PWM.wLimit_Init + 
					((DWORD)gDf.PWM.wVfb_Ratio * pFb->V.wVrevFb - 500)/1000 +
					((DWORD)gDf.PWM.wLimit_Ratio * wVrevNow + 50) / 100 +
					pFb->V.wPwmCurAdj ;						// PWM制限値
				if ( pFb->V.wPwm < pFb->V.wPwmLimit ) {		// 制限する
					pFb->V.wPwm = pFb->V.wPwmLimit ;		// 現在のPWM指示値
				}
			}
		}
	}
	

	//---------------------------------------
	// 操作量範囲オーバー時の制限,PWM設定値計算
	//---------------------------------------
	//PWM指示値がPWM最大絶対値より大きい場合は最大絶対値に合わせる
	if ( pFb->V.wPwm > gPwm.wRngMax ) {						// Max Limit
		pFb->V.wPwm = gPwm.wRngMax ;						// 実PWM指示値
	} else if ( pFb->V.wPwm < -gPwm.wRngMax ) {				// Min Limit
		pFb->V.wPwm = -gPwm.wRngMax ;						// 実PWM指示値
	}
	
	
	//---------------------------------------
	// PWM値反映
	//---------------------------------------
	pPwm = &gPwm ;							// PWM軸固有値Ptr
	// 非常停止時のPWM処理
	// FBで減速させる
	if ( pFb->P.bStopState == FB_STOP_EMM ) {				// 非常停止指示あり
	//実DutyにEMM停止時のPWM指示値を代入	
		pPwm->wDutyExe = pFb->V.wPwm ;						// Duty
		if(	gDbg.FLG.fDbg03 == 0 ){
			Sci_PutsText( UART_CH_DBG, "Duty_Emm_Stop\n" ) ;
			gDbg.FLG.fDbg03++ ;
		}

	// 通常停止時のPWM処理
	// 低速時なのであとはPWM値を直接操作
	} else if ( pFb->P.bStopState == FB_STOP_NORMAL ) {		// 通常停止指示あり
		if ( pPwm->wDutyExe > 0 ) {							//正転時
			//実Dutyから通常停止ステップを引いていく
//			pPwm->wDutyExe -= gDf.FB.wStopPStep  ;			// Duty
			pPwm->wDutyExe = pFb->V.wPwm  ;			// Duty FB
//			Sci_PutsText( UART_CH_DBG, "Duty_Stp1.\n" ) ;
			//実Dutyが0以下になった瞬間に実Dutyを0にする
			if ( pPwm->wDutyExe <= 0 ) {
				pPwm->wDutyExe = 0 ;						// Duty
			}
		} else if ( pPwm->wDutyExe < 0 ) {					//逆転時
//			pPwm->wDutyExe += gDf.FB.wStopPStep  ;			// Duty
			pPwm->wDutyExe = pFb->V.wPwm  ;			// Duty FB
			if ( pPwm->wDutyExe >= 0 ) {
				pPwm->wDutyExe = 0  ;						// Duty
			}
		}
	//POEが作動した場合、入力指示に関わらずモータへの出力信号が停止する	
	} else if ( pFb->P.bStopState == FB_STOP_POE ) {	// POE停止指示あり
		//ロータがメカにより既に停止しているため、即座にDutyを0する
		pPwm->wDutyExe = 0 ;
		Sci_PutsText( UART_CH_DBG, "Duty_POE.\n" ) ;
	// 非常停止以外のPWM指示(通常)
	} else {												// 通常運転
		pPwm->wDutyExe = pFb->V.wPwm ;						// Duty
	}
	//現在速度(ENCの回転数)が400より下で緊急or通常停止のステータスの場合
	//PWMを反映	
	if ( (wVrevNow < 400) &&
		 ((pFb->P.bStopState == FB_STOP_EMM) ||
		  (pFb->P.bStopState == FB_STOP_NORMAL) ||
		  (pFb->P.bStopState == FB_STOP_POE)) ) {			// 停止指示あり
		Int_MotExite( );
	}
	

	//---------------------------------------
	// 次回の計算用に保存
	//---------------------------------------
	pFb->V.wVdiffPrv = pFb->V.wVdiff ;						// 前回の偏差
	pFb->P.bEnablePrv = pFb->P.bEnable ;					// FB処理許可F 前回
	
	
	//---------------------------------------
	// ロータ停止判定したらFeedback止めて
	// 停止シーケンスに入る(pFb->bEnable = FALSE)
	// なるべく左右揃えて止めるように両軸チェックする
	//---------------------------------------
	if( ( pFb->P.bStopState == FB_STOP_EMM ) ||
	   ( pFb->P.bStopState == FB_STOP_POE ) ) {			// 非常停止orPOE停止
		if ( pEnc->wVrev == 0 ) {						// ロータ停止中
			pFb->P.bEnable = FALSE ;					// FB処理許可F
			Sci_PutsText( UART_CH_DBG, "Drv 823 FALSE.\n" ) ;
			pFb->V.bFbLock = TRUE ;						// Feedback Lock YES
		}
	} else {											// 通常運転
		if ( pEnc->wVrev == 0 ) {						// ロータ停止中
			Int_MotExite(  ) ;							// モータ励磁
//			if ( (pFb->V.wVrevFb == 0) &&
			if ( (pFb->V.wInpVal == 0) &&
				 (pEnc->wVrev == 0) ) {					// 指示停止中
				pFb->P.bEnable = FALSE ;				// FB処理許可F
				Sci_PutsText( UART_CH_DBG, "Drv 833 FALSE.\n" ) ;
//				xPrintf( UART_CH_DBG, "Count:%d\n", gDbg.CNT.dwCnt01 ) ;
			}
		}
	}
}

//=======================================================================
