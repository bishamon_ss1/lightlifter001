/***********************************************************************/
/*                                                                     */
/*  FILE        : Main.c or Main.cpp                                    */
/*  DATE        : Nov 18, 2015                                     */
/*  DESCRIPTION : Main Program                                          */
/*  CPU TYPE    : R5F52106BxFM
/*                                                                     */
/*  NOTE:THIS IS A TYPICAL EXAMPLE.                                    */
/*                                                                     */
/***********************************************************************/
#include	<machine.h>


#include	"SCI_RX210.h"			// SCIヘッダ

#define	_APP_DEF_

#include	"LL.h"							// アプリケーションヘッダ
#include	"LL_prototype.h"				// アプリケーションプロトタイプヘッダ
void main(void);

#pragma section
//---------------------------------------------------
//	システム情報初期化
//---------------------------------------------------
static void		sInz_SysInf( void )
{
	BYTE		bIdx1 ;						// AllayIdx
	
	//  各エンコーダの初期設定 (このプログラムではリセットをしているが
	//  実際にはデータフラッシュより現在値情報を読み出す必要があると思われる
	gEnc.Inp.ubUVW = 0 ;					// Enc UVWリセット
	gEnc.Inp.BIT.U = !PI_ENCU_IN ;			// Enc U相値
	gEnc.Inp.BIT.V = !PI_ENCV_IN ;			// Enc V相値
	gEnc.Inp.BIT.W = !PI_ENCW_IN ;			// Enc W相値

	gEnc.InpPrv = gEnc.Inp ;				// Enc UVW前回値
	gEnc.wDir = ENC_DIR_STOP ;				// Enc 回転方向
	gEnc.wDirPrv = gEnc.wDir ;				// Enc 回転方向(前回)
	gEnc.wPulseChk = ENC_RES_SLOW ;			// パルス数確認
	gEnc.wPulseCount = ENC_RES_SLOW ;		// 計測パルス数
	gEnc.wOvf = 0 ;							// Enc OverFlow数
	gEnc.dwExtTimer = 0 ;					// Enc 拡張TimerCount
	gEnc.wSpeed = 0 ;						// Enc 速度(0.1mS)
	gEnc.wVrev = 0 ;						// Enc 回転速度(rev/min)
	gEnc.wVrevPrv = gEnc.wVrev ;			// Enc 前回回転速度(rpm)
	gEnc.dwPos = 0 ;						// Enc 位置(enc)
	gEnc.fUpLimit = UP_FREE ;				// 上昇端ではない
		
	// 目標位置用 ※未実装		
	gFb.Q.bEnable = FALSE ;					// 有効F
	gFb.Q.dwPosTo = 0 ;						// 移動点(enc)
	// 速度フィードバック用
	gFb.V.wFbStep = FB_NONE_STEP ;			// 加減速ステップ
	gFb.V.wStopDecStep = gDf.FB.wStopStep ; // 停止及び減速ステップ
	gFb.V.wInpVal = 0 ;						// スロットル入力値
	gFb.V.dwFbP = 0 ;						// P:比例制御値    偏差
	gFb.V.dwFbI = 0 ;						// I:誤差の積分値
	gFb.V.dwFbD = 0 ;						// D:偏差の微分値
	gFb.V.wVdiff = 0 ;						// 偏差
	gFb.V.wVdiffPrv = 0 ;					// 前回の偏差
	gFb.V.wVdiffAbs = 0 ;					// 絶対値の偏差
	gFb.V.wPwm = 0 ;						// 現在のPWM指示値
	gFb.V.wPwmLimit = 0 ;					// PWM制限値
	gFb.V.wAccStat = FBV_ACDC_STOP ;		// 加減速ステータス
	gFb.V.wVrevFb = 0 ;						// FB処理速度(rpm)
	gFb.V.wVrev = 0 ;						// 加減速処理後処理速度
	gFb.V.wVrevPrv = 0 ;					// 加減速処理後処理速度 前回
	gFb.V.wVrevAbs = 0 ;					// 加減速処理後処理速度の偏差

	// その他＋位置FB用
	gFb.P.wVrevLimit = 0 ;					// 上限速度(rpm)
	gFb.P.wVrevInit = 0 ;					// 初期速度(rpm)
	gFb.P.bEnable = FALSE ;					// FB処理許可F
	gFb.P.bEnablePrv = FALSE ;				// FB処理許可F 前回
	gFb.P.bStopState = FB_STOP_NO ;			// 停止ステータス
	gFb.P.bStopStatePrv = 0 ;				// 停止ステータス 前回
	gFb.P.bRoterStop = FB_ROTER_STOP_YES ;	// ロータ停止Flag
	gFb.P.dwPdiff = 0 ;						// 偏差
	gFb.P.ubMotStat = STAT_MOT_STOP ;		// 現在の駆動ステータス
	gFb.ubFbType = gDf.FBV.bType ;			// フィードバックタイプ

	//	インターバルタイマ類初期化
	gCMT0.uwIntFb = 0 ;						// FeedBackタイマ
	gCMT0.uwDrvCtl = 0 ;					// 駆動制御用待ち
	gCMT0.T10M.uwCount = 0 ;				// 10mSec計測内部カウンタ
	gCMT0.T10M.uwTime = 0 ;					// 10mSec時間値
	gCMT0.T10M.uwErrMotStop = 0 ;			// エラー用タイマ	
	gCMT0.T100M.uwCount = 0 ;				// 100mSec計測内部カウンタ
	gCMT0.T100M.uwTime = 0 ;				// 100mSec時間値
/**/gCMT0.T100M.uwBrakeDelay = 0 ;			// 100mSecブレーキ遅延タイマ
	gCMT0.T1S.uwCount = 0 ;					// 1Sec計測内部カウンタ
	gCMT0.T1S.uwTime = 0 ;					// 1Sec時間値
	gCMT0.T1S.uwPWROFF = 0 ;				// AutoPowerOff timer
	gCMT0.T1S.uwUpTimOut = 0 ;				// 上昇タイムアウト用タイマ
	gCMT0.T1S.uwDownTimOut = 0 ;			// 下降タイムアウト用タイマ
	gCMT1.udw01ms = 0 ;						// AD用カウントタイマ
	gCMT0.T100M.udwLEDFlashON = 0 ;			// LED点灯タイマ
	gCMT0.T100M.udwLEDFlashOFF = 0 ;		// LED消灯タイマ
	
	//  12bitAD変換機初期化					// モータ電流 電源電圧計測用
	gAdc.Volt.V24.uwVal = S12AD.ADDR1 ;		// ADC値
	gAdc.Volt.V24.uwValMath = (UWORD)(10 * gAdc.Volt.V24.uwVal) ; // ADC値*10
	gAdc.Volt.uwV24 = (UWORD)( xDivide( gAdc.Volt.V24.uwValMath , AD_DEV ) 
										+ OFFSET_AD12 ) ;	// V24電圧(0.01V/u)
	gAdc.Volt.V24.udwSum = 0 ;				// ADC積算値
	gAdc.Volt.V24.uwCount = 0 ;				// ADC計測回数
	gAdc.Volt.fMeas24 = ADC_MEASURE_NO ;	// 24V計測Flag
	gAdc.Volt.uwPoffRaw = 0 ;				// POFF_INT電源電圧値



	gAdc.Mot.dwSum = 0 ;					// Motor UVW sum
	gAdc.Mot.uwSumCount = 0 ;				// Motor UVW sum count
	gAdc.Mot.dwCur = 0 ;					// Motor UVW curr.
//	gAdc.Mot.uwOfsTime = ADC12_OFS_TIME ;	// Motor curr. OffsetTimer
	gAdc.Mot.uwOfsTime = 0 ;				// Motor curr. OffsetTimer
/**/gAdc.Mot.udwOfsSum = 0 ;				// Motor curr. OffsetAvgSum
	gAdc.Mot.uwOfs = 0 ;					// Motor curr. Offset
	gAdc.Mot.ubOfsCheck = 0 ;				// チェック
	gAdc.Mot.fOfsGet = FALSE ;				// Offset Measure Flag
	gAdc.Mot.ubEncCount = 0 ;				// Motor Encoder Pulse Count
	gAdc.Mot.ubEncCountCmp = 1  ;			// Motor Encoder Pulse Count Cmp
//	gAdc.Mot.wEncVrevPrv = 0 ;				// Previous Motor rpm
	gAdc.Mot.fEncChg = TRUE ;				// Enc.PhaseChange

	// システム情報初期化
	gSysInf.fPwrOff	= POWER_KEEP ;		// オートパワーオフフラグ
//	gSysInf.fOpEnable = SYS_OPE_INITIAL ;	// 作動可能Flag
	gSysInf.wUpSpeed = DRV_SPEED_UP_NORMAL ; // 上昇速度(通常)
	gSysInf.wDownSpeed = DRV_SPEED_DOWN_NORMAL;  // 下降速度(通常)
	gSysInf.fPMaint = SYS_PMAINT_OFF ;		// ParmMaintMode Flag
	gSysInf.fEmm = SYS_EMM_YES ;			// 非常停止Flag
	gSysInf.bPoffInt = 0xff ;				// 電源遮断検出
	gSysInf.fPOEStop = SYS_POE_NO ;			// POE STOP
	gSysInf.fLVDErr = SYS_LVD1_OFF ;			// LVD ERR
	gSysInf.fEncErr = SYS_ENC_ERR_NO ;		// エンコーダエラーFlag
//	gSysInf.ubMotOvc = MOT_OVC_NO ;			// モータ過電流 Flag
	gSysInf.fEncErrPrc = FALSE ;			// エンコーダエラー処理Flag
//	gSysInf.ubMovStat = MOV_STOP ;			// 動作ステータスは停止
//	gSysInf.fMotDrv = SYS_MOT_STOP ;		// モータ停止中
	gSysInf.fDriveEnable = FALSE ;			// 駆動禁止
	gSysInf.ubMoFlag = SW_MOFLG_NONE;		// モータ速度フラグ
	gSysInf.fErrLoopFlag = ERR_NONE ;		// エラーループには入らない
	gSysInf.ubErrCode = ERR_NONE ;			// エラーコード
	gSysInf.uwMotUpStartCnt = 0 ;			// 上昇開始用動作カウンタ 
	gSysInf.uwMotDownStartCnt = 0 ;			// 下降開始用動作カウンタ 
	gSysInf.uwErrCount = 0 ;				// エラー監視用カウンタ
	gSysInf.ubErrCate = 0 ;					// エラーカテゴリー
	gSysInf.wErrVoltage = 0 ;				// 電圧エラー時の電圧
	gSysInf.ubLEDStat = LED_NORMAL ;		// LEDステータスなし
	gSysInf.bPat = LED_STAT_BIT	;			// LED配列
	for ( bIdx1 = 0 ; bIdx1 < LED_PAT ; bIdx1++ ) {
		gSysInf.ubLEDPat[ bIdx1 ] = LED_SHORT ;		// LED点灯時間
	}
	//デバック情報初期化
#ifdef	_DBG_SYS_
	gDbg.CNT.dwCnt01 = 0 ;					// Dbg用カウンタ
	gDbg.CNT.dwCnt02 = 0 ;					// Dbg用カウンタ
	gDbg.CNT.dwCnt03 = 0 ;					// Dbg用カウンタ
	gDbg.CNT.dwCnt04 = 0 ;					// Dbg用カウンタ
	gDbg.CNT.dwCnt05 = 0 ;					// Dbg用カウンタ
	gDbg.CNT.dwCnt06 = 0 ;					// Dbg用カウンタ
	gDbg.CNT.dwCnt07 = 0 ;					// Dbg用カウンタ
	gDbg.CNT.dwCnt08 = 0 ;					// Dbg用カウンタ
	gDbg.TIM.uwT1m_01 = 0 ;					// Dbg用1msタイマ
	gDbg.TIM.uwT1m_02 = 0 ;					// Dbg用1msタイマ
	gDbg.TIM.uwT10m_01 = 0 ;				// Dbg用10msタイマ
	gDbg.TIM.uwT1s_01 = 0 ;					// Dbg用1sタイマ
	gDbg.FLG.fDbg01 = 0 ;					// Dbg用フラグ 
	gDbg.FLG.fDbg02 = 0 ;					// Dbg用フラグ 
	gDbg.FLG.fDbg03 = 0 ;					// Dbg用フラグ 
	gDbg.FLG.dwDbg04 = 0 ;					// Dbg用フラグ 
	gDbg.FLG.dwDbg05 = 0 ;					// Dbg用フラグ 
	gDbg.PRV.wDbgPrv01= 0 ;					// Dbg用前回値
	gDbg.PRV.wDbgPrv02= 0 ;					// Dbg用前回値
	gDbg.PRV.wDbgPrv03= 0 ;					// Dbg用前回値
	gDbg.PRV.wDbgPrv04= 0 ;					// Dbg用前回値
	gDbg.PRV.wDbgPrv05= 0 ;					// Dbg用前回値
#endif //_DBG_SYS

}
//==============================================================
// 初期化エントリー
//==============================================================
static void	sInz( void )
{
	
	// 基本H/W設定
//	NonExistentPort ( ) ;					// 存在しないポートの設定
	Inz_SysClock( ) ;						// システムクロック設定
	Inz_IO_Port() ;							// I/Oポート初期化エントリー
	Inz_CRC( ) ;							// CRC演算器初期化
	Inz_CMT0( ) ;							// 1mSタイマ初期化
	Inz_CMT1( ) ;							// AD変換用タイマ
	Inz_CMT2( ) ;							// Enc0 Count
	Inz_LVD1( ) ;							// LVD1設定

	//	DIP-SW情報取得
	gDip.S1 = DIP01_PIDR ;					// DIP1 
	gDip.S2 = DIP02_PIDR ;					// DIP2
	gDip.S3 = DIP03_PIDR ;					// DIP3
	gDip.S4 = DIP04_PIDR ;					// DIP4
	gDip.S5 = DIP05_PIDR ;					// DIP5
	gDip.S6 = DIP06_PIDR ;					// DIP6
	gDip.S7 = DIP07_PIDR ;					// DIP7 
	gDip.S8 = DIP08_PIDR ;					// DIP8 

//	Inz_PinFunc( ) ;						// 端子機能選択設定

	// モータ用PWM初期化
	Inz_PWM( ) ;							// モータ出力

	// 確認用ポート初期設定
	Inz_MTU0( ) ;							// 変数確認用ポート
//	Inz_MTU1( ) ;							// フリータイマランカウンタ
	
	// エンコーダ入力初期化  ポーリング処理	※10/5追加
	Inz_Enc( ) ;							// エンコーダ用割込入力セットアップ
	
	// POE2#初期化
	Inz_POE( );

	// 12bitA/D変換器初期設定
	Inz_AD12( ) ;							// 12bit ADC
	AD12_GetOffset( ) ;						// 電流Offset,電圧計測

	// ProsessorStatusWord設定(割込許可)
	Inz_PSW( ) ;							// プロセッサの割り込み設定

	// システム情報初期化
	sInz_SysInf( ) ;						// Inz.SysInf
	
	// ウォッチドッグタイマの初期設定
	Inz_WDT( ) ;							// Inz WDT
	
    // UART設定							
//	fRet = SCI_Inz( UART_CH_DBG, UART_BAUD_19200, UART_PARITY_NONE, 
//					UART_STOP_1, UART_BIT_8 ) ;			// SCI1
	SCI_Inz( UART_CH_DBG, UART_BAUD_38400, UART_PARITY_NONE, 
					UART_STOP_1, UART_BIT_8 ) ;			// SCI1
	XDEV_OUT(Sci_Putc) ;					// 出力をUARTに結合

	//	デバックポートにバナー出力
	Sci_PutsText( UART_CH_DBG, "\x1b[0;0H" ) ;	// Cursor 0,0
	Sci_PutsText( UART_CH_DBG, "\x1b[2J" ) ;	// cls
	Sci_PutsText( UART_CH_DBG, "RX210test\n" ) ;
	Sci_PutsText( UART_CH_DBG, "---RX210 Testing ---\n" ) ;
	
	// データフラッシュ情報情報初期化
	xDF_ParmReadMain( ) ;					// DFからパラメータ読み出し
//	xDF_ParmWriteExe( ) ;					// デバッグ用
	
#define MOT_TYPE_100	
	
#ifdef MOT_TYPE_100

	gDf.FBV.bType = FBV_TYPE ;					// FBタイプ
	gDf.FBV.wFbConstP = FBV_CONST_KP ;			// P:比例定数(Kp)
	gDf.FBV.wFbConstI = FBV_CONST_KI ;			// I:積分定数(Ki)
	gDf.FBV.wFbConstD = FBV_CONST_KD ;			// D:微分定数(Kd)
	gDf.FBV.wFbAccelP = FBV_ACCP_KP ;			// P:比例定数加速%
	gDf.FBV.wFbAccelI = FBV_ACCP_KI ;			// I:積分定数加速%
	gDf.FBV.wFbAccelD = FBV_ACCP_KD ;			// D:微分定数加速%
	gDf.FBV.wFbDecelP = FBV_ACCM_KP ;			// P:比例定数減速%
	gDf.FBV.wFbDecelI = FBV_ACCM_KI ;			// I:積分定数減速%
	gDf.FBV.wFbDecelD = FBV_ACCM_KD ;			// D:微分定数減速%
	gDf.FBV.wPwmRatio = FBV_PWM_RATIO ;			// PWM変換係数
	gDf.FBV.wPwmOffset = FBV_PWM_OFFSET ;		// PWM指令Offset

#endif

#ifdef MOT_TYPE_200

	gDf.FBV.bType = FBV_TYPE ;					// FBタイプ
	gDf.FBV.wFbConstP = FBV_CONST_KP ;			// P:比例定数(Kp)
	gDf.FBV.wFbConstI = FBV_CONST_KI ;			// I:積分定数(Ki)
	gDf.FBV.wFbConstD = FBV_CONST_KD ;			// D:微分定数(Kd)
	gDf.FBV.wFbAccelP = FBV_ACCP_KP ;			// P:比例定数加速%
	gDf.FBV.wFbAccelI = FBV_ACCP_KI ;			// I:積分定数加速%
	gDf.FBV.wFbAccelD = FBV_ACCP_KD ;			// D:微分定数加速%
	gDf.FBV.wFbDecelP = FBV_ACCM_KP ;			// P:比例定数減速%
	gDf.FBV.wFbDecelI = FBV_ACCM_KI ;			// I:積分定数減速%
	gDf.FBV.wFbDecelD = FBV_ACCM_KD ;			// D:微分定数減速%
	gDf.FBV.wPwmRatio = FBV_PWM_RATIO ;			// PWM変換係数
	gDf.FBV.wPwmOffset = FBV_PWM_OFFSET ;		// PWM指令Offset

#endif
	
	
	nop( ) ;
	
}
//-----------------------------------------------------------------------------


//==============================================================
// ＭａｉｎＲｏｕｔｉｎｅ
//==============================================================

void main(void)
{
	FB				*pFb ;						// FB情報構造体
	ENC_INF			*pEnc ;						// エンコーダ
	ADC_MOT			*pMot ;						// モータ AD情報
	MTU_PWM			*pPwm ;						// PWM固有値
	BYTE			bErr_Judge = 0 ;			// エラー判断

#define	_DBG_SYS_

	sInz() ;									// 各種初期化エントリ
	
	xIoCtl_DipCheck () ;						// DipSW状態確認

	//-----------------------------------
	//	Loop of main
	//-----------------------------------
	
	while(1){
		// TEST用
//		BRAKE_PODR = BRAKE_FREE ;				//  Brake Free

		xSwDevStat( ) ;							// SW-Device状態取得
		if ( gSysInf.fErrLoopFlag == ERR_NONE ) {
			xIoCtlEMM( ) ;							// 非常停止SW用
			xIoCtl( ) ;								// Dev-I/O処理
			
			// エラー管理
			bErr_Judge = xErr_All( ) ;				// 常時監視	

#ifdef	_DBG_SYS_

			gDbg.CNT.dwCnt01 ++ ;
			if(gDbg.CNT.dwCnt01 >= 5000){
				gDbg.FLG.dwDbg05 = WDT.WDTSR.BIT.CNTVAL ;
//				Sci_PutsText( UART_CH_DBG, "main loop\n" ) ;
				// 電源電圧値
//				xPrintf( UART_CH_DBG, " V24:%d\n",gAdc.Volt.uwV24 ) ; 
				// エンコーダ回転数とデューティ比
//				xPrintf( UART_CH_DBG, "Enc:%d Duty:%d \n", 
//										pEnc->wVrev , pPwm->wDutyExe ) ;
				// エンコーダ速度と目標指示速度
				// PWM変換値設定用に使用:PID制御をNONEにすること
//				xPrintf( UART_CH_DBG, "Rev:%d Enc:%d \n", 
//											 pFb->V.wVrev,pEnc->wVrev ) ;
				// 速度偏差と絶対値と積分値
//				xPrintf( UART_CH_DBG, "diff:%d abs:%d FbI:%d\n", 
//						pFb->V.wVdiff , pFb->V.wVdiffAbs , pFb->V.dwFbI ) ;

				// 速度偏差と絶対値と積分値
				xPrintf( UART_CH_DBG, "diff:%d REV:%d FbI:%d AccStat:%d\n", 
					pFb->V.wVdiff , pEnc->wVrev , pFb->V.dwFbI,pFb->V.wAccStat );
				// 速度偏差と絶対値と積分値
//				xPrintf( UART_CH_DBG, "Rev:%d REV:%d FbI:%d Stat:%d TYPE:%d\n", 
//					pFb->V.wVrev , pEnc->wVrev , pFb->V.dwFbI,pFb->V.wAccStat );

				// モータ電流値とオフセット
//				xPrintf( UART_CH_DBG, "dwCur:%d uwOfs:%d ADDR0:%d\n"
//								,pMot->dwCur ,pMot->uwOfs ,S12AD.ADDR0) ;
				// 電流オフセット
//				xPrintf( UART_CH_DBG, "uwOfs:%d \n",gAdc.Mot.uwOfs ) ;
				// 
//				xPrintf( UART_CH_DBG, "dwSum:%d ADDR0:%d\n",
//												pMot->dwSum , S12AD.ADDR0 ) ;
				// AD00
//				xPrintf( UART_CH_DBG, "ADDR0:%d\n", S12AD.ADDR0 ) ;

				// 電流制限値関係
//				xPrintf( UART_CH_DBG, "PwmLimit:%d LimitI:%d PwmCurAdj:%d\n", 
//						pFb->V.wPwmLimit , pFb->V.dwLimitI ,pFb->V.wPwmCurAdj) ;
				// 電流制限値関係 I制御
//				xPrintf( UART_CH_DBG, "dwFbI:%d LimitI:%d \n", 
//											pFb->V.dwFbI , pFb->V.dwLimitI ) ;
				// PWM制限値を計算
//				xPrintf( UART_CH_DBG, "PwmLimit:%d CurLimit:%d CurAdj:%d\n", 
//					pFb->V.wPwmLimit , pFb->V.wPwmCurLimit ,pFb->V.wPwmCurAdj) ;
				// AD値生データ
//				xPrintf( UART_CH_DBG, "dwCur:%d uwOfs:%d ADDR0:%dEnc:%d \n"
//						,pMot->dwCur ,pMot->uwOfs ,S12AD.ADDR0,pEnc->wVrev) ;
				// 各相のDuty値
//				xPrintf( UART_CH_DBG, "DutyU:%d DutyV:%d DutyW:%d\n",
//								pPwm->uwDutyU,pPwm->uwDutyV,pPwm->uwDutyW ) ;
				// 
//				xPrintf( UART_CH_DBG, "wDirU:%d gMph:%d \n",
//							pPwm->wDirU,gMph[pEnc->Inp.ubUVW].wIu ) ;
				// 各相の実装電流値
//				xPrintf( UART_CH_DBG, "flIu:%d flIuv:%d flIw:%d\n", 
//									pMot->flIu  , pMot->flIv  , pMot->flIw ) ;
				// 現在ステータス
//				xPrintf( UART_CH_DBG, "STAT:%d\n", pFb->P.ubMotStat ) ;
		
//				xPrintf( UART_CH_DBG, "mode:%d\n", gDf.SYS.ubDrvMode ) ;
				//現在地
//				xPrintf( UART_CH_DBG, "Pos:%d\n", gDf.POS.dwPos ) ;

				gDbg.CNT.dwCnt01 = 0;

			}
#endif	//_DBG_SYS_	

	
			// フィードバック処理
			if (gCMT0.uwIntFb >= FB_INTERVAL ) {
				
				pFb = &gFb ;					// FB情報 Ptr
				pEnc = &gEnc ;					// Enc情報 Ptr
				pMot = &gAdc.Mot ;				// モータAD情報
				pPwm = &gPwm ;					// PWM情報 Ptr

			// 停止時の電流計測 ( オフセット電流 )
				// 指示値が0かつ実速度が0に場合
				if (( pFb->V.wPwm == 0 ) && (pEnc->wVrev == 0 )) {		
					AD12_GetOffset( ) ;			// Offset,電圧計測
				//オフセットカウントが16回以上で平均オフセット電流を求める
					if ( gAdc.Mot.uwOfsCount >= 16 ) {	// OffsetAvgSumCount
						gAdc.Mot.uwOfs = gAdc.Mot.udwOfsSum / 
								gAdc.Mot.uwOfsCount ;		// curr. Offset
						gAdc.Mot.udwOfsSum = 0 ;			// OffsetAvgSum
						gAdc.Mot.uwOfsCount = 0 ;			// OffsetAvgSumCount
					}
				}
			
//				sLowRevADC( pFb, pEnc, pMot ) ;
//					if ( pFb->P.bEnable == TRUE ) {			// 作動禁止
//						Sci_PutsText( UART_CH_DBG, "MainProg_True.\n" ) ;
//					}

			//	サーボロック用関数 成功できていない
//				Int_ServoLock( ) ;					// サーボロックをかける
				xDriveCtl( pFb, pEnc, pMot ) ;
				xFeedback_STOP( pFb, pEnc ) ;		// 停止処理
				xFeedback_Drv( pFb, pEnc, pMot) ;	// 加減速処理
				xFeedback_V( pFb, pEnc, pMot) ;		// 速度FB処理

				gCMT0.uwIntFb = 0 ;					// FeedBackタイマ
			
			// 定期的に確認を行うエラー
			bErr_Judge = xErr_Count( ) ;		// 数カウント毎

			//モータ状態別エラー
			switch ( gFb.P.ubMotStat ){
				case STAT_MOT_STOP :
				case STAT_MOT_STOP_EMM :
				case STAT_MOT_STOP_SWDEV :
				case STAT_MOT_STOP_MODE :
					bErr_Judge = xErr_Stop( ) ;
					break ;
				case STAT_MOT_UP :
				case STAT_MOT_UP_ACC_RESERVE :	
				case STAT_MOT_UP_ACC_START :
				case STAT_MOT_UP_ACC :
				case STAT_MOT_UP_CV	 :
				case STAT_MOT_UP_DEC_RESERVE :	
				case STAT_MOT_UP_DEC :	
				case STAT_MOT_UP_STOP_RESERVE :
				case STAT_MOT_UP_STOP :
					bErr_Judge = xErr_Up( ) ;	
					break ;
				case STAT_MOT_DW :	
				case STAT_MOT_DW_ACC_RESERVE :	
				case STAT_MOT_DW_ACC_START :
				case STAT_MOT_DW_ACC :	
				case STAT_MOT_DW_CV	 :
				case STAT_MOT_DW_DEC_RESERVE :	
				case STAT_MOT_DW_DEC :	
				case STAT_MOT_DW_STOP_RESERVE :
				case STAT_MOT_DW_STOP :
					bErr_Judge = xErr_Down( ) ;	
					break ;
			default :
					break ;
			}	
//				switch ( gSysInf.fMotDrv ) {
//					case SYS_MOT_STOP :			// 停止時
//						bErr_Judge = xErr_Stop( ) ;
//						break ;	
//					case SYS_MOT_UP :			// 上昇時
//						bErr_Judge = xErr_Up( ) ;	
//						break ;
//					case SYS_MOT_DOWN :			//	下降時
//						bErr_Judge = xErr_Down( ) ;	
//						break ;
//					default :
//						break ;
//				}	
		
			}


			// エラーがあった場合には停止ルーチンへ入る
			if ( bErr_Judge ) {
				xErrorStop( ) ;
			}
		
			// オートパワーオフタイマ(1Sec)が指定カウントに達したら電源を落とす
			if (( gCMT0.T1S.uwPWROFF >= AUTO_PWR_OFF_TIMER ) && 
						( !pEnc->wVrev ) && ( !pPwm->wDutyExe ) &&
										 ( BRAKE_PODR == BRAKE_LOCK )) {
				gSysInf.fPwrOff = AUTO_POWER_OFF ;				// 電源遮断状態
				xPowerDown( ) ;
			}
		
//			//試作基板用−耐久テスト時使用
//			//DIPが使用できないのでデバックモードへの入り方を考える
//			if ((gDip.S1 == PORT_IN_HIGH) && (gDip.S2 == PORT_IN_HIGH) &&
//				(gDip.S3 == PORT_IN_HIGH) && (gDip.S4 == PORT_IN_HIGH) &&
//				(gDip.S5 == PORT_IN_HIGH) && (gDip.S6 == PORT_IN_HIGH) &&
//				(gDip.S7 == PORT_IN_HIGH) && (gDip.S8 == PORT_IN_HIGH)) {
//				xCmdCon( ) ;						//デバックモードに切り替える
//			}
		} else {
			
			if ( gDbg.FLG.fDbg01 != TRUE ) {
				xDF_ParmWriteExe( ) ;				// データフラッシュ書込
				Sci_PutsText( UART_CH_DBG, "ERR_LOOP\n" ) ;
				gDbg.FLG.fDbg01 = TRUE ;
			}
			// システム遮断
			if ( ( gSwDev.Lvl.ubI04 == EMM_SW_ON ) &&
				 ( gSwDev.Long.ubI01 == SWDEV_LPUSH_YES ) &&
				 ( gSwDev.Long.ubI03 == SWDEV_LPUSH_NO ) &&
				 ( gSwDev.Long.ubI02 == SWDEV_LPUSH_YES )) {
					gSysInf.fPwrOff = ERR_SHUT_DOWN	;	// エラーシャットダウン	
					xPowerDown( ) ;						// Power Down
			}
			// LED 点灯
			Int_LED_Flash( ) ;
			// ステータスをエラーにする
			// 現状ではメインループには戻れないので、あまり意味がない
			gFb.P.ubMotStat = STAT_MOT_ERR ;		// 現在の駆動ステータス

		}
							//ウォッチドッグタイマのクリア
		WDT.WDTRR = 0x00;							// リフレッシュ（WDT 開始）
		WDT.WDTRR = 0xFF;							// WDT再起動
		nop() ;
	}

}

// end main program

//================================================================
// オプション設定メモリ OFS1はデフォルト
//================================================================
#pragma address OFS1_REG = 0xFFFFFF88   /* OFS1 register */
const unsigned long OFS1_REG = 0xFFFFFFFF;

//----------------------------------------------------------------
// ウォッチドッグタイマの設定
// 独立ウォッチドッグタイマは使用しない
// ウォッチドッグタイマのみ使用する
// 16384(3FFFh)サイクル PCLK/128 ウインドウ未使用 リセット許可
// PCLK25MHzなので 0.04μsec*128*16384 = 83.8msec でリセット
//----------------------------------------------------------------
#pragma address OFS0_REG = 0xFFFFFF8C   /* OFS0 register */
const unsigned long OFS0_REG = 0xFFFDFFFF;

/* End of File */
