//=====================================================================
//    
//    SCI(UART) Configurations
//    
//=====================================================================

// 多重定義対策
#ifndef _DEFINE_SCI_RX_CONF_
	#define _DEFINE_SCI_RX_CONF_

#include	"TypeDef.h"					// アプリケーション用型定義




//-----------------------------------------------------------
//	SCI処理用定数定義
//-----------------------------------------------------------

// 送受信バッファサイズ
#define SCI_RX_BUF_SIZE		64				// 受信バッファサイズ(16未満禁止)
//#define SCI_TX_BUF_SIZE		80				// 送信バッファサイズ
//#define SCI_TX_BUF_SIZE		120				// 送信バッファサイズ
#define SCI_TX_BUF_SIZE		180				// 送信バッファサイズ


// サポートチャンネル数定義 適宜変更する
#define	UART_CH_MAX			3				// UART最大使用チャンネル数
#define	UART_MCU_CHANNELS	10				// MCU内蔵Ch数

// 割込設定値 --- SCIの全種類の共通割り込みレベル
#define	SCI_INT_LVL			6				// 割込レベル

// 送信リトライ値
#define	SCI_PUT_RETRY_NUM	1000			// Sci_Putc() Retry数



/*
//-----------------------------------------------------------
//	RS485処理用定数定義
//	※ 必要に応じてポート定義を修正すること
//-----------------------------------------------------------
//	RS485使用チャンネル設定
//#define	RS485_CH0							// UART0をRS485として使用
//#define	RS485_CH1							// UART1をRS485として使用
//#define	RS485_CH2							// UART2をRS485として使用
#define	RS485_CH5							// UART5をRS485として使用
#define	RS485_CH6							// UART6をRS485として使用

//	RE,DE制御ポート定義
//	ポートの出力設定はUART初期化前に済ましておくこと
#ifdef	RS485_CH5
	#ifndef	RS485_USE
		#define	RS485_USE
	#endif	// #ifdef	RS485_USE
	#define	RS485_CH5_RE_PDR	PORTA.PDR.BIT.B1
	#define	RS485_CH5_RE_PODR	PORTA.PODR.BIT.B1
	#define	RS485_CH5_DE_PDR	PORTE.PDR.BIT.B4
	#define	RS485_CH5_DE_PODR	PORTE.PODR.BIT.B4
#endif		// #ifdef	RS485_CHx
#ifdef	RS485_CH6
	#ifndef	RS485_USE
		#define	RS485_USE
	#endif	// #ifdef	RS485_USE
	#define	RS485_CH6_RE_PDR	PORTB.PDR.BIT.B3
	#define	RS485_CH6_RE_PODR	PORTB.PODR.BIT.B3
	#define	RS485_CH6_DE_PDR	PORTB.PDR.BIT.B5
	#define	RS485_CH6_DE_PODR	PORTB.PODR.BIT.B5
#endif		// #ifdef	RS485_CHx


*/




#endif	// _DEFINE_SCI_RX_CONF_

//--------------------------------------------------------------------------
