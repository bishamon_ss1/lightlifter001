//=======================================================================
//
//	関数プロトタイプ
//	
//=======================================================================


#ifndef	_DEF_FUNC_PROTOTYPE_HDR_						// 多重定義対策
 #define	_DEF_FUNC_PROTOTYPE_HDR_

//#include	"StartUp.h"								// アプリケーションヘッダ

//========================================================================
//	関数プロトタイプ	RX210_DRV.c
//========================================================================

//---------------------------------------------------
//	フィードバック情報のリセット
//---------------------------------------------------
void	xFB_ResetInf(  volatile FB *pFb ) ;


//---------------------------------------------------
//	走行用速度フィードバック情報セットアップ処理
//	引数
//		BYTE    bStopState	
//			FB_STOP_NORMAL			減速停止する
//			FB_STOP_EMM				非常停止する
//			FB_STOP_POE				POEによる停止
//	戻り値
//---------------------------------------------------
void	xFeedback_Drv_Stop( BYTE bStopState ) ;

//---------------------------------------------------
//	走行用速度フィードバック情報セットアップ処理
//	引数
//		WORD    pwVrev		指示速度(rpm)
//							−値はバック
//							*pwVrev    :R
//	戻り値
//---------------------------------------------------
void	xFeedback_Drv_Setup( WORD *pwVrev ) ;

//---------------------------------------------------
//	停止フィードバック処理
//	引数
//		FB			*pFb		FB構造体ptr
//		ENC_INF		*pEnc		エンコーダ構造体ptr
//		ADC_MOT		*pMot		A/D変換軸固有値Ptr
//	戻り値
//---------------------------------------------------
void	xFeedback_STOP(	FB *pFb, ENC_INF *pEnc ) ;



void	xDriveCtl( FB *pFb, ENC_INF *pEnc, ADC_MOT *pMot ) ;

//---------------------------------------------------
//	走行用速度フィードバック処理
//	引数
//		FB			*pFb		該当軸のFB構造体ptr
//		ENC_INF		*pEnc		エンコーダ構造体ptr
//		ADC_MOT		*pMot		A/D変換軸固有値Ptr
//	戻り値
//---------------------------------------------------
void	xFeedback_Drv( 	FB *pFb, ENC_INF *pEnc,	ADC_MOT *pMot) ;

//---------------------------------------------------
//	速度フィードバック処理
//	引数
//		FB			*pFb		FB構造体ptr
//		ENC_INF		*pEnc		エンコーダ構造体ptr
//		ADC_MOT		*pMot		A/D変換軸固有値Ptr
//	戻り値
//---------------------------------------------------
void	xFeedback_V( FB *pFb, ENC_INF *pEnc, ADC_MOT *pMot) ;


//========================================================================
//	関数プロトタイプ		 hw&int.c 
//========================================================================

//---------------------------------------------------
// システムクロック設定
//---------------------------------------------------
void	Inz_SysClock( void ) ;


//---------------------------------------------------
//	I/Oポート初期化
//---------------------------------------------------
void	Inz_IO_Port( void ) ;

//---------------------------------------------------
// ウォッチドッグタイマのセットアップ
//---------------------------------------------------
void	Inz_WDT( void ) ;

//---------------------------------------------------
//	CRC演算器のセットアップ
//---------------------------------------------------
void	Inz_CRC( void ) ;

//---------------------------------------------------
//	CMTU0_CMT0 初期化
//---------------------------------------------------
void	Inz_CMT0( void ) ;

//---------------------------------------------------
//  コンペアマッチタイマ（CMT0)のスタート
//---------------------------------------------------
void Inz_CMT1(void);

//---------------------------------------------------
//  コンペアマッチタイマ（CMT2)のスタート
//---------------------------------------------------
void Inz_CMT2(void);

//---------------------------------------------------
//	MTU2 ch1 フリータイマランカウンタ
//	ホールセンサ60度の時間計測用
//	PCLK=25MHz(40ns)
//---------------------------------------------------
void	Inz_MTU1( void );

//---------------------------------------------------
//	タイマMTU2 ch0 TGRAコンペアマッチカウンタ(TGIA割込有り)
//	ロータ電気角１度毎の割込用
//	現在はPCLK=25MHz(40ns)
//---------------------------------------------------
void	Inz_MTU0( void ) ;

//---------------------------------------------------
//	12bitAD変換器 初期化
//	シングルシャント計測用
//	1サイクルスキャンモードで一気に変換させる
//---------------------------------------------------
void	Inz_AD12( void ) ;

//---------------------------------------------------
//	モータ電流オフセット計測			※10/15移植
//	モータ軸０(Axis0)		AN0
//	電源電圧(24V)			AN1
//---------------------------------------------------
void	AD12_GetOffset( void );

//---------------------------------------------------
//	PWM情報の初期化
//---------------------------------------------------
void	Inz_PWM_Inf( void ) ;

//---------------------------------------------------
//	タイマMTU3 MTU3-4 相補PWM 初期化	
//	
//	MTUのクロックはPCLK=25MHz
//	カウント周期は40ns
//  PWM周期=12.5Kz(80μS)
//---------------------------------------------------
void	Inz_PWM( void ) ;

//---------------------------------------------------
//	エンコーダ用割込み入力セットアップ
//	ENC0: U=IRQ6, V=IRQ5, W=IRQ4(Vector:70,69,68)
//---------------------------------------------------
void	Inz_Enc( void );

//---------------------------------------------------
//	PSW プロセッサステータスワード設定
//	プロセッサの割り込み設定をする
//---------------------------------------------------
void	Inz_PSW( void ) ;

//---------------------------------------------------
//	ソフトリセット実行
//	引数
//---------------------------------------------------
void	xSoftReset( void ) ;

//---------------------------------------------------
//	データのCRC演算値をセット
//	演算はITU-T準拠のx16+x12+x5+1
//	引数
//		UBYTE *pubData			データPtr
//		UWORD uwSize			演算バイト数
//	戻り値
//		CRC演算結果
//---------------------------------------------------
UWORD	xMakeCRC( UBYTE *pubData, UWORD uwSize ) ;

//---------------------------------------------------
//  電源遮断   制御戻らない
//---------------------------------------------------
void	xPowerDown( void ) ;

//---------------------------------------------------
//  接点入力デバイス状態取得
//---------------------------------------------------
void	xSwDevStat( void ) ;

//---------------------------------------------------
//	入力ポートをフィルタ処理して取得 (チャタリング対策)
//	各入力データをビットシフトして貯めていって全て一致
//	したら値を確定する
//	インターバルタイマからコールされる
//  入力から5回信号を左シフトしながら見ていく
//  一定時間ON(1f：11111:INP_FCHK_SW_VAL)になった時点で入力を判定する。
//  検出頻度はCMT0のマイコンペアマッチ毎
//---------------------------------------------------
static void	stSwDevGet( void ) ;


//---------------------------------------------------
// 電圧監視１割り込み/リセット設定
// 割り込み先で無限ループに入る設定
// CPUの動作電圧が2.7〜5.5V
// 今回は電圧降下時3.70Vにて動作する仕様とする
//---------------------------------------------------
void	Inz_LVD1( void ) ;

//---------------------------------------------------
// ポートアウトプットイネーブルセットアップ
// POE2#端子(PORTA.6)に立下りエッジの入力があった場合に
// POEの作動でMTUOC3B,D 4A,C 4B,Dがハイインピーダンスになる
//
// ※POE2にDown Edgeが入るとPOE2Fレジスタに"1"が立つ
//	 その際に割り込みが発生するので、DFへの記憶を行う
//	 POEを解除するにはPOE2Fに"0"を書き込む必要がある
//---------------------------------------------------
void	Inz_POE( void ) ;

//---------------------------------------------------
// 割り算処理
//	引数	UWORD 割られる値(分子)	UWORD 割る値(分母)
//	戻り値	UWORD 結果(商)
//---------------------------------------------------
UWORD xDivide( UWORD uwdata , UWORD uwdiv ) ;

//---------------------------------------------------
//	Enc情報リセット
//	引数
//		ENC_INF *pEnc		エンコーダ構造体ptr
//	戻り値
//---------------------------------------------------
void	xEnc_ResetInf( volatile ENC_INF *pEnc ) ;

//---------------------------------------------------
//	モータ励磁
//	引数
//---------------------------------------------------
void	Int_MotExite( void ) ;


//---------------------------------------------------
//	サーボロック
//	引数
//---------------------------------------------------
void	Int_ServoLock( void ) ;

//---------------------------------------------------
//	走行用電流フィードバック処理
//	入力:pFb->wPwm
//	出力:gPwm.wDutyExe (pPwm->wDutyExe)
//	引数
//		FB			*pFb		該当軸のFB構造体ptr
//		ENC_INF		*pEnc		エンコーダ構造体ptr
//	戻り値
//---------------------------------------------------
void	xFeedback_Pwm( 	FB *pFb, ENC_INF *pEnc ) ;

//---------------------------------------------------
//	AD変換値を読んで各相の電流,電圧値をセット
// PMSM モータの単一シャントセンサレスベクトル制御のP8を参照
// 参考ソースコード：motorcontrol.cのvoid MC_readc_SS()
//---------------------------------------------------
static void	Int_ReadAdc( void ) ;

//---------------------------------------------------
//	AD変換変換開始タイミングセット		
//	TCNTとのコンペアマッチで変換起動させる
// 参考ソースコード：motorcontrol.cのMC_Set_SS()
//---------------------------------------------------
static void	Int_SetAdStart( void ) ;

//-------------------------------------------------------------------------
// LED点滅用プログラム
// 現在はエラー時のみであるが、通常時の点灯もあるので、
// 将来的には割り込みを利用する可能性がある
//
// 現在は1サイクルの時間を決めているため、インターバルは点灯時間の長さによって
// 変動をすることとなる
//-------------------------------------------------------------------------
void Int_LED_Flash( void ) ;

//========================================================================
//						 LL_ERR.c 
//========================================================================

//---------------------------------------------------
//	エラー処理
//	制御は戻さない
//---------------------------------------------------
UBYTE	xErrorStop( void ) ;

//---------------------------------------------------
//	LED表示初期化(表示パターンのセット)
//	引数	ubMode	
//	戻り値
//		0		設定有効
//		1		設定無効
//	5bit目がエラーの有無を判断するbit
//	1〜4bitがLEDの点滅ステータスとなる
//	エラーがあった場合にはLEDのHIGHが点灯する
//---------------------------------------------------
void xLedInfInz( BYTE ubMode ) ;

//---------------------------------------------------
//	エラー発生後の無限ループ
//	非常停止ON中で上昇と下降が長押しの場合でソフトウェアリセットとなる
//---------------------------------------------------
void	xLoopInfinity( void ) ;

//---------------------------------------------------
//	エラー検出用 常時
//	引数
//	戻り値
//		0			エラー無し
//		それ以外	エラーあり(エラーコード)
//---------------------------------------------------
BYTE	xErr_All( void ) ;

//---------------------------------------------------
//	エラー検出用 数カウントに一回
//	引数
//	戻り値
//		0			エラー無し
//		それ以外	エラーあり(エラーコード)
//---------------------------------------------------
BYTE	xErr_Count( void ) ;

//---------------------------------------------------
//	エラー検出用 停止時
//	戻り値
//		0			エラー無し
//		それ以外	エラーあり(エラーコード)
//---------------------------------------------------
BYTE	xErr_Stop( void ) ;

//---------------------------------------------------
//	エラー検出用 上昇時
//	戻り値
//		0			エラー無し
//		それ以外	エラーあり(エラーコード)
//---------------------------------------------------
BYTE	xErr_Up( void ) ;

//---------------------------------------------------
//	エラー検出用 下降時
//	戻り値
//		0			エラー無し
//		それ以外	エラーあり(エラーコード)
//---------------------------------------------------
BYTE	xErr_Down( void ) ;



//========================================================================
//						 RX210_IOCTL.c
//========================================================================
//---------------------------------------------------
// DIP-SW状態確認							テスト用
// 電源投入時に一度だけDipの状態を確認する
//---------------------------------------------------
void	xIoCtl_DipCheck( void );
//---------------------------------------------------
//	昇降停止			
//	引数
//	戻り値 
//---------------------------------------------------
void	xIoCtlEMM( void ) ;

//---------------------------------------------------
// モータ回転速度変更モード
//---------------------------------------------------
void	xModeChange( void ) ;



//---------------------------------------------------
//	I/O制御
//		リフト基本制御
//	引数
//	戻り値 
//---------------------------------------------------
void	xIoCtl( void ) ;



//========================================================================
//						 RX210_Flash.c
//========================================================================

//---------------------------------------------------
//	データフラッシュ情報構造体を初期値でセット
//	引数
//	戻り値
//---------------------------------------------------
void xDF_SetParmInz( void ) ;

//---------------------------------------------------
//	データフラッシュ情報からパラメータ計算
//	引数
//	戻り値
//---------------------------------------------------
void	xDF_ParmCalc( void ) ;

//---------------------------------------------------
//	データフラッシュ初期化(消去)
//	引数
//	戻り値
//---------------------------------------------------
//void	xDF_Inz( void ) ;

//---------------------------------------------------
//	パラメータデータのデータフラッシュへの書き込み
//	引数
//	戻り値
//---------------------------------------------------
void	xDF_ParmWrite( void ) ;

//---------------------------------------------------
//	パラメータデータのデータフラッシュへの書き込み
//	引数
//	戻り値
//---------------------------------------------------
void	xDF_ParmWriteExe( void ) ;

//---------------------------------------------------
//	データフラッシュからパラメータデータへの読み込み
//	引数
//	戻り値
//---------------------------------------------------
void	xDF_ParmReadMain( void ) ;

//---------------------------------------------------
//	パラメータ番号からパラメータメンテ情報取得
//	引数
//		UWORD		uwIdx		パラメータ番号
//		DF_MAINT	*pMaint		パラメータメンテ情報
//	戻り値
//		BOOL					TRUE/FALSE
//---------------------------------------------------
BOOL	xDF_GetParmInf( UWORD uwIdx, DF_MAINT *pMaint ) ;

//---------------------------------------------------
//	指定パラメータ番号へデータを格納
//	引数
//		UWORD		uwIdx		パラメータ番号
//		WORD		wData ;		パラメータデータ
//	戻り値
//		BOOL					TRUE/FALSE
//---------------------------------------------------
BOOL	xDF_PutParmData( UWORD uwIdx, WORD wData ) ;

//========================================================================
//							 LL_CMD.c
//========================================================================

//---------------------------------------------------
//	入力ラインバッファの中から次の数値を読み出す
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//		DWORD					数値
//								変換不可の場合はCMDPRC_ERR_ARGUMENT
//---------------------------------------------------
DWORD	xGetNumeric( CHAR **ppcCmdLine ) ;

#endif	// _DEF_FUNC_PROTOTYPE_HDR_

//---------------------------------------------------
