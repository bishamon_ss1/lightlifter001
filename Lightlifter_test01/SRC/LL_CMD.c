//=======================================================================
//
//  FILE        : MB_CMD.c
//  DATE        : 2014.10.30
//  DESCRIPTION : コンソールコマンド 処理
//	Device(s)	: RX210 64pin
//
//=======================================================================

#include	<machine.h>						// Include machine.h
#include	"_iodefine.h"					// Include _iodefine.h

#include	"LL.h"							// アプリケーションヘッダ
#include	"ConCmd_Prc.h"					// アプリケーションヘッダ
#include	"LL_prototype.h"				//関数プロトタイプ



//--------------------------------------------------------------------------
//	マクロ定義
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//	定数定義
//--------------------------------------------------------------------------

// CMD pwm モード定義
#define	CMD_PWM_NONE_PARM		0		// 数値パラメータ無し(表示)
#define	CMD_PWM_EXE				1		// PWM操作
#define	CMD_PWM_PARM_ERR		9		// パラメータエラー

// CMD out モード定義
#define	CMD_OUT_NONE_PARM		0		// ポート指定無し(表示)
#define	CMD_OUT_SHOW			1		// ポート指定あり(表示)
#define	CMD_OUT_EXE				2		// ポート指定あり(設定)
#define	CMD_OUT_PARM_ERR		9		// パラメータエラー

// CMD df モード定義
#define	CMD_DF_NONE_PARM		0		// パラメータ無し
#define	CMD_DF_SHOW				1		// 番号指定の表示
#define	CMD_DF_WRITE			2		// 番号指定の書き込み
#define	CMD_DF_ERASE_ALL		3		// 全消去
#define	CMD_DF_FLUSH			4		// DF書き込み
#define	CMD_DF_PARM_ERR			9		// パラメータエラー



//-----------------------------------------------------------
//	グローバル変数,定数定義
//-----------------------------------------------------------

//---------------------------------------------------
// コマンド解析回りの定義
//---------------------------------------------------

// コマンド処理関数 prototype
static void	sCmdDataFlush( CHAR **ppcLine ) ;		// DF表示,メンテ
//static void	sCmdSensorStatus( CHAR **ppcLine ) ;	// センサステータス表示
static void	sCmdOutPort( CHAR **ppcLine ) ;			// 出力ポートテスト
static void	sCmdPwm( CHAR **ppcLine ) ;				// PWM操作
static void	sCmdDipShow( CHAR **ppcLine ) ;			// DIP-SW情報の表示
static void	sCmdReset( CHAR **ppcLine ) ;			// ソフトリセット
static void	sCmdHelp( CHAR **cppLine ) ;			// HELP コマンド一覧表示
static void	sCmdError( CHAR **ppcLine ) ;			// コマンドエラーの表示
//static void	sCmdTimer( CHAR **ppcLine ) ;	// インターバル間隔の変更 9/21追加


// コマンド解析ルーチンのための関数テーブル
static const	CMD_TBL	mCmdTbl[] = {
					{"DF",	sCmdDataFlush },
					{"MM",	sCmdDataFlush },
//					{"SS",	sCmdSensorStatus },
					{"OUT",	sCmdOutPort },
					{"PWM",	sCmdPwm },
					{"DIP",	sCmdDipShow },
					{"RESET",	sCmdReset },
					{"RESTART",	sCmdReset },
					{"HELP",	sCmdHelp },
//					{"TT",	sCmdTimer },		// 9/21追加
					{"\0",	sCmdError }			// これは必須
		} ;



//===========================================================================


//====================================================
//	コマンドコンソール  メインに戻らない
//	引数
//	戻り値
//  デバック時のコマンド用プログラム
//====================================================
void	xCmdCon( void ) 
{
	CHAR			cLine[80] ;					// 入力ラインバッファ
	CHAR			**Dummy ;
	FLAG			fRet ;
	
	
	// 初期化を行う
	// UARTの場合バッファサイズを80とする
	xConCmdPrc_Init( UART_CH_DBG, 80, &cLine[0], &mCmdTbl[0] ) ;
	Sci_PutsText( UART_CH_DBG, "\n  == Command Processor ==\n") ;
	Sci_PutsText( UART_CH_DBG, 
				" Enter the 'help' to show available commands.\n") ;
	
	//-----------------------------------
	//	Loop of main
	//-----------------------------------
	while(1) {
		fRet = xConCmdProc_Exe( ) ;				// CONコマンド解析&実行
		
		//EXITが入力された場合、終了を行う
		//Breakがコメントアウトの為、現在は無限ループ
		if ( fRet == CMDPRC_EXE_EXIT_YES ) {
			sCmdReset( Dummy ) ;				// ソフトリセット
			Sci_Puts( UART_CH_DBG, "\r\n" ) ;	// 改行
			break ;								//break;を消している為永久ループ
		}
	}
	
	
}

//******** Command Routine *************


//====================================================
//	パラメータエラー表示
//	引数
//	戻り値
//====================================================
static void	sSub_ShowParmErr( void ) 
{
	
	Sci_PutsText( UART_CH_DBG, "   Err! Unknown parameter.\n") ;
	
}


//---------------------------------------------------
//	2BYTE(16bit) １０進桁数を返す
//	引数
//		WORD	wData			調査数値
//	戻り値
//		WORD					桁数, マイナス値はマイナス桁数
//---------------------------------------------------
static WORD	sGetDecLen( WORD wData )
{
	WORD		wRet, wSign ;
	
	
	if ( wData < 0 ) {
		wSign =  -1 ;
		wData *= -1 ;
	} else {
		wSign =  1 ;
	}
	
	if ( wData >= 10000 ) {
		wRet = 5 ;
	} else if ( wData >= 1000 ) {
		wRet = 4 ;
	} else if ( wData >= 100 ) {
		wRet = 3 ;
	} else if ( wData >= 10 ) {
		wRet = 2 ;
	} else {
		wRet = 1 ;
	}
	
	
	return (wRet * wSign) ;
	
}


//---------------------------------------------------
//	符号無し2BYTE(16bit) １０進桁数を返す
//	引数
//		UWORD	uwData			調査数値
//	戻り値
//		WORD					桁数
//---------------------------------------------------
static WORD	sGetDecLen_U( UWORD uwData )
{
	WORD		wRet ;
	
	
	if ( uwData >= 10000 ) {
		wRet = 5 ;
	} else if ( uwData >= 1000 ) {
		wRet = 4 ;
	} else if ( uwData >= 100 ) {
		wRet = 3 ;
	} else if ( uwData >= 10 ) {
		wRet = 2 ;
	} else {
		wRet = 1 ;
	}
	
	
	return wRet ;
	
}


//---------------------------------------------------
//	データフラッシュメモリの表示
//	引数
//		UWORD		uwIdx		パラメータ(メモリ)番号
//		FLAG		fRemote 	リモート処理モード
//						CMD_DFR_LOCAL	ローカル処理(Main制御基板)
//						CMD_DFR_MAIN	メインセンサ処理(Mainセンサ基板)
//						CMD_DFR_SUB		サブセンサ処理(Subセンサ基板)
//	戻り値
//		BOOL					TRUE/FALSE
//---------------------------------------------------
//static BOOL	sCmdDataFlush_Show( UWORD uwIdx, FLAG fRemote ) 
static BOOL	sCmdDataFlush_Show( UWORD uwIdx ) 
{
	DF_MAINT		Maint ;					// メンテ情報
	DWORD			dwData ;				// 表示値
	BOOL			boRet ;					// 戻り値
	WORD			wLen ;					// 桁数
	WORD			wLoop ;					// ループ変数

	
	// データ取得
	//パラメータ番号をフラッシュより取得
	//入力されている番号がケースに引っかからなければFALSE
	boRet = xDF_GetParmInf( uwIdx, &Maint ) ;
		if ( boRet == FALSE ) {
			Sci_PutsText( UART_CH_DBG, " connect Error!\n") ;
		} else {
			if ( Maint.fType == DF_TYPE_NO_DATA ) {
				boRet = FALSE ;						// 戻り値
			}
		}
	
	
	// データ表示
	if ( boRet == TRUE ) {
		//符号付の場合
		if ( Maint.fType == DF_TYPE_SIGHNED ) {
			//パラメータの入力値の桁数を表示
			wLen = sGetDecLen( Maint.wData ) ;		// 桁数
			if ( wLen < 0 ) {
				wLen = (-1 * wLen) + 1 ;			// 桁数
			}
			dwData = Maint.wData ;					// 表示値
		} else {	//符号なしの場合
			wLen = sGetDecLen_U( (UWORD)Maint.wData ) ;	// 桁数
			dwData = (UWORD)Maint.wData ;			// 表示値
		}
		//パラメータ番号とその値を表示する。
		xPrintf( UART_CH_DBG, " No.%03d, Value: %d",
							uwIdx, dwData ) ; 
		//桁数が0になるまでスペースを打ち込む
		for ( wLoop = 7 - wLen ; wLoop ; wLoop-- ) {
			xPutc( UART_CH_DBG, ' ' ) ;
		}
		//パラメータ値を表示させる
		xPrintf( UART_CH_DBG, "(0x%04x)", dwData ) ; 
		//符号の有無を判断する
		if ( Maint.fType == DF_TYPE_SIGHNED ) {
			Sci_PutsText( UART_CH_DBG, " -- signed\n") ;
		} else {
			Sci_PutsText( UART_CH_DBG, " -- unsigned\n") ;
		}
	}
	
	return boRet ;
	
}


//---------------------------------------------------
//	値セット
//	引数
//		UWORD		uwIdx		パラメータ番号
//		WORD		wVal		セット値
//		DF_MAINT	Maint 		メンテ情報
//	戻り値
//		BOOL					TRUE/FALSE
//---------------------------------------------------
static BOOL	sCmdDataFlush_Put( UWORD uwIdx, 
								WORD wVal, 
								DF_MAINT Maint ) 
 
{
	BOOL			boRet ;						// 戻り値
	
	
	// 変更前データ表示と値の範囲チェック
	Sci_PutsText( UART_CH_DBG, "  Original ") ;
	boRet = sCmdDataFlush_Show( uwIdx/*, fRemote*/ ) ;
	if ( boRet == FALSE ) {
		return FALSE ;
	}
	gCMT0.uwScanTime = 0 ;						// ScanTime
	while ( gCMT0.uwScanTime < 10 ) {
		nop( ) ;
	}
	xPrintf( UART_CH_DBG, "  Modify to %d(0x%04x)", wVal, wVal) ;
	if ( (wVal < Maint.wMin) || (wVal > Maint.wMax) ) {
		Sci_PutsText( UART_CH_DBG, " is out of range.") ;
		xPrintf( UART_CH_DBG, " (%d - %d)", 
								Maint.wMin, Maint.wMax) ;
		return FALSE ;
	}
	
	// データを変更
	boRet = xDF_PutParmData( uwIdx, wVal ) ;	// put
	
	// 結果表示
	if ( boRet == TRUE ) {
		Sci_PutsText( UART_CH_DBG, " was Successed.\n") ;
	} else {
		Sci_PutsText( UART_CH_DBG, " was Failed.\n") ;
	}
	
	
	return boRet ;
	
}


//---------------------------------------------------
//	データフラッシュメモリの消去(初期化)
//	引数
//	戻り値
//---------------------------------------------------
static void	sCmdDataFlush_EraseAll( void ) 
{
	CHAR			*pcBuf ;					// 入力バッファPtr
	CHAR			*pcBuf2 ;					// 次の入力バッファPtr
	BOOL			boRet ;						// 戻り値
	
	
	// 実行の確認
	Sci_PutsText( UART_CH_DBG, "  All Data will destroy!\n") ;
	Sci_PutsText( UART_CH_DBG, "  Are you sure ? (YES/NO):") ;
	xConLineInput( ) ;							// ライン入力
	pcBuf = xConGetLineBufPtr( ) ;				// 入力バッファPtr
	pcBuf2 = xStrCmp( "YES", pcBuf ) ;			// 引数チェック
	if ( pcBuf2 == (CHAR *)NULL ) {				// "YES"でない
		Sci_PutsText( UART_CH_DBG, "  Aborted.") ;
		return ;
	}
	
	// 消去実行
	xDF_ParmWrite( ) ;							// 消去＆書込
	boRet = TRUE ;

	if ( boRet == TRUE ) {
		Sci_PutsText( UART_CH_DBG, "  Done.") ;
	} else {
		Sci_PutsText( UART_CH_DBG, "  Failed.") ;
	}
	
	
}


//---------------------------------------------------
//	データフラッシュへの書き込み
//	引数
//	戻り値
//---------------------------------------------------
static void	sCmdDataFlush_Writeback( void ) 
{
	BOOL			boRet ;							// 戻り値

	// 書き込み実行
	xDF_ParmWriteExe( ) ;								// write
	boRet = TRUE ;

	if ( boRet == TRUE ) {
		Sci_PutsText( UART_CH_DBG, "  Done.") ;
	} else {
		Sci_PutsText( UART_CH_DBG, "  Failed.") ;
	}
	
	
}


//---------------------------------------------------
//	データフラッシュメモリの表示,メンテ
//	書式:
//		df {mem_num {value} | flush | erase}	
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//---------------------------------------------------
static void	sCmdDataFlush( CHAR **ppcLine ) 
{
	FLAG			fMode ;								// 実行モード
	DWORD			dwMemNum ;							// メモリ番号
	DWORD			dwVal ;								// 引数数値
	CHAR			*pcBuf ;							// 次の入力バッファPtr
	BOOL			boRet ;								// 戻り値
	DF_MAINT		Maint ;								// メンテ情報
	WORD			wMaxNum ;							// 最大メモリ番号
	
	
	// 引数チェック
	Sci_PutsText( UART_CH_DBG, 
					" DataFlash-Memory(for parameter) show & maint.\n") ;
	if ( xIsLineEnd( ppcLine ) == 0 ) {					// 行末
		fMode = CMD_OUT_NONE_PARM ;						// パラメータ無し(表示)
	} else {
	
	//スペースを飛ばしていき、行末まで辿り着いてしまったらパラメータなし
	xSkipSpace( ppcLine ) ;								// ブランク読み飛ばし
		if ( xIsLineEnd( ppcLine ) == 0 ) {				// 行末
			fMode = CMD_OUT_NONE_PARM ;					// パラメータ無し(表示)
		} else {
	// メモリ番号,修正値,動作モード
			xSkipSpace( ppcLine ) ;						// ブランク読み飛ばし
			dwMemNum = xGetNumeric( ppcLine ) ;			// メモリ番号
			xSkipSpace( ppcLine ) ;						// ブランク読み飛ばし
			
	//引数が文字だった場合
			if ( dwMemNum == CMDPRC_ERR_ARGUMENT ) {	// 第１引数 数値でない
				pcBuf = xStrCmp( "FLUSH", *ppcLine ) ;	// 引数チェック
				if ( pcBuf != (CHAR *)NULL ) {			// 引数OK
						fMode = CMD_DF_FLUSH ;			// DF書き込み
					(*ppcLine) += 5 ;
				} else {								// 引数NG
					pcBuf = xStrCmp( "ERASE", *ppcLine ) ;	// 引数チェック
					if ( pcBuf != (CHAR *)NULL ) {		// 引数OK
						fMode = CMD_DF_ERASE_ALL ;		// DF書き込み 全消去
						(*ppcLine) += 5 ;
					} else {							// 引数NG
						fMode = CMD_OUT_PARM_ERR ;		// 引数Err
						sSub_ShowParmErr( ) ;			// ParmErr表示
					}
				}
	//引数が数値だった場合	
			} else {									// 第１引数 数値である
				dwVal = xGetNumeric( ppcLine ) ;		// メンテ値
				if ( dwVal == CMDPRC_ERR_ARGUMENT ) {	// 第２引数 数値でない
					fMode = CMD_DF_SHOW ;				// 番号指定の表示
				} else {								// 第２引数 数値である
					fMode = CMD_DF_WRITE ;				// 番号指定の書き込み
				}
			}
		}
	}
	// 余分な引数がないかチェック
	if ( fMode != CMD_OUT_PARM_ERR ) {					// 後続の入力可能性有り
		xSkipSpace( ppcLine ) ;							// ブランク読み飛ばし
		if ( xIsLineEnd( ppcLine ) != 0 ) {				// 行末でない
			Sci_PutsText( UART_CH_DBG, "  Error! No need extra parm."
										" Or missing parm.\n") ;
			fMode = CMD_OUT_PARM_ERR ;					// 引数Err
		}
	}
	
	// 実行モード別に処理
	Sci_PutsText( UART_CH_DBG, "  Operation mode is ") ;
	Sci_PutsText( UART_CH_DBG, "Main-Controller ") ;
	switch( fMode ) {
		//パラメータ番号が指定されていない時
		case CMD_DF_NONE_PARM :
			Sci_PutsText( UART_CH_DBG, "SHOW_ALL\n") ;
				wMaxNum = 999 ;							// 最大メモリ番号
			for ( dwMemNum = 1 ; dwMemNum <= wMaxNum ; dwMemNum++ ) {
				gCMT0.uwScanTime = 0 ;					// ScanTime
				while ( gCMT0.uwScanTime < 1 ) {
					nop( ) ;
				}
				boRet = sCmdDataFlush_Show( (UWORD)dwMemNum/*, fRemote*/ ) ;
				if ( boRet == TRUE ) {
					gCMT0.uwScanTime = 0 ;				// ScanTime
					while ( gCMT0.uwScanTime < 16 ) {
						nop( ) ;
					}
				}
			}
			break ;
		// 番号指定の表示
		case CMD_DF_SHOW :
			Sci_PutsText( UART_CH_DBG, "SHOW_MEM_VALUE\n") ;
			//フラッシュメモリのパラメータ番号と数値を表示させる
			boRet = sCmdDataFlush_Show( (UWORD)dwMemNum/*, fRemote*/ ) ;
			if ( boRet == FALSE ) {
				//指定のパラメータ番号が見つからない場合
				xPrintf( UART_CH_DBG, " Parm No.%03d is not exist.",
							dwMemNum ) ; 
			}
			break ;
		// 番号指定の書き込み
		case CMD_DF_WRITE :
			Sci_PutsText( UART_CH_DBG, "PUT_MEM_VALUE\n") ;
			boRet = xDF_GetParmInf( (UWORD)dwMemNum, &Maint ) ;
			if ( boRet == TRUE ) {
				sCmdDataFlush_Put( (UWORD)dwMemNum, (WORD)dwVal, Maint) ;
			} else {
				xPrintf( UART_CH_DBG, " Parm No.%03d is not exist.",
							dwMemNum ) ; 
			}
			break ;
		case CMD_DF_ERASE_ALL :
			Sci_PutsText( UART_CH_DBG, "ERASE_ALL\n") ;
			sCmdDataFlush_EraseAll( ) ;					// 消去
			break ;
		case CMD_DF_FLUSH :
			Sci_PutsText( UART_CH_DBG, "WRITE_TO_DATA_FLUSH\n") ;
			sCmdDataFlush_Writeback( ) ;				// writeback
			break ;
		default :
			Sci_PutsText( UART_CH_DBG, "PARM_ERROR.\n") ;
			Sci_PutsText( UART_CH_DBG, "  Command format:"
							"df {mem_num {value} | flush | erase}\n") ;
			break ;
	}
}

/*	何か流用できる部分がある可能性があるので、コメントアウトにて保持

//---------------------------------------------------
//	センサステータス表示 実行部
//	書式:
//		ss {M|S}
//	引数
//		UBYTE ubCh				センサチャンネル
//								AXIS_MAIN / AXIS_SUB
//	戻り値
//---------------------------------------------------
static void	sCmdSensorStatus_Show( UBYTE ubCh ) 
{
	BOOL			boRet ;							// 戻り値
	
	
	// データ取得
	//	角度,角速度
//	boRet = xAccGet_Main_Ch( ubCh ) ;
	if ( boRet == FALSE ) {
		Sci_PutsText( UART_CH_DBG, "  Remote connect Error!\n") ;
		return ;
	}
	//	Raw-Data
//	boRet = xAccGenericCmd_Main( ubCh, ACC_CMD_REQ_RAW ) ;		// Raw-Data
	
	// データ表示
	if ( ubCh == AXIS_MAIN ) {
		Sci_PutsText( UART_CH_DBG, "  Main-") ;
	} else {
		Sci_PutsText( UART_CH_DBG, "  Sub -") ;
	}
	if ( gSyncInf.AccDat[ubCh].eInz == INZ_YES ) {
		Sci_PutsText( UART_CH_DBG, "Sensor Initialize Done.\n") ;
	} else {
		Sci_PutsText( UART_CH_DBG, "Sensor Not Initialized !\n") ;
	}
	xPrintf( UART_CH_DBG, "  Angle:%06d(0.001Deg), Speed:%05d(0.001Deg/Sec)\n",
				gSyncInf.AccDat[ubCh].dwAng, 
				gSyncInf.AccDat[ubCh].dwSpeed ) ;
//	xPrintf( UART_CH_DBG, "  Earthquake:%d-", gSyncInf.fEq ) ;
//	if ( gSyncInf.fEq == ACC_EARTHQUAKE_YES ) {
//		Sci_PutsText( UART_CH_DBG, "Detect    ") ;
//	} else {
//		Sci_PutsText( UART_CH_DBG, "Not Detect") ;
//	}
	xPrintf( UART_CH_DBG, ", Thermal:%d-", gSyncInf.fTemp ) ;
	if ( gSyncInf.fTemp == ACC_TEMP_HIGH ) {
		Sci_PutsText( UART_CH_DBG, "Too Hot.\n") ;
	} else if ( gSyncInf.fTemp == ACC_TEMP_LOW ) {
		Sci_PutsText( UART_CH_DBG, "Too Cold.\n") ;
	} else {
		Sci_PutsText( UART_CH_DBG, "Good\n") ;
	}
	
	// Wait
	gCMT0.T10M.uwTime = 0 ;										// 10mSec時間値
	while ( gCMT0.T10M.uwTime < 10 ) {
		nop( ) ;
	}
	
	Sci_PutsText( UART_CH_DBG, "  *Accerometer-Raw(digit)\n") ;
	xPrintf( UART_CH_DBG, "   X:%7d,\tY:%7d,\tZ:%7d\n",
				mAccRes[ubCh].Res.Raw.wDataX, 
				mAccRes[ubCh].Res.Raw.wDataY, 
				mAccRes[ubCh].Res.Raw.wDataZ ) ;
	Sci_PutsText( UART_CH_DBG, "  *Gyro-Raw(digit)\n") ;
	xPrintf( UART_CH_DBG, "   X:%6d,\tY:%6d,\tZ:%6d\n",
				mAccRes[ubCh].Res.Raw.wGyroX, 
				mAccRes[ubCh].Res.Raw.wGyroY, 
				mAccRes[ubCh].Res.Raw.wGyroZ ) ;
	Sci_PutsText( UART_CH_DBG, "  *Temperature(0.01deg)\n") ;
	xPrintf( UART_CH_DBG, "   T:%2d\n", mAccRes[ubCh].Res.Raw.wTemp ) ;
	
	// Wait
	gCMT0.T10M.uwTime = 0 ;										// 10mSec時間値
	while ( gCMT0.T10M.uwTime < 10 ) {
		nop( ) ;
	}
	
	
	return ;
	
}


//---------------------------------------------------
//	センサステータス表示
//	書式:
//		ss {M|S}
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//---------------------------------------------------
static void	sCmdSensorStatus( CHAR **ppcLine ) 
{
	FLAG			fMode ;							// 実行モード
	CHAR			*pcBuf ;						// 次の入力バッファPtr
	UBYTE			ubCh ;							// センサチャンネル
	
	
	// 引数チェック
	Sci_PutsText( UART_CH_DBG, 
					" Show Sensor Status.\n") ;
	if ( xIsLineEnd( ppcLine ) == 0 ) {					// 行末
		fMode = CMD_DFR_ALL ;							// パラメータ無し(All)
	} else {
		// チャンネル指定確認
		xSkipSpace( ppcLine ) ;							// ブランク読み飛ばし
		pcBuf = xStrCmp( "M", *ppcLine ) ;				// 引数チェック
		if ( pcBuf != (CHAR *)NULL ) {					// 引数OK
			fMode = CMD_DFR_MAIN ;						// メインセンサモード
			(*ppcLine) += 1 ;
		} else {
			pcBuf = xStrCmp( "S", *ppcLine ) ;			// 引数チェック
			if ( pcBuf != (CHAR *)NULL ) {				// 引数OK
				fMode = CMD_DFR_SUB ;					// サブセンサモード
				(*ppcLine) += 1 ;
			}
		}
		xSkipSpace( ppcLine ) ;							// ブランク読み飛ばし
	}
	// 余分な引数がないかチェック
	if ( fMode != CMD_OUT_PARM_ERR ) {					// 後続の入力可能性有り
		xSkipSpace( ppcLine ) ;							// ブランク読み飛ばし
		if ( xIsLineEnd( ppcLine ) != 0 ) {				// 行末でない
			Sci_PutsText( UART_CH_DBG, "  Error! No need extra parm."
										" Or missing parm.\n") ;
			fMode = CMD_OUT_PARM_ERR ;					// 引数Err
		}
	}
	
	// 処理モード表示
	Sci_PutsText( UART_CH_DBG, "  Operation mode is ") ;
	if ( fMode == CMD_DFR_MAIN ) {						// メインセンサモード
		Sci_PutsText( UART_CH_DBG, "M(Right)") ;
	} else if ( fMode == CMD_DFR_SUB ) {				// サブセンサモード
		Sci_PutsText( UART_CH_DBG, "S(Left)") ;
	} else {											// ローカルモード
		Sci_PutsText( UART_CH_DBG, "M(Right)&S(Left)") ;
	}
	Sci_PutsText( UART_CH_DBG, " - Sensor Show\n") ;
	// リモートチャンネルセット
	if ( fMode == CMD_DFR_MAIN ) {
		ubCh = AXIS_MAIN ;							// センサチャンネル
	} else if ( fMode == CMD_DFR_SUB ) {
		ubCh = AXIS_SUB ;							// センサチャンネル
	}
	
	// 実行モード別に処理
	switch( fMode ) {
		case CMD_DFR_ALL :
			// Main
			sCmdSensorStatus_Show( AXIS_MAIN ) ;
			Sci_PutsText( UART_CH_DBG, "\n") ;
			// Wait
//			gCMT0.T10M.uwTime = 0 ;								// 10mSec時間値
//			while ( gCMT0.T10M.uwTime < 10 ) {
//				nop( ) ;
//			}
			// Sub
			sCmdSensorStatus_Show( AXIS_SUB ) ;
			break ;
		case CMD_DFR_MAIN :
		case CMD_DFR_SUB :
			sCmdSensorStatus_Show( ubCh ) ;
			break ;
		default :
			Sci_PutsText( UART_CH_DBG, "PARM_ERROR.\n") ;
			Sci_PutsText( UART_CH_DBG, "  Command format:"
							"ss {M|S}\n") ;
			break ;
	}
	
	
	
}
*/

//---------------------------------------------------
//	出力ポートテスト  表示/設定ルーチン
//	引数
//		BYTE	bPort			ポート指定 1-6
//		FLAG	fMode			実行モード CMD_OUT_SHOW/CMD_OUT_EXE
//		BYTE	bOut			ポート出力値 EXTOUT_ON/EXTOUT_OFF
//								実行モードがCMD_OUT_EXE時のみ
//	戻り値
//---------------------------------------------------
static void	sCmdOutPort_Exe( BYTE bPort, FLAG fMode, BYTE bOut ) 
{
	// fModeがCMD_OUT_SHOWかCMD_OUT_NONE_PARMの時に表示させる
	// 現在はコメントアウトにしてあるが、表示させたいポートがあった場合に
	// 都度設定を行う（現在のポート数が6だが、そちらの設定もおこなう
	// 表示
	if ( fMode == CMD_OUT_SHOW ) {
		xPrintf( UART_CH_DBG, "  Port%d:", bPort ) ;
		switch( bPort ) {
			case 1 :						// ポンプマグネットSW
				//現在はIOP_MFSWはCN16_PODRに設定されている
/*				xPrintf( UART_CH_DBG, "%d", IOP_MGSW ) ;
				//CN16がONの場合か否かを見ている
				if ( IOP_MGSW == EXTOUT_ON ) {
					Sci_PutsText( UART_CH_DBG, "(ON) ") ;
				} else {
					Sci_PutsText( UART_CH_DBG, "(OFF)") ;
				}
				Sci_PutsText( UART_CH_DBG, " --- Pump Magnet-SW\n") ;
*/				break ;
			case 2 :						// Mシャットオフ
/*				xPrintf( UART_CH_DBG, "%d", IOP_SHUTOFF_M ) ;
				if ( IOP_SHUTOFF_M == EXTOUT_ON ) {
					Sci_PutsText( UART_CH_DBG, "(ON) ") ;
				} else {
					Sci_PutsText( UART_CH_DBG, "(OFF)") ;
				}
				Sci_PutsText( UART_CH_DBG, " --- M Shutoff Valve\n") ;
*/				break ;
			case 3 :						// Sシャットオフ
/*				xPrintf( UART_CH_DBG, "%d", IOP_SHUTOFF_S ) ;
				if ( IOP_SHUTOFF_S == EXTOUT_ON ) {
					Sci_PutsText( UART_CH_DBG, "(ON) ") ;
				} else {
					Sci_PutsText( UART_CH_DBG, "(OFF)") ;
				}
				Sci_PutsText( UART_CH_DBG, " --- S Shutoff Valve\n") ;
*/				break ;
			case 4 :						// 下降バルブ
/*				xPrintf( UART_CH_DBG, "%d", IOP_DOWN_VALVE ) ;
				if ( IOP_DOWN_VALVE == EXTOUT_ON ) {
					Sci_PutsText( UART_CH_DBG, "(ON) ") ;
				} else {
					Sci_PutsText( UART_CH_DBG, "(OFF)") ;
				}
				Sci_PutsText( UART_CH_DBG, " --- Down Valve\n") ;
*/				break ;
			case 5 :						// 爪外しエアーバルブ
/*				xPrintf( UART_CH_DBG, "%d", IOP_LOCKOUT_AIR ) ;
				if ( IOP_LOCKOUT_AIR == EXTOUT_ON ) {
					Sci_PutsText( UART_CH_DBG, "(ON) ") ;
				} else {
					Sci_PutsText( UART_CH_DBG, "(OFF)") ;
				}
				Sci_PutsText( UART_CH_DBG, " --- Lockout Air Valve\n") ;
*/				break ;
			case 6 :						// 予約1
/*				xPrintf( UART_CH_DBG, "%d", IOP_RESERVED1 ) ;
				if ( IOP_RESERVED1 == EXTOUT_ON ) {
					Sci_PutsText( UART_CH_DBG, "(ON) ") ;
				} else {
					Sci_PutsText( UART_CH_DBG, "(OFF)") ;
				}
				Sci_PutsText( UART_CH_DBG, " --- Reserved\n") ;
*/				break ;
			default :
				break ;
		}
		gCMT0.T10M.uwTime = 0 ;								// 10mSec時間値
		while ( gCMT0.T10M.uwTime < 5 ) {
			nop( ) ;
		}
	}
	
	//実行モードがCMD_OUT_EXEだった場合
	//bOut(EXTOUT_ON or EXTOUT_OFF)を各種出力変数へ代入する
	// 設定
	if ( fMode == CMD_OUT_EXE ) {
		if ( (bOut != EXTOUT_ON) && (bOut != EXTOUT_OFF) ) {
			return ;
		}
		switch( bPort ) {
			case 1 :						// ポンプマグネットSW
//				IOP_MGSW = bOut ;
				break ;
			case 2 :						// Mシャットオフ
//				IOP_SHUTOFF_M = bOut ;
				break ;
			case 3 :						// Sシャットオフ
//				IOP_SHUTOFF_S = bOut ;
				break ;
			case 4 :						// 下降バルブ
//				IOP_DOWN_VALVE = bOut ;
				break ;
			case 5 :						// 爪外しエアーバルブ
//				IOP_LOCKOUT_AIR = bOut ;
				break ;
			case 6 :						// 予約1
//				IOP_RESERVED1 = bOut ;
				break ;
			default :
				break ;
		}
	}
	
	
}


//---------------------------------------------------
//	出力ポートテスト
//	書式:
//		out {[1-5] {[ON|OFF]} }
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//---------------------------------------------------
static void	sCmdOutPort( CHAR **ppcLine ) 
{
	FLAG			fMode ;							// 実行モード
	DWORD			dwPortNum ;						// ポート番号
	BYTE			bOut ;							// ポート出力値
	BYTE			bLoop ;							// LoopCount
	CHAR			*pcBuf ;						// 次の入力バッファPtr
	
	
	// 引数チェック
	Sci_PutsText( UART_CH_DBG, " Test/Show Output-Port.\n") ;
	//何も数字がなく行末まで行ってしまった場合ポート指定なし
	if ( xIsLineEnd( ppcLine ) == 0 ) {					// 行末
		fMode = CMD_OUT_NONE_PARM ;						// ポート指定なし(表示)
	} else {
	//何か文字に引っかかった場合にはポート指定あり
		xSkipSpace( ppcLine ) ;							// ブランク読み飛ばし
		dwPortNum = xGetNumeric( ppcLine ) ;			// ポート番号
	//引数エラー、もしくはポート番号が1未満、もしくはポート番号が6以上
	//その際はエラー出力をする
		if ( (dwPortNum == CMDPRC_ERR_ARGUMENT) ||
			 ((dwPortNum < 1) || (dwPortNum > 6)) ) {
			Sci_PutsText( UART_CH_DBG, 
						"  Error! Invalid port spec. Must be 1-6.\n") ;
			fMode = CMD_OUT_PARM_ERR ;					// 引数Err
		} else {
	//引数のエラーがない場合は、ポート指定有りの表示モードとする
			fMode = CMD_OUT_SHOW ;						// ポート指定あり(表示)
			xSkipSpace( ppcLine ) ;						// ブランク読み飛ばし
			//"ON"と入力されていないと0を返す
			pcBuf = xStrCmp( "ON", *ppcLine ) ;
			//正しく"ON"と入力されている場合は
			if ( pcBuf != (CHAR *)NULL ) {
			//ポート出力をONにして実行モードをポート指定の設定モードにする	
				bOut = EXTOUT_ON ;						// ポート出力値
				fMode = CMD_OUT_EXE ;					// ポート指定あり(設定)
			//ONが入力されたため、2文字分加える
				(*ppcLine) += 2 ;
			} else {
				//"OFF"と入力されていないと0を返す
				pcBuf = xStrCmp( "OFF", *ppcLine ) ;
				if ( pcBuf != (CHAR *)NULL ) {
			//ポート出力をOFFにして実行モードをポート指定の設定モードにする	
					bOut = EXTOUT_OFF ;					// ポート出力値
					fMode = CMD_OUT_EXE ;				// ポート指定あり(設定)
				//"OFF"と入力されていないと0を返す
					(*ppcLine) += 3 ;
				}
			}
		}
	}
	// 余分な引数がないかチェック
	//ポート指定なしではなく、ポート出力エラーでもなかった場合
	if ( (fMode != CMD_OUT_PARM_ERR) && 
		 (fMode != CMD_OUT_NONE_PARM) ) {				// 後続の入力可能性有り
		xSkipSpace( ppcLine ) ;							// ブランク読み飛ばし
	//余分な引数が入力されていた場合にエラーを出力する
		if ( xIsLineEnd( ppcLine ) != 0 ) {				// 行末でない
			Sci_PutsText( UART_CH_DBG, "  Error! No need extra parm.\n") ;
			fMode = CMD_OUT_PARM_ERR ;					// 引数Err
		}
	}
	
	// エラー表示又はPWM値セット
	// 現在のOUT実行モードによって動作が異なる
	switch( fMode ) {
		//引数エラーだった場合、エラー表記を行う
		case CMD_OUT_PARM_ERR :					// 引数エラー
			Sci_PutsText( UART_CH_DBG, "  out {[1-6] {[ON|OFF]} }\n") ;
			break ;
		//ポートの指定がなかった場合には全表示を行う。
		case CMD_OUT_NONE_PARM :				// 全表示
			for ( bLoop = 1 ; bLoop <= 6 ; bLoop++ ) {
				sCmdOutPort_Exe( bLoop, CMD_OUT_SHOW, bOut ) ;
			}
			break ;
		//ポート指定の表示モード
		case CMD_OUT_SHOW :						// 表示
			sCmdOutPort_Exe( (BYTE)dwPortNum, fMode, bOut ) ;
			break ;
		//ポート指定の編集モード
		//編集モードにて編集後、表示モードに切り替えて編集後の値を表示
		case CMD_OUT_EXE :						// 値セット
			sCmdOutPort_Exe( (BYTE)dwPortNum, fMode, bOut ) ;
			Sci_PutsText( UART_CH_DBG, " Port out to ...\n") ;
			sCmdOutPort_Exe( (BYTE)dwPortNum, CMD_OUT_SHOW, bOut ) ;
			break ;
		default :
			break ;
	}
	
	
}


//---------------------------------------------------
//	PWM操作
//	書式:
//		pwm {pwm_val}
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//---------------------------------------------------
static void	sCmdPwm( CHAR **ppcLine ) 
{
	FLAG			fMode ;								// 実行モード
	DWORD			dwPwm ;								// PWM値
	
	
	// 引数チェック
	Sci_PutsText( UART_CH_DBG, " Linear Valve PWM control test/show.\n") ;
	if ( xIsLineEnd( ppcLine ) == 0 ) {					// 行末
		fMode = CMD_PWM_NONE_PARM ;						// 数値引数無し
	} else {
		xSkipSpace( ppcLine ) ;							// ブランク読み飛ばし
		if ( xIsLineEnd( ppcLine ) == 0 ) {				// 行末
			fMode = CMD_PWM_NONE_PARM ;					// 数値引数無し
		} else {
			Sci_PutsText( UART_CH_DBG, "  Error! Parm needed first.\n") ;
			fMode = CMD_PWM_PARM_ERR ;					// 引数Err
		}
		
		//実行モードがPWMエラーでない場合
		if ( fMode != CMD_PWM_PARM_ERR ) {				// 引数Errなし
			xSkipSpace( ppcLine ) ;						// ブランク読み飛ばし
		
			if ( xIsLineEnd( ppcLine ) != 0 ) {			// 行末でない
			//入力エラーの時はエラー出力をする
				dwPwm = xGetNumeric( ppcLine ) ;		// PWM値取得
				if ( dwPwm == CMDPRC_ERR_ARGUMENT ) {
					Sci_PutsText( UART_CH_DBG, 
							"  Error! Invalid PWM value.\n") ;
					fMode = CMD_PWM_PARM_ERR ;			// 引数Err
				// 0未満、もしくは最大値よりも大きい場合もエラーを出力する
		
				}else if ( (dwPwm < 0) ||
							(dwPwm > (DWORD)gPwm.uwMax) ) {
					Sci_PutsText( UART_CH_DBG, 
							"  Error! PWM value is too large.\n") ;
					fMode = CMD_PWM_PARM_ERR ;			// 引数Err
		
				} else {
					//PWM値が0以上で最大値以下の場合はPWM操作を行う
					fMode = CMD_PWM_EXE ;				// PWM操作
				}
			}
		}
	}
	// 余分な引数がないかチェック
	if ( (fMode != CMD_PWM_PARM_ERR) && 
		 (fMode != CMD_PWM_NONE_PARM) ) {				// 後続の入力可能性有り
		xSkipSpace( ppcLine ) ;							// ブランク読み飛ばし
		if ( xIsLineEnd( ppcLine ) != 0 ) {				// 行末でない
			Sci_PutsText( UART_CH_DBG, "  Error! No need extra parm.\n") ;
			fMode = CMD_PWM_PARM_ERR ;					// 引数Err
		}
	}
	
	// エラー表示又はPWM値セット
	switch( fMode ) {
		case CMD_PWM_PARM_ERR :							// 引数エラー
			Sci_PutsText( UART_CH_DBG, " pwm {pwm_val}}\n") ;
			xPrintf( UART_CH_DBG, "  pwm_val is 0-%d\n", gPwm.uwMax ) ; 
			break ;
		case CMD_PWM_EXE :								// PWM値セット
			xPrintf( UART_CH_DBG, " Exec pwm_val:%d\n", dwPwm ) ;
			*(gFb.V.puwTGR) = (UWORD)dwPwm ;
			break ;
		default :
			break ;
	}
	
	// 現在値を表示
	xPrintf( UART_CH_DBG, "  Now pwm_val :%d",*(gFb.V.puwTGR)) ; 
	
}


//====================================================
//	DIP-SW情報の表示 - Sub
//		DIP-SWのON/OFF表示
//	引数
//		BYTE	bDip			DIP-SWの状態
//								DIP_ON/DIP_OFF
//	戻り値
// DIP01:多点停止編集モード
// DIP02:多点停止モード
// DIP03:停止ポイント1
// DIP04:停止ポイント2
// DIP05:停止ポイント3
// DIP06:停止ポイント4
// DIP07:停止ポイント5
// DIP08:停止ポイント6

//====================================================
void	sCmdDipShow_Stat( BYTE bDip ) 
{
	
	if ( bDip == DIP_ON ) {
		Sci_PutsText( UART_CH_DBG, " ON ") ;
	} else {
		Sci_PutsText( UART_CH_DBG, " OFF") ;
	}
	Sci_PutsText( UART_CH_DBG, " --- ") ;
	
}


//---------------------------------------------------
//	DIP-SW情報の表示
//	書式:
//		dip
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
// DIP01:多点停止編集モード
// DIP02:多点停止モード
// DIP03:停止ポイント1
// DIP04:停止ポイント2
// DIP05:停止ポイント3
// DIP06:停止ポイント4
// DIP07:停止ポイント5
// DIP08:停止ポイント6
//---------------------------------------------------
static void	sCmdDipShow( CHAR **ppcLine ) 
{
	
	
	// 行末チェック
	Sci_PutsText( UART_CH_DBG, " DIP-SW status is ...\n") ;
	if ( xIsLineEnd( ppcLine ) != 0 ) {
		sSub_ShowParmErr( ) ;								// ParmErr表示
		return ;
	}
	
	// DIP01
	Sci_PutsText( UART_CH_DBG, "  DIP01 :" ) ; 
	sCmdDipShow_Stat( DIP01_PIDR ) ;						// DIP表示
	Sci_PutsText( UART_CH_DBG, "Multi-Point Stop Mode Edit Program\n") ;
	
	// DIP02
	Sci_PutsText( UART_CH_DBG, "  DIP02 :" ) ; 
	sCmdDipShow_Stat( DIP02_PIDR ) ;						// DIP表示
	Sci_PutsText( UART_CH_DBG, "Multi-point stop setting mode\n														ON :setting of UP side\n															OFF:setting of DOWN side\n") ;
									
	// DIP03
	Sci_PutsText( UART_CH_DBG, "  DIP03 :" ) ; 
	sCmdDipShow_Stat( DIP03_PIDR ) ;						// DIP表示
	Sci_PutsText( UART_CH_DBG, "Stop Point 01\n") ;
	
	// DIP04
	Sci_PutsText( UART_CH_DBG, "  DIP04 :" ) ; 
	sCmdDipShow_Stat( DIP04_PIDR ) ;						// DIP表示
	Sci_PutsText( UART_CH_DBG, "Stop Point 02\n") ;
	
	// DIP05
	Sci_PutsText( UART_CH_DBG, "  DIP05 :" ) ; 
	sCmdDipShow_Stat( DIP05_PIDR ) ;						// DIP表示
	Sci_PutsText( UART_CH_DBG, "Stop Point 03\n") ;
	
	// DIP06
	Sci_PutsText( UART_CH_DBG, "  DIP06 :" ) ; 
	sCmdDipShow_Stat( DIP06_PIDR ) ;						// DIP表示
	Sci_PutsText( UART_CH_DBG, "Stop Point 04\n") ;
	
	// DIP07
	Sci_PutsText( UART_CH_DBG, "  DIP07 :" ) ; 
	sCmdDipShow_Stat( DIP07_PIDR ) ;						// DIP表示
	Sci_PutsText( UART_CH_DBG, "Stop Point 05\n") ;

	// DIP08
	Sci_PutsText( UART_CH_DBG, "  DIP08 :" ) ; 
	sCmdDipShow_Stat( DIP08_PIDR ) ;						// DIP表示
	Sci_PutsText( UART_CH_DBG, "Stop Point 06\n") ;
}


//---------------------------------------------------
//	ソフトウェアリセット
//	書式:
//		reset
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//---------------------------------------------------
static void	sCmdReset( CHAR **ppcLine ) 
{
	WORD			wRet ;
//	CHAR			*pcBuf ;							// 次の入力バッファPtr
	BOOL			boRet ;								// 戻り値
	
	
	// 引数チェック
	Sci_PutsText( UART_CH_DBG, " MCU soft reset execute.") ;
	// 余分な引数がないかチェック
	xSkipSpace( ppcLine ) ;								// ブランク読み飛ばし
	if ( xIsLineEnd( ppcLine ) != 0 ) {					// 行末でない
		sSub_ShowParmErr( ) ;							// ParmErr表示
		return ;
	}
	
	// 実行の確認
	Sci_PutsText( UART_CH_DBG, "  Are you sure ? (YES/NO):") ;
	wRet = xConOptInput( "YES" ) ;						// 入力チェック
	if ( wRet != 0 ) {									// Not YES
		Sci_PutsText( UART_CH_DBG, "  Aborted.") ;
		return ;
	}

	// リセット実行
	Sci_PutsText( UART_CH_DBG,"  Soft reset exec. \n") ;
	while ( Sci_IsTxData( UART_CH_DBG ) ) {
		nop( ) ;
	}
	gCMT0.T10M.uwTime = 0 ;								// 10mSec時間値
	while ( gCMT0.T10M.uwTime < 30 ) {
		nop( ) ;
	}
	xSoftReset( ) ;
	boRet = TRUE ;
	if ( boRet == TRUE ) {
		Sci_PutsText( UART_CH_DBG, "  Done.") ;
	} else {
		Sci_PutsText( UART_CH_DBG, "  Failed.") ;
	}
}


//---------------------------------------------------
//	HELP コマンド一覧表示
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//---------------------------------------------------
static void	sCmdHelp( CHAR **cppLine ) 
{
	
	Sci_PutsText( UART_CH_DBG, "* Command Help *\n\n" ) ;
	
	Sci_PutsText( UART_CH_DBG, 
					"DataFlash-Memory(for parameter) show & maint.\n") ;
	Sci_PutsText( UART_CH_DBG, 
					"  df {mem_num {value} | flush | erase}\n") ;
		
	
	gCMT0.T10M.uwTime = 0 ;									// 10mSec時間値
	while ( gCMT0.T10M.uwTime < 10 ) {
		nop( ) ;
	}
	
	Sci_PutsText( UART_CH_DBG, "Test/Show Output-Port.\n") ;
	Sci_PutsText( UART_CH_DBG, "  out {[1-6] {[ON|OFF]} }\n") ;
	
	Sci_PutsText( UART_CH_DBG, "Linear Valve PWM control test/show.\n") ;
	Sci_PutsText( UART_CH_DBG, "  pwm {pwm_val}}\n") ;	
	
	gCMT0.T10M.uwTime = 0 ;									// 10mSec時間値
	while ( gCMT0.T10M.uwTime < 10 ) {
		nop( ) ;
	}
	
	Sci_PutsText( UART_CH_DBG, "DIP-SW show status.\n") ;
	Sci_PutsText( UART_CH_DBG, "  dip\n") ;
	Sci_PutsText( UART_CH_DBG, "MCU Software reset.\n") ;
	Sci_PutsText( UART_CH_DBG, "  reset \n") ;	
	
	Sci_PutsText( UART_CH_DBG, 
					"\n XX_val/XX_num/value is in decimal or hex(0xnnnn)\n") ;
	
}


//---------------------------------------------------
//	コマンドエラーの表示
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//---------------------------------------------------
static void	sCmdError( CHAR **ppcLine ) 
{
	
	Sci_PutsText( UART_CH_DBG, "*** No such Command ***") ;
	
}


/*
//奥村さん追加部分//
//---------------------------------------------------
//	インターバルタイマ間隔を変更
//	引数
//		CHAR	**ppcCmdLine	現在のコマンド処理ポインタ
//	戻り値
//---------------------------------------------------
static void	sCmdTimer( CHAR **ppcLine ) 
{
		FLAG			fMode ;							// 実行モード
	
	Sci_PutsText( UART_CH_DBG, "* Interval Timer Settings *\n\n" ) ;
	
		// 引数チェック
	if ( xIsLineEnd( ppcLine ) == 0 ) {					// 行末
		fMode = CMD_PWM_NONE_PARM ;						// 数値引数無し
	} else {
		xSkipSpace( ppcLine ) ;							// ブランク読み飛ばし
		if ( **ppcLine == 'R' ) {
			fMode = CMD_PWM_NONE_PARM ;					// 数値引数無し
		} else if ( xIsLineEnd( ppcLine ) == 0 ) {		// 行末
			fMode = CMD_PWM_NONE_PARM ;					// 数値引数無し
		} else {
			Sci_PutsText( UART_CH_DBG, "  Error! [R] parm needed first.\n") ;
			fMode = CMD_PWM_PARM_ERR ;					// 引数Err
		}
		if ( fMode != CMD_PWM_PARM_ERR ) {				// 引数Errなし
			(*ppcLine)++ ;
			xSkipSpace( ppcLine ) ;						// ブランク読み飛ばし
			if ( xIsLineEnd( ppcLine ) != 0 ) {			// 行末でない
				gItv0 = xGetNumeric( ppcLine ) ;		// インターバル値取得
				if ( (gItv0 < 0) ||
							(gItv0 > 100 ) ) {
					Sci_PutsText( UART_CH_DBG, 
							"  Error! Time value is too large.\n") ;
					fMode = CMD_PWM_PARM_ERR ;			// 引数Err
				} else {
					fMode = CMD_PWM_EXE ;				// PWM操作
				}
			}
		}
	}
	// 余分な引数がないかチェック
	if ( (fMode != CMD_PWM_PARM_ERR) && 
		 (fMode != CMD_PWM_NONE_PARM) ) {				// 後続の入力可能性有り
		xSkipSpace( ppcLine ) ;							// ブランク読み飛ばし
		if ( xIsLineEnd( ppcLine ) != 0 ) {				// 行末でない
			Sci_PutsText( UART_CH_DBG, "  Error! No need extra parm.\n") ;
			fMode = CMD_PWM_PARM_ERR ;					// 引数Err
		}
	}
	
	// エラー表示又はPWM値セット
	switch( fMode ) {
		case CMD_PWM_PARM_ERR :					// 引数エラー
			Sci_PutsText( UART_CH_DBG, "  TT {[R] {time_val}}\n") ;
			xPrintf( UART_CH_DBG, "  Time_val is %d\n", gItv0 ) ; 
			break ;
		case CMD_PWM_EXE :						// PWM値セット
			xPrintf( UART_CH_DBG, " Exec Time_val:%d\n", gItv0 ) ;
//			*(mFb.V[ubAxis].puwTGR) = (UWORD)dwPwm ;
			break ;
		default :
			break ;
	}
	
	
	// 現在値を表示
	xPrintf( UART_CH_DBG, "  Now Time_val :%d\n", gItv0 ) ; 
			
	}

*/
//===========================================================================
